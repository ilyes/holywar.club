log = (function() {
  var self = this

  var LogLevels = {
    debug: 'Debug',
    info: 'Info',
    warning: 'Warning',
    error: 'Error'
  }

  var previousLog = []

  self.debug = function(message) {
    addPreviousLog(LogLevels.debug + ': ' + message)
  }

  self.info = function(message, error) {
    log(LogLevels.info, message, error)
  }

  self.warning = function(message, error) {
    log(LogLevels.warning, message, error)
  }

  self.error = function(message, error) {
    log(LogLevels.error, message, error)
  }

  self.getElementFields = function(htmlElement) {
    var result = '<' + htmlElement.tagName
    if (htmlElement.id) {
      result += ' id="' + htmlElement.id + '"'
    }
    if (htmlElement.className) {
      result += ' class="' + htmlElement.className + '"'
    }
    result += ' />'
    return result
  }

  function log(logLevel, message, error) {
    var errorData = createErrorData(logLevel, message, error)
    if (!errorData) {
      return
    }
    send(errorData)
    addPreviousLog(logLevel + ': ' + errorData.message)
  }

  function send(data) {
    fetch(__ENV.API_URL + '/log', {
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
      method: 'POST'
    }).catch(function(err) {
      console.log(JSON.stringify(err))
    })
  }

  function createErrorData(logLevel, message, error) {
    var tagName = !error.target ? '' : error.target.tagName
    if (tagName && tagName.toLowerCase() !== 'script') {
      return
    }

    var fileName = logUtil.getFileName(error)
    error = error || {}
    return {
      level: logLevel,
      message: message || error.message,
      tagName: tagName,
      fileName: fileName,
      lineNumber: error.lineno || error.lineNumber,
      columnNumber: error.colno || error.columnNumber,
      stack: logUtil.getStack(error),
      browser: logUtil.getBrowserVersion(),
      url: location.href,
      runTime: window.runTime.get(),
      previousLog: previousLog.slice(Math.max(previousLog.length - 25, 0))
    }
  }

  function addPreviousLog(text) {
    previousLog.push('[' + window.runTime.get() + '] ' + text)
  }

  setTimeout(function() {
    // setTimeout because there is some click events fired by code on initialization not by user
    document.addEventListener('click', function(event) {
      if (!event.target) {
        return
      }
      var text = self.getElementFields(event.target)
      addPreviousLog('Click: ' + text)
    })

    document.addEventListener('keydown', function(event) {
      if (!event.target) {
        return
      }
      var text = 'Input: ' + self.getElementFields(event.target)
      if (
        previousLog[previousLog.length - 1] &&
        previousLog[previousLog.length - 1].indexOf(text) >= 0
      ) {
        return
      }
      addPreviousLog(text)
    })
  })

  return self
})()

errors = (function() {
  var self = {}

  if (typeof window !== 'undefined') {
    window.addEventListener('error', onError, true)
    function onError(e) {
      if (!e) {
        return
      }
      var error = e.error || e
      if (logUtil.isIgnoreMessage(error.message)) {
        return
      }
      if (logUtil.isOtherDomain(error)) {
        return
      }
      log.error(error.message, error)
    }
  }

  return self
})()

runTime = (function() {
  var self = {}

  self.startTime = new Date()

  self.get = function() {
    return new Date(new Date() - self.startTime).toJSON().split('T')[1]
  }

  return self
})()

var logUtil = {
  ignoreMessages: ['Script error.'],
  isIgnoreMessage: function(message) {
    return !message || logUtil.ignoreMessages.indexOf(message) !== -1
  },
  isOtherDomain: function(error) {
    var fileName = logUtil.getFileName(error)
    if (!fileName || !URL) {
      return false
    }
    return new URL(fileName).hostname !== location.hostname
  },
  getFileName: function(error) {
    return (
      error.filename ||
      error.fileName ||
      (!error.target
        ? ''
        : error.target.href || error.target.src || error.target.data)
    )
  },
  getStack: function(error) {
    if (error.stack && error.stack.length > 0) {
      return error.stack
    }
    if (error.error && error.error.stack && error.error.stack.length > 0) {
      return error.error.stack
    }

    return (
      'Unable to find stack trace of: {' + Object.keys(error).join(',') + '}'
    )
  },
  getBrowserVersion: function() {
    try {
      var ua = navigator.userAgent,
        tem,
        M =
          ua.match(
            /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i
          ) || []
      if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || []
        return 'IE ' + (tem[1] || '')
      }
      if (M[1] === 'Chrome') {
        tem = ua.match(/\b(OPR|Edge)\/(\d+)/)
        if (tem != null)
          return tem
            .slice(1)
            .join(' ')
            .replace('OPR', 'Opera')
      }
      M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?']
      if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1])
      return M.join(' ')
    } catch (e) {
      return JSON.stringify(e)
    }
  }
}
