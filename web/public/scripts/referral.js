var urlParams = new URLSearchParams(window.location.search)
var referral = urlParams.get('referral')
if (referral && window.localStorage) {
  window.localStorage.setItem('referral', referral)
}
