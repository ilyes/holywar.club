import React from 'react'

export const H1 = ({ children, className }: any) => (
  <h1 className={className}>{children}</h1>
)

export const H2 = ({ children }: any) => (
  <h2 style={{ textAlign: 'center' }}>{children}</h2>
)
export const H3 = ({ children }: any) => (
  <h3 style={{ textAlign: 'center' }}>{children}</h3>
)
