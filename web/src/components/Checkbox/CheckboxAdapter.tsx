import React from 'react'
import { Checkbox } from './Checkbox'

export const CheckboxAdapter = ({ input, meta, ...rest }: any) => (
  <Checkbox
    {...input}
    {...rest}
    onChange={({ target: { value } }: any) => input.onChange(value)}
  />
)
