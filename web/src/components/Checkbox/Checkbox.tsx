import React from 'react'
import Form from 'react-bootstrap/Form'

interface CheckboxProps {
  id: string
  name: string
  className: string
  label: string
  onChange: (e: any) => void
}

export const Checkbox = ({
  id,
  name,
  className,
  label,
  onChange
}: CheckboxProps) => {
  return (
    <Form.Check
      type="checkbox"
      custom
      id={id}
      name={name}
      className={className}
      label={label}
      onChange={onChange}
    />
  )
}
