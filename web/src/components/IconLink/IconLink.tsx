import React from 'react'
import classnames from 'classnames'
import { Link } from 'react-router-dom'

import { Icon, IconName } from '../Icon/Icon'

import styles from './IconLink.module.css'

interface IconLinkProps {
  children: any
  to: string
  iconName: IconName
  className?: string
  childrenWrapName?: string
}

export const IconLink = ({
  children,
  to,
  iconName,
  className,
  childrenWrapName
}: IconLinkProps) => {
  return (
    <Link to={to} className={classnames(styles.wrap, className)}>
      <Icon name={iconName} />
      <span className={classnames(styles.children, childrenWrapName)}>
        {children}
      </span>
    </Link>
  )
}
