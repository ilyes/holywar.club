import React from 'react'
import classnames from 'classnames'

import styles from './Delimiter.module.css'

type Props = {
  color?: 'default' | 'light' | 'primary' | 'secondary'
  gap?: '0' | 's' | 'm' | 'l'
}

export const Delimiter: React.FC<Props> = ({
  gap = 'm',
  color = 'default'
}) => {
  return (
    <hr
      className={classnames(styles[`gap_${gap}`], styles[`color_${color}`])}
    />
  )
}
