import React from 'react'
import classnames from 'classnames'

import styles from './Expandable.module.css'

export interface ExpandableProps {
  title: any
  children: any
  className?: string
  childrenClassName?: string
  round?: boolean
  expanded?: boolean
  active?: boolean
  disabled?: boolean
  up?: boolean
  onTitleClick?: (event: any) => void
  onMouseEnter?: () => void
  onMouseLeave?: () => void
}

export const Expandable = ({
  title,
  children,
  className,
  childrenClassName,
  active,
  disabled,
  expanded,
  onTitleClick,
  onMouseEnter,
  onMouseLeave
}: ExpandableProps) => {
  const expandableClass = classnames(styles.expandable, className)
  const titleClass = classnames(styles.expandableTitle, {
    [styles.active]: active,
    [styles.disabled]: disabled
  })
  const childrenClass = classnames(styles.expandableChildren, childrenClassName)
  return (
    <div
      className={expandableClass}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
    >
      <div onClick={onTitleClick} className={titleClass}>
        {renderTitle(title)}
      </div>
      {expanded ? <div className={childrenClass}>{children}</div> : null}
    </div>
  )
}

function renderTitle(title: any) {
  if (typeof title === 'string') {
    return <div className={styles.expandableTitleText}>{title}</div>
  }
  return title
}

// function isOnScreen(elm: any) {
//   if (!elm) return false
//   const rect = elm.getBoundingClientRect()
//   const viewHeight = Math.max(
//     document.documentElement.clientHeight,
//     window.innerHeight
//   )
//   const result = !(rect.bottom < 0 || rect.top + rect.height - viewHeight >= 0)
//   return result
// }
