import React, { useState, useEffect } from 'react'
import Modal from 'react-bootstrap/Modal'

import { Button } from 'components/Button/Button'
import { Icon, IconName } from 'components/Icon/Icon'
import { ModalContext } from './ModalContext'

import styles from './Modal.module.css'
import { withRouter } from 'react-router'

interface ModalProviderProps {
  children: any
  history: History
}

const ModalProviderInternal = ({ children, history }: any) => {
  const [modal, setModal] = useState(undefined)

  useEffect(() => history.listen(hideModal), [history])

  function showModal(modalContent: any) {
    setModal(modalContent)
  }

  function hideModal() {
    setModal(undefined)
  }

  const value = {
    showModal,
    hideModal
  }

  return (
    <ModalContext.Provider value={value}>
      {children}
      <Modal show={!!modal} onHide={hideModal}>
        <Button className={styles.close} onClick={hideModal} variant="link">
          <Icon name={IconName.close} />
        </Button>
        <Modal.Body className={styles.body}>{modal}</Modal.Body>
      </Modal>
    </ModalContext.Provider>
  )
}

export const ModalProvider = withRouter(ModalProviderInternal)
