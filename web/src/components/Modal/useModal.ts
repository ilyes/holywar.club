import { useContext } from 'react'

import { ModalContext } from './ModalContext'
interface UseModalResult {
  showModal: (modal: any, title?: string) => void
  hideModal: () => void
}

export const useModal = (): UseModalResult => {
  const context = useContext(ModalContext)
  return context
}
