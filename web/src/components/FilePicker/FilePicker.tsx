// @flow

import React from 'react'
import classnames from 'classnames'

import styles from './FilePicker.module.css'

interface FilePickerProps {
  children: any
  className?: string
  onChange: (e: any) => void
  accept?: string
  multiple?: boolean
}

export const FilePicker = ({
  children,
  className,
  onChange,
  accept,
  multiple
}: FilePickerProps) => {
  const randomId = 'file-upload-' + Math.random() * 2000000000
  return (
    <label
      htmlFor={randomId}
      className={classnames(styles.fileUpload, className)}
    >
      {children}
      <input
        id={randomId}
        type="file"
        multiple={multiple}
        accept={accept}
        onChange={onChange}
      />
    </label>
  )
}
