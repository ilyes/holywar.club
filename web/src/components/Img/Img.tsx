import React, { useEffect, useState } from 'react'
import { useCallback } from 'react'
import classnames from 'classnames'

import styles from './Img.module.css'
import { Icon, IconName } from 'components/Icon/Icon'
import { Spinner } from 'components/Spinner/Spinner'

type Props = { className?: string; src: string; nullable?: boolean }

export const Img: React.FC<Props> = ({ className, src, nullable }) => {
  const [loadingSrc, setLoading] = useState<string>()
  const [isSuccess, setSuccess] = useState(false)
  const [isError, setError] = useState(false)

  const handleSuccess = useCallback(() => {
    setSuccess(true)
    setError(false)
  }, [setLoading, setError, setSuccess])
  const handleError = useCallback(() => {
    setSuccess(false)
    setError(true)
  }, [setLoading, setError, setSuccess])

  useEffect(() => {
    if (!src) {
      if (loadingSrc) {
        setLoading(undefined)
      }
      return
    }

    if (loadingSrc === src) {
      return
    }

    setLoading(src)
    setSuccess(false)
    setError(false)

    const image = new Image()

    image.src = src

    image.onload = handleSuccess
    image.onerror = handleError
  }, [src, loadingSrc, setLoading, handleSuccess, handleError])

  if (isError && nullable) {
    return null
  }

  if (isSuccess) {
    return <img className={classnames(styles.img, className)} src={src} />
  }

  return (
    <i
      className={classnames(styles.root, className)}
      style={{
        backgroundImage: isSuccess ? `url(${src})` : undefined
      }}
    >
      {loadingSrc && !isSuccess && !isError && <Spinner />}
      {isError && <Icon name={IconName.image} />}
    </i>
  )
}
