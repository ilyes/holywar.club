import React from 'react'

import { Button } from '../Button/Button'

import styles from './InviteButton.module.css'
import { Icon, IconName } from 'components/Icon/Icon'

export const InviteButton = ({ text, size, onClick }: any) => {
  return (
    <Button
      size={size || 'sm'}
      className={styles.button}
      variant="secondary"
      onClick={onClick}
    >
      <Icon name={IconName.invite} />
      <span className={styles.label}>Пригласить</span>
    </Button>
  )
}
