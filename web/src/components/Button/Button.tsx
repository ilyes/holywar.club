import React from 'react'
import classnames from 'classnames'
import BootstrapButton, {
  ButtonProps as BootstrapButtonProps
} from 'react-bootstrap/Button'
import { Spinner } from '../Spinner/Spinner'

import styles from './Button.module.css'

interface ButtonProps extends BootstrapButtonProps {
  onClick?: Function
  loading?: boolean
  children: any
  className?: string
  wide?: boolean
}

export const Button = ({
  onClick,
  loading,
  disabled,
  children,
  className,
  wide,
  ...rest
}: ButtonProps) => {
  return (
    <BootstrapButton
      onClick={(e: any) => onClick && onClick(e)}
      {...rest}
      disabled={loading || disabled}
      className={classnames(className, { [styles.wide]: wide })}
    >
      <>
        {loading ? (
          <div className={styles.loading}>
            <Spinner small />
          </div>
        ) : null}
        <span className={loading ? styles.loadingChildren : undefined}>
          {children}
        </span>
      </>
    </BootstrapButton>
  )
}
