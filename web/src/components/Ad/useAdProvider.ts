import { useConfig } from 'helpers'

import { AdProviders } from 'models'

import * as Yandex from './providers/yandex'
import * as Google from './providers/google'

export const useAdProvider = () => {
  const config = useConfig()

  if (config.adProvider === AdProviders.google) {
    return Google
  }

  return Yandex
}
