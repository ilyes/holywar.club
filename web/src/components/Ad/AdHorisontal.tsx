import React from 'react'

import styles from './Ad.module.css'
import { useEnv } from 'helpers'
import { useAdProvider } from './useAdProvider'

export const AdHorisontal = () => {
  const env = useEnv()
  const ad = useAdProvider()

  if (!env.IS_PRODUCTION) {
    return (
      <div className={styles.horizontal}>
        <div className={styles.fakeAdHorisontal}></div>
      </div>
    )
  }

  return (
    <div className={styles.horizontal}>
      <ad.Horisontal />
    </div>
  )
}
