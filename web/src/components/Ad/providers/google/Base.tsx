import React, { memo, useEffect, useState } from 'react'
import { ready } from 'helpers'

export const Base = memo(
  ({ slot, style }: { slot: string; style?: string }) => {
    const [isRendered, setRendered] = useState(false)

    useEffect(() => {
      if (isRendered) {
        return
      }
      setRendered(true)
      const w = window as any
      if (!w) {
        return
      }
      ready(() => (w.adsbygoogle || []).push({}))
    })

    return (
      <div
        dangerouslySetInnerHTML={{
          __html: `
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <ins class="adsbygoogle"
             style="${style || 'display:block'}"
             data-ad-client="ca-pub-7274312014687219"
             data-ad-slot="${slot}"
             data-ad-format="auto"
             data-full-width-responsive="true"></ins>
        `
        }}
      ></div>
    )
  }
)
