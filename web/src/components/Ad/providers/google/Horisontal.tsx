import React from 'react'

import { Base } from './Base'
import { useEnv } from 'helpers'

export const Horisontal = () => {
  const env = useEnv()
  if (env.IS_PHONE) {
    return (
      <Base
        slot="8748914833"
        style="display:inline-block;width:320px;height:90px"
      />
    )
  }
  if (env.IS_PHONE) {
    return (
      <Base
        slot="6805277779"
        style="display:inline-block;width:680px;height:100px"
      />
    )
  }
  return (
    <Base
      slot="2279515881"
      style="display:inline-block;width:1200px;height:120px"
    />
  )
}
