import React from 'react'

import { Base } from './Base'

import { AdSquareProps } from '../../AdSquare'

export const Square = ({ second, third, fourth }: AdSquareProps) => {
  if (second) {
    return <Base slot="1233887901" />
  }
  if (third) {
    return <Base slot="6007131614" />
  }
  if (fourth) {
    return <Base slot="9336034355" />
  }
  return <Base slot="2417857509" />
}
