import React, { useEffect, useState } from 'react'

export const Base = ({ blockId }: any) => {
  // if it rendered on server size
  // we nee to rerender it one more time
  const [isRendered, setRendered] = useState(false)

  const renderTo = blockId
  useEffect(() => {
    if (isRendered) {
      return
    }
    setRendered(true)
    renderAd(window, 'yandexContextAsyncCallbacks')
  })

  function renderAd(w: Window, n: string) {
    if (!w) {
      return
    }
    w[n] = w[n] || []
    w[n].push(function() {
      ;(w as any).Ya.Context.AdvManager.render({
        blockId,
        renderTo,
        async: true
      })
    })

    const src = '//an.yandex.ru/system/context.js'
    const d: any = w.document
    const scripts: Element[] = d.getElementsByTagName('script')
    let script = find(
      scripts,
      (s: any) => s.src && s.src.indexOf(src) !== -1
    ) as HTMLScriptElement

    const t = scripts[0]
    if (!t.parentNode) {
      return
    }
    script = d.createElement('script')
    if (!script) {
      return
    }
    script.type = 'text/javascript'
    script.src = src
    script.async = true
    t.parentNode.insertBefore(script, t)
  }

  return <div id={renderTo}></div>
}

function find(arr: any, predicate: Function): any {
  for (let i = 0; i < arr.length; i++) {
    if (predicate(arr[i])) {
      return arr[i]
    }
  }
  return null
}
