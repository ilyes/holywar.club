import React from 'react'

import { Base } from './Base'

import { AdSquareProps } from '../../AdSquare'

export const Square = ({ second, third }: AdSquareProps) => {
  if (second) {
    return <Base blockId="R-A-449161-8" />
  }
  if (third) {
    return <Base blockId="R-A-449161-9" />
  }
  return <Base blockId="R-A-449161-1" />
}
