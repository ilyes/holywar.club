import React from 'react'

import styles from './Ad.module.css'
import { useEnv } from 'helpers'
import { useAdProvider } from './useAdProvider'

export interface AdSquareProps {
  second?: boolean
  third?: boolean
  fourth?: boolean
}

export const AdSquare = ({ second, third }: AdSquareProps) => {
  const env = useEnv()
  const ad = useAdProvider()

  if (!env.IS_PRODUCTION) {
    return (
      <div className={styles.square}>
        <div className={styles.fakeAd}></div>
      </div>
    )
  }

  return (
    <div className={styles.square}>
      <ad.Square second={second} third={third} />
    </div>
  )
}
