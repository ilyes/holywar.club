import React from 'react'

import styles from './Ad.module.css'
import { useEnv } from 'helpers'
import { useAdProvider } from './useAdProvider'

export const AdVertical = () => {
  const env = useEnv()
  const ad = useAdProvider()

  if (!env.IS_PRODUCTION) {
    return (
      <div className={styles.vertical}>
        <div className={styles.fakeAdVertical}></div>
      </div>
    )
  }

  return (
    <div className={styles.vertical}>
      <ad.Vertical />
    </div>
  )
}
