import React from 'react'

import styles from './Ad.module.css'
import { useEnv } from 'helpers'
import { useAdProvider } from './useAdProvider'

export const AdSmall = () => {
  const env = useEnv()
  const ad = useAdProvider()

  if (!env.IS_PRODUCTION) {
    return (
      <div className={styles.small}>
        <div className={styles.fakeAd}></div>
      </div>
    )
  }

  return (
    <div className={styles.small}>
      <ad.Small />
    </div>
  )
}
