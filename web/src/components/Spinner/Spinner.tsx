import React from 'react'
import classnames from 'classnames'
import BootstrapSpinner from 'react-bootstrap/Spinner'

import styles from './Spinner.module.css'

interface SpinnerProps {
  small?: boolean
  micro?: boolean
  absolute?: boolean
  className?: string
}

export const Spinner = ({
  micro,
  small,
  absolute,
  className
}: SpinnerProps) => {
  return (
    <div
      className={classnames(styles.spinner, className, {
        [styles.spinnerAbsolute]: absolute,
        [styles.micro]: micro
      })}
    >
      <BootstrapSpinner animation="border" size={small ? 'sm' : undefined} />
    </div>
  )
}
