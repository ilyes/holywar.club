import React from 'react'
import classnames from 'classnames'

import { Icon, IconName } from 'components/Icon/Icon'

import styles from './Like.module.css'
import { Spinner } from '../Spinner/Spinner'
import LikeIcon from './components/LikeIcon'

interface LikeProps {
  likesCount: number
  dislikesCount: number
  loading?: boolean
  onLikeClick?: (isLike: boolean) => void
  className?: string
}

export const Like = ({
  likesCount,
  dislikesCount,
  loading,
  onLikeClick,
  className
}: LikeProps) => {
  return (
    <div className={classnames(className, styles.likes)}>
      <Icon
        className={styles.likesIcon}
        name={IconName.like}
        onClick={() => onLikeClick && onLikeClick(true)}
      />
      {!loading ? (
        <span
          className={
            likesCount > dislikesCount
              ? styles.likesCount
              : styles.dislikesCount
          }
        >
          {(likesCount || 0) - (dislikesCount || 0)}
        </span>
      ) : (
        <Spinner micro className={styles.spinner} />
      )}
      <Icon
        className={styles.dislikesIcon}
        name={IconName.like}
        onClick={() => onLikeClick && onLikeClick(false)}
      />
    </div>
  )
}
