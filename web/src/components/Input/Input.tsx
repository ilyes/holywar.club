import React from 'react'
import FormControl, { FormControlProps } from 'react-bootstrap/FormControl'
import Spinner from 'react-bootstrap/Spinner'

import styles from './Input.module.css'

interface InputProps extends FormControlProps {
  value: any
  loading?: boolean
  placeholder?: string
  className?: string
}

export const Input = ({
  value,
  loading,
  disabled,
  placeholder,
  className,
  ...rest
}: InputProps) => {
  return (
    <>
      <FormControl
        disabled={loading || disabled}
        placeholder={placeholder}
        className={className}
        value={value}
        {...rest}
      />
      {loading ? (
        <span className={styles.loadingWrap}>
          <Spinner animation="border" size="sm" />
        </span>
      ) : null}
    </>
  )
}
