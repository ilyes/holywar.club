import React from 'react'
import ReactTooltip from 'react-tooltip'

import { TooltipPlacement } from './Tooltip'
import { Icon, IconName } from '../Icon/Icon'

interface TooltipIconProps {
  id: string
  children: React.ReactNode
  iconName: IconName
  placement: TooltipPlacement
  className: string
}

export const TooltipIcon = ({
  id,
  children,
  iconName,
  placement,
  className
}: TooltipIconProps) => (
  <>
    <Icon
      data-tip
      data-event="click"
      data-for={id}
      name={iconName}
      className={className}
    />
    <ReactTooltip id={id} aria-haspopup="true" globalEventOff="click">
      {children}
    </ReactTooltip>
  </>
)
