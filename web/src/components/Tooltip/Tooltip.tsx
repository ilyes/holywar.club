import React from 'react'
import Overlay from 'react-bootstrap/Overlay'
import BootstrapTooltip from 'react-bootstrap/Tooltip'

export type TooltipPlacement = 'right' | 'left'

interface TooltipProps {
  children: React.ReactNode
  placement: TooltipPlacement
  isShown: boolean
  target: any
}

export const Tooltip = ({
  placement,
  isShown,
  target,
  children
}: TooltipProps) => {
  return (
    <Overlay target={target} show={isShown} placement={placement}>
      {(props: any) => (
        <BootstrapTooltip id="bootstrap-tooltip" {...props}>
          {children}
        </BootstrapTooltip>
      )}
    </Overlay>
  )
}
