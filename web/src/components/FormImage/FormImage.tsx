import React, { useState, useEffect } from 'react'
import Form from 'react-bootstrap/Form'
import classname from 'classnames'

import { Icon, IconName } from '../Icon/Icon'

import styles from './FormImage.module.css'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'

interface FormImageProps {
  imageUrl: string
  className?: string
  imageClassName?: string
  placeholder?: string
  onChange?: (value: string) => void
  onClear?: () => void
  withoutClear?: boolean
}

export const FormImage = ({
  imageUrl,
  className,
  imageClassName,
  placeholder,
  onChange,
  onClear,
  withoutClear
}: FormImageProps) => {
  const [url, setUrl] = useState<string | undefined>()
  const [valid, setValid] = useState<boolean | undefined>()

  useEffect(() => {
    setUrl(imageUrl)
  }, [imageUrl])

  function onUrlTextChange({ target: { value } }: any) {
    setUrl(value)
    setValid(undefined)
  }

  function onImageLoad() {
    setValid(true)
    if (imageUrl !== url) {
      onChange && onChange(url || '')
    }
  }

  function onImageLoadError() {
    setValid(false)
  }

  function onClearClick() {
    setUrl(undefined)
    setValid(undefined)
    onChange && onChange('')
    onClear && onClear()
  }

  if (!url || valid === false) {
    return (
      <>
        <Form.Control placeholder={placeholder} onChange={onUrlTextChange} />
        {valid === false ? (
          <ErrorMessage error="Не удалось загрузить картинку" />
        ) : null}
      </>
    )
  }

  return (
    <div className={classname(className, styles.imageWrap)}>
      <img
        alt=""
        src={url}
        className={imageClassName}
        onLoad={onImageLoad}
        onError={onImageLoadError}
      />
      {url && !withoutClear ? (
        <button type="button" onClick={onClearClick}>
          <Icon name={IconName.close} />
        </button>
      ) : null}
    </div>
  )
}
