import React from 'react'
import { FormImage } from './FormImage'

export const FormImageAdapter = ({
  input,
  placeholder,
  className,
  withoutClear
}: any) => {
  return (
    <FormImage
      imageUrl={input.value}
      placeholder={placeholder}
      className={className}
      onChange={(value: string) => input.onChange(value)}
      withoutClear={withoutClear}
    />
  )
}
