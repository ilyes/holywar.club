import React from 'react'
import classnames from 'classnames'

import styles from './Avatar.module.css'
import { Icon, IconName } from 'components/Icon/Icon'

interface AvatarProps {
  avatarUrl: string
  size?: 'l' | 'm' | 's' | 'xs' | 'xxs'
  className?: string
}

export const Avatar = ({ avatarUrl, size = 'm', className }: AvatarProps) => (
  <div
    className={classnames(styles.avatar, styles[`avatar_${size}`], className)}
    style={{
      backgroundImage: avatarUrl ? `url(${avatarUrl})` : undefined
    }}
  >
    {!avatarUrl ? <Icon name={IconName.profile} /> : null}
  </div>
)
