import React from 'react'
import Form from 'react-bootstrap/Form'

export const FormControlAdapter = ({ input, meta, ...rest }: any) => (
  <Form.Control
    {...input}
    {...rest}
    title={rest.placeholder}
    onChange={({ target: { value } }: any) => input.onChange(value)}
  />
)
