import React from 'react'
import Form from 'react-bootstrap/Form'

export const ArrayControlAdapter = ({ input, meta, ...rest }: any) => {
  return (
    <Form.Control
      {...input}
      {...rest}
      value={Array.isArray(input.value) ? input.value.join(';') : ''}
      onChange={({ target: { value } }: any) =>
        input.onChange(value.split(';'))
      }
    />
  )
}
