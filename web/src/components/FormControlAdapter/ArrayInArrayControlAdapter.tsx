import React from 'react'
import Form from 'react-bootstrap/Form'

export const ArrayInArrayControlAdapter = ({
  input,
  meta,
  valueParser,
  ...rest
}: any) => (
  <Form.Control
    {...input}
    {...rest}
    value={
      input.value ? input.value.map((ip: any[]) => ip.join('.')).join(';') : ''
    }
    onChange={({ target: { value } }: any) =>
      input.onChange(
        value
          .split(';')
          .map((ip: string) => ip.split('.').map(valueParser || defaultParser))
      )
    }
  />
)

function defaultParser(s: string): string {
  return s
}
