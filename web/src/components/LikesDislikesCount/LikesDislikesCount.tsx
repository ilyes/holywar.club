import React from 'react'
import classnames from 'classnames'

import { Icon, IconName } from 'components/Icon/Icon'

import styles from './LikesDislikesCount.module.css'

interface LikesDislikesCountProps {
  likesCount: number
  dislikesCount: number
  className?: string
  withoutIcon?: boolean
}

export const LikesDislikesCount = ({
  likesCount,
  dislikesCount,
  className,
  withoutIcon
}: LikesDislikesCountProps) => {
  let icon = null
  if (!withoutIcon) {
    icon =
      likesCount >= dislikesCount ? (
        <Icon className={styles.likesIcon} name={IconName.like} />
      ) : (
        <Icon className={styles.dislikesIcon} name={IconName.like} />
      )
  }
  const hasLikes = likesCount > 0
  const hasDislikes = dislikesCount > 0
  return (
    <div className={classnames(className, styles.layout)}>
      {icon}
      {hasLikes ? (
        <span className={classnames(styles.likesCount)}>+{likesCount}</span>
      ) : null}
      {hasLikes && hasDislikes ? '/' : ''}
      {hasDislikes ? (
        <span className={classnames(styles.dislikesCount)}>
          -{dislikesCount}
        </span>
      ) : null}
      {!hasLikes && !hasDislikes ? (
        <span className={styles.likesCount}>0</span>
      ) : null}
    </div>
  )
}
