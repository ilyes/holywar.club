import React from 'react'
import classnames from 'classnames'

import styles from './Icon.module.css'

export enum IconName {
  eye = 'icon-visibility-24px',
  comment = 'icon-chat_bubble_outline-24px',
  like = 'icon-thumb_up-24px',
  close = 'icon-close-24px',
  currency = 'currency',
  link = 'icon-link-24px',
  profile = 'icon-account_circle-24px',
  notifications = 'icon-notifications-24px',
  title = 'icon-title-24px',
  image = 'icon-insert_photo-24px',
  text = 'icon-subject-24px',
  plus = 'icon-add_circle_outline-24px',
  invite = 'icon-person_add-24px',
  question = 'icon-contact_support-24px',
  settings = 'icon-settings_applications-24px',
  menu = 'icon-menu-24px'
}

interface IconProps {
  name: IconName
  className?: string
  title?: string
  onClick?: (e: any) => any
  ref?: any
}

export const Icon = ({
  name,
  title,
  className,
  onClick,
  ref,
  ...rest
}: IconProps) => {
  const icon = getTextIcon(name)
  return (
    <i
      title={title}
      className={classnames(className, name, { [styles.clickable]: onClick })}
      onClick={onClick}
      ref={ref}
      {...rest}
    >
      {icon}
    </i>
  )
}

function getTextIcon(name: string) {
  if (name === IconName.currency) {
    return <span className={styles.textIcon}>₽</span>
  }
  return undefined
}
