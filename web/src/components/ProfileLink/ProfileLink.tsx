import React from 'react'
import classnames from 'classnames'
import { Link } from 'react-router-dom'

import { Paths } from 'routing'
import { IconName, Icon } from 'components/Icon/Icon'

import styles from './ProfileLink.module.css'
import { Avatar } from 'components/Avatar/Avatar'

interface ProfileLinkProps {
  avatarUrl: string
  userName: string
  className?: string
}

export const ProfileLink = ({
  avatarUrl,
  userName,
  className
}: ProfileLinkProps) => {
  return (
    <Link
      className={classnames(styles.link, className)}
      to={Paths.GetProfile()}
    >
      {avatarUrl ? (
        <Avatar avatarUrl={avatarUrl} size="s" className={styles.avatar} />
      ) : (
        <Icon name={IconName.profile} />
      )}
      <span className={styles.title}>{userName}</span>
    </Link>
  )
}
