// @flow

import React, { PureComponent } from 'react'
import { Expandable, ExpandableProps } from '../Expandable/Expandable'

interface DropdownRenderFunc {
  (requestCollapse: () => void): any
}

interface DropdownProps extends ExpandableProps {
  displayOnHover?: boolean
  onExpanded?: () => void
  onCollapsed?: () => void
  children: any | DropdownRenderFunc
  withCloseButton?: boolean
}

type State = {
  isExpanded: boolean
}

abstract class DropdownableInternal extends PureComponent<
  DropdownProps,
  State
> {
  state = { isExpanded: false }
  isCollapsing = false

  abstract handleTitleClick(event: any): void
  abstract handleMouseEnter(): void
  abstract handleMouseLeave(): void

  expand = () => {
    const { onExpanded } = this.props
    this.setState(() => ({ isExpanded: true }), onExpanded)
  }

  collapse = () => {
    const { onCollapsed } = this.props
    this.isCollapsing = true
    this.setState(() => ({ isExpanded: false }), onCollapsed)
    setTimeout(() => {
      this.isCollapsing = false
    }, 500)
  }

  handleRequestCollapsing = () => {
    this.collapse()
  }

  renderChildren() {
    const { children } = this.props
    if (typeof children === 'function') {
      return children(this.handleRequestCollapsing)
    }
    return children
  }

  render() {
    const { title, expanded } = this.props
    const { isExpanded } = this.state
    return (
      <Expandable
        {...this.props}
        title={title}
        onTitleClick={this.handleTitleClick}
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
        expanded={isExpanded || expanded}
      >
        {this.renderChildren()}
      </Expandable>
    )
  }
}

class DropdownableByHover extends DropdownableInternal {
  handleTitleClick(): void {}
  handleMouseEnter(): void {
    this.expand()
  }
  handleMouseLeave(): void {
    this.collapse()
  }
}

class DropdownableByClick extends DropdownableInternal {
  componentWillUnmount() {
    this.unBindWindowEvent()
  }

  bindWindowEvent = () => {
    if (typeof window === 'undefined') {
      return
    }
    window.addEventListener('resize', this.collapse)
    window.addEventListener('contextmenu', this.collapse)
    window.addEventListener('mousedown', this.collapse)
    window.addEventListener('click', this.collapse)
    window.addEventListener('scroll', this.collapse)
  }

  unBindWindowEvent = () => {
    if (typeof window === 'undefined') {
      return
    }
    window.removeEventListener('resize', this.collapse)
    window.removeEventListener('contextmenu', this.collapse)
    window.removeEventListener('mousedown', this.collapse)
    window.removeEventListener('click', this.collapse)
    window.removeEventListener('scroll', this.collapse)
  }

  handleTitleClick = (event: any) => {
    event.stopPropagation()
    if (this.props.disabled || this.isCollapsing) {
      return
    }

    if (!this.state.isExpanded) {
      this.expand()
    } else {
      this.collapse()
    }
  }

  handleMouseEnter = () => this.unBindWindowEvent()

  handleMouseLeave = () => this.bindWindowEvent()
}

export const Dropdown = (props: DropdownProps) =>
  props.displayOnHover === true ? (
    <DropdownableByHover {...props} />
  ) : (
    <DropdownableByClick {...props} />
  )
