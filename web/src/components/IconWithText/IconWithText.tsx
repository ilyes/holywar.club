import React from 'react'
import classnames from 'classnames'

import { Icon, IconName } from '../Icon/Icon'

import styles from './IconWithText.module.css'

interface IconWithTextProps {
  label?: string
  text: any
  iconName: IconName
  className?: string
  onClick?: () => void
}

export const IconWithText = ({
  label,
  text,
  iconName,
  className,
  onClick
}: IconWithTextProps) => {
  return (
    <div className={classnames(styles.wrap, className)} onClick={onClick}>
      {label ? <span className={styles.label}>{label}</span> : null}
      <Icon className={styles.icon} name={iconName} />
      {text}
    </div>
  )
}
