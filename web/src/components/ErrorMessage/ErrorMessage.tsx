import React from 'react'

import styles from './ErrorMessage.module.css'

interface ErrorMessageProps {
  error?: string
  errors?: string[]
}

export const ErrorMessage = ({ error, errors }: ErrorMessageProps) => {
  if (!errors && !error) {
    return null
  }
  return (
    <div className={styles.errors}>
      {error ? <div className={styles.error}>{mapErrorKey(error)}</div> : null}
      {errors
        ? errors
            .filter(e => typeof e === 'string')
            .map(e => (
              <div key={e} className={styles.error}>
                {mapErrorKey(e)}
              </div>
            ))
        : null}
    </div>
  )
}

const ErrorKeys = {
  Prohibited:
    'Вы не можете оставлять сообщения на сайте, так как нарушили правила.',
  SelfCommentsProhibited:
    'Давайте не будем комментировать собственные холивары чтобы не превращать страницу в свалку. Вы можете написать все что думаете в самом холиваре. Для этого нажмите на кнопку "Изменить", которая доступна в течении 45 минут после публикации.'
}

function mapErrorKey(key: string): string {
  return ErrorKeys[key] || key
}
