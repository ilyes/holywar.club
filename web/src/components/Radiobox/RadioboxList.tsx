import React, { ReactNode } from 'react'

import { Radiobox } from './Radiobox'

type Value = string | number | boolean

interface RadioboxListItem {
  value: Value
  label: ReactNode
}

interface RadioboxListProps {
  name: string
  className?: string
  onChange: (value: Value) => void
  items: RadioboxListItem[]
  value: Value
}

export const RadioboxList = ({
  name,
  className,
  onChange,
  items,
  value
}: RadioboxListProps) => {
  return (
    <div className={className}>
      {items.map(i => (
        <Radiobox
          id={i.value.toString()}
          key={i.value.toString()}
          name={name}
          label={i.label}
          onChange={() => onChange(i.value)}
          value={i.value}
        />
      ))}
    </div>
  )
}
