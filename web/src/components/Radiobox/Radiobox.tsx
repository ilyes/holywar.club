import React, { ReactNode } from 'react'
import Form from 'react-bootstrap/Form'
import classnames from 'classnames'

import styles from './Radiobox.module.css'

interface RadioboxProps {
  id?: string
  name: string
  className?: string
  label: ReactNode
  onChange: (e: any) => void
  value: any
}

export const Radiobox = ({
  id,
  name,
  className,
  label,
  onChange,
  value
}: RadioboxProps) => {
  return (
    <Form.Check
      type="radio"
      custom
      id={id}
      name={name}
      className={classnames(styles.radio, className)}
      label={label}
      onChange={onChange}
      value={value}
    />
  )
}
