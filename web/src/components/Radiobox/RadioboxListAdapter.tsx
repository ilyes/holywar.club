import React from 'react'
import { RadioboxList } from './RadioboxList'

export const RadioboxListAdapter = ({ items, input, meta, ...rest }: any) => {
  return (
    <RadioboxList
      {...input}
      {...rest}
      onChange={(value: any) => {
        input.onChange(value)
      }}
      items={items}
    />
  )
}
