import React from 'react'
import classnames from 'classnames'

import styles from './Words.module.css'

type Props = {
  size?: 'xxl' | 'xl' | 'l' | 'm' | 's' | 'xs' | 'xxs'
  color?: 'default' | 'primary' | 'secondary'
  bold?: boolean
}

export const Words: React.FC<Props> = ({
  children,
  size = 'm',
  color = 'default',
  bold
}) => {
  return (
    <div
      className={classnames(styles[`size_${size}`], styles[`color_${color}`], {
        [styles.bold]: bold
      })}
    >
      {children}
    </div>
  )
}
