import React from 'react'
import classnames from 'classnames'

import styles from './LabeledInfo.module.css'

interface LabeledInfoProps {
  label: string
  children: any
  horizontal?: boolean
  classNames?: ClassNames
}

interface ClassNames {
  root?: string
  label?: string
  children?: string
}

export const LabeledInfo = ({
  label,
  children,
  horizontal,
  classNames = {}
}: LabeledInfoProps) => {
  return (
    <div
      className={classnames(styles.wrap, classNames.root, {
        [styles.horizontal]: horizontal
      })}
    >
      <div className={classNames.label}>{label}</div>
      <div className={classNames.children}>{children}</div>
    </div>
  )
}
