import React from 'react'
import Form from 'react-bootstrap/Form'

interface SelectItem {
  value: any
  label: string
}

interface SelectProps {
  items: SelectItem[]
  placeholder?: string
  value?: any
  onChange: (target: any) => void
}

export const Select = ({
  items,
  value,
  placeholder,
  onChange
}: SelectProps) => {
  return (
    <Form.Control as="select" value={value} onChange={onChange}>
      {placeholder ? <option>{placeholder}</option> : null}
      {items.map((i) => (
        <option key={i.value} value={i.value}>
          {i.label}
        </option>
      ))}
    </Form.Control>
  )
}
