import React from 'react'
import { Select } from './Select'

export const FormSelectAdapter = ({ input, ...rest }: any) => (
  <Select
    {...input}
    {...rest}
    onChange={({ target: { value } }: any) => {
      input.onChange(value)
    }}
  />
)
