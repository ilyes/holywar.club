import { combineReducers } from 'redux'

export { IS_ADMIN, setAdmin, addRelogin } from './admin/actions'
import { admin } from './admin/reducer'

export { ATHENTICATED, setAuthenticated } from './auth/actions'
import { auth } from './auth/reducer'

import { cache } from './cache/reducer'
export {
  SAVE_TO_CACHE,
  saveToCache,
  REMOVE_FROM_CACHE,
  removeFromCache,
  CLEAR_CACHE,
  clearCache
} from './cache/actions'

export const reducers = combineReducers({
  admin,
  auth,
  cache
})
