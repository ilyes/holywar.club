export const ATHENTICATED = 'ATHENTICATED'

export function setAuthenticated(token: string) {
  return {
    type: ATHENTICATED,
    isAuthenticated: Boolean(token),
    token
  }
}
