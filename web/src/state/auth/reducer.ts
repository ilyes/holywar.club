import { storeAuthToken, clearAuthToken } from 'helpers'

import { ATHENTICATED } from './actions'

interface AuthState {
  isAuthenticated: boolean
  token: string
}

interface AuthAction {
  type: string
  isAuthenticated: boolean
  token: string
}

export const auth = (
  state: AuthState | undefined,
  action: AuthAction
): AuthState | undefined => {
  switch (action.type) {
    case ATHENTICATED: {
      const { isAuthenticated, token } = action
      if (token) {
        storeAuthToken(token)
      } else {
        clearAuthToken()
      }
      return { isAuthenticated, token }
    }
    default: {
      return state || { isAuthenticated: false, token: '' }
    }
  }
}
