import { IS_ADMIN, ADD_RELOGIN } from './actions'

interface AdminState {
  isAdmin: boolean
  reloginTokens: string[]
}

interface AdminAction {
  type: string
  isAdmin: boolean
  token: string
}

const RELOGIN_TOKENS_KEY = 'RELOGIN_TOKENS_KEY'

const defaultState = { isAdmin: false, reloginTokens: restoreReloginTokens() }

export const admin = (
  state: AdminState | undefined = defaultState,
  action: AdminAction
): AdminState | undefined => {
  switch (action.type) {
    case IS_ADMIN: {
      const { isAdmin } = action
      return { ...state, isAdmin }
    }
    case ADD_RELOGIN: {
      const { reloginTokens } = state
      const { token } = action
      const newTokens = [...reloginTokens, token]
      storeReloginTokens(newTokens)
      return { ...state, reloginTokens: newTokens }
    }
    default: {
      return state || defaultState
    }
  }
}

function restoreReloginTokens() {
  if (typeof localStorage === 'undefined') {
    return []
  }
  return (localStorage.getItem(RELOGIN_TOKENS_KEY) || '').split(',')
}

function storeReloginTokens(tokens: string[]) {
  if (typeof localStorage === 'undefined') {
    return
  }
  localStorage.setItem(RELOGIN_TOKENS_KEY, tokens.join(','))
}

function clearReloginTokens() {
  if (typeof localStorage === 'undefined') {
    return
  }
  localStorage.removeItem(RELOGIN_TOKENS_KEY)
}
