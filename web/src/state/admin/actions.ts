export const IS_ADMIN = 'IS_ADMIN'
export const ADD_RELOGIN = 'ADD_RELOGIN'

export function setAdmin(isAdmin: boolean) {
  return {
    type: IS_ADMIN,
    isAdmin
  }
}

export function addRelogin(token: string) {
  return {
    type: ADD_RELOGIN,
    token
  }
}
