import { SAVE_TO_CACHE, REMOVE_FROM_CACHE, CLEAR_CACHE } from './actions'

interface CacheAction {
  type: string
  key: string
  value: any
  date: Date
}

export const cache = (state: any = {}, action: CacheAction): any => {
  switch (action.type) {
    case SAVE_TO_CACHE: {
      const { key, value, date } = action
      return { ...state, [key]: { value, date } }
    }
    case REMOVE_FROM_CACHE: {
      const { key } = action
      return { ...state, [key]: null }
    }
    case CLEAR_CACHE: {
      return {}
    }
    default: {
      return state
    }
  }
}
