export const SAVE_TO_CACHE = 'SAVE_TO_CACHE'
export const REMOVE_FROM_CACHE = 'REMOVE_FROM_CACHE'
export const CLEAR_CACHE = 'CLEAR_CACHE'

export function saveToCache(key: string, value: any) {
  return {
    type: SAVE_TO_CACHE,
    key,
    value,
    date: new Date()
  }
}

export function removeFromCache(key: string) {
  return {
    type: REMOVE_FROM_CACHE,
    key
  }
}

export function clearCache() {
  return {
    type: CLEAR_CACHE
  }
}
