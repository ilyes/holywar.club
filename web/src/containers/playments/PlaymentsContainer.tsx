import React from 'react'
import { PaymentRequestContainer } from './PaymentRequestContainer'
import { useGet } from 'helpers'
import { PaymentList } from 'composite'
import { useApi } from 'helpers/api'

export const PlaymentsContainer = () => {
  const { paymentsUrl } = useApi()
  const { response, loading, reload } = useGet(paymentsUrl())
  return (
    <div>
      <h1>Выплаты</h1>
      <PaymentRequestContainer onComplete={reload} />
      <PaymentList payments={response} loading={loading} />
    </div>
  )
}
