import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

import { Button } from 'components/Button/Button'
import { useCurrentUser, usePost, useConfig } from 'helpers'
import { AskForPayment } from 'composite'
import { Paths } from 'routing'
import { getMinPayment } from 'models'
import { useApi } from 'helpers/api'

export const PaymentRequestContainer = ({ onComplete }: any) => {
  const { askForPaymentUrl } = useApi()
  const [user, reload] = useCurrentUser()
  const [ask, setAsk] = useState(false)
  const [complete, setComplete] = useState(false)
  const [submitAsk, askResult] = usePost(askForPaymentUrl())
  const config = useConfig()

  useEffect(() => {
    if (!complete && askResult.response && askResult.response.success) {
      reload()
      setComplete(true)
      onComplete()
    }
  }, [askResult, reload, complete, onComplete])

  if (!user) {
    return null
  }

  if (askResult.response && askResult.response.success) {
    return (
      <div style={{ textAlign: 'center' }}>
        <p>Запрос на выплату отправлен.</p>
      </div>
    )
  }

  const limit = getMinPayment(user, config)
  if (user.balance < limit) {
    return (
      <div style={{ textAlign: 'center' }}>
        <i>Минимальная сумма выплаты {limit} рублей.</i>
        <p>
          <Link to={Paths.Limits}>Как уменьшить лимит?</Link>
        </p>
      </div>
    )
  }

  if (!ask) {
    return (
      <div style={{ textAlign: 'center' }}>
        <Button variant="link" onClick={() => setAsk(true)}>
          Запросить выплату
        </Button>
      </div>
    )
  }

  return (
    <AskForPayment
      onSubmit={(values) => submitAsk(values)}
      loading={askResult.loading}
      error={askResult.error}
    />
  )
}
