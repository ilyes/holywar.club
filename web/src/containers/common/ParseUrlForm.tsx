import React, { useEffect, useState } from 'react'

import { usePost, mapErrorMessage } from 'helpers'
import { ParseUrlInput } from 'composite'
import { useApi } from 'helpers/api'

export interface ParseResult {
  url: string
  title: string
  imageUrl: string
  description: string
}

interface ParseUrlFormProps {
  onComplete: (result: ParseResult) => void
  checkDiscussion?: boolean
}

export const ParseUrlForm: React.FC<ParseUrlFormProps> = ({
  onComplete,
  checkDiscussion
}) => {
  const { parseUrl } = useApi()
  const [url, setUrl] = useState('')
  const [parse, parseResult] = usePost(parseUrl())

  const parseError = mapErrorMessage(parseResult.error, parseResult.response)

  useEffect(() => {
    if (parseResult.response) onComplete({ url, ...parseResult.response })
  }, [parseResult.response])

  return (
    <ParseUrlInput
      loading={parseResult.loading}
      error={parseError}
      onChange={(url) => {
        setUrl(url)
        parse({ url, checkDiscussion })
      }}
    />
  )
}
