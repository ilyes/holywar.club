import React from 'react'

import { useGet } from 'helpers'
import { MenuDropdown } from 'composite'
import { CategoriesListContainer } from 'containers'
import { useModal } from 'components/Modal'
import { useApi } from 'helpers/api'

export const MenuDropdownContainer = () => {
  const { categoriesUrl } = useApi()
  const { response } = useGet(categoriesUrl())
  const { showModal } = useModal()

  return (
    <MenuDropdown
      categories={response || []}
      onSettingsClick={() => showModal(<CategoriesListContainer />)}
    />
  )
}
