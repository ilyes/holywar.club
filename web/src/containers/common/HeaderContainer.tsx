import React, { memo, ReactNode } from 'react'

import { Header } from 'composite'

import { InviteButtonContainer } from '../user/InviteButtonContainer'
import { ProfileLinkContainer } from '../user/ProfileLinkContainer'
import { UnreadMessagesContainer } from '../messages/UnreadMessagesContainer'
import { MenuDropdownContainer } from './MenuDropdownContainer'

interface HeaderContainerProps {
  categories?: ReactNode
}

export const HeaderContainer = memo(({ categories }: HeaderContainerProps) => {
  return (
    <Header
      messages={<UnreadMessagesContainer />}
      menu={<MenuDropdownContainer />}
      profile={<ProfileLinkContainer />}
      categories={categories}
    />
  )
})
