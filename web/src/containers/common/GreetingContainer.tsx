import React, { useEffect } from 'react'

import { useModal } from 'components/Modal'
import { Greeting } from 'composite'
import { isGreetingDisplayed, setGreetingDisplayed, useConfig } from 'helpers'

export const GreetingContainer = () => {
  const { showModal, hideModal } = useModal()
  const config = useConfig()
  useEffect(() => {
    if (!isGreetingDisplayed()) {
      setGreetingDisplayed()
      showModal(
        <Greeting
          onOkClick={hideModal}
          howToDiscussionId={config.howToDiscussionId}
        />
      )
    }
  }, [])
  return null
}
