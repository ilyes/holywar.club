import React, { memo } from 'react'

import { CommentList } from 'composite'
import { CommentWithUserModel, CommentViewModel, UserModel } from 'models'
import { CommentContainer } from './CommentContainer'
import { useCurrentUser, parseDate, getTimeoffset } from 'helpers'

interface CommentListContainerProps {
  comments: CommentWithUserModel[]
  onReply?: (message: string, imageUrl: string, parentCommentId: string) => void
  onEdit?: (message: string, imageUrl: string, commentId: string) => void
  noLikes?: boolean
  plain?: boolean
  withLink?: boolean
  submittingReplyTo?: string
}

export const CommentListContainer = memo(
  ({
    comments,
    onReply,
    onEdit,
    noLikes,
    plain,
    withLink,
    submittingReplyTo
  }: CommentListContainerProps) => {
    const [currentUser] = useCurrentUser()
    if (!comments || !comments.length) {
      return <p>Комментариев пока нет</p>
    }
    const mappedComments = plain
      ? (comments as CommentViewModel[])
      : mapComments(comments, (c) => !c.comment.parentCommentId)

    return (
      <CommentList
        comments={mappedComments}
        Comment={({ commentWithUser }: any) => (
          <CommentContainer
            commentWithUser={commentWithUser}
            onReply={onReply}
            onEdit={onEdit}
            noLikes={noLikes}
            withLink={withLink}
            canEdit={canEdit(commentWithUser, currentUser)}
            replying={commentWithUser.comment.id === submittingReplyTo}
          />
        )}
      />
    )
  }
)

function canEdit(
  commentWithUser: CommentViewModel,
  currentUser: UserModel | null
) {
  return Boolean(
    currentUser &&
      currentUser.id === commentWithUser.user.id &&
      !editTimeIsLeft(commentWithUser.comment.creationDate)
  )
}

function editTimeIsLeft(commentCreationDate: string) {
  return (
    new Date().getTime() - parseDate(commentCreationDate).getTime() >
    10 * 60 * 1000
  )
}

function mapComments(
  comments: CommentWithUserModel[],
  filter: (value: CommentWithUserModel) => boolean
): CommentViewModel[] {
  const rootComments = comments.filter(filter)
  return rootComments.map((c: CommentWithUserModel) => ({
    comment: c.comment,
    discussionAlias: c.discussionAlias,
    user: c.user,
    children: mapComments(
      comments,
      (cc) => cc.comment.parentCommentId === c.comment.id
    )
  }))
}
