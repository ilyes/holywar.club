import { Delimiter } from 'components/Delimiter/Delimiter'
import { Spinner } from 'components/Spinner/Spinner'
import { Words } from 'components/Words/Words'
import { CommentForTop } from 'composite/discussions/CommentForTop/CommentForTop'
import { CurrencySign } from 'constants/currency'
import { useConfig, useGet } from 'helpers'
import { useApi } from 'helpers/api'
import { Panel } from 'layouts/common/Panel/Panel'
import { CommentWithUserModel } from 'models'
import React from 'react'
import { Link } from 'react-router-dom'
import { Paths } from 'routing'

export const CommentsOfTheWeek = () => {
  const { weekJackpot } = useConfig()
  const { ratingCommentsOfTheWeekUrl } = useApi()
  const { finished, response, error } = useGet(
    ratingCommentsOfTheWeekUrl({ limit: 3 })
  )
  if (!finished) {
    return (
      <Panel bg>
        <Spinner />
      </Panel>
    )
  }

  if (error || response?.length === 0) {
    return null
  }

  return (
    <Panel bg>
      <div>
        <Words size="m" bold>
          Комменты недели
        </Words>
        <Words color="secondary" size="s">
          Джекпот на этой неделе: {weekJackpot} {CurrencySign.Rub}
        </Words>
      </div>
      <Delimiter gap="0" color="light" />
      {response &&
        response.map((c: CommentWithUserModel, i: number) => (
          <>
            <CommentForTop commentWithUser={c} />
            <Delimiter gap="0" color="light" />
          </>
        ))}
      <Words size="s">
        В конце недели автор лучшего коммента забирает джекпот!
      </Words>
      <Link to={Paths.RatingsComments}>Посмотреть лучшие комменты</Link>
    </Panel>
  )
}
