import React, { useState, useEffect, useRef, memo } from 'react'

import { Button } from 'components/Button/Button'
import { Comment, CreateMessageForm } from 'composite'
import { useAuth, usePost, useCurrentUser, useCacheItem } from 'helpers'
import { CommentWithUserModel, UserRole } from 'models'
import { CommentLayout, HorizontalLayout, Panel } from 'layouts'
import { Paths } from 'routing'
import { Link } from 'react-router-dom'
import { useApi } from 'helpers/api'
import { useAdminApi } from 'helpers/admin/api'
import { AdminPaths } from 'routing/admin/AdminPaths'

interface CommentContainerProps {
  commentWithUser: CommentWithUserModel
  onReply?: (text: string, imageUrl: string, parentCommentId: string) => void
  onEdit?: (text: string, imageUrl: string, commentId: string) => void
  canEdit?: boolean
  noLikes?: boolean
  withLink?: boolean
  replying?: boolean
}

export const CommentContainer = memo(
  ({
    onReply,
    onEdit,
    canEdit,
    commentWithUser,
    noLikes,
    withLink,
    replying
  }: CommentContainerProps) => {
    const { user, discussionAlias } = commentWithUser
    const [isFormVisible, setIsFormVisible] = useState(false)
    const [edit, setEdit] = useState(false)
    const [comment, setComment] = useState(commentWithUser.comment)
    const { isAuthenticated } = useAuth()
    const { likeCommentUrl } = useApi()
    const { deleteCommentUrl } = useAdminApi()
    const [submitLike, submitLikeResult] = usePost(likeCommentUrl())
    const [deleteComment, deleteResult] = usePost(deleteCommentUrl())
    const ref = useRef(null)
    const shouldScrollToId = getScrollToId()
    const { value: scrolledTo, save: setScrolledTo } =
      useCacheItem(`SCROLLED_TO`)
    const [currentUser] = useCurrentUser()

    useEffect(() => {
      if (submitLikeResult.response && submitLikeResult.response.comment) {
        if (comment) {
          comment.likesCount = submitLikeResult.response.comment.likesCount
          comment.dislikesCount =
            submitLikeResult.response.comment.dislikesCount
        }
        setComment(submitLikeResult.response.comment)
      } // eslint-disable-next-line
    }, [submitLikeResult.response])

    useEffect(() => {
      if (
        scrolledTo !== comment.id &&
        shouldScrollToId === comment.id &&
        ref &&
        ref.current
      ) {
        window.scrollTo(0, (ref.current as any).offsetTop)
        setScrolledTo(shouldScrollToId)
      }
      // eslint-disable-next-line
    }, [])

    if (edit) {
      return (
        <CreateMessageForm
          comment={comment}
          submitLabel="Сохранить"
          onSubmit={({ text, imageUrl }) => {
            onEdit && onEdit(text, imageUrl, commentWithUser.comment.id)
            setEdit(false)
          }}
        />
      )
    }

    return (
      <CommentLayout setRef={ref} highlight={scrolledTo === comment.id}>
        <Comment
          commentWithUser={{ comment, user, discussionAlias }}
          onLikeClick={
            !noLikes
              ? (comment, isLike) =>
                  submitLike({ commentId: comment.id, isLike })
              : undefined
          }
          likeLoading={submitLikeResult.loading}
        />
        {withLink && (
          <Panel horizontal padding="s">
            <Link to={Paths.GetDiscussion(discussionAlias, comment.id)}>
              Посмотреть
            </Link>
          </Panel>
        )}
        {onReply && isAuthenticated ? (
          isFormVisible ? (
            <CreateMessageForm
              onSubmit={({ text, imageUrl }) =>
                onReply(text, imageUrl, commentWithUser.comment.id)
              }
            />
          ) : (
            <HorizontalLayout>
              <Button
                variant="link"
                loading={replying}
                onClick={() => setIsFormVisible(true)}
              >
                Ответить
              </Button>
              {currentUser && currentUser.role === UserRole.admin ? (
                <div>
                  <Button
                    variant="link"
                    size="sm"
                    loading={deleteResult.loading}
                    disabled={
                      deleteResult.response && deleteResult.response.success
                    }
                    onClick={() =>
                      deleteComment({ commentId: commentWithUser.comment.id })
                    }
                  >
                    {deleteResult.response && deleteResult.response.success
                      ? 'Удален'
                      : 'Удалить'}
                  </Button>
                  <Link
                    to={AdminPaths.CommentLikes(commentWithUser.comment.id)}
                  >
                    Лайки
                  </Link>
                </div>
              ) : null}
              {canEdit ? (
                <Button variant="link" onClick={() => setEdit(true)}>
                  Изменить
                </Button>
              ) : (
                <span>&nbsp;</span>
              )}
            </HorizontalLayout>
          )
        ) : null}
      </CommentLayout>
    )
  }
)

function getScrollToId() {
  if (typeof window === 'undefined' || !window.location.hash) {
    return undefined
  }
  const hashParts = window.location.hash.split('=')
  if (hashParts.length !== 2 || hashParts[0] !== '#comment') {
    return undefined
  }
  return hashParts[1]
}
