import React from 'react'

import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { Spinner } from 'components/Spinner/Spinner'
import { useApi } from 'helpers/api'
import { useGet } from 'helpers/fetch/useGet'
import { Panel } from 'layouts/common/Panel/Panel'
import { UserCard } from 'composite'
import { UserModel } from 'models'
import { Link } from 'react-router-dom'
import { Paths } from 'routing'

export const TopUsers = () => {
  const { ratingUrl } = useApi()
  const { loading, error, response } = useGet(ratingUrl())
  return (
    <Panel bg>
      {loading && <Spinner />}
      {error && <ErrorMessage error={error} />}
      {response?.slice(0, 3).map((u: UserModel) => (
        <UserCard key={u.id} user={u} small />
      ))}
      <Link to={Paths.Ratings}>Посмотреть весь рейтинг</Link>
    </Panel>
  )
}
