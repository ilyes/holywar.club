import React from 'react'

import { useCurrentUser } from 'helpers'
import { Paths } from 'routing'
import { IconLink } from 'components/IconLink/IconLink'
import { IconName } from 'components/Icon/Icon'
import { ProfileLink } from 'components/ProfileLink/ProfileLink'

export const ProfileLinkContainer = () => {
  const [user] = useCurrentUser()
  if (!user) {
    return (
      <IconLink to={Paths.SignIn} iconName={IconName.profile}>
        Войти
      </IconLink>
    )
  }

  return <ProfileLink avatarUrl={user.avatarUrl} userName={user.name} />
}
