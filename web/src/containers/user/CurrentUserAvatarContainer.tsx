import React, { useState, useEffect } from 'react'

import { CurrentUserAvatar } from 'composite'
import { useCurrentUser, useUploader } from 'helpers'
import { UserModel } from 'models'
import { useApi } from 'helpers/api'

interface CurrentUserAvatarContainerProps {
  user: UserModel
}

export const CurrentUserAvatarContainer = ({
  user
}: CurrentUserAvatarContainerProps) => {
  const { uploadUserAvatarUrl } = useApi()
  const [avatar, setAvatar] = useState(user.avatarUrl)
  const [upload, { pending, result, error }] = useUploader()
  const [, reloadUser] = useCurrentUser()

  useEffect(() => {
    if (result && result.avatarUrl) {
      reloadUser()
      setAvatar(result.avatarUrl)
    }
  }, [result])

  return (
    <CurrentUserAvatar
      avatarUrl={avatar}
      pending={pending}
      error={error}
      onAvatarPicked={(file) => upload(uploadUserAvatarUrl(), file)}
    />
  )
}
