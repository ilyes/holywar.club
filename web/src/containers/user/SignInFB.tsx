import React, { useEffect } from 'react'
import { getQueryParam, usePost, useAuth } from 'helpers'
import { withRouter } from 'react-router'
import { Paths } from 'routing'
import { useApi } from 'helpers/api'

export const SignInFB = withRouter(({ history }: any) => {
  const { signInFacebookUrl } = useApi()
  const referralId = getQueryParam('referral')
  const [submit, { response }] = usePost(signInFacebookUrl())
  const { setAuthenticated } = useAuth()

  useEffect(() => {
    if (response && response.success === true) {
      history.push(Paths.Home)
      setAuthenticated(response.token)
    }
  }, [response, history, setAuthenticated])

  function handleClick() {
    const w: any = window
    w.FB.login(
      function (response: any) {
        if (response.authResponse) {
          const { accessToken } = response.authResponse
          submit({ accessToken, referralId })
        }
      },
      { scope: 'email' }
    )
  }

  if (typeof window === 'undefined') {
    return null
  }

  return <img src="/images/fb_share_32.png" alt="FB" onClick={handleClick} />
})
