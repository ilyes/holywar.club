import React from 'react'

import { useModal } from 'components/Modal'

import { ReferedUsersContainer } from './ReferedUsersContainer'
import { InviteButton } from 'components/InviteButton/InviteButton'

export const InviteButtonContainer = ({ text, size }: any) => {
  const { showModal } = useModal()
  return (
    <InviteButton
      text={text}
      size={size}
      onClick={() => showModal(<ReferedUsersContainer />)}
    />
  )
}
