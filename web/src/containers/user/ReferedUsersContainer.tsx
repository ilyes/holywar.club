import React from 'react'

import { ReferedUsers } from 'composite'
import { useGet, useAuth } from 'helpers'

import { Share } from '../share'
import { Paths } from 'routing'
import { Link } from 'react-router-dom'
import { useApi } from 'helpers/api'

export const ReferedUsersContainer = () => {
  const { userReferralsUrl, currentUserUrl } = useApi()
  const { isAuthenticated } = useAuth()
  const referralsResult = useGet(userReferralsUrl())
  const currentUserResult = useGet(currentUserUrl())

  if (!isAuthenticated) {
    return (
      <div>
        <p>Чтобы уаствовать в реферальной программе нужно залогиниться.</p>
        <p>
          <Link to={Paths.SignIn}>Войти</Link>
        </p>
      </div>
    )
  }

  const url = currentUserResult.response
    ? Paths.GetAbsoluteReferralUrl(currentUserResult.response.id)
    : null
  return (
    <ReferedUsers
      userId={
        currentUserResult.response ? currentUserResult.response.id : undefined
      }
      referralPayments={
        currentUserResult.response
          ? currentUserResult.response.referralPayments
          : 0
      }
      loading={referralsResult.loading}
      error={referralsResult.error}
      referrals={referralsResult.response}
      shareButtons={url ? <Share url={url} /> : null}
    />
  )
}
