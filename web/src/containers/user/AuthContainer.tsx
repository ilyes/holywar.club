import { restoreAuthToken, useAuth, useEffectWithPrev, useGet } from 'helpers'
import { useApi } from 'helpers/api'

export const AuthContainer = () => {
  const { authUrl, currentUserUrl } = useApi()
  const restoredToken = restoreAuthToken()
  const { setAuthenticated, token } = useAuth()
  const auth = useGet(authUrl(), {
    headers: { Auth: restoredToken }
  })
  const user = useGet(currentUserUrl())

  useEffectWithPrev(
    ([{ response }]) => {
      if (!auth.loading && auth.response !== response) {
        setAuthenticated(auth.response?.token, true)
      }
    },
    [auth, setAuthenticated]
  )

  useEffectWithPrev(
    ([prevToken]) => {
      if (token && token !== prevToken) {
        user.reload()
      }
    },
    [token, user]
  )

  return null
}
