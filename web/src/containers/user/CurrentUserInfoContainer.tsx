import React from 'react'
import { withRouter } from 'react-router-dom'
import * as H from 'history'

import { useModal } from 'components/Modal'
import { UserModel } from 'models'
import { CurrentUserInfo } from 'composite'
import { useAuth } from 'helpers'
import { Paths } from 'routing'

import { CurrentUserAvatarContainer } from './CurrentUserAvatarContainer'
import { ChangePasswordContainer } from './ChangePasswordContainer'
import { PlaymentsContainer } from '../playments/PlaymentsContainer'

interface CurrentUserInfoContainerProps {
  user: UserModel
  history: H.History
}

const CurrentUserInfoContainerInternal = ({
  user,
  history
}: CurrentUserInfoContainerProps | any) => {
  const { setAuthenticated } = useAuth()
  const { showModal, hideModal } = useModal()

  function handleSignOut() {
    setAuthenticated('')
    history.push(Paths.Home)
  }

  function handleChangePasswordClick() {
    showModal(<ChangePasswordContainer onComplete={hideModal} />)
  }

  function handlePaymentRequestClick() {
    showModal(<PlaymentsContainer />)
  }

  return (
    <CurrentUserInfo
      user={user}
      Avatar={<CurrentUserAvatarContainer user={user} />}
      onSignOutClick={handleSignOut}
      onChangePasswordClick={handleChangePasswordClick}
      onPaymentRequestClick={handlePaymentRequestClick}
    />
  )
}

export const CurrentUserInfoContainer = withRouter(
  CurrentUserInfoContainerInternal
)
