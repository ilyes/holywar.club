import React from 'react'

import { ChangePassword } from 'composite'
import { usePost } from 'helpers'
import { useApi } from 'helpers/api'

interface ChangePasswordContainerProps {
  onComplete: Function
}

export const ChangePasswordContainer = ({
  onComplete
}: ChangePasswordContainerProps) => {
  const { changePasswordUrl } = useApi()
  const [submit, { loading, response, error }] = usePost(changePasswordUrl())
  return (
    <ChangePassword
      loading={loading}
      error={error}
      success={response && response.success}
      onSubmit={(v) => submit(v)}
      onOkClick={onComplete}
    />
  )
}
