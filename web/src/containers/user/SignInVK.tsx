import React, { useEffect } from 'react'
import { withRouter } from 'react-router'

import { Paths } from 'routing'
import { usePost, getQueryParam, useAuth } from 'helpers'
import { useApi } from 'helpers/api'

export const SignInVK = withRouter(({ history }: any) => {
  const { signInVkUrl } = useApi()
  const code = getQueryParam('code')
  const referralId = getQueryParam('referral')
  const [submit, { response }] = usePost(signInVkUrl())
  const { setAuthenticated } = useAuth()

  useEffect(() => {
    if (code) {
      submit({ code, redirectUri: Paths.GetAbsoluteSignInUrl(), referralId })
    }
    // eslint-disable-next-line
  }, [code])

  useEffect(() => {
    if (response && response.success === true) {
      history.push(Paths.Home)
      setAuthenticated(response.token)
    }
  }, [response, history, setAuthenticated])

  if (typeof window === 'undefined') {
    return null
  }

  if (code) {
    return <img src="/images/vk_share_32.png" alt="VK" />
  }

  const params = [
    ['client_id', '7160257'],
    ['display', 'page'],
    ['redirect_uri', Paths.GetAbsoluteSignInUrl()],
    ['scope', 'email'],
    ['response_type', 'code'],
    ['v', '5.102']
  ]
  const paramsStr = params.map(([k, v]) => `${k}=${v}`).join('&')
  return (
    <a href={`https://oauth.vk.com/authorize?${paramsStr}`}>
      <img src="/images/vk_share_32.png" alt="VK" />
    </a>
  )
})
