import React, { useEffect } from 'react'
import { withRouter, RouteComponentProps } from 'react-router'

import { Paths } from 'routing'
import { SignInForm } from 'composite'
import { useAuth, usePost, mapErrorMessage } from 'helpers'
import { SignInVK, SignInFB } from 'containers'
import { useApi } from 'helpers/api'

interface Props extends RouteComponentProps {
  onSignedIn?: (token: string) => void
}

export const SignInContainer = withRouter(({ history, onSignedIn }: Props) => {
  const { signInUrl } = useApi()
  const { setAuthenticated } = useAuth()
  const [submit, { loading, error, response }] = usePost(signInUrl())
  const errorMessage = mapErrorMessage(error, response)

  useEffect(() => {
    if (response && response.success === true) {
      history.push(Paths.Home)
      setAuthenticated(response.token)
      onSignedIn && onSignedIn(response.token)
    }
  }, [response, history, setAuthenticated])

  return (
    <SignInForm
      loading={loading}
      error={errorMessage}
      onSubmit={(values) => (!loading ? submit(values) : undefined)}
      social={
        <>
          <SignInVK />
          <SignInFB />
        </>
      }
    />
  )
})
