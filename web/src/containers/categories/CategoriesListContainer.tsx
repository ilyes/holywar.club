import React, { useEffect, useState } from 'react'

import { CategoriesList } from 'composite'
import { usePost, useGet, useCurrentUser } from 'helpers'
import { CategoryModel } from 'models'
import { H2 } from 'components'
import { useApi } from 'helpers/api'

export const CategoriesListContainer = () => {
  const { saveUserCategoriesUrl, categoriesUrl, discussionsUrl } = useApi()
  const [submit, { response: saveResponse }] = usePost(saveUserCategoriesUrl())
  const { response } = useGet(categoriesUrl())
  const { reload } = useGet(discussionsUrl())
  const [userCategoriesIds, setUserCategoriesIds] = useState<string[]>([])
  const [user] = useCurrentUser()
  const categories: CategoryModel[] = response

  useEffect(() => {
    if (saveResponse && saveResponse.success) {
      if (user) {
        user.categoryIds = userCategoriesIds
      }
      reload()
    }
  }, [saveResponse])

  useEffect(() => {
    if (userCategoriesIds) {
      return
    }
    if (user && categories) {
      if (
        user.categoryIds &&
        user.categoryIds.length &&
        user.categoryIds.length !== categories.length
      ) {
        reload()
      }
      const userCategoriesIds =
        user.categoryIds && user.categoryIds.length
          ? user.categoryIds
          : categories.map((c) => c.id)
      setUserCategoriesIds(userCategoriesIds)
    }
  }, [user, categories, userCategoriesIds])

  if (!response || !userCategoriesIds) {
    return null
  }

  function handleCategoryIdsClick(categoryId: string) {
    const categoryIds = userCategoriesIds.some((i: string) => i === categoryId)
      ? userCategoriesIds.filter((i: string) => i !== categoryId)
      : userCategoriesIds.concat([categoryId])
    setUserCategoriesIds(categoryIds)
    submit({ categoryIds })
  }

  return (
    <>
      <H2>Выбрать интересующие категории</H2>
      <CategoriesList
        categories={categories}
        userCategoriesIds={userCategoriesIds}
        onClick={handleCategoryIdsClick}
      />
    </>
  )
}
