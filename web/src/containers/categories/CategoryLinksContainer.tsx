import React from 'react'
import { useGet } from 'helpers'
import { CategoryLinks } from 'composite'
import { useModal } from 'components/Modal'
import { CategoriesListContainer } from './CategoriesListContainer'
import { useApi } from 'helpers/api'

export const CategoryLinksContainer = () => {
  const { categoriesUrl } = useApi()
  const { response } = useGet(categoriesUrl())
  const { showModal } = useModal()
  if (!response) {
    return null
  }
  return (
    <CategoryLinks
      categories={response}
      onSettingsClick={() => showModal(<CategoriesListContainer />)}
    />
  )
}
