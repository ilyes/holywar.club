import React from 'react'

import { Paths } from 'routing'

interface ShareWatsapProps {
  url: string
}

export const ShareWatsap = ({ url }: ShareWatsapProps) => {
  return (
    <a
      href={`whatsapp://send?text=${encodeURI(url)}`}
      data-action="share/whatsapp/share"
      target="_blank"
      rel="noopener noreferrer"
    >
      <img src="/images/watsap_share_32.png" alt="whatsap" />
    </a>
  )
}
