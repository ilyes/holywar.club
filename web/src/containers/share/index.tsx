import React from 'react'

import { ShareFB } from './ShareFB'
import { ShareTG } from './ShareTG'
import { ShareVK } from './ShareVK'
import { ShareWatsap } from './ShareWatsap'

interface ShareProps {
  url: string
}

export const Share = ({ url }: ShareProps) => {
  return (
    <>
      <ShareVK url={url} />
      <ShareWatsap url={url} />
      <ShareTG url={url} />
      <ShareFB url={url} />
    </>
  )
}
