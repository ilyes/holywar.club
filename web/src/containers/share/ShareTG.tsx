import React from 'react'

import { Paths } from 'routing'

interface ShareTGProps {
  url: string
}

export const ShareTG = ({ url }: ShareTGProps) => {
  return (
    <a
      href={`https://telegram.me/share/url?url=${encodeURI(url)}`}
      target="_blank"
      rel="noopener noreferrer"
    >
      <img src="/images/tg_share_32.png" alt="tg" />
    </a>
  )
}
