import React from 'react'
import { Paths } from 'routing'

interface ShareFBProps {
  url: string
}

export const ShareFB = ({ url }: ShareFBProps) => {
  return (
    <a
      href={`https://www.facebook.com/dialog/feed?app_id=2966497203364770&display=page&redirect_uri=${encodeURI(
        url
      )}&link=${encodeURI(url)}`}
      target="_blank"
      rel="noopener noreferrer"
    >
      <img src="/images/fb_share_32.png" alt="fb" />
    </a>
  )
}
