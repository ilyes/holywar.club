import React from 'react'

interface ShareVKProps {
  url: string
}

export const ShareVK = ({ url }: ShareVKProps) => {
  return (
    <a
      href={`https://vk.com/share.php?url=${encodeURI(url)}`}
      target="_blank"
      rel="noopener noreferrer"
    >
      <img src="/images/vk_share_32.png" alt="vk" />
    </a>
  )
}
