import React, { useState, useEffect } from 'react'

import { AddBalanceForm } from 'composite'
import { UserModel } from 'models'
import { usePost } from 'helpers'
import { useAdminApi } from 'helpers/admin/api'

interface AddBalanceContainerProps {
  weekUsersId: string
  users: UserModel[]
}

const AddBalanceContainer = ({
  weekUsersId,
  users
}: AddBalanceContainerProps) => {
  const { saveBalancesUrl } = useAdminApi()
  const [usersWithBanace, setUsersWithBanace] = useState<UserModel[]>([])
  const [total, setTotal] = useState(0)
  const [balanceSum, setBalanceSum] = useState(0)
  const [submit, { loading, response, error }] = usePost(saveBalancesUrl())

  useEffect(() => {
    users.forEach((user) => {
      updateReferalPayment(user, users)
    })

    updatePaymentLimitWeekReduce(users)
    setUsersWithBanace(users)
    setTotal(calculateBalanceSum(users))
    setBalanceSum(calculateBalanceSum(users))
  }, [])

  function handlePaidChange(paid: number, userId: string) {
    const user = usersWithBanace.find((u) => u.id === userId)
    if (!user) {
      return
    }
    user.balance = user.balance - user.paid + paid
    user.paid = paid
    updateReferalPayment(user, usersWithBanace)
    setUsersWithBanace([...usersWithBanace])
    const sum = calculateBalanceSum(usersWithBanace)
    setTotal(sum)
    setBalanceSum(sum)
  }

  function handleTotalChange(value: number) {
    const totalWeights = usersWithBanace.reduce<number>(
      (s, user) => s + getWeekUserWeight(user),
      0
    )
    usersWithBanace.forEach((user) => {
      const percentage = getWeekUserWeight(user) / totalWeights
      const paid = roundPaidSum(value * percentage) || 0
      user.balance = user.balance - user.paid + paid
      user.paid = paid
      updateReferalPayment(user, usersWithBanace)
    })
    updatePaymentLimitWeekReduce(usersWithBanace)
    setUsersWithBanace([...usersWithBanace])
    setTotal(value || 0)
    setBalanceSum(calculateBalanceSum(usersWithBanace))
  }

  return (
    <AddBalanceForm
      users={users}
      total={total}
      balanceSum={balanceSum}
      saving={loading}
      saveError={error}
      success={response && response.success}
      onPaidChange={handlePaidChange}
      onTotalChange={handleTotalChange}
      onSubmit={() =>
        submit({
          weekUsersId,
          users: usersWithBanace.map(
            ({
              id: userId,
              paid: addMoney,
              referralPayments,
              paymentLimitWeekReduce
            }) => ({
              userId,
              addMoney,
              referralPayments,
              paymentLimitWeekReduce
            })
          )
        })
      }
    />
  )
}

export default AddBalanceContainer

function updateReferalPayment(user: UserModel, users: UserModel[]) {
  if (user.referralId) {
    const referralPayments = calculateReferralPayment(user.paid)
    const referral = users.find((u) => u.id === user.referralId)
    if (referral) {
      referral.referralPayments = referralPayments >= 1 ? referralPayments : 0
    }
  }
}

function updatePaymentLimitWeekReduce(users: UserModel[]) {
  users.forEach((u: UserModel) => {
    u.paymentLimitWeekReduce = calculatePaymentLimitWeekReduce(u.id, users)
  })
}

function calculatePaymentLimitWeekReduce(userId: string, users: UserModel[]) {
  const referralsPaid = users
    .filter((u) => u.referralId === userId)
    .map((r) => r.paid)
  return referralsPaid.reduce<number>(
    (referalPaid: number, result: number): number => {
      return result + referalPaid
    },
    0
  )
}

function calculateBalanceSum(users: UserModel[]) {
  return users.reduce<number>((s, b) => s + b.paid + b.referralPayments, 0)
}

function getWeekUserWeight(user: UserModel): number {
  return user.weekRating
}

function roundPaidSum(val: number): number {
  if (val < 0.1) return 0
  if (val < 1) return 1
  return Math.floor(val)
}

function calculateReferralPayment(paid: number) {
  return Math.floor(paid / 20)
}
