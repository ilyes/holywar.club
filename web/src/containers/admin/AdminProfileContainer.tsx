import React from 'react'
import { Button } from 'components/Button/Button'
import { usePost } from 'helpers'
import { Panel } from 'layouts'
import { Link } from 'react-router-dom'
import { useAdminApi } from 'helpers/admin/api'
import { AdminPaths } from 'routing/admin/AdminPaths'

const AdminProfileContainer = ({ userId }: any) => {
  const { blockUserUrl } = useAdminApi()
  const [blockUser, { loading, response }] = usePost(blockUserUrl())
  return (
    <Panel horizontal>
      <Button
        variant="danger"
        loading={loading}
        disabled={response && response.success}
        onClick={() => blockUser({ userId })}
      >
        {response && response.success ? 'Заблокирован' : 'Заблокировать'}
      </Button>
      <Link to={AdminPaths.UserLikes(userId)}>Кого лайкал</Link>
    </Panel>
  )
}

export default AdminProfileContainer
