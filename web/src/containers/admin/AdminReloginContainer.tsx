import React, { useEffect } from 'react'
import { withRouter, RouteComponentProps } from 'react-router'

import { Button } from 'components/Button/Button'
import { SignInContainer } from '../user/SignInContainer'
import { useAuth, useAdmin } from 'helpers'
import { Paths } from 'routing'
import { useModal } from 'components/Modal'

const AdminReloginContainer = ({ history }: RouteComponentProps) => {
  const { showModal } = useModal()
  const reloginUsers = useReloginUsers()
  const { setAuthenticated } = useAuth()
  return (
    <>
      <Button
        variant="link"
        onClick={() =>
          showModal(<SignInContainer onSignedIn={() => reloadPage(history)} />)
        }
      >
        Add relogin
      </Button>
      {reloginUsers.sort().map((u) => (
        <Button
          key={u}
          variant="link"
          onClick={() => {
            setAuthenticated(u)
            reloadPage(history)
          }}
        >
          {u}
        </Button>
      ))}
    </>
  )
}

export default withRouter(AdminReloginContainer)

function reloadPage(history: any) {
  const { pathname } = history.location
  history.push(Paths.SignIn)
  setTimeout(() => history.push(pathname))
}

const useReloginUsers = (): string[] => {
  const { reloginTokens, addRelogin } = useAdmin()
  const { token } = useAuth()
  useEffect(() => {
    const reloginUser = reloginTokens.find((t) => t === token)
    if (!reloginUser) {
      addRelogin(token)
    }
  }, [token])

  return reloginTokens
}
