import React, { Suspense, useEffect, useState } from 'react'
import { useCurrentUser, useAdmin } from 'helpers'
import { UserRole } from 'models'
import { Spinner } from 'components/Spinner/Spinner'

const AdminReloginContainer = React.lazy(
  () => import('./AdminReloginContainer')
)

const AdminFooterContainer = () => {
  return (
    <>
      <AdminRelogin />
    </>
  )
}

const AdminRelogin = () => {
  const [user] = useCurrentUser()
  const { isAdmin, setAdmin } = useAdmin()

  useEffect(() => {
    if (user?.role === UserRole.admin) {
      setAdmin(true)
    }
  }, [user])

  if (!isAdmin) {
    return null
  }

  return (
    <Suspense fallback={<Spinner />}>
      <AdminReloginContainer />
    </Suspense>
  )
}

export default AdminFooterContainer
