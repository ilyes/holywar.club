import React from 'react'
import { Link } from 'react-router-dom'

import { Panel } from 'layouts'
import { Button } from 'components/Button/Button'
import AdminConfigContainer from './AdminConfigContainer'
import { useModal } from 'components/Modal'
import { AdminPaths } from 'routing/admin/AdminPaths'

const AdminPanelContainer = () => {
  const { showModal } = useModal()
  return (
    <Panel horizontal justifyContent="center" wrap>
      <Button
        onClick={() => showModal(<AdminConfigContainer />)}
        variant="link"
      >
        Настройки
      </Button>
      <Link to={AdminPaths.Comments}>Комменты</Link>
      <Link to={AdminPaths.Holywars}>Холивары</Link>
      <Link to={AdminPaths.Users}>Пользователи</Link>
    </Panel>
  )
}

export default AdminPanelContainer
