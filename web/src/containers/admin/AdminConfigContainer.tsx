import React from 'react'
import Form from 'react-bootstrap/Form'
import { Form as Final, Field } from 'react-final-form'

import { mapFormErrors, useGet, usePost } from 'helpers'
import { AdProviders } from 'models'
import { H1 } from 'components'
import { FormControlAdapter } from 'components/FormControlAdapter/FormControlAdapter'
import { ArrayControlAdapter } from 'components/FormControlAdapter/ArrayControlAdapter'
import { ArrayInArrayControlAdapter } from 'components/FormControlAdapter/ArrayInArrayControlAdapter'
import { Button } from 'components/Button/Button'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { FormSelectAdapter } from 'components/Select/FormSelectAdapter'
import { useAdminApi } from 'helpers/admin/api'

const AdminConfigContainer = () => {
  const { getAdminConfigUrl, saveAdminConfigUrl } = useAdminApi()
  const configResult = useGet(getAdminConfigUrl())
  const [submit, { loading, error }] = usePost(saveAdminConfigUrl())
  return (
    <>
      <H1>Настройки</H1>
      {configResult.response ? (
        <Final
          onSubmit={submit}
          initialValues={configResult.response}
          render={({ handleSubmit, errors, touched }) => (
            <Form onSubmit={handleSubmit}>
              <Field
                name="frontendUrl"
                size="lg"
                placeholder="FrontendUrl"
                component={FormControlAdapter}
              />
              <Field
                name="emailConfirmedPathTemplate"
                size="lg"
                placeholder="EmailConfirmedPathTemplate"
                component={FormControlAdapter}
              />
              <Field
                name="emailConfirmPathTemplate"
                size="lg"
                placeholder="EmailConfirmPathTemplate"
                component={FormControlAdapter}
              />
              <Field
                name="webPath"
                size="lg"
                placeholder="WebPath"
                component={FormControlAdapter}
              />
              <Field
                name="avatarsFolder"
                size="lg"
                placeholder="AvatarsFolder"
                component={FormControlAdapter}
              />
              <Field
                name="staticUrl"
                size="lg"
                placeholder="StaticUrl"
                component={FormControlAdapter}
              />
              <Field
                name="staticPath"
                size="lg"
                placeholder="StaticPath"
                component={FormControlAdapter}
              />
              <Field
                name="setPasswordPathTemplate"
                size="lg"
                placeholder="SetPasswordPathTemplate"
                component={FormControlAdapter}
              />
              <Field
                name="discussionPathTemplate"
                size="lg"
                placeholder="DiscussionPathTemplate"
                component={FormControlAdapter}
              />
              <Field
                name="commentReplyPathTemplate"
                size="lg"
                placeholder="CommentReplyPathTemplate"
                component={FormControlAdapter}
              />
              <Field
                name="featureDiscussionIds"
                size="lg"
                placeholder="FeatureDiscussionIds"
                component={ArrayControlAdapter}
              />
              <Field
                name="howToDiscussionId"
                size="lg"
                placeholder="HowToDiscussionId"
                component={FormControlAdapter}
              />
              <Field
                name="holywarCandidatesStep"
                size="lg"
                placeholder="HolywarCandidatesStep"
                component={FormControlAdapter}
              />
              <Field
                name="minPaymentLimit"
                size="lg"
                placeholder="MinPaymentLimit"
                component={FormControlAdapter}
              />
              <Field
                name="startPaymentLimit"
                size="lg"
                placeholder="StartPaymentLimit"
                component={FormControlAdapter}
              />
              <Field
                name="levelLimitReduceBy"
                size="lg"
                placeholder="LevelLimitReduceBy"
                component={FormControlAdapter}
              />
              <Field
                name="levelSteps"
                size="lg"
                placeholder="LevelSteps"
                component={ArrayControlAdapter}
              />
              <Field
                name="adProvider"
                size="lg"
                placeholder="AdProvider"
                component={FormSelectAdapter}
                items={[AdProviders.google, AdProviders.yandex].map(
                  (value) => ({
                    value,
                    label: value
                  })
                )}
              />
              <Field
                name="adminUserId"
                size="lg"
                placeholder="AdminUserId"
                component={FormControlAdapter}
              />
              <Field
                name="availableServieIps"
                size="lg"
                placeholder="AvailableServieIps"
                component={ArrayInArrayControlAdapter}
                valueParser={ipPartParser}
              />
              <Field
                name="weekRating"
                size="lg"
                placeholder="WeekRating"
                component={FormControlAdapter}
              />
              <Field
                name="weekJackpot"
                size="lg"
                placeholder="WeekJackpot"
                component={FormControlAdapter}
              />
              <Button
                variant="primary"
                size="lg"
                type="submit"
                loading={loading}
              >
                Сохранить
              </Button>
              <ErrorMessage errors={mapFormErrors(errors, error, touched)} />
            </Form>
          )}
        />
      ) : null}
    </>
  )
}

export default AdminConfigContainer

function ipPartParser(s: string) {
  const result = parseInt(s)
  return !result && result !== 0 ? '' : result
}
