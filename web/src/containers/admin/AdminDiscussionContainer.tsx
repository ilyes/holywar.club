import React from 'react'
import { Button } from 'components/Button/Button'
import { usePost } from 'helpers'
import { AdminDiscussionPanel } from 'layouts'
import { useApi } from 'helpers/api'
import { useAdminApi } from 'helpers/admin/api'

const AdminDiscussionContainer = ({ discussionId }: any) => {
  const { deleteDiscussionUrl } = useApi()
  const {
    unwantedDiscussionUrl,
    setAdDiscussionUrl,
    holywarApproveUrl,
    holywarDeclineUrl,
    setTopDiscussionUrl,
    setNotTopDiscussionUrl,
    setModeratedUrl,
    setNotModeratedUrl
  } = useAdminApi()

  const [deleteDiscussion, deleteResult] = usePost(deleteDiscussionUrl())
  const [unwantDiscussion, unwantResult] = usePost(unwantedDiscussionUrl())
  const [setAdDiscussion, adResult] = usePost(setAdDiscussionUrl())
  const [approveHolywar, approveResult] = usePost(holywarApproveUrl())
  const [declineHolywar, declineResult] = usePost(holywarDeclineUrl())
  const [setTop, topResult] = usePost(setTopDiscussionUrl())
  const [unsetTop, untopResult] = usePost(setNotTopDiscussionUrl())
  const [setModerated, moderatedResult] = usePost(setModeratedUrl())
  const [setNotModerated, notModeratedResult] = usePost(setNotModeratedUrl())

  return (
    <>
      <AdminDiscussionPanel>
        <Button
          variant="warning"
          loading={adResult.loading}
          disabled={adResult.response && adResult.response.success}
          onClick={() => setAdDiscussion({ discussionId })}
        >
          {adResult.response && adResult.response.success ? 'Все' : 'Реклама'}
        </Button>
        <Button
          variant="dark"
          loading={unwantResult.loading}
          disabled={unwantResult.response && unwantResult.response.success}
          onClick={() => unwantDiscussion({ discussionId })}
        >
          {unwantResult.response && unwantResult.response.success
            ? 'Скрыто'
            : 'Скрыть'}
        </Button>
        <Button
          variant="danger"
          loading={deleteResult.loading}
          disabled={deleteResult.response && deleteResult.response.success}
          onClick={() => deleteDiscussion({ discussionId })}
        >
          {deleteResult.response && deleteResult.response.success
            ? 'Удалена'
            : 'Удалить'}
        </Button>
        <Button
          variant="info"
          loading={approveResult.loading}
          disabled={approveResult.response && approveResult.response.success}
          onClick={() => approveHolywar({ discussionId })}
        >
          {approveResult.response && approveResult.response.success
            ? 'Готово'
            : 'Холивар'}
        </Button>
        <Button
          variant="danger"
          loading={declineResult.loading}
          disabled={declineResult.response && declineResult.response.success}
          onClick={() => declineHolywar({ discussionId })}
        >
          {declineResult.response && declineResult.response.success
            ? 'Ваше отстой'
            : 'Не холивар'}
        </Button>
        <Button
          variant="warning"
          loading={topResult.loading}
          disabled={topResult.response && topResult.response.success}
          onClick={() => setTop({ discussionId })}
        >
          {topResult.response && topResult.response.success
            ? 'Готово'
            : 'Топчик'}
        </Button>
        <Button
          variant="dark"
          loading={untopResult.loading}
          disabled={untopResult.response && untopResult.response.success}
          onClick={() => unsetTop({ discussionId })}
        >
          {untopResult.response && untopResult.response.success
            ? 'В жопе'
            : 'Убрать из топа'}
        </Button>
        <Button
          variant="success"
          loading={moderatedResult.loading}
          disabled={
            moderatedResult.response && moderatedResult.response.success
          }
          onClick={() => setModerated({ discussionId })}
        >
          {moderatedResult.response && moderatedResult.response.success
            ? 'Одобрямснут'
            : 'Одобрямс'}
        </Button>
        <Button
          variant="secondary"
          loading={notModeratedResult.loading}
          disabled={
            notModeratedResult.response && notModeratedResult.response.success
          }
          onClick={() => setNotModerated({ discussionId })}
        >
          {notModeratedResult.response && notModeratedResult.response.success
            ? 'Неодобрямснут'
            : 'Неодобрямс'}
        </Button>
      </AdminDiscussionPanel>
    </>
  )
}

export default AdminDiscussionContainer
