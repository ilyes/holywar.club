import React, { useState, useEffect, SyntheticEvent, ReactNode } from 'react'

import { MoreButtonLayout } from 'layouts'
import { useGet, useCacheItem } from 'helpers'
import { DiscussionCardList } from 'composite'
import { DiscussionCardBigContainer } from 'containers'
import { DiscussionModel } from 'models'
import { Spinner } from 'components/Spinner/Spinner'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { Button } from 'components/Button/Button'
import { useCallback } from 'react'
import { DiscussionBigSkeleton } from 'composite/discussions/DiscussionBigSkeleton/DiscussionBigSkeleton'

const RESPONSE_LENGTH = 10

interface CachedDiscussions {
  discussions: DiscussionModel[]
  id: string
}

interface Props {
  fromId?: string
  apiUrlCreator: (id: string) => string
  pathUrlCreator: (id: string) => string
  cacheName: string
}

export const InfiniteDiscussionsListContainer = ({
  fromId,
  apiUrlCreator,
  pathUrlCreator,
  cacheName
}: Props) => {
  const { value, save } = useCacheItem<CachedDiscussions>(cacheName)
  const { discussions = [], id } = value || {
    discussions: [],
    id: (fromId = '')
  }
  const [canLoadMore, setCanLoadMore] = useState(true)
  const url = apiUrlCreator(id)
  const { finished, response, error } = useGet(url)
  const discussionsToRender = discussions.length ? discussions : response
  const nextId =
    discussionsToRender && discussionsToRender.length
      ? discussionsToRender[discussionsToRender.length - 1].id
      : fromId

  useEffect(() => {
    if (
      response &&
      response.every((d: any) => discussions.every((dd: any) => dd.id !== d.id))
    ) {
      save({ discussions: discussions.concat(response), id })
      setCanLoadMore(response.length >= RESPONSE_LENGTH)
    } // eslint-disable-next-line
  }, [response])

  const handleLoadPrevClick = useCallback(
    (e: SyntheticEvent) => {
      e.preventDefault()
      save({ discussions, id: nextId })
      return false
    },
    [discussions, nextId]
  )

  return (
    <>
      <DiscussionCardList
        discussions={discussionsToRender}
        component={DiscussionCardBigContainer}
        adulterate={adulterate}
      />
      {!finished ? <DiscussionBigSkeleton /> : null}
      {error ? <ErrorMessage error={error} /> : null}
      {finished && canLoadMore ? (
        <MoreButtonLayout>
          <a href={pathUrlCreator(nextId)} onClick={handleLoadPrevClick}>
            <Button variant="secondary" wide>
              Показать еще
            </Button>
          </a>
        </MoreButtonLayout>
      ) : null}
    </>
  )
}

function adulterate(list: ReactNode[]) {
  // list.splice(1, 0, <AdSquare key="ad1" />)
  // if (list.length > 6) {
  //   list.splice(6, 0, <AdSquare key="ad2" second />)
  // }
  // if (list.length > 13) {
  //   list.splice(13, 0, <AdSquare key="ad3" third />)
  // }
  // if (list.length > 20) {
  //   list.splice(20, 0, <AdSquare key="ad4" fourth />)
  // }
}
