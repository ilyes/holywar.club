import React, { useState, useEffect } from 'react'

import { SurveyVoteForm, SurveyResults } from 'composite'

import { SurveyModel, SurveyVotesModel, SurveyItemModel } from 'models'
import { usePost, useCurrentUser } from 'helpers'
import { useApi } from 'helpers/api'

interface Props {
  survey: SurveyModel
  surveyVotes: SurveyVotesModel[]
}

export const SurveyContainer = ({ survey, surveyVotes }: Props) => {
  const { surveyVoteUrl } = useApi()
  const [votes, setVotes] = useState(surveyVotes)
  const [submitVote, voteResult] = usePost(surveyVoteUrl())
  const totalVotes = surveyVotes.reduce((s, c) => s + c.votes, 0)
  const [isEdit, setIsEdit] = useState(false)
  const [currentUser] = useCurrentUser()

  const canVote = Boolean(currentUser)

  useEffect(() => {
    if (!voteResult.response || !voteResult.response.success) {
      return
    }
    setIsEdit(false)
    setVotes(voteResult.response.surveyVotes)
  }, [voteResult.response])

  if (survey == null) {
    return null
  }

  if (isEdit || totalVotes === 0) {
    return (
      <SurveyVoteForm
        survey={survey}
        onVoteClick={(surveyItemId) =>
          submitVote({ surveyId: survey.id, surveyItemId })
        }
        isVoteSubmitting={voteResult.loading}
        onCancelClick={() => setIsEdit(false)}
      />
    )
  }

  return (
    <SurveyResults
      survey={survey}
      votes={votes}
      canVote={canVote}
      onVoteClick={() => setIsEdit(true)}
    />
  )
}
