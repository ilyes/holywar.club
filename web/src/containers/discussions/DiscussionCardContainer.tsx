import React from 'react'

import { DiscussionCard } from 'composite'
import { DiscussionModel, CategoryModel } from 'models'
import { useGet } from 'helpers'
import { useApi } from 'helpers/api'

interface DiscussionCardContainerProps {
  discussion: DiscussionModel
}

export const DiscussionCardContainer = ({
  discussion
}: DiscussionCardContainerProps) => {
  const { categoriesUrl } = useApi()
  const { response } = useGet(categoriesUrl())
  const category = response
    ? response.find((c: CategoryModel) => c.id === discussion.categoryId)
    : undefined
  return (
    <DiscussionCard
      discussion={discussion}
      categoryName={category ? category.name : undefined}
    />
  )
}
