import React from 'react'

import { DiscussionCardBig } from 'composite'
import { DiscussionModel, CategoryModel } from 'models'
import { useGet } from 'helpers'
import { useApi } from 'helpers/api'

interface DiscussionCardBigContainerProps {
  discussion: DiscussionModel
}

export const DiscussionCardBigContainer = ({
  discussion
}: DiscussionCardBigContainerProps) => {
  const { categoriesUrl } = useApi()
  const { response } = useGet(categoriesUrl())
  const category = response
    ? response.find((c: CategoryModel) => c.id === discussion.categoryId)
    : undefined
  return (
    <DiscussionCardBig
      discussion={discussion}
      categoryName={category ? category.name : undefined}
    />
  )
}
