import React from 'react'

import { useGet } from 'helpers'
import { EternalDiscussionsList } from 'composite'
import { useApi } from 'helpers/api'

export const EternalDiscussionsContainer = () => {
  const { eternalDiscussionsUrl } = useApi()
  const { response } = useGet(eternalDiscussionsUrl())
  return <EternalDiscussionsList discussions={response} />
}
