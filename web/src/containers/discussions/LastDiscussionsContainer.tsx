import React from 'react'

import { useGet } from 'helpers'
import { DiscussionCardList } from 'composite'
import { DiscussionCardBigContainer } from 'containers'
import { DiscussionModel } from 'models'
import { useApi } from 'helpers/api'

export const LastDiscussionsContainer = () => {
  const { discussionsUrl } = useApi()
  const { loading, response: discussions, error } = useGet(discussionsUrl())
  return (
    <>
      <DiscussionCardList
        discussions={discussions}
        loading={loading}
        error={error}
        component={DiscussionCardBigContainer}
        adulterate={adulterate}
      />
      {/* {discussions && discussions.length ? (
        <MoreButtonLayout>
          <Link
            to={Paths.GetDiscussions(discussions[discussions.length - 1].id)}
          >
            <Button variant="secondary" wide>
              Показать еще
            </Button>
          </Link>
        </MoreButtonLayout>
      ) : null} */}
    </>
  )
}

function adulterate(list: any[], featured?: DiscussionModel[]) {
  // list.splice(0, 0, <EternalDiscussionsContainer key="eternal" />)
  // list.splice(4, 0, <AdSquare key="ad1" />)
  // if (list.length > 6) {
  //   list.splice(6, 0, <AdSquare key="ad2" second />)
  // }
  // if (list.length > 13) {
  //   list.splice(13, 0, <AdSquare key="ad3" third />)
  // }
  //list.length = 24
}
