import React, { useEffect, useState, memo } from 'react'
import { UnreadMessages } from 'composite'
import { useGet, usePost } from 'helpers'
import { useApi } from 'helpers/api'

export const UnreadMessagesContainer = memo(() => {
  const { unreadMessagesUrl, setMessagesReadUrl } = useApi()
  const { response, reload } = useGet(unreadMessagesUrl())
  const [setRead, { response: setReadRespone }] = usePost(setMessagesReadUrl())
  const [messages, setMessages] = useState([])
  const [expanded, setExpanded] = useState(false)

  useEffect(() => {
    if (setReadRespone && setReadRespone.success) {
      reload()
    }
  }, [setReadRespone])

  useEffect(() => {
    if (response) {
      setMessages(response)
    }
  }, [response])

  function onExpanded() {
    setExpanded(true)
    setRead()
  }

  function onCollapsed() {
    setExpanded(false)
  }

  return (
    <UnreadMessages
      messages={messages}
      onExpanded={onExpanded}
      onCollapsed={onCollapsed}
      expanded={expanded}
    />
  )
})
