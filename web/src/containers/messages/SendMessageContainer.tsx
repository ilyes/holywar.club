import React, { useEffect } from 'react'

import { usePost } from 'helpers'
import { SendMessageForm } from 'composite'
import { UserModel } from 'models'
import { useApi } from 'helpers/api'

interface SendMessageContainerProps {
  user: UserModel
  onComplete: () => void
}

export const SendMessageContainer = ({
  user,
  onComplete
}: SendMessageContainerProps) => {
  const { sendMessageUrl } = useApi()
  const [submitMessage, { response, error, loading }] = usePost(
    sendMessageUrl()
  )

  useEffect(() => {
    if (response && response.success) {
      onComplete()
    }
  }, [response, onComplete])

  return (
    <SendMessageForm
      toUserName={user.name}
      error={error}
      loading={loading}
      onSubmit={(text: string) => submitMessage({ text, toUserId: user.id })}
    />
  )
}
