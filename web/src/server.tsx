import express, { Request, Response } from 'express'
import React from 'react'
import { renderToString } from 'react-dom/server'
import { StaticRouter, matchPath } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import axios from 'axios'
import dotenv from 'dotenv'
import MobileDetect from 'mobile-detect'

import { App } from './App'
import { getPreloadUrls } from './preload'
import { reactAndMinifyCss } from './helpers/server/css'
import { EnvObject } from 'helpers'
import { ConfigModel, AdProviders, DiscussionModel } from 'models'
import { Paths } from 'routing'
import { getApiUrls } from 'helpers/api'

const dotenvConfig = dotenv.config()
const clientEnv: EnvObject = dotenvConfig.parsed as any
const { configUrl, discussionUrl } = getApiUrls(clientEnv.API_URL)

const inline = {
  css: {
    bootstrap: reactAndMinifyCss('./public/styles/bootstrap.css'),
    fonts: reactAndMinifyCss('./public/styles/fonts.css'),
    icons: reactAndMinifyCss('./public/styles/icons.css'),
    index: reactAndMinifyCss('./public/styles/index.css')
  }
}
let assets: any

type PreloadData = [string, any | null]

const syncLoadAssets = () => {
  assets = require(process.env.RAZZLE_ASSETS_MANIFEST!)
}
syncLoadAssets()

const server = express()
  .disable('x-powered-by')
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR!))
  .use(async (req, res) => {
    const preloadUrls = getPreloadUrls(clientEnv.API_URL, req.url)
    const preloadState = await preload(preloadUrls)
    const [_, config] = await loadUrl(configUrl())
    await checkExternalView(req, preloadState)
    console.log('[REQUEST]', req.path)
    renderResponseHtml(req, res, config as ConfigModel, preloadState)
  })

async function preload(urls: string[]): Promise<PreloadData[]> {
  if (!urls || !urls.length) {
    return []
  }
  return await Promise.all(urls.map((url: string) => loadUrl(url)))
}

async function loadUrl(url: string): Promise<PreloadData> {
  try {
    const { data: response } = await axios.get(url)
    return [url, response]
  } catch (error) {
    console.error('[ERROR]', error)
    return [url, null]
  }
}

async function checkExternalView(req: Request, prealodState: PreloadData[]) {
  try {
    // сайт с которого был произведен переход на страницу
    const referer = req.header('Referer')
    // пользователь который разместил ссылку на станицу на стороннем сайте
    const referralUserId = req.query.referral
    if (!referer) {
      return
    }
    const refererUrl = new URL(referer)
    if (refererUrl.host === clientEnv.FRONTEND_HOST) {
      return
    }
    const match = matchPath<any>(req.url, {
      path: Paths.Discussion,
      exact: true
    })
    if (!match) {
      return
    }
    const { id } = match.params
    const prealodUrl = discussionUrl(id, true)
    const prealodData = prealodState.find((s) => s[0] === prealodUrl)
    if (!prealodData) {
      return
    }
    if (!prealodData[1]) {
      return
    }
    const discussion = prealodData[1].discussion as DiscussionModel
    await axios.post(`${clientEnv.API_URL}/discussion/externalView`, {
      discussionId: discussion.id,
      referralUserId
    })
  } catch (error) {
    console.log('[ERROR]', error)
  }
}

function renderResponseHtml(
  req: Request,
  res: Response,
  config: ConfigModel,
  preloadState?: [string, object | null][]
) {
  const context = {}
  const md = new MobileDetect(req.headers['user-agent'] || '')
  const env = {
    ...clientEnv,
    IS_PHONE: Boolean(md.phone()),
    IS_TABLET: Boolean(md.tablet())
  }
  const markup = renderToString(
    <StaticRouter context={context} location={req.url}>
      <Helmet defaultTitle="Holywar.club - ставим точки в холиварах. Сайт холиваров, холивар форум." />
      <App
        preloadState={preloadState}
        env={{ ...env, IS_NODE: true }}
        config={config}
      />
    </StaticRouter>
  )
  const helmet = Helmet.renderStatic()
  res.send(
    `<!doctype html>
  <html ${helmet.htmlAttributes.toString()}>
  <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta charSet='utf-8' />
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="shortcut icon" href="/images/favicon32.png" />
      ${helmet.title.toString()}
      ${helmet.meta.toString()}
      ${helmet.link.toString()}
      <link
        rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous"
      />
      <style>${inline.css.bootstrap}</style>
      <style>${inline.css.fonts}</style>
      <style>${inline.css.icons}</style>
      <style>${inline.css.index}</style>
      ${
        config.adProvider === AdProviders.google
          ? '<script data-ad-client="ca-pub-7274312014687219" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>'
          : ''
      }
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            appId: '2966497203364770',
            autoLogAppEvents: true,
            xfbml: true,
            version: 'v4.0'
          })
        }
      </script>
      <script
        async
        defer
        src="https://connect.facebook.net/en_US/sdk.js"
      ></script>
      <script>
        window.__APP_INITIAL_DATA = ${JSON.stringify(preloadState)};
        window.__ENV = ${JSON.stringify(env)}
        window.__CONFIG = ${JSON.stringify(config)}
      </script>
      <script type="text/javascript" src="/scripts/log.js?v2" crossorigin=”anonymous”></script>
      <script type="text/javascript" src="/scripts/referral.js"></script>
      ${
        assets.client.css
          ? `<link rel="stylesheet" href="${assets.client.css}">`
          : ''
      }
      ${
        process.env.NODE_ENV === 'production'
          ? `<script src="${assets.client.js}" defer></script>`
          : `<script src="${assets.client.js}" defer crossorigin></script>`
      }
  </head>
  <body>
      <div id="root">${markup}</div>
  </body>
</html>`
  )
}

export default server
