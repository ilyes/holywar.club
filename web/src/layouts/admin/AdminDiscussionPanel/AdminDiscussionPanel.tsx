import React from 'react'

import { Panel } from '../../common/Panel/Panel'

import styles from './AdminDiscussionPanel.module.css'

export function AdminDiscussionPanel({ children }: any) {
  return <Panel className={styles.wrap}>{children}</Panel>
}
