import React from 'react'

interface PlaceholderProps {
  width?: number
  height?: number
}

export const Placeholder = ({ width, height }: PlaceholderProps) => {
  return (
    <div
      style={{
        width: width ? width + 'px' : '100%',
        height: height ? height + 'px' : '30px'
      }}
    ></div>
  )
}
