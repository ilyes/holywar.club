import React from 'react'

import styles from './HorizontalLayout.module.css'

interface HorizontalLayoutProps {
  children: any
}

export const HorizontalLayout = ({ children }: HorizontalLayoutProps) => {
  return <div className={styles.wrap}>{children}</div>
}
