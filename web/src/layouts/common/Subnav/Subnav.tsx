import React from 'react'

import styles from './Subnav.module.css'

export const Subnav = ({ children }: any) => {
  return <div className={styles.layout}>{children}</div>
}
