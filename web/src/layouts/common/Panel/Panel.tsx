import React from 'react'
import classnames from 'classnames'

import styles from './Panel.module.css'

interface PanelProps {
  children: any
  className?: string
  horizontal?: boolean
  justifyContent?: 'start' | 'end' | 'center' | 'space-between'
  alignItems?: 'start' | 'end' | 'center' | 'space-between'
  wrap?: boolean
  bg?: boolean
  padding?: '0' | 's' | 'm' | 'l'
  gap?: '0' | 's' | 'm' | 'l'
}

export const Panel = ({
  children,
  className,
  horizontal,
  wrap,
  bg,
  justifyContent = 'start',
  alignItems = 'start',
  padding = 'm',
  gap = 'm'
}: PanelProps) => {
  return (
    <div
      className={classnames(
        styles.root,
        styles[`padding_${padding}`],
        styles[`gap_${gap}`],
        styles[`justifyContent_${justifyContent}`],
        styles[`alignItems_${alignItems}`],
        {
          [styles.horizontal]: horizontal,
          [styles.wrap]: wrap,
          [styles.bg]: bg
        },
        className
      )}
    >
      {children}
    </div>
  )
}
