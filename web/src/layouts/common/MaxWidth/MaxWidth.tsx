import React, { useState, ReactNode } from 'react'
import classnames from 'classnames'

import { Button } from 'components/Button/Button'

import styles from './MaxWidth.module.css'

interface MaxWidthProps {
  children: ReactNode
  className?: string
  instead?: ReactNode
  width?: '600' | '1024'
  showHide?: ReactNode[]
  showHideBig?: boolean
  showHideClass?: string
  showHideUnder?: boolean
}

export const MaxWidth = ({
  className,
  children,
  instead,
  width,
  showHide,
  showHideBig,
  showHideClass,
  showHideUnder
}: MaxWidthProps) => {
  const [visible, setVisible] = useState(false)
  const wrapClassName = width === '600' ? styles.wrap600 : styles.wrap1024
  const insteadClassName =
    width === '600' ? styles.instead600 : styles.instead1024
  const showHideWrapClass =
    width === '600' ? styles.showHideWrap600 : styles.showHideWrap1024
  const showHideButton = (
    <ShowHide
      showHide={showHide}
      showHideBig={showHideBig}
      showHideClass={showHideClass}
      showHideWrapClass={showHideWrapClass}
      visible={visible}
      onShowClick={() => setVisible(true)}
      onHideClick={() => setVisible(false)}
    />
  )
  return (
    <>
      {showHideUnder ? showHideButton : null}
      <div
        className={classnames(!visible ? wrapClassName : undefined, className)}
      >
        {children}
      </div>
      {instead ? <div className={insteadClassName}>{instead}</div> : null}
      {!showHideUnder ? showHideButton : null}
    </>
  )
}

const ShowHide = ({
  showHide,
  showHideBig,
  showHideClass,
  showHideWrapClass,
  visible,
  onShowClick,
  onHideClick
}: any) => {
  if (!showHide || showHide.length !== 2) {
    return null
  }
  const [showTitle = 'Показать', hideTitle = 'Скрыть'] = showHide
  return (
    <div className={showHideWrapClass}>
      {visible ? (
        <Button
          variant="link"
          className={classnames(showHideClass, {
            [styles.showHideBig]: showHideBig
          })}
          onClick={onHideClick}
        >
          {hideTitle}
        </Button>
      ) : (
        <Button
          variant="link"
          className={classnames(showHideClass, {
            [styles.showHideBig]: showHideBig
          })}
          onClick={onShowClick}
        >
          {showTitle}
        </Button>
      )}
    </div>
  )
}
