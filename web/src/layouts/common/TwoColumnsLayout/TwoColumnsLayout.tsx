import React from 'react'

import styles from './TwoColumnsLayout.module.css'

interface FeatureLayoutProps {
  left: any
  right: any
}

export const TwoColumnsLayout = ({ left, right }: FeatureLayoutProps) => (
  <div className={styles.wrap}>
    <div className={styles.left}>{left}</div>
    <div className={styles.right}>{right}</div>
  </div>
)
