import React from 'react'
import classnames from 'classnames'

import styles from './OneColumnLayout.module.css'

interface OneColumnLayoutProps {
  children: any
  small?: boolean
  wide?: boolean
}

export const OneColumnLayout = ({
  children,
  small,
  wide
}: OneColumnLayoutProps) => (
  <div
    className={classnames(styles.wrap, {
      [styles.small]: small,
      [styles.wide]: wide
    })}
  >
    {children}
  </div>
)
