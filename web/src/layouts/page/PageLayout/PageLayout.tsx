import React from 'react'

import { Footer } from 'composite'

export const PageLayout = ({ children }: any) => {
  return (
    <>
      {children}
      <Footer />
    </>
  )
}
