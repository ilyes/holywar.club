import React from 'react'

import { PageLayout } from '../PageLayout/PageLayout'
import { HeaderContainer } from 'containers'

import styles from './ContentPageLayout.module.css'

export const ContentPageLayout = ({ children }: any) => {
  return (
    <PageLayout>
      <HeaderContainer />
      <main className={styles.contentPage}>{children}</main>
    </PageLayout>
  )
}
