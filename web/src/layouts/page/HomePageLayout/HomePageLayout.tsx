import React, { ReactNode } from 'react'

import { PageLayout } from '../PageLayout/PageLayout'

import styles from './HomePageLayout.module.css'
import { HeaderContainer, CategoryLinksContainer } from 'containers'
import { H1 } from 'components'
import { TopUsers } from 'containers/user/TopUsers'
import { CommentsOfTheWeek } from 'containers/comment/CommentsOfTheWeek'

interface HomePageLayoutProps {
  children: any
  feature?: ReactNode
  title?: string
}

export const HomePageLayout = ({
  children,
  title,
  feature
}: HomePageLayoutProps) => {
  return (
    <PageLayout>
      <HeaderContainer />
      {feature}
      {title && <H1 className={styles.title}>{title}</H1>}
      <main className={styles.homePage}>
        <div className={styles.left}>
          <CategoryLinksContainer />
        </div>
        <div>{children}</div>
        <div className={styles.right}>
          <TopUsers />
          <CommentsOfTheWeek />
        </div>
      </main>
    </PageLayout>
  )
}
