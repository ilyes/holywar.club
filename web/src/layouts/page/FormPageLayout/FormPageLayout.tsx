import React from 'react'
import classnames from 'classnames'

import { Footer } from 'composite'
import { HeaderContainer } from 'containers'

import styles from './FormPageLayout.module.css'

interface FormPageLayoutProps {
  children: any
  wide?: boolean
  strait?: boolean
  noHeader?: boolean
}

export const FormPageLayout = ({
  children,
  wide,
  strait,
  noHeader
}: FormPageLayoutProps) => {
  return (
    <>
      {!noHeader ? <HeaderContainer /> : null}
      <main
        className={classnames(styles.formPage, {
          [styles.formPageWide]: wide,
          [styles.formPageStrait]: strait
        })}
      >
        {children}
      </main>
      <Footer />
    </>
  )
}
