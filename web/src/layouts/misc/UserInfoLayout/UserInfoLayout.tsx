import React from 'react'

import styles from './UserInfoLayout.module.css'
import { H1 } from 'components'

interface UserInfoLayoutProps {
  avatar: any
  name: any
  email?: any
  after?: any
}

export const UserInfoLayout = ({
  avatar,
  name,
  email,
  after
}: UserInfoLayoutProps) => {
  return (
    <div className={styles.wrap}>
      <h3>{name}</h3>
      {avatar}
      {email ? <div className={styles.email}>{email}</div> : null}
      {after}
    </div>
  )
}
