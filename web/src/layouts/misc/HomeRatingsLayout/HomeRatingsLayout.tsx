import React from 'react'

import styles from './HomeRatingsLayout.module.css'

export const HomeRatingsLayout = ({ children }: any) => {
  return <div className={styles.layout}>{children}</div>
}
