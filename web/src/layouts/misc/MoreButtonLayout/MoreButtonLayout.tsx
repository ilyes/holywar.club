import React from 'react'

import styles from './MoreButtonLayout.module.css'

interface MoreButtonLayoutProps {
  children: any
}

export const MoreButtonLayout = ({ children }: MoreButtonLayoutProps) => {
  return <div className={styles.wrap}>{children}</div>
}
