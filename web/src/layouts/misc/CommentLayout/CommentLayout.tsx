import React from 'react'
import classnames from 'classnames'

import styles from './CommentLayout.module.css'

interface CommentLayoutProps {
  children: any
  setRef: any
  highlight?: boolean
}

export const CommentLayout = ({
  highlight,
  setRef,
  children
}: CommentLayoutProps) => {
  return (
    <div
      ref={setRef}
      className={classnames(styles.root, { [styles.highlight]: highlight })}
    >
      {children}
    </div>
  )
}
