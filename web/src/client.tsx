import React from 'react'
import { hydrate } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Helmet } from 'react-helmet'

import { App } from './App'
import { GreetingContainer } from 'containers'

const w: any = window
hydrate(
  <BrowserRouter>
    <Helmet defaultTitle="Holywar.club - ставим точки в холиварах. Сайт холиваров, холивар форум." />
    <App
      preloadState={w.__APP_INITIAL_DATA}
      env={w.__ENV}
      config={w.__CONFIG}
      greeting={<GreetingContainer />}
    />
  </BrowserRouter>,
  document.getElementById('root')
)

if (module.hot) {
  module.hot.accept()
}
