import React from 'react'

import { PartnerInfoModel, UserModel } from 'models'

import styles from './PartnerInfo.module.css'
import { IconWithText } from 'components/IconWithText/IconWithText'
import { IconName } from 'components/Icon/Icon'
import { LabeledInfo } from 'components/LabeledInfo/LabeledInfo'
import { Button } from 'components/Button/Button'
import ProgressBar from 'react-bootstrap/ProgressBar'

interface Props {
  info: PartnerInfoModel
  user: UserModel
  weekRating: number
  onPaymentRequestClick: () => void
}

export const PartnerInfo = ({
  info,
  user,
  weekRating,
  onPaymentRequestClick
}: Props) => {
  return (
    <>
      <p className={styles.description}>
        Чтобы принять участие в партнерской программе нужно набрать за неделю{' '}
        {weekRating} очков рейтинга. Рейтинг начисляется за внешние переходы на
        страницы с вашими холиварами, за уникальных комментаторов в ваших
        холиварах, за рефералов, за лайки и дислайки ваших холиваров и комментов
        и за общее количество просмотров.
      </p>
      <div className={styles.accent}>
        <div className={styles.accentItem}>
          <p className={styles.accentLabel}>Баланс</p>
          <IconWithText
            iconName={IconName.currency}
            text={user.balance}
            className={styles.money}
          />
        </div>
        <div className={styles.accentItem}>
          <p className={styles.accentLabel}>От реффералов</p>
          <IconWithText
            iconName={IconName.currency}
            text={user.referralPayments}
            className={styles.money}
          />
        </div>
      </div>
      <div className={styles.balanceWrap}>
        <p className={styles.balanceDescription}>
          Зачисление вознаграждения производится в конце каждой недели, в 22:00
          по московскому времени, тем кто набрал необходимое количество очков
          рейтинга
        </p>
        <Button
          variant="outline-primary"
          className={styles.getMoney}
          onClick={onPaymentRequestClick}
        >
          Выплаты
        </Button>
      </div>
      <div className={styles.rating}>
        <p>Набрано очков рейтинга за неделю</p>
        <ProgressBar
          now={info.weekUser.weekRating}
          max={weekRating}
          label={`${info.weekUser.weekRating}/${weekRating}`}
        />
      </div>
      <div className={styles.accent}>
        <LabeledInfo
          label="Внешних переходов"
          classNames={{
            root: styles.accentItem,
            children: styles.accentItemChildren
          }}
        >
          {info.weekUser.discussionExternalViewsCount}
        </LabeledInfo>
        <LabeledInfo
          label="Уникальных комментаторов"
          classNames={{
            root: styles.accentItem,
            children: styles.accentItemChildren
          }}
        >
          {info.weekUser.uniqueCommentatorsCount}
        </LabeledInfo>
        <LabeledInfo
          label="Рефералов"
          classNames={{
            root: styles.accentItem,
            children: styles.accentItemChildren
          }}
        >
          {info.weekUser.referralsCount}
        </LabeledInfo>
      </div>
      <div className={styles.other}>
        <LabeledInfo
          label="Лайков"
          classNames={{
            root: styles.otherItem,
            children: styles.otherItemChildren
          }}
        >
          {info.weekUser.likesCount}
        </LabeledInfo>
        <LabeledInfo
          label="Дизлайков"
          classNames={{
            root: styles.otherItem,
            children: styles.otherItemChildren
          }}
        >
          {info.weekUser.dislikesCount}
        </LabeledInfo>
        <LabeledInfo
          label="Комментов"
          classNames={{
            root: styles.otherItem,
            children: styles.otherItemChildren
          }}
        >
          {info.weekUser.commentsCount}
        </LabeledInfo>
        <LabeledInfo
          label="Обсуждений"
          classNames={{
            root: styles.otherItem,
            children: styles.otherItemChildren
          }}
        >
          {info.weekUser.discussionsCount}
        </LabeledInfo>
        <LabeledInfo
          label="Просмотров"
          classNames={{
            root: styles.otherItem,
            children: styles.otherItemChildren
          }}
        >
          {info.weekUser.discussionViewsCount}
        </LabeledInfo>
      </div>
    </>
  )
}
