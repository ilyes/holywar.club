import React from 'react'
import { Link } from 'react-router-dom'
import classnames from 'classnames'

import { Paths } from 'routing'
import { UserModel } from 'models'

import styles from './UserCard.module.css'
import { Avatar } from 'components/Avatar/Avatar'
import { LikesDislikesCount } from 'components/LikesDislikesCount/LikesDislikesCount'
import { IconWithText } from 'components/IconWithText/IconWithText'
import { IconName } from 'components/Icon/Icon'

interface UserCardProps {
  user: UserModel
  small?: boolean
}

export const UserCard = ({
  user: {
    id,
    name,
    avatarUrl,
    commentsCount,
    discussionsCount,
    likesCount,
    dislikesCount,
    balance,
    referralsCount
  },
  small
}: UserCardProps) => {
  return (
    <Link to={Paths.GetProfile(id)} className={styles.layout}>
      <Avatar avatarUrl={avatarUrl} size={small ? 's' : 'm'} />
      <div className={styles.info}>
        <div className={classnames(styles.header, { [styles.small]: small })}>
          {name}
        </div>
        <div className={styles.stats}>
          <LikesDislikesCount
            likesCount={likesCount}
            dislikesCount={dislikesCount}
          />
          <IconWithText
            iconName={IconName.comment}
            text={(commentsCount + discussionsCount).toString()}
          />
        </div>
      </div>
    </Link>
  )
}
