import React from 'react'
import { Link } from 'react-router-dom'

import { UserModel } from 'models'
import { UserInfoLayout } from 'layouts'
import { Paths } from 'routing'

import styles from './CurrentUserInfo.module.css'
import { LikesDislikesCount } from 'components/LikesDislikesCount/LikesDislikesCount'
import { IconWithText } from 'components/IconWithText/IconWithText'
import { IconName } from 'components/Icon/Icon'
import { Button } from 'components/Button/Button'

interface CurrentUserInfoProps {
  user: UserModel
  Avatar: any
  onSignOutClick: () => void
  onChangePasswordClick: () => void
  onPaymentRequestClick: () => void
}

export const CurrentUserInfo = ({
  user,
  Avatar,
  onSignOutClick,
  onChangePasswordClick
}: CurrentUserInfoProps) => {
  return (
    <UserInfoLayout
      avatar={Avatar}
      name={user.name}
      email={<div className={styles.email}>{user.email}</div>}
      after={
        <>
          <div className={styles.icons}>
            <LikesDislikesCount
              likesCount={user.likesCount}
              dislikesCount={user.dislikesCount}
            />
            <IconWithText
              iconName={IconName.profile}
              text={user.referralsCount.toString()}
            />
            <IconWithText
              iconName={IconName.comment}
              text={user.commentsCount.toString()}
            />
          </div>
          <div className={styles.level}>Ваш уровень: {user.level + 1}</div>
          <div className={styles.levelProgress}>
            [
            {user.levelProgress && user.levelProgress.next > 0
              ? `До следующего уровня: ${user.levelProgress.current}/${user.levelProgress.next}`
              : 'Максимальный уровень'}
            ]
          </div>
          <Link to={Paths.Partner} className={styles.partnerProgram}>
            Партнерская программа
          </Link>
          <Link to={Paths.LinkCreate}>Поделиться ссылкой</Link>
          <Link to={Paths.ArticleCreate}>Написать статью</Link>
          <Button
            variant="link"
            className={styles.changePassword}
            onClick={onChangePasswordClick}
          >
            Изменить пароль
          </Button>
          <Button
            variant="link"
            className={styles.signOut}
            onClick={onSignOutClick}
          >
            Выйти
          </Button>
        </>
      }
    />
  )
}
