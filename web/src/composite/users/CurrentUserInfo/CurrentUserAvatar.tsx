import { Avatar } from 'components/Avatar/Avatar'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { FilePicker } from 'components/FilePicker/FilePicker'
import { Spinner } from 'components/Spinner/Spinner'
import React from 'react'

import styles from './CurrentUserAvatar.module.css'

interface CurrentUserAvatarProps {
  avatarUrl: string
  pending: boolean
  error: string
  onAvatarPicked: (file: any) => void
}

export const CurrentUserAvatar = ({
  avatarUrl,
  pending,
  error,
  onAvatarPicked
}: CurrentUserAvatarProps) => {
  return (
    <div className={styles.wrap}>
      <FilePicker
        accept="image/jpeg,image/png"
        onChange={({ target }) => onAvatarPicked(target.files[0])}
      >
        {!pending ? (
          <Avatar size="l" avatarUrl={avatarUrl} className={styles.avatar} />
        ) : (
          <Spinner />
        )}
      </FilePicker>
      <ErrorMessage error={error} />
    </div>
  )
}
