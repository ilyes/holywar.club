import React, { ReactNode } from 'react'

import { UserModel } from 'models'

import styles from './UserInfo.module.css'
import { H1 } from 'components'
import { Avatar } from 'components/Avatar/Avatar'
import { LabeledInfo } from 'components/LabeledInfo/LabeledInfo'
import { LikesDislikesCount } from 'components/LikesDislikesCount/LikesDislikesCount'

interface UserInfoProps {
  user: UserModel
  links: ReactNode
  avatar: ReactNode
}

export const UserInfo = ({ user, links, avatar }: UserInfoProps) => {
  return (
    <div className={styles.wrap}>
      <div className={styles.avatarAndInfo}>
        <div className={styles.avatar}>
          {avatar || <Avatar size="l" avatarUrl={user.avatarUrl} />}
        </div>
        <div className={styles.info}>
          <H1>{user.name}</H1>
          <div className={styles.icons}>
            <LabeledInfo label="Комменты">
              {user.commentsCount + user.discussionsCount}
            </LabeledInfo>
            <LabeledInfo label="Рейтинг">
              <LikesDislikesCount
                withoutIcon
                likesCount={user.likesCount}
                dislikesCount={user.dislikesCount}
              />
            </LabeledInfo>
          </div>
        </div>
      </div>
      <div className={styles.links}>{links}</div>
    </div>
  )
}
