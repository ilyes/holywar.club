import React from 'react'

import { UserCard } from '../UserCard/UserCard'
import { UserModel } from 'models'

import styles from './UserCardList.module.css'

interface UserCardProps {
  users: UserModel[]
}

export const UserCardList = ({ users }: UserCardProps) => {
  if (!users) {
    return null
  }
  return (
    <div className={styles.userCardList}>
      {users.map(c => (
        <UserCard user={c} key={c.id} />
      ))}
    </div>
  )
}
