import React, { useState, useEffect } from 'react'
import Clipboard from 'react-clipboard.js'
import classnames from 'classnames'

import { Paths } from 'routing'
import { UserModel } from 'models'
import { UserCardList } from '../UserCardList/UserCardList'

import styles from './ReferedUsers.module.css'
import { Input } from 'components/Input/Input'
import { Spinner } from 'components/Spinner/Spinner'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'

interface ReferedUsersProps {
  userId: string
  referralPayments: number
  loading: boolean
  referrals: UserModel[]
  error: string
  shareButtons?: any
}

export const ReferedUsers = ({
  userId,
  loading,
  referrals,
  error,
  shareButtons
}: ReferedUsersProps) => {
  const [url, setUtl] = useState('')
  const [isCopied, setIsCopied] = useState(false)

  useEffect(() => {
    if (userId) {
      setUtl(Paths.GetAbsoluteReferralUrl(userId))
    }
  }, [userId])

  return (
    <div className={classnames(styles.wrap)}>
      <h2>Пригласи друга</h2>
      <p className={styles.shareDescription}>
        Опубликуйте ссылку в соцсетях или скопируйте ссылку и поделитесь с
        друзьями
      </p>
      <div className={styles.shareButtons}>{shareButtons}</div>
      <Input value={url} disabled={!url} className={styles.textBox} />
      <Clipboard
        data-clipboard-text={url}
        className="btn btn-primary"
        style={{ visibility: url ? 'visible' : 'hidden' }}
        onSuccess={() => setIsCopied(true)}
      >
        Скопировать ссылку
      </Clipboard>
      <div
        className={styles.linkCopied}
        style={{ visibility: isCopied ? 'visible' : 'hidden' }}
      >
        Ссылка скопирована
      </div>
      {loading ? <Spinner /> : null}
      {error ? <ErrorMessage error={error} /> : null}
      {referrals && referrals.length === 0 ? (
        <h3>Пока приглашенных нет</h3>
      ) : null}
      {referrals && referrals.length > 0 ? (
        <>
          <h2>Приглашенные</h2>
          <UserCardList users={referrals} />
        </>
      ) : null}
    </div>
  )
}
