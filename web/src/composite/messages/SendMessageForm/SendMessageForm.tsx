import React from 'react'

import { CreateMessageForm } from 'composite'

import styles from './SendMessageForm.module.css'

interface SendMessageFormProps {
  toUserName: string
  error: string
  loading: boolean
  onSubmit: (text: string) => void
}

export const SendMessageForm = ({
  toUserName,
  error,
  loading,
  onSubmit
}: SendMessageFormProps) => {
  return (
    <div>
      <h2 className={styles.title}>Написать сообщение</h2>
      <div className={styles.toUser}>
        <span>Пользователю </span>
        <span className={styles.toUserName}>{toUserName}</span>
      </div>
      <CreateMessageForm
        onSubmit={({ text }) => onSubmit(text)}
        error={error}
        loading={loading}
        noLink
      />
    </div>
  )
}
