import React from 'react'
import { Link } from 'react-router-dom'
import Linkify from 'react-linkify'

import { Icon, IconName } from 'components/Icon/Icon'
import { Paths } from 'routing'
import { MessageWithUserModel, MessageType, ConfigModel } from 'models'

import styles from './UnreadMessages.module.css'
import { tryPanseJson, useConfig } from 'helpers'
import { Dropdown } from 'components/Dropdown/Dropdown'
import { Button } from 'components/Button/Button'

interface UnreadMessagesProps {
  messages: MessageWithUserModel[]
  onExpanded: () => void
  onCollapsed: () => void
  expanded: boolean
}

export const UnreadMessages = ({
  messages,
  onExpanded,
  onCollapsed,
  expanded
}: UnreadMessagesProps) => {
  const unreadCount = messages.filter((m) => !m.message.isRead).length
  const config = useConfig()

  function handleExpanded() {
    if (unreadCount) {
      onExpanded()
    }
  }

  return (
    <Dropdown
      title={
        <Button variant="light" className={styles.button}>
          <Icon
            name={IconName.notifications}
            className={unreadCount ? styles.hasUnread : undefined}
          />
          {unreadCount ? <sup>{unreadCount}</sup> : null}
          <span className={styles.label}>Сообщения</span>
        </Button>
      }
      withCloseButton
      className={styles.wrap}
      childrenClassName={styles.children}
      onExpanded={handleExpanded}
      onCollapsed={onCollapsed}
      expanded={expanded}
    >
      {(handleRequestCollapsing: () => void) => (
        <>
          <div className={styles.title}>
            Сообщения{' '}
            <Icon name={IconName.close} onClick={handleRequestCollapsing} />
          </div>
          {messages?.length ? (
            messages.map((m) => (
              <div key={m.message.id} className={styles.unreadMessage}>
                {renderMessage(m, config)}
              </div>
            ))
          ) : (
            <p>Нет новых сообщений</p>
          )}
        </>
      )}
    </Dropdown>
  )
}

function renderMessage(m: MessageWithUserModel, config: ConfigModel) {
  if (m.message.type === MessageType.Reply) {
    return renderReply(m)
  }
  if (m.message.type === MessageType.AddMoney) {
    return renderAddMoney(m)
  }
  if (m.message.type === MessageType.HowTo) {
    return renderHowTo(m, config)
  }
  return renderType(m.message.type) || renderCommon(m)
}

interface IAddMoneyData {
  addMoney: number
  referralPayments: number
}

const typeNames = {
  [MessageType.PaymentCreated]: 'Получена заявка на выплату',
  [MessageType.PaymentApproved]: 'Выплата одобрена',
  [MessageType.PaymentDeclined]: 'Выплата отклонена'
}

const classNames = {
  [MessageType.PaymentCreated]: styles.yellow,
  [MessageType.PaymentApproved]: styles.green,
  [MessageType.PaymentDeclined]: styles.red
}

function renderType(type: MessageType) {
  const text = typeNames[type]
  if (!text) {
    return undefined
  }
  const className = classNames[type]
  return <span className={className}>{text}</span>
}

function renderHowTo(m: MessageWithUserModel, config: ConfigModel) {
  if (!config.howToDiscussionId) {
    return null
  }
  return (
    <div>
      Узнайте как{' '}
      <Link to={Paths.GetDiscussion(config.howToDiscussionId)}>
        выйти на стабильный доход
      </Link>
    </div>
  )
}

function renderAddMoney(m: MessageWithUserModel) {
  const addData = tryPanseJson<IAddMoneyData>(m.message.content)
  if (!addData) {
    return <div>Пополнение баланса</div>
  }
  const ref =
    addData.referralPayments > 0 ? (
      <span>
        По реферальной системе:
        <span className={styles.addMoney}>
          {' '}
          +{addData.referralPayments}
          <Icon name={IconName.currency} />.{' '}
        </span>
      </span>
    ) : null
  return (
    <div>
      Пополнение баланса
      <span className={styles.addMoney}>
        {' '}
        +{addData.addMoney}
        <Icon name={IconName.currency} />.{' '}
      </span>
      {ref}
    </div>
  )
}

function renderCommon(m: MessageWithUserModel) {
  return (
    <>
      <span>
        Новое сообщение
        {!m.fromUser ? null : (
          <>
            <span> от </span>
            <Link to={Paths.GetProfile(m.fromUser.id)}>{m.fromUser.name}</Link>
          </>
        )}
      </span>
      <div>
        <Linkify
          componentDecorator={(decoratedHref, decoratedText, key) => (
            <a target="blank" href={decoratedHref} key={key}>
              {decoratedText}
            </a>
          )}
        >
          {m.message.content || `Message:${m.message.type}`}
        </Linkify>
      </div>
    </>
  )
}

function renderReply(m: MessageWithUserModel) {
  return (
    <span>
      {!m.fromUser ? null : (
        <>
          <Link to={Paths.GetProfile(m.fromUser.id)}>{m.fromUser.name}</Link>
          <span> ответил на ваше сообщение </span>
          <Link to={m.message.content}>сообщение</Link>
        </>
      )}
    </span>
  )
}
