import React from 'react'
import Form from 'react-bootstrap/Form'
import { Form as Final, Field } from 'react-final-form'

import { validatePassword, required, mapFormErrors } from 'helpers'
import { FormControlAdapter } from 'components/FormControlAdapter/FormControlAdapter'
import { Button } from 'components/Button/Button'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'

interface ChangePasswordValues {
  oldPassword: string
  newPassword: string
}

interface ChangePasswordProps {
  onSubmit: (values: ChangePasswordValues) => void
  onOkClick: Function
  loading: boolean
  success: boolean
  error: string
}

export const ChangePassword = ({
  loading,
  success,
  error,
  onOkClick,
  onSubmit
}: ChangePasswordProps) => (
  <Final
    onSubmit={onSubmit}
    validateOnBlur={true}
    render={({ handleSubmit, errors, touched }) => (
      <>
        <h1>Изменить пароль</h1>
        <Form onSubmit={handleSubmit}>
          <Field
            name="oldPassword"
            type="password"
            placeholder="Старый пароль"
            size="lg"
            component={FormControlAdapter}
            validate={required('Нужно указать старый пароль')}
          />
          <Field
            name="newPassword"
            type="password"
            placeholder="Новый пароль"
            size="lg"
            component={FormControlAdapter}
            validate={validatePassword}
          />
          <Field
            name="passwordConfirmation"
            type="password"
            placeholder="Подтвердите пароль"
            size="lg"
            component={FormControlAdapter}
            validate={validatePasswordConfirmation}
          />
          {!success ? (
            <Button variant="primary" size="lg" type="submit" loading={loading}>
              Изменить пароль
            </Button>
          ) : (
            <>
              <p>Пароль успешно изменен</p>
              <Button
                variant="secondary"
                size="lg"
                type="button"
                onClick={onOkClick}
              >
                Ок
              </Button>
            </>
          )}
          <ErrorMessage errors={mapFormErrors(errors, error, touched)} />
        </Form>
      </>
    )}
  />
)

function validatePasswordConfirmation(value: string, allValues: any) {
  if (value !== allValues.newPassword) {
    return 'Подтверждение пароля должно совпадать с паролем'
  }
  return undefined
}
