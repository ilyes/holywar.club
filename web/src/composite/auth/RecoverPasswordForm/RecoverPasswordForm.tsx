import React from 'react'
import Form from 'react-bootstrap/Form'
import { Form as Final, Field } from 'react-final-form'
import { Link } from 'react-router-dom'

import { Paths } from 'routing'
import { mapFormErrors } from 'helpers'

import styles from './RecoverPasswordForm.module.css'
import { Button } from 'components/Button/Button'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { FormControlAdapter } from 'components/FormControlAdapter/FormControlAdapter'

interface RecoverPasswordFormValues {
  email: string
}

interface RecoverPasswordFormProps {
  loading: boolean
  error: string
  success: boolean
  onSubmit: (values: RecoverPasswordFormValues) => void
}

export const RecoverPasswordForm = ({
  loading,
  error,
  success,
  onSubmit
}: RecoverPasswordFormProps) => {
  if (success) {
    return (
      <p>
        Мы отправили письмо со ссылкой на восстановление пароля на ваш email.
        Перейдите по ней и задайте новый пароль.
      </p>
    )
  }
  return (
    <Final
      onSubmit={onSubmit}
      render={({ handleSubmit, errors, touched }) => (
        <Form onSubmit={handleSubmit}>
          <Field
            name="email"
            type="email"
            size="lg"
            placeholder="Email"
            component={FormControlAdapter}
          />
          <Button variant="primary" size="lg" type="submit" loading={loading}>
            Отправить ссылку
          </Button>
          <Link className={styles.signIn} to={Paths.SignIn}>
            Войти
          </Link>
          <ErrorMessage errors={mapFormErrors(errors, error, touched)} />
        </Form>
      )}
    />
  )
}
