import React from 'react'
import Form from 'react-bootstrap/Form'
import { Form as Final, Field } from 'react-final-form'
import { Link } from 'react-router-dom'

import { Paths } from 'routing'
import { mapFormErrors } from 'helpers'

import styles from './SignInForm.module.css'
import { Button } from 'components/Button/Button'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { FormControlAdapter } from 'components/FormControlAdapter/FormControlAdapter'

interface SignInFormValues {
  email: string
  password: string
}

interface SignInFormProps {
  loading: boolean
  error: string
  onSubmit: (values: SignInFormValues) => void
  social: any
}

export const SignInForm = ({
  loading,
  error,
  onSubmit,
  social
}: SignInFormProps) => (
  <Final
    onSubmit={onSubmit}
    render={({ handleSubmit, errors, touched }) => (
      <Form onSubmit={handleSubmit}>
        <Field
          name="email"
          type="email"
          size="lg"
          placeholder="Email"
          component={FormControlAdapter}
        />
        <Field
          name="password"
          type="password"
          size="lg"
          placeholder="Пароль"
          component={FormControlAdapter}
        />
        <div className={styles.forgonAndSocial}>
          <Link to={Paths.PasswordRecover}>Забыли пароль?</Link>
          <div className={styles.social}>{social}</div>
        </div>
        <Button variant="primary" size="lg" type="submit" loading={loading}>
          Войти
        </Button>
        <p className={styles.policyLayout}>
          Регистрируясь вы соглашаетесь с
          <Link className={styles.policy} to={Paths.Policy}>
            политикой обработки персональных данных
          </Link>
        </p>
        <Link className={styles.signUp} to={Paths.SignUp}>
          Зарегистрироваться
        </Link>
        <ErrorMessage errors={mapFormErrors(errors, error, touched)} />
      </Form>
    )}
  />
)
