import React from 'react'
import Form from 'react-bootstrap/Form'
import { Form as Final, Field } from 'react-final-form'
import { Link } from 'react-router-dom'

import { Paths } from 'routing'
import {
  validatePassword,
  validateEmail,
  required,
  mapFormErrors
} from 'helpers'

import styles from './SignUpForm.module.css'
import { FormControlAdapter } from 'components/FormControlAdapter/FormControlAdapter'
import { Button } from 'components/Button/Button'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'

interface SignUpFormValues {
  email: string
  password: string
}

interface SignUpFormProps {
  onSubmit: (values: SignUpFormValues) => void
  loading: boolean
  error: string
}

export const SignUpForm = ({ loading, error, onSubmit }: SignUpFormProps) => (
  <Final
    onSubmit={onSubmit}
    validateOnBlur={true}
    render={({ handleSubmit, errors, touched }) => (
      <Form onSubmit={handleSubmit}>
        <Field
          name="name"
          type="text"
          placeholder="Имя"
          size="lg"
          component={FormControlAdapter}
          validate={required('Без имени никак')}
        />
        <Field
          name="email"
          type="email"
          placeholder="Email"
          size="lg"
          component={FormControlAdapter}
          validate={validateEmail}
        />
        <Field
          name="password"
          type="password"
          placeholder="Пароль"
          size="lg"
          component={FormControlAdapter}
          validate={validatePassword}
        />
        <Field
          name="passwordConfirmation"
          type="password"
          placeholder="Подтвердите пароль"
          size="lg"
          component={FormControlAdapter}
          validate={validatePasswordConfirmation}
        />
        <p>
          Регистрируясь вы соглашаетесь с
          <Link className={styles.policy} to={Paths.Policy}>
            политикой обработки персональных данных
          </Link>
        </p>
        <Button variant="primary" size="lg" type="submit" loading={loading}>
          Зарегистрироваться
        </Button>
        <Link className={styles.signIn} to={Paths.SignIn}>
          Войти
        </Link>
        <ErrorMessage errors={mapFormErrors(errors, error, touched)} />
      </Form>
    )}
  />
)

function validatePasswordConfirmation(value: string, allValues: any) {
  if (value !== allValues.password) {
    return 'Подтверждение пароля должно совпадать с паролем'
  }
  return undefined
}
