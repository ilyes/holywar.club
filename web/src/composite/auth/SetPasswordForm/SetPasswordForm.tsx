import React from 'react'
import Form from 'react-bootstrap/Form'
import { Link } from 'react-router-dom'
import { Form as Final, Field } from 'react-final-form'

import { validatePassword, mapFormErrors } from 'helpers'
import { Paths } from 'routing'
import { FormControlAdapter } from 'components/FormControlAdapter/FormControlAdapter'
import { Button } from 'components/Button/Button'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'

interface ChangePasswordValues {
  password: string
}

interface ChangePasswordProps {
  onSubmit: (values: ChangePasswordValues) => void
  loading: boolean
  success: boolean
  error: string
}

export const SetPasswordForm = ({
  loading,
  success,
  error,
  onSubmit
}: ChangePasswordProps) => (
  <Final
    onSubmit={onSubmit}
    validateOnBlur={true}
    render={({ handleSubmit, errors, touched }) => (
      <Form onSubmit={handleSubmit}>
        <Field
          name="password"
          type="password"
          placeholder="Новый пароль"
          size="lg"
          component={FormControlAdapter}
          validate={validatePassword}
        />
        <Field
          name="passwordConfirmation"
          type="password"
          placeholder="Подтвердите пароль"
          size="lg"
          component={FormControlAdapter}
          validate={validatePasswordConfirmation}
        />
        {!success ? (
          <Button variant="primary" size="lg" type="submit" loading={loading}>
            Изменить пароль
          </Button>
        ) : (
          <>
            <p>Пароль успешно изменен</p>
            <Link to={Paths.SignIn}>Войти</Link>
          </>
        )}
        <ErrorMessage errors={mapFormErrors(errors, error, touched)} />
      </Form>
    )}
  />
)

function validatePasswordConfirmation(value: string, allValues: any) {
  if (value !== allValues.password) {
    return 'Подтверждение пароля должно совпадать с паролем'
  }
  return undefined
}
