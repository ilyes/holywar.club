import React from 'react'
import classnames from 'classnames'

import styles from './HolywarLabel.module.css'

export const HolywarLabel = ({ className }: { className?: string }) => {
  return <label className={classnames(className, styles.wrap)}>Холивар!</label>
}
