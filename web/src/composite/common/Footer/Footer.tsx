import React, { useCallback } from 'react'
import { Link } from 'react-router-dom'

import { Paths } from 'routing'
import { SendMessageContainer } from 'containers'

import styles from './Footer.module.css'
import { useConfig, useGet } from 'helpers'
import { Button } from 'components/Button/Button'
import { useModal } from 'components/Modal'
import { useApi } from 'helpers/api'
import { Panel } from 'layouts/common/Panel/Panel'

export const Footer = () => {
  const { userUrl } = useApi()
  const { showModal, hideModal } = useModal()
  const { adminUserId } = useConfig()
  const profileUrl = userUrl(adminUserId)
  const adminProfileResult = useGet(profileUrl)
  const handleMessageToAdmin = useCallback(
    () =>
      showModal(
        <SendMessageContainer
          user={adminProfileResult.response}
          onComplete={hideModal}
        />
      ),
    [showModal, hideModal, adminProfileResult.response]
  )
  return (
    <footer className={styles.wrap}>
      <Panel horizontal justifyContent="space-between">
        <Link to={Paths.Home} className={styles.companyName}>
          Holywar<sup>club</sup>
        </Link>
        <ul className={styles.links}>
          <li>
            <Link to={Paths.Rules}>Правила клуба холиварщиков</Link>
          </li>
          <li>
            <Link to={Paths.Policy}>
              Политика обработки персональных данных
            </Link>
          </li>
          {adminProfileResult.response ? (
            <li>
              <Button variant="link" onClick={handleMessageToAdmin}>
                Написать администрации
              </Button>
            </li>
          ) : null}
        </ul>
        <ul>
          <li>
            <Link to={Paths.Ratings}>Рейтинг пользователей</Link>
          </li>
          <li>
            <Link to={Paths.RatingsComments}>Топ комментов</Link>
          </li>
        </ul>
        <div className={styles.auth}>
          <Link to={Paths.SignIn} className={styles.signin}>
            Войти
          </Link>
        </div>
      </Panel>
    </footer>
  )
}
