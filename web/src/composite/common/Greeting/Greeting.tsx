import React from 'react'

import { H1, H3 } from 'components'
import { Link } from 'react-router-dom'
import { Paths } from 'routing'

import styles from './Greeting.module.css'
import { Button } from 'components/Button/Button'
import { Panel } from 'layouts/common/Panel/Panel'
import { Words } from 'components/Words/Words'

interface GreetingProps {
  onOkClick: () => void
  howToDiscussionId: string
}

export const Greeting = ({ onOkClick, howToDiscussionId }: GreetingProps) => {
  return (
    <div>
      <H1>Добро пожаловать в Холивар клуб!</H1>
      <ul className={styles.list}>
        <li className={styles.listItem1}>
          Первое правило Холивар клуба: всегда рассказывай о Холивар клубе!
        </li>
        <li className={styles.listItem2}>
          Второе правило Холивар клуба: всегда и всем рассказывай о Холивар
          клубе!
        </li>
        <li className={styles.listItem3}>
          Третье правило Холивар клуба: лучший коммент недели забирает Джекпот!
        </li>
        <li className={styles.listItem4}>
          Четвертое и последнее правило Холивар клуба: новичек должен написать
          мощный коммент и лайкнуть/дизлайкнуть 5 комментов, а неновичек тоже!
        </li>
      </ul>
      <Panel gap="m" alignItems="center">
        <Button variant="primary" onClick={onOkClick}>
          Я хочу вступить в клуб!
        </Button>
        <Button variant="secondary" onClick={onOkClick}>
          Я очень хочу вступить в клуб!
        </Button>
      </Panel>
    </div>
  )
}
