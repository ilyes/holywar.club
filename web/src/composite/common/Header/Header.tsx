import React, { ReactNode } from 'react'
import { Link } from 'react-router-dom'

import { Paths } from 'routing'
import { useAuth } from 'helpers'

import styles from './Header.module.css'
import { IconLink } from 'components/IconLink/IconLink'
import { IconName } from 'components/Icon/Icon'
interface HeaderProps {
  invite?: ReactNode
  messages: ReactNode
  menu: ReactNode
  profile: ReactNode
  categories?: ReactNode
}

export const Header = ({
  invite,
  messages,
  menu,
  profile,
  categories
}: HeaderProps) => {
  const { isAuthenticated } = useAuth()
  return (
    <nav className={styles.header}>
      <div className={styles.top}>
        <div className={styles.left}>
          <Link to={Paths.Home} className={styles.headerCompanyName}>
            <span className={styles.headerCompanyName_firstWord}>Holywar</span>
            <sup className={styles.headerCompanyName_secondWord}>club</sup>
            <span className={styles.slogan}>Тут бывает жарко!</span>
          </Link>
          <div className={styles.menu}>
            <Link to={Paths.Home} className={styles.firstMenuLink}>
              Популярные
            </Link>
            <Link to={Paths.Last}>Новые</Link>
          </div>
          {menu}
        </div>
        <div className={styles.headerLinks}>
          <IconLink
            iconName={IconName.question}
            to={Paths.Faq}
            className={styles.faqIcon}
          >
            Ответы
          </IconLink>
          {isAuthenticated ? (
            <>
              <IconLink
                to={Paths.LinkCreate}
                iconName={IconName.plus}
                childrenWrapName={styles.createIconLabel}
              >
                Холивар
              </IconLink>
              {invite}
              {messages}
            </>
          ) : null}
          {profile}
        </div>
      </div>
      {categories}
    </nav>
  )
}
