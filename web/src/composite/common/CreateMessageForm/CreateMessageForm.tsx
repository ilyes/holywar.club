import React, { useState, useCallback } from 'react'
import classnames from 'classnames'

import { SubmitCommentModel } from 'models'
import { ParseUrlForm } from 'containers'

import styles from './CreateMessageForm.module.css'
import { useModal } from 'components/Modal'
import { FormImage } from 'components/FormImage/FormImage'
import { Button } from 'components/Button/Button'
import { Icon, IconName } from 'components/Icon/Icon'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'

interface CreateMessageFormProps {
  loading?: boolean
  error?: string
  noLink?: boolean
  onSubmit: (comment: SubmitCommentModel) => void
  comment?: SubmitCommentModel
  submitLabel?: string
}

export const CreateMessageForm = ({
  loading,
  error,
  noLink,
  onSubmit,
  comment,
  submitLabel = 'Написать'
}: CreateMessageFormProps) => {
  const [text, setText] = useState(comment?.text || '')
  const [imageUrl, setImage] = useState(comment?.imageUrl || '')
  const { showModal, hideModal } = useModal()
  const handleLinkClick = useCallback(
    () =>
      showModal(
        <ParseUrlForm
          onComplete={({ url, title, imageUrl }) => {
            if (title) setText(title + (url ? '\r\n' + url : ''))
            if (imageUrl) setImage(imageUrl)
            hideModal()
          }}
        />
      ),
    [showModal, hideModal, setText, setImage]
  )
  const handleSubmit = useCallback(
    (e) => {
      e.preventDefault()
      if (!text && !imageUrl) {
        return
      }
      onSubmit({ text, imageUrl })
      setText('')
      setImage('')
    },
    [text, imageUrl, onSubmit, setText, setImage]
  )

  return (
    <form className={classnames(styles.form)} onSubmit={handleSubmit}>
      <textarea
        onChange={({ target }: any) => setText(target.value)}
        value={text}
      />
      {imageUrl ? (
        <FormImage
          imageUrl={imageUrl}
          className={styles.imageWrap}
          imageClassName={styles.image}
          onClear={() => setImage('')}
        />
      ) : null}
      <div className={styles.controls}>
        <Button loading={loading} size="sm" type="submit">
          {submitLabel}
        </Button>
        {noLink ? null : (
          <Icon
            className={styles.urlButton}
            name={IconName.link}
            onClick={handleLinkClick}
          />
        )}
      </div>
      <ErrorMessage error={error} />
    </form>
  )
}
