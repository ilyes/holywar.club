import React, { FunctionComponent } from 'react'
import { Link } from 'react-router-dom'

import { CategoryModel } from 'models'
import { Icon, IconName } from 'components/Icon/Icon'
import { Paths } from 'routing'
import { CategoryLink } from 'composite'

import styles from './MenuDropdown.module.css'
import { Dropdown } from 'components/Dropdown/Dropdown'
import { IconWithText } from 'components/IconWithText/IconWithText'

interface IMenuDropdownProps {
  categories: CategoryModel[]
  onSettingsClick: () => void
}

export const MenuDropdown: FunctionComponent<IMenuDropdownProps> = ({
  categories,
  onSettingsClick
}) => {
  return (
    <Dropdown
      childrenClassName={styles.root}
      title={<Icon className={styles.icon} name={IconName.menu} />}
    >
      {(handleRequestCollapsing: () => void) => (
        <>
          <Icon
            className={styles.close}
            name={IconName.close}
            onClick={handleRequestCollapsing}
          />
          <div className={styles.items}>
            <Link
              className={styles.mainLink}
              to={Paths.Home}
              onClick={handleRequestCollapsing}
            >
              Популярные
            </Link>
            <Link
              className={styles.mainLink}
              to={Paths.Last}
              onClick={handleRequestCollapsing}
            >
              Новые
            </Link>
            {categories.map((c: CategoryModel) => (
              <CategoryLink
                key={c.id}
                className={styles.category}
                to={Paths.GetDiscussionsByCategory(c.id)}
                onClick={handleRequestCollapsing}
              >
                {c.name}
              </CategoryLink>
            ))}
            {/* <IconWithText
          className={styles.settings}
          onClick={onSettingsClick}
          iconName={IconName.settings}
          text="Настроить категории"
        /> */}
          </div>
        </>
      )}
    </Dropdown>
  )
}
