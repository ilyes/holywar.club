import React from 'react'
import Form from 'react-bootstrap/Form'

import { UserModel } from 'models'
import { UserCard } from '../../users/UserCard/UserCard'
import { IconName } from 'components/Icon/Icon'
import { IconWithText } from 'components/IconWithText/IconWithText'

interface AddBalanceToUserProps {
  user: UserModel
  onPaidChange: (paid: number, userId: string) => void
}

export const AddBalanceToUser = ({
  user,
  onPaidChange
}: AddBalanceToUserProps) => {
  return (
    <div>
      <UserCard user={user} />
      <div>Недельный рейтинг:{user.weekRating}</div>
      <Form.Control
        value={user.paid.toString()}
        onChange={({ target }: any) =>
          onPaidChange(parseInt(target.value) || 0, user.id)
        }
      />
      <IconWithText
        label="По рефферальной программе:"
        iconName={IconName.currency}
        text={user.referralPayments ? user.referralPayments.toString() : '0'}
      />
    </div>
  )
}
