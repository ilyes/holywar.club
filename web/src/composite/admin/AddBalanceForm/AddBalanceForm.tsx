import React from 'react'

import { IconName, Icon } from 'components/Icon/Icon'
import { UserModel } from 'models'
import { AddBalanceToUser } from './AddBalanceToUser'

import styles from './AddBalanceForm.module.css'
import { Input } from 'components/Input/Input'
import { Button } from 'components/Button/Button'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'

interface AddBalanceFormProps {
  users: UserModel[]
  total: number
  balanceSum: number
  success: boolean
  saving: boolean
  saveError: string
  onPaidChange: (paid: number, userId: string) => void
  onTotalChange: (value: number) => void
  onSubmit: (users: UserModel[]) => void
}

export const AddBalanceForm = ({
  users,
  total,
  balanceSum,
  success,
  saving,
  saveError,
  onPaidChange,
  onTotalChange,
  onSubmit
}: AddBalanceFormProps) => {
  return (
    <div className={styles.wrap}>
      <div className={styles.sum}>
        <Input
          value={total}
          onChange={({ target }: any) => onTotalChange(parseInt(target.value))}
          placeholder="Начислить"
        />
      </div>
      <div className={styles.sum}>
        <span>Сумма начислений:&nbsp;</span>
        <strong>
          {balanceSum}&nbsp;
          <Icon name={IconName.currency} />
        </strong>
      </div>
      <h3>Пользователям</h3>
      <div className={styles.users}>
        {users.map((u) => (
          <AddBalanceToUser user={u} onPaidChange={onPaidChange} />
        ))}
      </div>
      <Button loading={saving} onClick={onSubmit} size="lg">
        Сохранить
      </Button>
      {success ? <p>Изменения сохранены</p> : null}
      {saveError ? <ErrorMessage error={saveError} /> : null}
    </div>
  )
}
