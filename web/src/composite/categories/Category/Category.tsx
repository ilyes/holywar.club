import React from 'react'
import classnames from 'classnames'

import styles from './Category.module.css'

interface CategoryProps {
  title: string
  className?: string
  inactive?: boolean
  onClick?: () => void
}

export const Category = ({
  title,
  inactive,
  onClick,
  className
}: CategoryProps) => {
  return (
    <div
      onClick={onClick}
      className={classnames(styles.category, 'category', className, {
        [styles.inactive]: inactive
      })}
    >
      {title || ''}
    </div>
  )
}
