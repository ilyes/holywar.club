import React from 'react'

import { CategoryModel } from 'models'
import { Paths } from 'routing'
import { Icon, IconName } from 'components/Icon/Icon'
import { CategoryLink } from '../CategoryLink/CategoryLink'

import styles from './CategoryLinks.module.css'

interface CategoryLinksProps {
  categories: CategoryModel[]
  onSettingsClick: () => void
}

export const CategoryLinks = ({
  categories,
  onSettingsClick
}: CategoryLinksProps) => {
  return (
    <div className={styles.layout}>
      {categories.map((c: CategoryModel) => {
        return (
          <CategoryLink
            className={styles.link}
            key={c.id}
            to={Paths.GetDiscussionsByCategory(c.id)}
          >
            {c.name}
          </CategoryLink>
        )
      })}
      {/* <Icon
        className={styles.settings}
        name={IconName.settings}
        onClick={onSettingsClick}
      /> */}
    </div>
  )
}
