import React from 'react'
import { Link, LinkProps } from 'react-router-dom'
import classnames from 'classnames'

import styles from './CategoryLink.module.css'

export const CategoryLink = ({ className, ...rest }: LinkProps) => {
  return <Link className={classnames(styles.link, className)} {...rest} />
}
