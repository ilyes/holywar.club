import React from 'react'

import { CategoryModel } from 'models'
import { Category } from '../Category/Category'

import styles from './CategoriesList.module.css'

interface CategoriesListProps {
  categories: CategoryModel[]
  userCategoriesIds: string[]
  onClick: (categoryId: string) => void
}

export const CategoriesList = ({
  categories,
  userCategoriesIds,
  onClick
}: CategoriesListProps) => {
  return (
    <div className={styles.layout}>
      {categories.map(c => {
        return (
          <Category
            title={c.name}
            inactive={isInactive(c.id, userCategoriesIds)}
            onClick={() => onClick(c.id)}
            key={c.id}
          />
        )
      })}
    </div>
  )
}

function isInactive(id: string, userCategoriesIds: string[]) {
  if (!userCategoriesIds.length) {
    return false
  }
  return userCategoriesIds.every(i => i !== id)
}
