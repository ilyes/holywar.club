import React from 'react'

import { Icon, IconName } from 'components/Icon/Icon'
import { PaymentStatus } from 'models'

import styles from './Payment.module.css'
import { Select } from 'components/Select/Select'
import { Link } from 'react-router-dom'
import { Paths } from 'routing'

export const Payment = ({ payment, onStatusChange }: any) => {
  return (
    <div className={styles.layout}>
      {payment.user ? (
        <span>
          <Link
            to={Paths.GetProfile(payment.user.id)}
            className={styles.authorName}
          >
            {payment.user.name}
          </Link>
        </span>
      ) : null}
      <span>
        {payment.sum}
        <Icon name={IconName.currency} />
      </span>
      <span>{payment.instructions}</span>
      {typeof onStatusChange !== 'function' ? (
        <span className={classNameMapping[payment.status]}>
          {statusTextMapping[payment.status]}
        </span>
      ) : (
        <Select
          items={allStatuses.map((value) => ({
            value,
            label: statusTextMapping[value]
          }))}
          value={payment.status}
          onChange={({ target }) => onStatusChange(target.value, payment.id)}
        />
      )}
    </div>
  )
}

const classNameMapping: { [key: string]: string } = {
  [PaymentStatus.declined]: styles.declined,
  [PaymentStatus.paid]: styles.paid,
  [PaymentStatus.queued]: styles.pending,
  [PaymentStatus.pending]: styles.pending
}

const statusTextMapping: { [key: string]: string } = {
  [PaymentStatus.declined]: 'Отклонен',
  [PaymentStatus.paid]: 'Оплачен',
  [PaymentStatus.pending]: 'В ожидании',
  [PaymentStatus.queued]: 'В обработке'
}

const allStatuses = [
  PaymentStatus.declined,
  PaymentStatus.paid,
  PaymentStatus.pending,
  PaymentStatus.queued
]
