import React from 'react'

import { Spinner } from 'components/Spinner/Spinner'
import { Payment } from './Payment'

import styles from './PaymentList.module.css'

export const PaymentList = ({
  title,
  payments,
  loading,
  onStatusChange
}: any) => {
  if (loading) {
    return <Spinner />
  }
  if (!payments) {
    return null
  }
  return (
    <div className={styles.layout}>
      <h3>{title || 'История выплат'}</h3>
      {payments.map((p: any) => (
        <Payment key={p.id} payment={p} onStatusChange={onStatusChange} />
      ))}
    </div>
  )
}
