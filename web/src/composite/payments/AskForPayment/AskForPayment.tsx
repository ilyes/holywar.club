import React from 'react'
import Form from 'react-bootstrap/Form'
import { Form as Final, Field } from 'react-final-form'

import { mapFormErrors } from 'helpers'
import { FormControlAdapter } from 'components/FormControlAdapter/FormControlAdapter'
import { Button } from 'components/Button/Button'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'

interface AskForPaymentValues {
  sum: number
  instructions: string
}

interface AskForPaymentProps {
  onSubmit: (values: AskForPaymentValues) => void
  error: string
  loading: boolean
}

export const AskForPayment = ({
  onSubmit,
  error,
  loading
}: AskForPaymentProps) => {
  return (
    <Final
      onSubmit={onSubmit}
      render={({ handleSubmit, errors, touched }) => (
        <Form onSubmit={handleSubmit}>
          <Field
            name="sum"
            type="number"
            size="lg"
            placeholder="Сумма"
            component={FormControlAdapter}
          />
          <Field
            name="instructions"
            type="text"
            size="lg"
            as="textarea"
            placeholder="Номер телефона или карты на которые нужно совершить выплату"
            component={FormControlAdapter}
          />
          <Button variant="primary" size="lg" type="submit" loading={loading}>
            Запросить
          </Button>
          <ErrorMessage errors={mapFormErrors(errors, error, touched)} />
        </Form>
      )}
    />
  )
}
