import React from 'react'

import { DiscussionModel } from 'models'
import { H1 } from 'components'
import { Link } from 'react-router-dom'

import styles from './EternalDiscussionsList.module.css'
import { Paths } from 'routing'
import { kMaxLength } from 'buffer'
import { maxLength } from 'helpers'
import { IconWithText } from 'components/IconWithText/IconWithText'
import { IconName } from 'components/Icon/Icon'
import { Spinner } from 'components/Spinner/Spinner'

interface EternalDiscussionsListProps {
  discussions: DiscussionModel[]
}

export const EternalDiscussionsList = ({
  discussions
}: EternalDiscussionsListProps) => (
  <div className={styles.EternalDiscussionsList}>
    <H1>Вечные холивары</H1>
    {discussions ? (
      <div className={styles.list}>
        {discussions.map((d: DiscussionModel) => (
          <Link
            to={Paths.GetDiscussion(d.alias)}
            className={styles.item}
            key={d.id}
          >
            <img src={d.imageUrl} />
            <div className={styles.title}>
              <span>{maxLength(d.title, 70)}</span>
              <IconWithText
                className={styles.commentsIcon}
                iconName={IconName.comment}
                text={d.commentsCount}
              />
            </div>
          </Link>
        ))}
      </div>
    ) : (
      <Spinner />
    )}
    <Link className={styles.more} to={Paths.GetEternalDiscussions()}>
      Посмотреть все
    </Link>
  </div>
)
