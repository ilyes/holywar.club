import React, { ReactNode, useMemo } from 'react'
import classnames from 'classnames'

import { DiscussionModel } from 'models'

import { DiscussionCard } from '../DiscussionCard/DiscussionCard'

import styles from './DiscussionCardList.module.css'
import { Spinner } from 'components/Spinner/Spinner'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { DiscussionBigSkeleton } from '../DiscussionBigSkeleton/DiscussionBigSkeleton'

interface DiscussionCardListProps {
  discussions: DiscussionModel[]
  loading?: boolean
  error?: string
  big?: boolean
  component: any
  adulterate?: (nodes: ReactNode[]) => void
}

export const DiscussionCardList = ({
  discussions,
  loading,
  error,
  big,
  component: Component = DiscussionCard,
  adulterate
}: DiscussionCardListProps) => {
  if (loading) {
    return <DiscussionBigSkeleton />
  }
  if (error) {
    return <ErrorMessage error={error} />
  }
  if (discussions) {
    if (!discussions.length) {
      return <div className={styles.noDiscussions}>Холиваров не найдено</div>
    }

    const nodes = useMemo(() => {
      const result = discussions.map((c) => (
        <Component discussion={c} key={c.id} />
      ))
      if (typeof adulterate === 'function') {
        adulterate(result)
      }
      return result
    }, [])

    return (
      <div
        className={classnames(styles.discussionCardList, { [styles.big]: big })}
      >
        {nodes}
      </div>
    )
  }
  return <div></div>
}
