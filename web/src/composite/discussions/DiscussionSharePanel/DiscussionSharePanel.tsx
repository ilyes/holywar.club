import React from 'react'

import { Panel } from 'layouts'

import styles from './DiscussionSharePanel.module.css'

export const DiscussionSharePanel = ({ children }: { children: any }) => {
  return (
    <Panel className={styles.panel} horizontal justifyContent="center">
      <div className={styles.children}>{children}</div>
    </Panel>
  )
}
