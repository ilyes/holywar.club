import React from 'react'
import { Link } from 'react-router-dom'
import Linkify from 'react-linkify'

import { Paths } from 'routing'
import { formatDateString } from 'helpers'

import { CommentWithUserModel, CommentModel } from 'models'

import { Avatar } from 'components/Avatar/Avatar'
import { Like } from 'components/Like/Like'
import { IconWithText } from 'components/IconWithText/IconWithText'
import { IconName } from 'components/Icon/Icon'
import { Img } from 'components/Img/Img'

import styles from './Comment.module.css'
import { LikesDislikesCount } from 'components/LikesDislikesCount/LikesDislikesCount'
import { Panel } from 'layouts/common/Panel/Panel'

interface CommentProps {
  commentWithUser: CommentWithUserModel
  onLikeClick?: (comment: CommentModel, isLike: boolean) => void
  likeLoading?: boolean
  maxHeight?: number
}

export const Comment = ({
  commentWithUser: { comment, user, discussionAlias },
  onLikeClick,
  likeLoading,
  maxHeight
}: CommentProps) => {
  const { text, imageUrl, dislikesCount, likesCount, creationDate } = comment
  const { name, avatarUrl } = user
  const linkified = (
    <Linkify
      componentDecorator={(decoratedHref, decoratedText, key) => (
        <a target="blank" href={decoratedHref} key={key}>
          {decoratedText}
        </a>
      )}
    >
      {text}
    </Linkify>
  )
  const textComp = maxHeight ? (
    <div style={{ maxHeight, overflow: 'hidden' }}>{linkified}</div>
  ) : (
    linkified
  )
  return (
    <div className={styles.comment}>
      <div className={styles.avatar}>
        <Avatar size="s" avatarUrl={avatarUrl} />
        <Link to={Paths.GetProfile(user.id)}>{name}</Link>
        <span className={styles.date}>{formatDateString(creationDate)}</span>
      </div>
      <div className={styles.content}>
        <pre className={styles.text}>{textComp}</pre>
        {imageUrl ? (
          <div>
            <Img className={styles.image} src={imageUrl} nullable />
          </div>
        ) : null}
      </div>
      <div className={styles.likes}>
        {typeof onLikeClick === 'function' ? (
          <Like
            onLikeClick={(isLike) => onLikeClick(comment, isLike)}
            likesCount={likesCount}
            dislikesCount={dislikesCount}
            loading={likeLoading}
          />
        ) : (
          <LikesDislikesCount
            likesCount={likesCount}
            dislikesCount={dislikesCount}
          />
        )}
      </div>
    </div>
  )
}
