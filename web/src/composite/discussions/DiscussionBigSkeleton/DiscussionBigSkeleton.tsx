import { Spinner } from 'components/Spinner/Spinner'
import React from 'react'

import styles from './DiscussionBigSkeleton.module.css'

export const DiscussionBigSkeleton = () => {
  return (
    <div className={styles.root}>
      <Spinner />
    </div>
  )
}
