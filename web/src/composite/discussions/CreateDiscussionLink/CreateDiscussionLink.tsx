import React from 'react'
import { Link } from 'react-router-dom'
import classnames from 'classnames'

import style from './CreateDiscussionLink.module.css'

interface CreateDiscussionLinkProps {
  to: string
  className?: string
}

export const CreateDiscussionLink = ({
  to,
  className
}: CreateDiscussionLinkProps) => {
  return (
    <Link
      to={to}
      className={classnames(
        style.createDiscussionLink,
        className,
        'btn btn-primary btn-sm'
      )}
    >
      <span className={style.whole}>Начать холивар!</span>
      <span className={style.short}>Начать!</span>
    </Link>
  )
}
