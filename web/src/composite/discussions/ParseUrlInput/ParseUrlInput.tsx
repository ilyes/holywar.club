import React, { useState, useEffect } from 'react'
import FormControl from 'react-bootstrap/FormControl'

import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { validateUrl } from 'helpers'

import styles from './ParseUrlInput.module.css'
import Spinner from 'react-bootstrap/Spinner'

interface ParseUrlInputProps {
  loading: boolean
  error: string
  onChange: (url: string) => void
}

export const ParseUrlInput = ({
  loading,
  error,
  onChange
}: ParseUrlInputProps) => {
  const [url, setUrl] = useState('')
  const hasUrl = !!url && validateUrl(url) === false

  useEffect(() => {
    if (hasUrl) {
      onChange && onChange(url)
    } // eslint-disable-next-line
  }, [hasUrl, url])

  return (
    <div className={styles.wrap}>
      <FormControl
        placeholder="Ссылка"
        value={url}
        onChange={({ target }: any) => setUrl(target.value)}
        disabled={(hasUrl || loading) && !error}
      />
      <ErrorMessage error={error} />
      {loading ? (
        <Spinner className={styles.spinner} animation="border" size="sm" />
      ) : null}
    </div>
  )
}
