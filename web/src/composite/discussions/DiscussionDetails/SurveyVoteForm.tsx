import React from 'react'
import Form from 'react-bootstrap/Form'
import { Form as Final, Field } from 'react-final-form'

import { SurveyItemModel, SurveyModel } from 'models'

import styles from './DiscussionDetails.module.css'
import { RadioboxListAdapter } from 'components/Radiobox/RadioboxListAdapter'
import { Button } from 'components/Button/Button'

interface SurveyVoteFormProps {
  onVoteClick: (surveyItemId: string) => void
  survey: SurveyModel
  isVoteSubmitting: boolean
  onCancelClick: () => void
}

export const SurveyVoteForm = ({
  onVoteClick,
  onCancelClick,
  survey,
  isVoteSubmitting
}: SurveyVoteFormProps) => {
  return (
    <Final
      onSubmit={({ surveyItemId }) => onVoteClick(surveyItemId)}
      render={({ handleSubmit }) => (
        <Form onSubmit={handleSubmit} className={styles.surveyForm}>
          <Field
            name="surveyItemId"
            component={RadioboxListAdapter}
            items={survey.items.map(({ title, id }: SurveyItemModel) => ({
              value: id,
              label: title
            }))}
          />
          <Button
            type="submit"
            variant="primary"
            className={styles.surveyButton}
            loading={isVoteSubmitting}
          >
            Отправить голос
          </Button>
          <Button
            variant="link"
            className={styles.surveyButton}
            onClick={onCancelClick}
            disabled={isVoteSubmitting}
          >
            Посмотреть результаты
          </Button>
        </Form>
      )}
    />
  )
}
