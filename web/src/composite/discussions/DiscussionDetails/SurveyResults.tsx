import React from 'react'
import ProgressBar from 'react-bootstrap/ProgressBar'

import { Button } from 'components/Button/Button'
import { SurveyVotesModel, SurveyModel } from 'models'

import styles from './DiscussionDetails.module.css'

interface Props {
  survey: SurveyModel
  votes: SurveyVotesModel[]
  canVote: boolean
  onVoteClick: () => void
}

export const SurveyResults = ({
  survey,
  votes,
  canVote,
  onVoteClick
}: Props) => {
  const totalVotes = votes.reduce((s, c) => s + c.votes, 0)

  if (totalVotes === 0) {
    return null
  }

  return (
    <div className={styles.surveyForm}>
      {survey.items.map((i) => {
        const surveyVotesItem = votes.find((s) => s.id === i.id)
        const now = ((surveyVotesItem?.votes || 0) / totalVotes) * 100
        return (
          <div key={`${i.id}_${now}`}>
            <div>{i.title}</div>
            <ProgressBar now={now} label={`${now.toFixed(2)}%`} />
          </div>
        )
      })}
      {canVote ? (
        <Button variant="link" onClick={onVoteClick}>
          Проголосовать
        </Button>
      ) : null}
    </div>
  )
}
