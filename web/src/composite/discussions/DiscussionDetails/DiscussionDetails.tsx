import React, { Fragment, ReactNode } from 'react'
import { Link } from 'react-router-dom'
import Linkify from 'react-linkify'

import { DiscussionModel, DiscussionBodyItemType, UserModel } from 'models'

import { Paths } from 'routing'

import { HolywarLabel } from 'composite/common/HolywarLabel/HolywarLabel'
import { Category } from 'composite'
import { formatDateString } from 'helpers'

import styles from './DiscussionDetails.module.css'
import { Avatar } from 'components/Avatar/Avatar'
import { Like } from 'components/Like/Like'
import { Img } from 'components/Img/Img'
import { Panel } from 'layouts/common/Panel/Panel'

interface DiscussionDetailsProps {
  discussion: DiscussionModel
  user: UserModel
  categoryName: string
  onLikeClick: (discussionId: string, isLike: boolean) => void
  survey: ReactNode
  editButton: ReactNode
}

export const DiscussionDetails = ({
  discussion,
  user,
  survey,
  categoryName,
  editButton,
  onLikeClick
}: DiscussionDetailsProps) => {
  const {
    id,
    title,
    creationDate,
    likesCount,
    dislikesCount,
    body,
    isHolywar
  } = discussion
  return (
    <div className={styles.discussion}>
      <h1>{title}</h1>
      <div className={styles.top}>
        <div className={styles.labels}>
          <Category title={categoryName} className={styles.category} />
          {isHolywar ? <HolywarLabel /> : null}
        </div>
        <div className={styles.author}>
          <Avatar size="xs" avatarUrl={user.avatarUrl} />
          <Link to={Paths.GetProfile(user.id)} className={styles.authorName}>
            {user.name}
          </Link>
        </div>
        <div>{editButton}</div>
      </div>
      <div className={styles.body}>
        <DiscussionDescription discussion={discussion} />
        {body
          ? body.map((item, index) => (
              <Fragment key={index}>
                {item.type === DiscussionBodyItemType.SubTitle ? (
                  <h2>{item.value}</h2>
                ) : null}
                {item.type === DiscussionBodyItemType.Text ? (
                  <Panel bg className={styles.bodyText}>
                    <pre>
                      <Linkify
                        componentDecorator={(
                          decoratedHref,
                          decoratedText,
                          key
                        ) => (
                          <a target="blank" href={decoratedHref} key={key}>
                            {decoratedText}
                          </a>
                        )}
                      >
                        {item.value}
                      </Linkify>
                    </pre>
                  </Panel>
                ) : null}
                {item.type === DiscussionBodyItemType.Image ? (
                  <Img src={item.value} nullable />
                ) : null}
              </Fragment>
            ))
          : null}
        {survey}
        <div className={styles.dateAndLikes}>
          <span className={styles.date}>
            Опубликовано {formatDateString(creationDate)}
          </span>
          <Like
            likesCount={likesCount}
            dislikesCount={dislikesCount}
            onLikeClick={(isLike) => onLikeClick(id, isLike)}
            className={styles.authorLikes}
          />
        </div>
      </div>
    </div>
  )
}

interface DiscussionDescriptionProps {
  discussion: DiscussionModel
}

const DiscussionDescription: React.FC<DiscussionDescriptionProps> = ({
  discussion
}) => {
  const { title, imageUrl, description, url } = discussion

  const embedableData = tryParseEmbedableData(url)
  if (embedableData) {
    return <DiscussionEmbed embedableData={embedableData} />
  }

  return (
    <>
      {imageUrl ? <Img src={imageUrl} nullable /> : null}
      <Panel bg className={styles.description}>
        <pre>
          <Linkify
            componentDecorator={(decoratedHref, decoratedText, key) => (
              <a target="blank" href={decoratedHref} key={key}>
                {decoratedText}
              </a>
            )}
          >
            {description}
          </Linkify>
        </pre>
      </Panel>
      {url ? (
        <Panel horizontal justifyContent="center" gap="s">
          Источник:
          <a
            className={styles.url}
            href={url}
            target="_blank"
            rel="noopener noreferrer"
          >
            {url}
          </a>
        </Panel>
      ) : null}
    </>
  )
}

interface EmbedableData {
  youtube?: { videoId: string }
}

function tryParseEmbedableData(url: string): EmbedableData | null {
  if (!url) {
    return null
  }

  const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/
  const match = url.match(regExp)

  if (match && match[2].length === 11) {
    return { youtube: { videoId: match[2] } }
  }

  return null
}

interface DiscussionEmbedProps {
  embedableData: EmbedableData
}

const DiscussionEmbed: React.FC<DiscussionEmbedProps> = ({ embedableData }) => {
  if (embedableData.youtube) {
    return (
      <iframe
        className={styles.frame}
        src={`//www.youtube.com/embed/${embedableData.youtube.videoId}`}
        frameBorder="0"
        allowFullScreen
      ></iframe>
    )
  }

  return null
}
