import React from 'react'
import { Link } from 'react-router-dom'

import { Paths } from 'routing'
import { DiscussionModel } from 'models'

import styles from './DiscussionCard.module.css'
import { HolywarLabel, Category } from 'composite'
import { Button } from 'components/Button/Button'
import { Img } from 'components/Img/Img'

export interface DiscussionCardProps {
  discussion: DiscussionModel
  categoryName: string
}

export const DiscussionCard = ({
  discussion: { alias, imageUrl, title, isHolywar },
  categoryName
}: DiscussionCardProps) => (
  <Link to={Paths.GetDiscussion(alias)} className={styles.discussionCard}>
    <div className={styles.content}>
      {imageUrl && <Img className={styles.image} src={imageUrl} nullable />}
      <h3>{title}</h3>
    </div>
    <div className={styles.discussionCardLinks}>
      <Category title={categoryName} className={styles.category} />
      <Button variant="link" size="sm">
        Обсудить...
      </Button>
    </div>
    {isHolywar ? <HolywarLabel className={styles.holywarLabel} /> : null}
  </Link>
)
