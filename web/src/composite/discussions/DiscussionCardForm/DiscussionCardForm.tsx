import React from 'react'
import Form from 'react-bootstrap/Form'
import { Form as Final, Field } from 'react-final-form'
import arrayMutators from 'final-form-arrays'

import { mapFormErrors, required } from 'helpers'
import {
  DiscussionBodyItemModel,
  CategoryModel,
  SurveyItemModel,
  SurveyCreateModel
} from 'models'

import { BodyFields } from './BodyFields'
import { SurveyFields } from './SurveyFields'

import styles from './DiscussionCardForm.module.css'
import { FormControlAdapter } from 'components/FormControlAdapter/FormControlAdapter'
import { FormImageAdapter } from 'components/FormImage/FormImageAdapter'
import { FormSelectAdapter } from 'components/Select/FormSelectAdapter'
import { Button } from 'components/Button/Button'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'

interface DiscussionCardFormValues {
  title?: string
  imageUrl?: string
  description?: string
  body?: DiscussionBodyItemModel[]
  survey?: SurveyCreateModel
}

interface DiscussionCardFormProps {
  loading: boolean
  error: string
  categories: CategoryModel[]
  initialValues?: DiscussionCardFormValues
  onSubmit: (values: DiscussionCardFormValues) => void
  submitLabel?: string
}

export const DiscussionCardForm = ({
  loading,
  error,
  categories,
  initialValues,
  onSubmit,
  submitLabel
}: DiscussionCardFormProps) => (
  <Final
    onSubmit={onSubmit}
    initialValues={initialValues}
    mutators={{
      ...arrayMutators
    }}
    render={({
      handleSubmit,
      errors,
      touched,
      values,
      form: {
        mutators: { push }
      }
    }) => (
      <Form onSubmit={handleSubmit} className={styles.discussionCardForm}>
        <Field name="id" type="hidden" component={FormControlAdapter} />
        <Field
          name="title"
          type="text"
          as="textarea"
          size="lg"
          placeholder="Заголовок"
          component={FormControlAdapter}
          validate={required('Без заголовка нельзя')}
        />
        <Field
          name="imageUrl"
          placeholder="Ссылка на картинку"
          className={styles.image}
          component={FormImageAdapter}
        />
        <Field
          name="description"
          type="text"
          as="textarea"
          rows={5}
          placeholder="Описание"
          component={FormControlAdapter}
        />
        <Field
          name="categoryId"
          placeholder="Категория"
          component={FormSelectAdapter}
          items={categories.map(({ id, name }) => ({ value: id, label: name }))}
          validate={required('Нужно выбрать категорию')}
        />
        <hr className={styles.bodyDelimiter} />
        <BodyFields
          push={(i: DiscussionBodyItemModel) => push('body', i)}
          body={values.body}
        />
        <hr className={styles.bodyDelimiter} />
        <SurveyFields push={(i: SurveyItemModel) => push('survey.items', i)} />
        <Button
          variant="primary"
          size="lg"
          type="submit"
          loading={loading}
          className={styles.submit}
        >
          {submitLabel || 'Начать'}
        </Button>
        <ErrorMessage errors={mapFormErrors(errors, error, touched)} />
      </Form>
    )}
  />
)
