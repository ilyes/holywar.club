import React from 'react'
import { Field } from 'react-final-form'
import { FieldArray } from 'react-final-form-arrays'

import { DiscussionBodyItemType } from 'models'

import styles from './DiscussionCardForm.module.css'
import { FormControlAdapter } from 'components/FormControlAdapter/FormControlAdapter'
import { FormImageAdapter } from 'components/FormImage/FormImageAdapter'
import { Icon, IconName } from 'components/Icon/Icon'

export const BodyFields = ({ body, push }: any) => (
  <>
    <FieldArray name="body">
      {({ fields }: any) =>
        fields.map((name: string, index: number) => (
          <div key={name} className={styles.bodyItem}>
            {body[index].type === DiscussionBodyItemType.Text ? (
              <Field
                name={`${name}.value`}
                type="text"
                as="textarea"
                rows={10}
                placeholder="Текст"
                component={FormControlAdapter}
              />
            ) : null}
            {body[index].type === DiscussionBodyItemType.SubTitle ? (
              <Field
                name={`${name}.value`}
                type="text"
                as="textarea"
                size="lg"
                rows={2}
                placeholder="Подзаголовок"
                component={FormControlAdapter}
              />
            ) : null}
            {body[index].type === DiscussionBodyItemType.Image ? (
              <Field
                name={`${name}.value`}
                placeholder="Ссылка на картинку"
                className={styles.image}
                component={FormImageAdapter}
                withoutClear
              />
            ) : null}
            <Field
              name={`${name}.type`}
              type="hidden"
              component={FormControlAdapter}
            />
            <Icon
              className={styles.close}
              onClick={() => fields.remove(index)}
              name={IconName.close}
            />
          </div>
        ))
      }
    </FieldArray>
    <p className={styles.addLayout}>
      <label>Добавить</label>
      <Icon
        name={IconName.title}
        onClick={() => push({ type: DiscussionBodyItemType.SubTitle })}
        className={styles.addBodyItemIcon}
      />
      <Icon
        name={IconName.image}
        onClick={() => push({ type: DiscussionBodyItemType.Image })}
        className={styles.addBodyItemIcon}
      />
      <Icon
        name={IconName.text}
        onClick={() => push({ type: DiscussionBodyItemType.Text })}
        className={styles.addBodyItemIcon}
      />
    </p>
  </>
)
