import React from 'react'
import { Field } from 'react-final-form'
import { FieldArray } from 'react-final-form-arrays'

import styles from './DiscussionCardForm.module.css'
import { SurveyItemModel } from 'models'
import { TooltipIcon } from 'components/Tooltip/TooltipIcon'
import { FormControlAdapter } from 'components/FormControlAdapter/FormControlAdapter'
import { Icon, IconName } from 'components/Icon/Icon'
import { Button } from 'components/Button/Button'

export const SurveyFields = ({ push }: any) => {
  return (
    <>
      <p className={styles.addLayout}>
        <label>Опрос (В чем заключается холивар)</label>
        <TooltipIcon
          id="survey-fields-tooltip"
          iconName={IconName.question}
          placement="right"
          className={styles.tooltipIcon}
        >
          Можно написать пару вопроса обозначающих разные точки зрения на
          вопрос.
        </TooltipIcon>
      </p>
      <FieldArray name="survey.items">
        {({ fields }: any) =>
          fields.map((name: string, index: number) => (
            <div key={name} className={styles.bodyItem}>
              <Field
                name={`${name}.title`}
                type="text"
                placeholder={`Вопрос №${index}`}
                component={FormControlAdapter}
              />
              <Icon
                className={styles.close}
                onClick={() => fields.remove(index)}
                name={IconName.close}
              />
            </div>
          ))
        }
      </FieldArray>
      <Button
        className={styles.addSurveyItem}
        variant="link"
        onClick={() => push({ title: '' })}
      >
        <Icon name={IconName.plus} />
        Добавить вопрос
      </Button>
    </>
  )
}
