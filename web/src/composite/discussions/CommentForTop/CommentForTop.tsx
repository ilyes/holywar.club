import { Avatar } from 'components/Avatar/Avatar'
import { Img } from 'components/Img/Img'
import { LikesDislikesCount } from 'components/LikesDislikesCount/LikesDislikesCount'
import { Words } from 'components/Words/Words'
import { formatDateString } from 'helpers'
import { Panel } from 'layouts/common/Panel/Panel'
import { CommentWithUserModel } from 'models'
import React from 'react'
import { Link } from 'react-router-dom'
import { Paths } from 'routing'

import styles from './CommentForTop.module.css'

type Props = {
  commentWithUser: CommentWithUserModel
}

export const CommentForTop: React.FC<Props> = ({
  commentWithUser: { comment, user, discussionAlias }
}) => {
  const { text, imageUrl, dislikesCount, likesCount, creationDate } = comment
  const { name, avatarUrl } = user
  return (
    <Panel padding="s" gap="s">
      <Panel horizontal padding="0" gap="s">
        <Avatar size="xxs" avatarUrl={avatarUrl} />
        <Link to={Paths.GetProfile(user.id)}>{name}</Link>
      </Panel>
      <div>{text}</div>
      {imageUrl && <Img src={imageUrl} nullable />}
      <Panel horizontal padding="0" gap="s">
        <LikesDislikesCount
          likesCount={likesCount}
          dislikesCount={dislikesCount}
        />
        <Words>{formatDateString(creationDate)}</Words>
        <Link to={Paths.GetDiscussion(discussionAlias, comment.id)}>
          Посмотреть
        </Link>
      </Panel>
    </Panel>
  )
}
