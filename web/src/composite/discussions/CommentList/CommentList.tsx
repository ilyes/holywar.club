import React from 'react'
import classnames from 'classnames'

import { CommentViewModel } from 'models'

import styles from './CommentList.module.css'

interface CommentListProps {
  comments: CommentViewModel[]
  Comment: any
  isChild?: boolean
  className?: string
}

export const CommentList = ({
  comments,
  className,
  Comment
}: CommentListProps) => {
  if (!comments || !comments.length) {
    return null
  }

  return (
    <div className={classnames(styles.commentList, className)}>
      {comments.map((c) => {
        return (
          <div className={styles.comment} key={c.comment.id}>
            <Comment commentWithUser={c} />
            <CommentList
              comments={c.children}
              Comment={Comment}
              isChild={true}
              className={styles.children}
            />
          </div>
        )
      })}
    </div>
  )
}
