import React from 'react'
import { Link } from 'react-router-dom'

import { Paths } from 'routing'

import { DiscussionModel } from 'models'

import { Category } from 'composite/categories/Category/Category'
import { HolywarLabel } from 'composite'
import { Avatar } from 'components/Avatar/Avatar'
import { Img } from 'components/Img/Img'
import { LikesDislikesCount } from 'components/LikesDislikesCount/LikesDislikesCount'
import { IconWithText } from 'components/IconWithText/IconWithText'
import { IconName } from 'components/Icon/Icon'
import { Button } from 'components/Button/Button'

import styles from './DiscussionCardBig.module.css'
export interface DiscussionCardBigProps {
  discussion: DiscussionModel
  categoryName: string
}

export const DiscussionCardBig = ({
  discussion: {
    alias,
    imageUrl,
    title,
    description,
    isHolywar,
    author,
    viewsCount,
    likesCount,
    dislikesCount,
    commentsCount
  },
  categoryName
}: DiscussionCardBigProps) => (
  <div className={styles.discussionCardBig}>
    {author && (
      <Link className={styles.author} to={Paths.GetProfile(author.userId)}>
        <Avatar size="xs" avatarUrl={author.avatarUrl} />
        <span className={styles.authorName}>{author.name}</span>
      </Link>
    )}
    <Link className={styles.title} to={Paths.GetDiscussion(alias)}>
      <div className={styles.content}>
        {imageUrl && <Img className={styles.image} src={imageUrl} nullable />}
        <div className={styles.text}>
          <h3>{title}</h3>
          <p>{description}</p>
        </div>
      </div>
    </Link>
    <div className={styles.discussionCardLinks}>
      <Category title={categoryName} className={styles.category} />
      <div className={styles.discussionCardLinksLeft}>
        <LikesDislikesCount
          likesCount={likesCount}
          dislikesCount={dislikesCount}
        />
        <IconWithText
          className={styles.counts}
          iconName={IconName.comment}
          text={commentsCount}
        />
        <IconWithText
          className={styles.counts}
          iconName={IconName.eye}
          text={viewsCount}
        />
        <Link to={Paths.GetDiscussion(alias)}>
          <Button variant="link" size="sm">
            Обсудить...
          </Button>
        </Link>
      </div>
    </div>
    {isHolywar ? <HolywarLabel className={styles.holywarLabel} /> : null}
  </div>
)
