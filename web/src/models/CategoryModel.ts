export interface CategoryModel {
  id: string
  alias: string
  name: string
}

export enum CategoryAliases {
  Politics = 'Politics',
  Economics = 'Economics',
  Religion = 'Religion',
  Incidents = 'Incidents',
  Culture = 'Culture',
  Sports = 'Sports',
  Society = 'Society',
  Health = 'Health',
  Technology = 'Technology',
  Entertainment = 'Entertainment',
  Humor = 'Humor',
  Goods = 'Goods',
  Services = 'Services'
}
