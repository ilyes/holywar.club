export interface SurveyModel {
  id: string
  discusionId: string
  items: SurveyItemModel[]
}

export interface SurveyCreateModel {
  items: SurveyItemModel[]
}

export interface SurveyItemModel {
  id?: string
  title: string
}

export interface SurveyVotesModel {
  id: string
  votes: number
}
