import { ConfigModel } from './ConfigModel'

export interface UserModel {
  id: string
  name: string
  email?: string
  avatarUrl: string
  referralsCount: number
  referralId: string
  likesCount: number
  dislikesCount: number
  commentsCount: number
  discussionsCount: number
  discussionViewsCount: number
  discussionExternalViewsCount: number
  uniqueCommentatorsCount: number
  holywarsStarted: number
  holywarsParticipant: number
  weekRating: number
  balance: number
  paid: number
  referralPayments: number
  role: string
  categoryIds: string[]
  paymentLimit: number
  paymentLimitWeekReduce: number
  level: number
  levelProgress: { current: number; next: number }
}

export enum UserRole {
  client = 'client',
  admin = 'admin'
}

export function getMinPayment(user: UserModel, config: ConfigModel): number {
  const paymentLimitLevelReduce = user.level * config.levelLimitReduceBy
  return Math.max(
    user.paymentLimit - user.paymentLimitWeekReduce - paymentLimitLevelReduce,
    Number(config.minPaymentLimit)
  )
}

export interface PartnerInfoModel {
  weekUser: UserModel
}
