export interface ConfigModel {
  levelLimitReduceBy: number
  startPaymentLimit: number
  minPaymentLimit: number
  adProvider: string
  weekRating: number
  howToDiscussionId: string
  adminUserId: string
  weekJackpot: number
}

export const AdProviders = {
  google: 'google',
  yandex: 'yandex'
}
