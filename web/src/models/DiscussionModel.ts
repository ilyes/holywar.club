import { SurveyModel, SurveyVotesModel } from './SurveyModel'

export interface DiscussionModel {
  id: string
  alias: string
  creationDate: string
  imageUrl: string
  title: string
  description: string
  url: string
  viewsCount: number
  commentsCount: number
  likesCount: number
  dislikesCount: number
  commetsCount: number
  body: DiscussionBodyItemModel[]
  categoryId: string
  isHolywar: boolean
  survey: SurveyModel
  surveyVotes: SurveyVotesModel[]
  author: DiscussionAuthorModel
}

export interface DiscussionAuthorModel {
  userId: string
  name: string
  avatarUrl: string
}
export interface DiscussionBodyItemModel {
  value: string
  type: DiscussionBodyItemType
}

export enum DiscussionBodyItemType {
  Text = 1,
  Image = 2,
  Url = 3,
  Video = 4,
  SubTitle = 5
}
