export * from './CategoryModel'
export * from './CommentModel'
export * from './DiscussionModel'
export * from './MessageModel'
export * from './UserModel'
export * from './PaymentStatus'
export * from './ConfigModel'
export * from './LikeModel'
export * from './SurveyModel'

export interface IWithId {
  id: string
}
