import { UserModel } from './UserModel'

export interface CommentModel {
  id: string
  discussionId: string
  text: string
  imageUrl: string
  parentCommentId: string
  likesCount: number
  dislikesCount: number
  creationDate: string
}

export interface SubmitCommentModel {
  text: string
  imageUrl: string
}

export interface CommentWithUserModel {
  comment: CommentModel
  user: UserModel
  discussionAlias: string
}

export interface CommentViewModel extends CommentWithUserModel {
  children: CommentViewModel[]
}
