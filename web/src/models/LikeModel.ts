import { UserModel } from './UserModel'

export interface LikeWithUserDto {
  user: UserModel
  targetUser: UserModel
  like: LikeDto
}

export interface LikeDto {
  userId: string
  targetUserId: string
  commentId: string
  discussionId: string
  isLike: boolean
}
