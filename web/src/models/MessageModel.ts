import { UserModel } from './UserModel'

export interface MessageWithUserModel {
  message: MessageModel
  fromUser: UserModel
}

export interface MessageModel {
  id: string
  type: number
  content: string
  isRead: boolean
}

export enum MessageType {
  Common = 0,
  LikesCount = 1,
  Reply = 2,
  AddMoney = 3,
  PaymentCreated = 4,
  PaymentApproved = 5,
  PaymentDeclined = 6,
  HowTo = 7
}
