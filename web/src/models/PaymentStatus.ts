export enum PaymentStatus {
  pending = 'pending',
  paid = 'paid',
  declined = 'declined',
  queued = 'queued'
}
