import csso from 'csso'
// @ts-ignore
import { readFileSync } from 'fs'

export function reactAndMinifyCss(path: string): string {
  const css = readFileSync(path, 'utf8')
  return csso.minify(css).css
}
