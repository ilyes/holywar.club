import { useEffect, useRef, DependencyList } from 'react'

type TEffectCallback = (deps: DependencyList) => void | (() => void | undefined)

export const useEffectWithPrev = (
  effect: TEffectCallback,
  deps: DependencyList
): void => {
  const ref = useRef(deps)

  useEffect(() => {
    effect(ref.current)
    ref.current = deps
  }, deps)
}
