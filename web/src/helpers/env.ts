import React, { useContext } from 'react'

export interface EnvObject {
  API_URL: string
  FRONTEND_HOST: string
  IS_PRODUCTION: boolean
  IS_NODE: boolean
  IS_TABLET: boolean
  IS_PHONE: boolean
}

export const defaultEnv: EnvObject = {
  API_URL: '',
  FRONTEND_HOST: '',
  IS_PRODUCTION: false,
  IS_NODE: false,
  IS_TABLET: false,
  IS_PHONE: false
}

export const EnvContext = React.createContext(defaultEnv)

export function useEnv(): EnvObject {
  const env = useContext(EnvContext)
  return env
}
