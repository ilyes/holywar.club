import { useDispatch, useSelector } from 'react-redux'

import { setAuthenticated, clearCache } from 'state'
import { useCallback, useMemo } from 'react'

const AUTH_KEY = 'AUTH_KEY'

interface AuthResult {
  isAuthenticated: boolean
  token: string
  setAuthenticated: (token: string, keepCache?: boolean) => void
}

export function useAuth(): AuthResult {
  const dispatch = useDispatch()
  const { isAuthenticated, token } = useSelector((state: any) => state.auth)

  const setAuthenticatedFunc = useCallback(
    (token: string, keepCache = false) => {
      dispatch(setAuthenticated(token))
      if (!keepCache) {
        dispatch(clearCache())
      }
    },
    [dispatch]
  )

  const result = useMemo(
    () => ({ isAuthenticated, token, setAuthenticated: setAuthenticatedFunc }),
    [isAuthenticated, token, setAuthenticatedFunc]
  )

  return result
}

export function restoreAuthToken() {
  if (typeof localStorage === 'undefined') {
    return ''
  }
  return localStorage.getItem(AUTH_KEY) || ''
}

export function storeAuthToken(token: string) {
  if (typeof localStorage === 'undefined') {
    return
  }
  localStorage.setItem(AUTH_KEY, token)
}

export function clearAuthToken() {
  if (typeof localStorage === 'undefined') {
    return
  }
  localStorage.removeItem(AUTH_KEY)
}
