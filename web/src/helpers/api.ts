import { useMemo } from 'react'
import { useEnv } from './env'
import { getQuery } from './query'

const CONFIG = `/config`

const COMMON_CURRENT_WEEK = `/common/current-week`

const GET_AUTH = `/auth`
const SIGN_IN = `/auth`
const SIGN_IN_VK = `/auth/vk`
const SIGN_IN_FACEBOOK = `/auth/facebook`
const SIGN_IN_GOOGLE = `/auth/google`
const SIGN_OUT = `/auth`
const CHANGE_PASSWORD = `/auth/change-password`
const PASSWORD_RECOVER = `/auth/recover-password`
const CHANGE_PASSWORD_BY_TOKEN = `/auth/change-password-by-token`

const PARSE_URL = `/parse-url`

const CREATE_DISCUSSION = `/discussion`
const LAST_DISCUSSIONS = `/discussion`
const GET_DISCUSSION = `/discussion`
const POPULAR_DISCUSSIONS = `/discussion/popular`
const FEATURED_DISCUSSION = `/discussion/featured`
const ETERNAL_DISCUSSIONS = `/discussion/eternal`
const LIKE_DISCUSSION = `/discussion/like`
const EDIT_DISCUSSION = `/discussion/edit`
const DELETE_DISCUSSION = `/discussion/delete`

const SURVEY_VOTE = `/survey/vote`

const CATEGORIES = `/categories`
const SAVE_USER_CATEGORIES = `/categories`

const SIGN_UP = `/user`
const USER = `/user`
const PARTNER_INFO = `/user/partner-info`
const UPLOAD_USER_AVATAR = `/user/avatar`
const USERS_OF_THE_WEEK = `/user/of-the-week`
const USER_REFERRALS = `/user/referrals`

const RATING = `/rating`
const RATING_BY_LIKES = `/rating/by-likes`
const RATING_BY_DISLIKES = `/rating/by-dislikes`
const RATING_BY_BALANCE_ADDED = `/rating/by-balance-added`
const RATING_BY_REFERALS = `/rating/by-referals`
const COMMENTS_OF_THE_WEEK = `/rating/week-comments`

const CREATE_COMMENT = `/comment`
const EDIT_COMMENT = `/comment`
const LIKE_COMMENT = `/comment/like`

const UNREAD_MESSAGES = `/message/last`
const SEND_MESSAGE = `/message`
const SET_MESSAGES_READ = `/message/read`

const ASK_FOR_PAYMENT = `/payment`
const PAYMENTS = `/payment/list`

type DiscussionsQuery = {
  userId?: string
  categoryIds?: string[]
  limit?: number
  afterDiscussionId?: string
}

type EternalDiscussionsQuery = {
  limit?: number
  afterDiscussionId?: string
}

export const getApiUrls = (API_URL: string) => ({
  configUrl: () => `${API_URL}${CONFIG}`,
  commonCurrentWeekUrl: () => `${API_URL}${COMMON_CURRENT_WEEK}`,

  authUrl: () => `${API_URL}${GET_AUTH}`,
  signInUrl: () => `${API_URL}${SIGN_IN}`,
  signInVkUrl: () => `${API_URL}${SIGN_IN_VK}`,
  signInFacebookUrl: () => `${API_URL}${SIGN_IN_FACEBOOK}`,
  signInGoogleUrl: () => `${API_URL}${SIGN_IN_GOOGLE}`,
  signOutUrl: () => `${API_URL}${SIGN_OUT}`,
  changePasswordUrl: () => `${API_URL}${CHANGE_PASSWORD}`,
  passwordRecoverUrl: () => `${API_URL}${PASSWORD_RECOVER}`,
  changePasswordByTokenUrl: () => `${API_URL}${CHANGE_PASSWORD_BY_TOKEN}`,

  parseUrl: () => `${API_URL}${PARSE_URL}`,

  createDiscussionUrl: () => `${API_URL}${CREATE_DISCUSSION}`,
  discussionsUrl: ({ limit = 100, ...rest }: DiscussionsQuery = {}) =>
    `${API_URL}${LAST_DISCUSSIONS}?${getQuery({ limit, ...rest })}`,
  popularDiscussionsUrl: () => `${API_URL}${POPULAR_DISCUSSIONS}`,
  featuredDiscussionsUrl: () => `${API_URL}${FEATURED_DISCUSSION}`,
  eternalDiscussionsUrl: ({
    limit = 10,
    ...rest
  }: EternalDiscussionsQuery = {}) =>
    `${API_URL}${ETERNAL_DISCUSSIONS}?${getQuery({ limit, ...rest })}`,
  discussionUrl: (id: string, notAView?: boolean) =>
    `${API_URL}${GET_DISCUSSION}/${id}${notAView ? '?notAView' : ''}`,
  likeDiscussionUrl: () => `${API_URL}${LIKE_DISCUSSION}`,
  editDiscussionUrl: () => `${API_URL}${EDIT_DISCUSSION}`,
  deleteDiscussionUrl: () => `${API_URL}${DELETE_DISCUSSION}`,

  surveyVoteUrl: () => `${API_URL}${SURVEY_VOTE}`,

  categoriesUrl: () => `${API_URL}${CATEGORIES}`,
  saveUserCategoriesUrl: () => `${API_URL}${SAVE_USER_CATEGORIES}`,

  signUpUrl: () => `${API_URL}${SIGN_UP}`,
  currentUserUrl: () => `${API_URL}${USER}`,
  partnerInfoUrl: () => `${API_URL}${PARTNER_INFO}`,
  userUrl: (id: string) => `${API_URL}${USER}/${id}`,
  uploadUserAvatarUrl: () => `${API_URL}${UPLOAD_USER_AVATAR}`,
  usersOfTheWeekUrl: () => `${API_URL}${USERS_OF_THE_WEEK}`,
  userReferralsUrl: () => `${API_URL}${USER_REFERRALS}`,

  ratingUrl: () => `${API_URL}${RATING}`,
  ratingByLikesUrl: () => `${API_URL}${RATING_BY_LIKES}`,
  ratingByDislikesUrl: () => `${API_URL}${RATING_BY_DISLIKES}`,
  ratingByBalanceAddedUrl: () => `${API_URL}${RATING_BY_BALANCE_ADDED}`,
  ratingByReferalsUrl: () => `${API_URL}${RATING_BY_REFERALS}`,
  ratingCommentsOfTheWeekUrl: (params: { limit: number; weekId?: number }) =>
    `${API_URL}${COMMENTS_OF_THE_WEEK}?${getQuery(params)}`,

  createCommentUrl: () => `${API_URL}${CREATE_COMMENT}`,
  editCommentUrl: () => `${API_URL}${EDIT_COMMENT}`,
  likeCommentUrl: () => `${API_URL}${LIKE_COMMENT}`,

  unreadMessagesUrl: () => `${API_URL}${UNREAD_MESSAGES}`,
  sendMessageUrl: () => `${API_URL}${SEND_MESSAGE}`,
  setMessagesReadUrl: () => `${API_URL}${SET_MESSAGES_READ}`,

  askForPaymentUrl: () => `${API_URL}${ASK_FOR_PAYMENT}`,
  paymentsUrl: () => `${API_URL}${PAYMENTS}`
})

export function useApi() {
  const { API_URL } = useEnv()

  const result = useMemo(() => getApiUrls(API_URL), [API_URL])

  return result
}
