export function maxLength(str: string, maxLen: number): string {
  if (!str) {
    return str
  }

  if (str.length > maxLen) {
    return str.substr(0, maxLen) + '...'
  }

  return str
}
