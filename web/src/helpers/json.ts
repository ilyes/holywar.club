export function tryPanseJson<T>(str: string): T | null {
  try {
    return JSON.parse(str) as T
  } catch (e) {
    return null
  }
}
