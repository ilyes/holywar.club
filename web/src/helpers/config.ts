import React, { useContext } from 'react'
import { ConfigModel, AdProviders } from 'models'

export const defaultConfig: ConfigModel = {
  levelLimitReduceBy: 30,
  startPaymentLimit: 500,
  minPaymentLimit: 100,
  adProvider: AdProviders.yandex,
  weekRating: 250,
  weekJackpot: 1000,
  howToDiscussionId: '',
  adminUserId: ''
}

export const ConfigContext = React.createContext(defaultConfig)

export function useConfig(): ConfigModel {
  const config = useContext(ConfigContext)
  return config
}
