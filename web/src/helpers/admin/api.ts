import { useEnv } from 'helpers'
import { getQuery } from 'helpers/query'

const BALANCE_CANDIDATES = `/admin/add-balance/candidates`
const SAVE_BALANCES = `/admin/add-balance`
const ADMIN_PAYMENTS = `/admin/payment/list`
const ADMIN_SET_PAYMENT_STATUS = `/admin/payment/status`
const GET_ADMIN_CONFIG = `/admin/config`
const SAVE_ADMIN_CONFIG = `/admin/config`
const BLOCK_USER = `/admin/user/block`
const LAST_USERS = `/admin/user/last`
const DELETE_DISCUSSION = `/admin/discussion/delete`
const UNWANTED_DISCUSSION = `/admin/discussion/unwanted`
const SET_AD_DISCUSSION = `/admin/discussion/ad`
const SET_TOP_DISCUSSION = `/admin/discussion/top`
const SET_MODERATED = `/admin/discussion/moderated`
const SET_NOT_MODERATED = `/admin/discussion/not-moderated`
const SET_NOT_TOP_DISCUSSION = `/admin/discussion/untop`
const DELETE_COMMENT = `/admin/comment/delete`
const COMMENT_LIST = `/admin/comment/list`
const HOLYWAR_CANDIDATES = `/admin/holywar/candidates`
const HOLYWAR_APPROVE = `/admin/holywar/approve`
const HOLYWAR_DECLINE = `/admin/holywar/decline`
const COMMENT_LIKES = `/admin/comment/likes/`
const USER_LIKES = `/admin/user/likes/`

type UsersQuery = { limit?: number }

export function useAdminApi() {
  const { API_URL } = useEnv()

  return {
    balanceCandidatesUrl: (params: { week: number }) =>
      `${API_URL}${BALANCE_CANDIDATES}?${getQuery(params)}`,
    saveBalancesUrl: () => `${API_URL}${SAVE_BALANCES}`,
    adminPaymentsUrl: () => `${API_URL}${ADMIN_PAYMENTS}`,
    adminSetPaymentStatusUrl: () => `${API_URL}${ADMIN_SET_PAYMENT_STATUS}`,
    getAdminConfigUrl: () => `${API_URL}${GET_ADMIN_CONFIG}`,
    saveAdminConfigUrl: () => `${API_URL}${SAVE_ADMIN_CONFIG}`,
    blockUserUrl: () => `${API_URL}${BLOCK_USER}`,
    lastUsersUrl: ({ limit = 100, ...rest }: UsersQuery = {}) =>
      `${API_URL}${LAST_USERS}?${getQuery({ limit, ...rest })}`,
    deleteDiscussionUrl: () => `${API_URL}${DELETE_DISCUSSION}`,
    unwantedDiscussionUrl: () => `${API_URL}${UNWANTED_DISCUSSION}`,
    setAdDiscussionUrl: () => `${API_URL}${SET_AD_DISCUSSION}`,
    setTopDiscussionUrl: () => `${API_URL}${SET_TOP_DISCUSSION}`,
    setModeratedUrl: () => `${API_URL}${SET_MODERATED}`,
    setNotModeratedUrl: () => `${API_URL}${SET_NOT_MODERATED}`,
    setNotTopDiscussionUrl: () => `${API_URL}${SET_NOT_TOP_DISCUSSION}`,
    deleteCommentUrl: () => `${API_URL}${DELETE_COMMENT}`,
    commentListUrl: () => `${API_URL}${COMMENT_LIST}`,
    holywarCandidatesUrl: () => `${API_URL}${HOLYWAR_CANDIDATES}`,
    holywarApproveUrl: () => `${API_URL}${HOLYWAR_APPROVE}`,
    holywarDeclineUrl: () => `${API_URL}${HOLYWAR_DECLINE}`,
    commentLikesUrl: (commentId: string) =>
      `${API_URL}${COMMENT_LIKES}${commentId}`,
    userLikesUrl: (userId: string) => `${API_URL}${USER_LIKES}${userId}`
  }
}
