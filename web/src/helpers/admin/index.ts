import { useDispatch, useSelector } from 'react-redux'

import { setAdmin, addRelogin } from 'state'

interface AdminResult {
  isAdmin: boolean
  setAdmin: (value: boolean) => void
  reloginTokens: string[]
  addRelogin: (token: string) => void
}

export function useAdmin(): AdminResult {
  const dispatch = useDispatch()
  const { isAdmin, reloginTokens } = useSelector((state: any) => state.admin)

  function setAdminFunc(value: boolean) {
    dispatch(setAdmin(value))
  }

  function addReloginFunc(token: string) {
    dispatch(addRelogin(token))
  }

  return {
    isAdmin,
    setAdmin: setAdminFunc,
    reloginTokens,
    addRelogin: addReloginFunc
  }
}
