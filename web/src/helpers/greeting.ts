import { restoreAuthToken } from './auth'

const GREETING_KEY = 'GREETING_DISPLAYED'

export function isGreetingDisplayed() {
  const auth = restoreAuthToken()
  if (auth) {
    setGreetingDisplayed()
    return true
  }
  if (typeof localStorage === 'undefined') {
    return true
  }
  return localStorage.getItem(GREETING_KEY) === 'TRUE'
}

export function setGreetingDisplayed() {
  if (typeof localStorage === 'undefined') {
    return
  }
  localStorage.setItem(GREETING_KEY, 'TRUE')
}
