import { UserModel } from 'models'
import { useApi } from './api'

import { useAuth } from './auth'
import { useGet } from './fetch'

export const useCurrentUser = (): [UserModel | null, () => void] => {
  const { isAuthenticated } = useAuth()
  const { currentUserUrl } = useApi()
  const currentUserResult = useGet(currentUserUrl())

  if (!isAuthenticated) {
    return [null, () => 0]
  }

  if (!currentUserResult.response) {
    return [null, () => 0]
  }

  return [currentUserResult.response, currentUserResult.reload]
}
