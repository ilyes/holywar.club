export function getQueryParam(name: string): string | null {
  if (typeof window === 'undefined') {
    return null
  }
  const urlParams = new URLSearchParams(window.location.search)
  return urlParams.get(name)
}

export function appendTime(url: string) {
  const delimiter = url.includes('?') ? '&' : '?'
  return `${url}${delimiter}time=${new Date().getTime()}`
}

export function getQuery(params: any): string {
  return Object.entries(params).reduce((result, [key, value]) => {
    if (!value) {
      return result
    }

    if (Array.isArray(value)) {
      return `${result}&${value.map((v, i) => `${key}[${i}]=${v}`).join('&')}`
    }

    return `${result}&${key}=${value}`
  }, '')
}
