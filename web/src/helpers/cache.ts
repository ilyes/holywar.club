import { useDispatch, useSelector } from 'react-redux'

import { saveToCache, removeFromCache } from 'state'
import { useState, useEffect, useCallback, useMemo } from 'react'

const CACHE_LIFETIME_SECONDS = 10 * 60

export interface CacheItem {
  value: any
  date: Date
}

interface Cache {
  [key: string]: CacheItem
}

interface CacheItemResult<T> {
  value: T | undefined
  save: (value: T) => void
  remove: () => void
}

export function useCacheItem<T>(key: string): CacheItemResult<T> {
  const dispatch = useDispatch()
  const cache = useSelector((state: any) => state.cache) as Cache
  const { value } = cache[key] || {}
  const saveToCacheFunc = useCallback(
    (value: T) => {
      dispatch(saveToCache(key, value))
    },
    [dispatch, key]
  )

  const removeFromCacheFunc = useCallback(() => {
    dispatch(removeFromCache(key))
  }, [dispatch, key])

  const result = useMemo(
    () => ({
      value,
      save: saveToCacheFunc,
      remove: removeFromCacheFunc
    }),
    [value, saveToCacheFunc, removeFromCacheFunc]
  )

  return result
}

function getValue<T>(cache: Cache, key: string): T | undefined {
  if (!key) {
    return undefined
  }
  const item = cache[key]
  if (!item) {
    return undefined
  }
  const secondsLeft = (new Date().getTime() - item.date.getTime()) / 1000
  const isExpired = secondsLeft > CACHE_LIFETIME_SECONDS
  if (isExpired) {
    return undefined
  } else {
    return item.value
  }
}
