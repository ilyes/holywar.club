export interface FetchResult {
  loading: boolean
  finished: boolean
  error: any
  response: any
}
