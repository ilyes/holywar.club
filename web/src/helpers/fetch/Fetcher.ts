import { Dictionary } from 'ts-essentials'

export class Fetcher {
  private pending: Dictionary<Promise<object> | undefined> = {}

  get(url: string, options: RequestInit = {}) {
    if (typeof fetch === 'undefined') {
      return Promise.reject(new Error('BrowserNotSupported'))
    }

    const exists = this.pending[url]
    if (exists) {
      return exists
    }

    options.method = 'GET'
    options.headers = Object.assign(
      {
        'Content-Type': 'application/json'
      },
      options.headers
    )

    const promise = new Promise<object>((resolve, reject) => {
      fetch(url, options)
        .then(result => {
          if (result.status !== 200) {
            return { error: `Response error: ${result.status}` }
          }
          if (result.headers.get('Content-Length') === '0') {
            return null
          }
          return result.json()
        })
        .then(json => {
          this.pending[url] = undefined
          if (json && json.error) {
            reject(new Error(json.error))
            return
          }
          resolve(json)
        })
        .catch(error => {
          console.error('fetch error', url, error)
          this.pending[url] = undefined
          reject(new Error(error.message))
        })
    })

    this.pending[url] = promise

    return promise
  }
}
