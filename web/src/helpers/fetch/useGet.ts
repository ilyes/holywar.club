import { useContext, useMemo } from 'react'

import { useAuth } from '../auth'
import { useCacheItem } from '../cache'
import { FetchResult } from './FetchResult'
import { FetchContext } from './FetchContext'
import { useEffectWithPrev } from 'helpers/react'

interface GetResult extends FetchResult {
  reload: () => void
  clear: () => void
}

const defaultValue = {
  response: undefined,
  loading: false,
  finished: false,
  error: undefined
}

export function useGet(url: string, options: any = {}): GetResult {
  const { value, remove, save } = useCacheItem<FetchResult>(url)
  const fetcher = useContext(FetchContext)
  const { token } = useAuth()
  const { response, loading, error, finished } = value || defaultValue

  function load(force: boolean = false) {
    if (!fetcher) {
      return
    }
    if (!url) {
      save({
        loading: false,
        finished: false,
        response: undefined,
        error: undefined
      })
      return
    }
    if (loading) {
      return
    }
    if (!force && (typeof response !== 'undefined' || error)) {
      return
    }

    save({
      loading: true,
      finished: false,
      response: undefined,
      error: undefined
    })

    options.headers = Object.assign({ Auth: token }, options.headers)
    fetcher
      .get(url, options)
      .then((json: any = null) => {
        save({
          loading: false,
          finished: true,
          response: json,
          error: undefined
        })
      })
      .catch((error: Error) => {
        console.error('fetch error', url, error)
        save({
          loading: false,
          finished: true,
          response: undefined,
          error: error.message || 'unknown error'
        })
      })
  }

  function reload() {
    load(true)
  }

  function clear() {
    remove()
  }

  useEffectWithPrev(
    ([prevUrl]) => {
      if (!response && !loading && !error) {
        load()
      } else if (prevUrl !== url) {
        reload()
      }
    },
    [url, response, loading, options, token]
  )

  const result = useMemo(
    () => ({
      reload,
      clear,
      response,
      loading,
      error,
      finished
    }),
    [response, loading, error, finished, url, options, token]
  )

  return result
}
