import { createContext } from 'react'
import { Fetcher } from './Fetcher'

export const FetchContext = createContext<Fetcher | null>(null)
