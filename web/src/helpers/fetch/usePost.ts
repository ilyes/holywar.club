import { useState, useCallback, useMemo } from 'react'

import { useAuth } from '../auth'
import { FetchResult } from './FetchResult'

interface PostResult extends FetchResult {
  request: any
}

type ReturnType = [(data?: any) => void, PostResult]

export const usePost = createSubmitFunction('POST')
export const usePatch = createSubmitFunction('PATCH')

function createSubmitFunction(method: string) {
  return function (url: string, options: any = {}): ReturnType {
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(undefined)
    const [response, setResponse] = useState(undefined)
    const [request, setRequest] = useState(undefined)
    const { token } = useAuth()

    function submit(data: any) {
      if (typeof fetch === 'undefined') {
        return
      }
      setLoading(true)
      setRequest(data)
      options.method = method
      options.body = JSON.stringify(data)
      options.headers = Object.assign(
        {
          'Content-Type': 'application/json',
          Auth: token
        },
        options.headers
      )
      fetch(url, options)
        .then((result) => result.json())
        .then((json) => {
          if (json.error) {
            setResponse(undefined)
            setError(json.error)
            setLoading(false)
            return
          }
          setResponse(json)
          setError(undefined)
          setLoading(false)
        })
        .catch((error) => {
          console.error('fetch error', error)
          setResponse(undefined)
          setError(error.message)
          setLoading(false)
        })
    }

    const result: ReturnType = useMemo(
      () => [
        submit,
        {
          response,
          loading,
          error,
          request,
          finished: Boolean(response || error)
        }
      ],
      [response, loading, error, request, token, url, options]
    )

    return result
  }
}
