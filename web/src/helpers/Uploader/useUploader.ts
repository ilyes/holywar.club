import { useContext, useState } from 'react'

import { UploaderContext } from './UploaderContext'

export interface UseUploaderUpload {
  (url: string, file: File): void
}

interface UseUploaderState {
  result: any
  error: any
  progress: number
  pending: boolean
}

export const useUploader = (): [UseUploaderUpload, UseUploaderState] => {
  const { upload } = useContext(UploaderContext)
  const [state, setState] = useState({
    result: undefined,
    error: undefined,
    progress: 0,
    pending: false
  })

  function uploadInternal(url: string, file: File) {
    const uploading = upload(url, file)
    const unsubscribeSuccess = uploading.subscribeSuccess(
      ({ result, error, progress, pending }: any) =>
        setState({ result, error, progress, pending })
    )
    const unsubscribeError = uploading.subscribeError(
      ({ result, error, progress, pending }: any) =>
        setState({ result, error, progress, pending })
    )
    const unsubscribeProgress = uploading.subscribeProgress(
      ({ result, error, progress, pending }: any) =>
        setState({ result, error, progress, pending })
    )
    const unsubscribeFinished = uploading.subscribeFinished(() => {
      unsubscribeSuccess()
      unsubscribeError()
      unsubscribeProgress()
      unsubscribeFinished()
    })
  }

  return [uploadInternal, state]
}
