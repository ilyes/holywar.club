import React from 'react'

import { Uploading } from './Uploading'
import { UploaderContext } from './UploaderContext'

import { useAuth } from '../auth'

interface UploaderProviderProps {
  children: any
}

export const UploaderProvider = ({ children }: UploaderProviderProps) => {
  const uploadings = []
  const { token } = useAuth()

  function upload(url: string, file: File): Uploading {
    const uploading = new Uploading(async ({ complete, failed }) => {
      try {
        var formData = new FormData()
        formData.append('file', file)
        const options = {
          method: 'POST',
          body: formData,
          headers: {
            Auth: token
          }
        }
        const result = await fetch(url, options)
        const response = await result.json()
        complete(response)
      } catch (error) {
        failed(error.message)
      }
    })
    uploadings.push(uploading)
    return uploading
  }

  const value = {
    upload
  }

  return (
    <UploaderContext.Provider value={value}>
      {children}
    </UploaderContext.Provider>
  )
}
