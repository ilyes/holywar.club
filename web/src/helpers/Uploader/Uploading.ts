import EventEmitter from 'events'

const FINISHED_EVENT_NAME = 'finished'
const SUCCESS_EVENT_NAME = 'success'
const ERROR_EVENT_NAME = 'error'
const PROGRESS_EVENT_NAME = 'progress'

interface UploadingCallbacks {
  (callbacks: {
    complete: (result: any) => void
    failed: (error: any) => void
    progress: (progress: number) => void
  }): void
}

export class Uploading {
  result = undefined
  error = undefined
  pending = true
  progress = 0

  _emitter = new EventEmitter()

  constructor(callbacks: UploadingCallbacks) {
    callbacks({
      complete: (result: any) => {
        this.result = result
        this.pending = false
        this.progress = 1
        this._emitter.emit(SUCCESS_EVENT_NAME, this)
        this._emitter.emit(FINISHED_EVENT_NAME, this)
      },
      failed: (error: any) => {
        this.error = error
        this.pending = false
        this.progress = 0
        this._emitter.emit(ERROR_EVENT_NAME, this)
        this._emitter.emit(FINISHED_EVENT_NAME, this)
      },
      progress: (progress: number) => {
        this.progress = progress
        this._emitter.emit(PROGRESS_EVENT_NAME, this)
      }
    })
  }

  subscribeFinished = (callback: (...args: any[]) => void) =>
    this._subscribe(FINISHED_EVENT_NAME, callback)
  unsubscribeFinished = (callback: (...args: any[]) => void) =>
    this._unsubscribe(FINISHED_EVENT_NAME, callback)

  subscribeSuccess = (callback: (...args: any[]) => void) =>
    this._subscribe(SUCCESS_EVENT_NAME, callback)
  unsubscribeSuccess = (callback: (...args: any[]) => void) =>
    this._unsubscribe(SUCCESS_EVENT_NAME, callback)

  subscribeError = (callback: (...args: any[]) => void) =>
    this._subscribe(ERROR_EVENT_NAME, callback)
  unsubscribeError = (callback: (...args: any[]) => void) =>
    this._unsubscribe(ERROR_EVENT_NAME, callback)

  subscribeProgress = (callback: (...args: any[]) => void) =>
    this._subscribe(PROGRESS_EVENT_NAME, callback)
  unsubscribeProgress = (callback: (...args: any[]) => void) =>
    this._unsubscribe(PROGRESS_EVENT_NAME, callback)

  _subscribe = (eventName: string, callback: (...args: any[]) => void) => {
    this._emitter.addListener(eventName, callback)
    return () => this._unsubscribe(eventName, callback)
  }

  _unsubscribe = (eventName: string, callback: (...args: any[]) => void) =>
    this._emitter.removeListener(eventName, callback)
}
