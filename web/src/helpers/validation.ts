export const composeValidators = (...validators: Function[]) => (
  value: string
) =>
  validators.reduce((error, validator) => error || validator(value), undefined)

export function validateUrl(value: string): boolean | string {
  if (!value) {
    return false
  }
  return (
    !/^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(
      value
    ) && 'Неверный формат ссылки'
  )
}

export function validatePhone(value: string) {
  if (!value) {
    return false
  }
  return (
    !/^[0-9\u0020()/+-]*$/.test(value) && 'Введите корректный номер телефона'
  )
}

export function validatePassword(value: string) {
  if (!value) {
    return 'Введите пароль'
  }
  if (value.length < 8) {
    return 'Пароль должен содержать как минимум 8 символов'
  }
  if (value.length > 100) {
    return 'Пароль должен содержать не более 100 символов'
  }
  if (!/^[-@^\\*!&/\\#,+()$~%.'":*?<>{}_A-Za-z\d]+$/.test(value)) {
    return 'Пароль может состоять только из латинских букв и цифр'
  }
  if (!/^(?=.*[A-Z])/.test(value)) {
    return 'Пароль должен содержать как минимум одну заглавную букву'
  }
  if (!/^(?=.*[a-z])/.test(value)) {
    return 'Пароль должен содержать как минимум одну прописную букву'
  }
  if (!/^(?=.*\d)/.test(value)) {
    return 'Пароль должен содержать как минимум одну цифру'
  }
  return undefined
}

export function validateEmail(value: string) {
  if (!value) {
    return 'Введите e-mail'
  }
  if (
    !/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      value
    )
  ) {
    return 'Введите корректный  e-mail'
  }
  return undefined
}

export const required = (errorMessage: string) => (value: string) =>
  !value ? errorMessage : null
