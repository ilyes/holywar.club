export function mapErrorMessage(error: any, response: any) {
  if (error) {
    if (typeof error === 'string') {
      return error
    }
    if (typeof error.message === 'string') {
      return error.message
    }
  }
  if (response && response.error) {
    return response.error
  }
}

export function mapFormErrors(
  errors: any,
  error: string,
  touched: any
): string[] {
  const result = Object.keys(errors)
    .map(key => (touched[key] && errors[key] ? errors[key] : undefined))
    .filter(e => !!e)
  if (typeof error === 'string' && error.length > 0) {
    result.push(error)
  }
  return result
}
