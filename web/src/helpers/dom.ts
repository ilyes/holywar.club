export function ready(callback: Function) {
  const d = document as any
  if (d.readyState != 'loading') callback()
  else if (d.addEventListener) d.addEventListener('DOMContentLoaded', callback)
  else if (d.attachEvent)
    d.attachEvent('onreadystatechange', function() {
      if (d.readyState == 'complete') callback()
    })
}
