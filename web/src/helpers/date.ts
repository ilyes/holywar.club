const monthNames = [
  'янв',
  'фев',
  'мар',
  'апр',
  'мая',
  'июн',
  'июл',
  'авг',
  'сен',
  'окт',
  'ноя',
  'дек'
]

export function formatDateString(str: string) {
  if (!str) {
    return ''
  }

  const date = parseDate(str)
  const datePart = determineDatePart(date)
  const timePart = getTimePart(date)
  return `${datePart} ${timePart}`
}

export function parseDate(str: string, addOffset: boolean = true) {
  const date = new Date(str)
  if (addOffset === true) {
    const timeOffset = getTimeoffset()
    date.setTime(date.getTime() - timeOffset)
  }
  return date
}

export function getTimeoffset() {
  return new Date().getTimezoneOffset() * 60000
}

function determineDatePart(date: Date) {
  const days = daysLeft(date, new Date())
  if (days < 1) return 'сегодня'
  if (days < 2) return 'вчера'
  const monthName = monthNames[date.getMonth()]
  return `${date.getDate()} ${monthName}`
}

function daysLeft(firstDate: any, secondDate: any): number {
  const oneDay = 24 * 60 * 60 * 1000 // hours*minutes*seconds*milliseconds
  return Math.round(Math.abs((firstDate - secondDate) / oneDay))
}

function getTimePart(date: Date) {
  return `${tryAddZero(date.getHours())}:${tryAddZero(date.getMinutes())}`
}

function tryAddZero(num: number): string {
  const result = num.toString()
  if (result.length === 2) return result
  return '0' + result
}
