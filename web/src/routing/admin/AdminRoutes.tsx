import React from 'react'

import { Switch } from 'react-router-dom'
import { AppRoute } from 'routing/AppRoute'
import { AdminPaths } from './AdminPaths'

import * as Pages from 'pages/admin'

const AdminRoutes = () => {
  return (
    <Switch>
      <AppRoute
        exact
        path={AdminPaths.Comments}
        component={Pages.AdminComments}
        admin
      />
      <AppRoute
        exact
        path={AdminPaths.Holywars}
        component={Pages.AdminHolywars}
        admin
      />
      <AppRoute
        exact
        path={AdminPaths.Users}
        component={Pages.AdminUsers}
        admin
      />
      <AppRoute
        exact
        path={AdminPaths.CommentLikes()}
        component={Pages.AdminCommentLikes}
        admin
      />
      <AppRoute
        exact
        path={AdminPaths.UserLikes()}
        component={Pages.AdminUserLikes}
        admin
      />
    </Switch>
  )
}

export default AdminRoutes
