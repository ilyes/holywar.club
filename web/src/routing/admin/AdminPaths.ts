export const AdminPaths = {
  Root: '/admin',
  AddBallance: '/admin/add-balance',
  Payments: '/admin/payments',
  Comments: '/admin/comments',
  Holywars: '/admin/holywar',
  Users: '/admin/users',
  CommentLikes: (commentId?: string) =>
    `/admin/comment-likes/${commentId || ':id'}`,
  UserLikes: (userId?: string) => `/admin/user-likes/${userId || ':id'}`
}
