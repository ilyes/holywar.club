import React, { Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'

import * as Pages from 'pages'

import { AppRoute } from './AppRoute'
import { Paths } from './Paths'
import { AdminPaths } from './admin/AdminPaths'
import { Spinner } from 'components/Spinner/Spinner'

const AdminRoutes = React.lazy(() => import('./admin/AdminRoutes'))

export const Routes = () => (
  <Switch>
    <AppRoute exact path={Paths.Home} component={Pages.Home} anonymous />
    <AppRoute
      exact
      path={Paths.Last}
      component={Pages.LastDiscussions}
      anonymous
    />
    <AppRoute exact path={Paths.Faq} component={Pages.Faq} anonymous />
    <AppRoute exact path={Paths.SignIn} component={Pages.SignIn} anonymous />
    <AppRoute exact path={Paths.SignUp} component={Pages.SignUp} anonymous />
    <AppRoute
      exact
      path={Paths.SignUpConfirm}
      component={Pages.SignUpConfirm}
      anonymous
    />
    <AppRoute
      exact
      path={Paths.SignUpConfirmed}
      component={Pages.SignUpConfirmed}
      anonymous
    />
    <AppRoute
      exact
      path={Paths.PasswordRecover}
      component={Pages.RecoverPassword}
      anonymous
    />
    <AppRoute
      exact
      path={Paths.SetPasswordByToken}
      component={Pages.SetPasswordByToken}
      anonymous
    />
    <AppRoute exact path={Paths.LinkCreate} component={Pages.LinkCreate} />
    <AppRoute
      exact
      path={Paths.ArticleCreate}
      component={Pages.ArticleCreate}
    />
    <AppRoute
      exact
      path={Paths.DiscussionEdit}
      component={Pages.DiscussionEdit}
    />
    <AppRoute
      exact
      path={Paths.Discussion}
      component={Pages.Discussion}
      anonymous
    />
    <AppRoute
      exact
      path={Paths.DiscussionsByCategory}
      component={Pages.DiscussionsByCategory}
      anonymous
    />
    <AppRoute
      exact
      path={Paths.EternalDiscussions}
      component={Pages.EternalDiscussions}
      anonymous
    />
    <AppRoute
      exact
      path={Paths.Discussions}
      component={Pages.Discussions}
      anonymous
    />
    <AppRoute
      exact
      path={Paths.CurrentUserProfile}
      component={Pages.CurrentUserProfile}
    />
    <AppRoute exact path={Paths.Profile} component={Pages.Profile} anonymous />
    <AppRoute exact path={Paths.Ratings} component={Pages.ByRating} anonymous />
    <AppRoute
      exact
      path={Paths.RatingsComments}
      component={Pages.WeekComments}
      anonymous
    />
    {/* <AppRoute exact path={Paths.Partner} component={Pages.Partner} /> */}
    {/* <AppRoute
      exact
      path={Paths.RatingsByDislikes}
      component={Pages.ByDislikes}
      anonymous
    />
    <AppRoute
      exact
      path={Paths.RatingsByReferals}
      component={Pages.ByReferals}
      anonymous
    /> */}
    <AppRoute exact path={Paths.Policy} component={Pages.Policy} anonymous />
    <Route
      path={AdminPaths.Root}
      render={() => (
        <Suspense fallback={<Spinner />}>
          <AdminRoutes />
        </Suspense>
      )}
    />
  </Switch>
)
