import React, { useEffect } from 'react'
import { Redirect, Route } from 'react-router-dom'
import ReactGA from 'react-ga'

import { Paths } from './Paths'
import { useAuth, useCurrentUser, useEnv } from 'helpers'
import { UserRole } from 'models'

interface AppRouteProps {
  anonymous?: boolean
  admin?: boolean
  exact?: boolean
  path: string
  component: any
}

export const AppRoute = ({
  anonymous,
  admin,
  component: Component,
  path,
  ...rest
}: AppRouteProps) => {
  const env = useEnv()
  const { isAuthenticated } = useAuth()
  const [user] = useCurrentUser()

  useEffect(() => sendPageview(), [])

  if (!isAuthenticated && !anonymous) {
    if (env.IS_NODE) {
      return <Route component={() => null} path={path} {...rest} />
    }
    return <Redirect to={Paths.SignIn} />
  }
  if (admin && (!user || user.role !== UserRole.admin)) {
    if (env.IS_NODE) {
      return <Route component={() => null} path={path} {...rest} />
    }
    return <Redirect to={Paths.SignIn} />
  }

  return <Route component={Component} path={path} {...rest} />
}

let prevPath = ''

function sendPageview() {
  if (typeof window === 'undefined') {
    return
  }
  const path = window.location.pathname + window.location.search
  if (prevPath === path) {
    return
  }
  prevPath = path
  const w = window as any
  try {
    ReactGA.pageview(path)
  } catch (error) {
    console.error(error)
  }
}
