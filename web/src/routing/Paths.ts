export const Paths = {
  Home: '/',
  Last: '/new',
  Policy: '/policy',
  Faq: '/faq',
  Rules: '/faq#rules',
  Limits: '/faq#limits',

  SignIn: '/auth/signin',
  GetAbsoluteSignInUrl: () => `${window.location.origin}/auth/signin`,
  SignUp: '/auth/signup',
  SignUpConfirm: '/auth/signup/confirm',
  SignUpConfirmed: '/auth/signup/confirmed',
  PasswordRecover: '/auth/recover',
  SetPasswordByToken: '/auth/set-password',
  GetAbsoluteReferralUrl: (referral: string) =>
    `${window.location.origin}/about.html?referral=${referral}`,

  LinkCreate: '/discussion/create/link',
  ArticleCreate: '/discussion/create/article',
  Discussion: '/discussion/:id',
  GetDiscussion: (id: string, commentId?: string) =>
    `/discussion/${id}${commentId ? '#comment=' + commentId : ''}`,
  GetShareDiscussionUrl: (discussionId: string, referral: string) =>
    `${window.location.origin}/discussion/${discussionId}&referral=${referral}`,
  Discussions: '/discussions/:id',
  GetDiscussions: (id: string) => `/discussions/${id}`,
  DiscussionEdit: '/discussion/:id/edit',
  GetDiscussionEdit: (id: string) => `/discussion/${id}/edit`,
  DiscussionsByCategory: '/discussions/by-category/:id/:afterDiscussionId?',
  GetDiscussionsByCategory: (id: string, afterDiscussionId: string = '') =>
    `/discussions/by-category/${id}${
      afterDiscussionId ? '/' + afterDiscussionId : ''
    }`,
  EternalDiscussions: '/discussions/eternal/:afterDiscussionId?',
  GetEternalDiscussions: (afterDiscussionId: string = '') =>
    `/discussions/eternal${afterDiscussionId ? '/' + afterDiscussionId : ''}`,

  Profile: '/profile/:id/:afterDiscussionId?',
  GetProfile: (id: string = '', afterDiscussionId: string = '') =>
    `/profile/${id}${afterDiscussionId ? '/' + afterDiscussionId : ''}`,
  CurrentUserProfile: '/profile',
  Partner: '/partner',

  Ratings: '/ratings',
  RatingsByLikes: '/ratings/by-likes',
  RatingsByDislikes: '/ratings/by-dislikes',
  RatingsByReferals: '/ratings/by-referals',
  RatingsComments: '/ratings/comments',
  GetRatingsComments: (weekId?: number) =>
    '/ratings/comments' + (weekId ? `?weekId=${weekId}` : '')
}
