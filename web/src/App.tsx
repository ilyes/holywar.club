import React, { Component, ReactNode } from 'react'
import { Provider as ReduxProvider } from 'react-redux'
import { createStore } from 'redux'
import ReactGA from 'react-ga'
import { Dictionary } from 'ts-essentials'

import { reducers } from 'state'
import { Routes } from 'routing'
import {
  UploaderProvider,
  FetchContext,
  Fetcher,
  EnvContext,
  EnvObject,
  defaultEnv,
  ConfigContext,
  defaultConfig,
  CacheItem
} from 'helpers'
import { ConfigModel } from 'models'
import { ModalProvider } from 'components/Modal'
import { AuthContainer } from 'containers/user/AuthContainer'

interface AppProps {
  preloadState?: [string, object | null][]
  env?: EnvObject
  config?: ConfigModel
  greeting?: ReactNode
}

interface AppState {
  store: any
  fetcher: Fetcher
}

export class App extends Component<AppProps, AppState> {
  constructor(props: AppProps) {
    super(props)
    const { preloadState } = this.props
    const cache = mapPreloadToCache(preloadState)
    const store = createStore(reducers, { cache })
    const fetcher = new Fetcher()
    this.state = { store, fetcher }
  }

  componentDidMount() {
    ReactGA.initialize('UA-2834558-14')
  }

  componentDidCatch(message: any, { componentStack }: any) {
    console.error(message)
    const w = window as any
    if (w?.log) {
      w.log.error(message.toString(), { stack: componentStack })
    }
  }

  render() {
    const { env = defaultEnv, config = defaultConfig, greeting } = this.props
    const { store, fetcher } = this.state
    return (
      <EnvContext.Provider value={env}>
        <ConfigContext.Provider value={config}>
          <ReduxProvider store={store}>
            <FetchContext.Provider value={fetcher}>
              <UploaderProvider>
                <ModalProvider>
                  <AuthContainer />
                  <Routes />
                  {greeting}
                </ModalProvider>
              </UploaderProvider>
            </FetchContext.Provider>
          </ReduxProvider>
        </ConfigContext.Provider>
      </EnvContext.Provider>
    )
  }
}

function mapPreloadToCache(
  preload?: [string, object | null][]
): Dictionary<CacheItem> {
  if (!preload) {
    return {}
  }
  return preload.reduce<Dictionary<CacheItem>>((r, [url, response]: any) => {
    r[url] = { value: { response }, date: new Date() }
    return r
  }, {})
}
