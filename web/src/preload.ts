import { getApiUrls } from 'helpers/api'
import { matchPath } from 'react-router-dom'

import { Paths } from 'routing'

type ApiUrls = ReturnType<typeof getApiUrls>

interface PreloadUrlsCheck {
  (apiUrls: ApiUrls, url: string): string[] | null
}

const homePreloadUrlsCheck = (
  { popularDiscussionsUrl, featuredDiscussionsUrl, categoriesUrl }: ApiUrls,
  url: string
) =>
  matchPath<any>(url, {
    path: Paths.Home,
    exact: true
  })
    ? [popularDiscussionsUrl(), featuredDiscussionsUrl(), categoriesUrl()]
    : null

const lastPreloadUrlsCheck = (
  { discussionsUrl, categoriesUrl }: ApiUrls,
  url: string
) =>
  matchPath<any>(url, {
    path: Paths.Last,
    exact: true
  })
    ? [discussionsUrl(), categoriesUrl()]
    : null

const discussionsPreloadUrlsCheck = (
  { discussionsUrl }: ApiUrls,
  url: string
) => {
  const match = matchPath<any>(url, {
    path: Paths.Discussions,
    exact: true
  })
  if (!match) {
    return null
  }
  const { id } = match.params
  return [discussionsUrl({ afterDiscussionId: id })]
}

const discussionPreloadUrlsCheck = (
  { discussionUrl }: ApiUrls,
  url: string
) => {
  const match = matchPath<any>(url, {
    path: Paths.Discussion,
    exact: true
  })
  if (!match) {
    return null
  }
  const { id } = match.params
  return [discussionUrl(id)]
}

const profilePreloadUrlsCheck = (
  { discussionsUrl, userUrl }: ApiUrls,
  url: string
) => {
  const match = matchPath<any>(url, {
    path: Paths.Profile,
    exact: true
  })
  if (!match) {
    return null
  }
  const { id, afterDiscussionId } = match.params
  return [discussionsUrl({ userId: id, afterDiscussionId }), userUrl(id)]
}

const discussionsByCategoryPreloadUrlsCheck = (
  { categoriesUrl, discussionsUrl }: ApiUrls,
  url: string
) => {
  const match = matchPath<any>(url, {
    path: Paths.DiscussionsByCategory,
    exact: true
  })
  if (!match) {
    return null
  }
  const { id, afterDiscussionId } = match.params
  return [
    categoriesUrl(),
    discussionsUrl({ afterDiscussionId, categoryIds: [id] })
  ]
}

const eternalDiscussionsPreloadUrlsCheck = (
  { eternalDiscussionsUrl }: ApiUrls,
  url: string
) => {
  const match = matchPath<any>(url, {
    path: Paths.EternalDiscussions,
    exact: true
  })
  if (!match) {
    return null
  }
  const { afterDiscussionId } = match.params
  return [eternalDiscussionsUrl({ afterDiscussionId })]
}

const urlsMap: PreloadUrlsCheck[] = [
  homePreloadUrlsCheck,
  lastPreloadUrlsCheck,
  profilePreloadUrlsCheck,
  eternalDiscussionsPreloadUrlsCheck,
  discussionsByCategoryPreloadUrlsCheck,
  discussionsPreloadUrlsCheck,
  discussionPreloadUrlsCheck
]

export function getPreloadUrls(apiUrl: string, url: string): string[] {
  let preloadUrls: string[] | null
  const apiUrls = getApiUrls(apiUrl)
  for (let i = 0; i < urlsMap.length; i++) {
    preloadUrls = urlsMap[i].call(null, apiUrls, url)
    if (preloadUrls) {
      return preloadUrls
    }
  }
  return []
}
