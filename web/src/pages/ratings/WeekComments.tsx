import React from 'react'
import { useGet } from 'helpers'
import { ContentPageLayout, OneColumnLayout, Placeholder, Panel } from 'layouts'
import { H1 } from 'components'
import { Link, withRouter } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import { Spinner } from 'components/Spinner/Spinner'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { useApi } from 'helpers/api'
import { CommentWithUserModel } from 'models'
import { Paths } from 'routing'
import { CommentContainer } from 'containers'

export const WeekComments = withRouter(({ history }) => {
  const weekId = Number(
    new URLSearchParams(history.location.search).get('weekId')
  )
  const { ratingCommentsOfTheWeekUrl, commonCurrentWeekUrl } = useApi()
  const currentWeekResult = useGet(commonCurrentWeekUrl())
  const { loading, error, response } = useGet(
    ratingCommentsOfTheWeekUrl({ limit: 100, weekId: weekId || undefined })
  )

  const canMoveForward = weekId && currentWeekResult.response > weekId

  return (
    <ContentPageLayout>
      <Helmet title="Комменты недели на Holywar.club" />
      <OneColumnLayout>
        <H1>Комменты недели</H1>
        <Panel alignItems="center" horizontal justifyContent="center">
          <Link
            to={Paths.GetRatingsComments(
              Number(weekId || currentWeekResult.response) - 1
            )}
          >
            Прошлая неделя
          </Link>
          |
          <Link
            to={Paths.GetRatingsComments(
              Number(weekId || currentWeekResult.response) + 1
            )}
            style={{
              pointerEvents: canMoveForward ? undefined : 'none',
              color: canMoveForward ? undefined : 'gray'
            }}
          >
            Следуюущая неделя
          </Link>
        </Panel>
        {loading ? (
          <Panel alignItems="center" justifyContent="center">
            <Spinner />
          </Panel>
        ) : null}
        {error ? (
          <Panel alignItems="center" justifyContent="center">
            <ErrorMessage error={error} />
          </Panel>
        ) : null}
        {response ? (
          <Panel alignItems="center" justifyContent="center" gap="m">
            {response.map((c: CommentWithUserModel) => (
              <CommentContainer commentWithUser={c} withLink />
            ))}
          </Panel>
        ) : null}
        <Placeholder />
        {/* <AdSquare /> */}
      </OneColumnLayout>
    </ContentPageLayout>
  )
})
