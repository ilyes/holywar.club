import React from 'react'
import { useGet } from 'helpers'
import {
  ContentPageLayout,
  OneColumnLayout,
  Subnav,
  Placeholder
} from 'layouts'
import { H1, H3 } from 'components'
import { UserCardList } from 'composite'
import { Link } from 'react-router-dom'
import { Paths } from 'routing'
import { Helmet } from 'react-helmet'
import { Spinner } from 'components/Spinner/Spinner'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { useApi } from 'helpers/api'

export const ByDislikes = () => {
  const { ratingByDislikesUrl } = useApi()
  const { loading, error, response } = useGet(ratingByDislikesUrl())
  return (
    <ContentPageLayout>
      <Helmet title="Рейтинг по дизлайкам за неделю на Holywar.club" />
      {/* <AdHorisontal /> */}
      <OneColumnLayout>
        <H1>Рейтинг по дизлайкам</H1>
        <Subnav>
          <Link to={Paths.RatingsByLikes}>Рейтинг по лайкам</Link>
          <Link to={Paths.RatingsByReferals}>Рейтинг по рефералам</Link>
        </Subnav>
        {loading ? <Spinner /> : null}
        {error ? <ErrorMessage error={error} /> : null}
        {response ? <UserCardList users={response} /> : null}
        <Placeholder />
        {/* <AdSquare /> */}
      </OneColumnLayout>
    </ContentPageLayout>
  )
}
