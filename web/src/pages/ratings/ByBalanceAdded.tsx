import React from 'react'
import { useGet } from 'helpers'
import {
  ContentPageLayout,
  OneColumnLayout,
  Subnav,
  Placeholder
} from 'layouts'
import { H1, H3 } from 'components'
import { UserCardList } from 'composite'
import { Link } from 'react-router-dom'
import { Paths } from 'routing'
import { UserModel } from 'models'
import { Helmet } from 'react-helmet'
import { Spinner } from 'components/Spinner/Spinner'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { useApi } from 'helpers/api'

export const ByBalanceAdded = () => {
  const { ratingByBalanceAddedUrl } = useApi()
  const { loading, error, response } = useGet(ratingByBalanceAddedUrl())
  const users = response
    ? response.map(({ paid, ...rest }: UserModel) => ({
        ...rest,
        balance: paid
      }))
    : []
  return (
    <ContentPageLayout>
      <Helmet title="Рейтинг по начислениям за прошлую неделю на Holywar.club" />
      {/* <AdHorisontal /> */}
      <OneColumnLayout>
        <H1>Рейтинг</H1>
        <H3>по начислениям за прошлую неделю</H3>
        <Subnav>
          <Link to={Paths.RatingsByLikes}>Рейтинг по лайкам</Link>
          <Link to={Paths.RatingsByDislikes}>Рейтинг по дизлайкам</Link>
          <Link to={Paths.RatingsByReferals}>Рейтинг по рефералам</Link>
        </Subnav>
        {loading ? <Spinner /> : null}
        {error ? <ErrorMessage error={error} /> : null}
        {response ? <UserCardList users={users} /> : null}
        <Placeholder />
        {/* <AdSquare /> */}
      </OneColumnLayout>
    </ContentPageLayout>
  )
}
