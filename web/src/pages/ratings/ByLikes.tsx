import React from 'react'
import { useGet } from 'helpers'
import { ContentPageLayout, OneColumnLayout, Placeholder } from 'layouts'
import { H1 } from 'components'
import { UserCardList } from 'composite'
import { Helmet } from 'react-helmet'
import { Spinner } from 'components/Spinner/Spinner'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { useApi } from 'helpers/api'

export const ByLikes = () => {
  const { ratingByLikesUrl } = useApi()
  const { loading, error, response } = useGet(ratingByLikesUrl())
  return (
    <ContentPageLayout>
      <Helmet title="Рейтинг по лайкам за неделю на Holywar.club" />
      {/* <AdHorisontal /> */}
      <OneColumnLayout>
        <H1>По лайкам</H1>
        {loading ? <Spinner /> : null}
        {error ? <ErrorMessage error={error} /> : null}
        {response ? <UserCardList users={response} /> : null}
        <Placeholder />
        {/* <AdSquare /> */}
      </OneColumnLayout>
    </ContentPageLayout>
  )
}
