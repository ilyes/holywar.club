import React from 'react'
import { withRouter } from 'react-router'

import { HomePageLayout } from 'layouts'
import { Paths } from 'routing'
import { H1 } from 'components'
import { Helmet } from 'react-helmet'
import { InfiniteDiscussionsListContainer } from 'containers'
import { useGet } from 'helpers'
import { CategoryModel } from 'models'
import { useApi } from 'helpers/api'

export const DiscussionsByCategory = withRouter(({ match }) => {
  const { id, afterDiscussionId } = match.params
  const { categoriesUrl, discussionsUrl } = useApi()
  const { response } = useGet(categoriesUrl())
  const category = response
    ? response.find((c: CategoryModel) => c.id === id)
    : undefined
  return (
    <HomePageLayout title={category ? category.name : 'Холивары по категории'}>
      <Helmet
        title={`Холивары по категории ${
          category ? category.name : ''
        } на Holywar.club`}
      />
      <InfiniteDiscussionsListContainer
        fromId={afterDiscussionId}
        apiUrlCreator={(afterDiscussionId: string) =>
          discussionsUrl({ categoryIds: [id], afterDiscussionId })
        }
        pathUrlCreator={(discussionId: string) =>
          Paths.GetDiscussionsByCategory(id, discussionId)
        }
        cacheName={`DISCUSSIONS_BY_CATEGORY_${id}`}
      />
    </HomePageLayout>
  )
})
