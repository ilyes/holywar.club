import React from 'react'

import { HomePageLayout } from 'layouts'
import { H1 } from 'components'
import { Helmet } from 'react-helmet'
import { LastDiscussionsContainer } from 'containers'

export const LastDiscussions = () => {
  return (
    <HomePageLayout title="Новые холивары">
      <Helmet title={`Новые холивары на Holywar.club`} />
      <LastDiscussionsContainer />
    </HomePageLayout>
  )
}
