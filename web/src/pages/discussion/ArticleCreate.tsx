import React, { useState, useEffect } from 'react'
import { FormPageLayout } from 'layouts'
import { Link } from 'react-router-dom'
import { Paths } from 'routing'
import { DiscussionCardForm } from 'composite'
import { usePost, useGet } from 'helpers'
import { Spinner } from 'components/Spinner/Spinner'
import { useApi } from 'helpers/api'

export const ArticleCreate = () => {
  const {
    createDiscussionUrl,
    categoriesUrl,
    discussionsUrl,
    popularDiscussionsUrl
  } = useApi()
  const [submit, { loading, error, response }] = usePost(createDiscussionUrl())
  const categories = useGet(categoriesUrl())
  const [discussionId, setDiscussionId] = useState('')
  const { clear: clearLast } = useGet(discussionsUrl())
  const { clear: clearPopular } = useGet(popularDiscussionsUrl())

  useEffect(() => {
    if (response && !error) {
      const { discussionId } = response
      clearLast()
      clearPopular()
      setDiscussionId(discussionId)
    }
    // eslint-disable-next-line
  }, [response, error])

  if (discussionId) {
    return (
      <FormPageLayout>
        <div style={{ minHeight: '40vh', marginBottom: '30px' }}>
          <h1>Холивар начат</h1>
          <p>Отлично! Теперь вы можете написать первый коментарий</p>
          <p>
            <Link to={Paths.GetDiscussion(discussionId)}>
              Перейти к холивару
            </Link>
          </p>
        </div>
      </FormPageLayout>
    )
  }

  return (
    <FormPageLayout>
      <h1>Написать статью</h1>
      <p>
        <Link to={Paths.LinkCreate}>Опубликовать ссылку</Link>
      </p>
      {categories.response ? (
        <DiscussionCardForm
          categories={categories.response}
          loading={loading}
          error={error}
          onSubmit={submit}
        />
      ) : (
        <Spinner />
      )}
    </FormPageLayout>
  )
}
