import React from 'react'
import { withRouter } from 'react-router'

import { ContentPageLayout } from 'layouts'
import { Paths } from 'routing'
import { H1 } from 'components'
import { Helmet } from 'react-helmet'
import { InfiniteDiscussionsListContainer } from 'containers'
import { useApi } from 'helpers/api'

export const Discussions = withRouter(({ match }) => {
  const { id } = match.params
  const { discussionsUrl } = useApi()
  return (
    <ContentPageLayout>
      <Helmet title="Все холивары на Holywar.club" />
      <H1>Холивары</H1>
      <InfiniteDiscussionsListContainer
        fromId={id}
        apiUrlCreator={(id) => discussionsUrl({ afterDiscussionId: id })}
        pathUrlCreator={Paths.GetDiscussions}
        cacheName="DISCUSSIONS_PAGE"
      />
    </ContentPageLayout>
  )
})
