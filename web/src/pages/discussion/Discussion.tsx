import React, { useState, useEffect, Suspense } from 'react'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import Helmet from 'react-helmet'

import { ContentPageLayout, OneColumnLayout, Placeholder, Panel } from 'layouts'
import {
  usePost,
  useGet,
  useAuth,
  useEnv,
  usePatch,
  parseDate,
  useCurrentUser
} from 'helpers'
import { H2 } from 'components'
import {
  DiscussionDetails,
  CreateMessageForm,
  DiscussionCardList,
  DiscussionSharePanel
} from 'composite'
import {
  CommentListContainer,
  Share,
  DiscussionCardContainer,
  SurveyContainer
} from 'containers'
import { Paths } from 'routing'
import {
  UserRole,
  DiscussionModel,
  CommentWithUserModel,
  CategoryModel,
  UserModel
} from 'models'
import { Spinner } from 'components/Spinner/Spinner'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { useApi } from 'helpers/api'

const AdminDiscussionContainer = React.lazy(
  () => import('containers/admin/AdminDiscussionContainer')
)

export const Discussion = withRouter(({ match }) => {
  const env = useEnv()
  const {
    discussionUrl,
    discussionsUrl,
    createCommentUrl,
    editCommentUrl,
    likeDiscussionUrl,
    categoriesUrl
  } = useApi()
  const { isAuthenticated } = useAuth()
  const [stateComments, setComments] = useState<CommentWithUserModel[]>([])
  const [stateDiscussion, setDiscussion] = useState<DiscussionModel>()
  const { id } = match.params
  const url = discussionUrl(id)
  const { loading, error, response } = useGet(url)
  const moreResult = useGet(
    response
      ? discussionsUrl({
          limit: 20,
          categoryIds: [response.discussion.categoryId]
        })
      : ''
  )
  const [submitComment, commentResult] = usePost(createCommentUrl())
  const [editComment, editResult] = usePatch(editCommentUrl())
  const [submitLike, likeResult] = usePost(likeDiscussionUrl())
  const [currentUser] = useCurrentUser()
  const categoriesResult = useGet(categoriesUrl())

  useEffect(() => {
    if (response && response.comments) {
      setComments(response.comments)
      setDiscussion(response.discussion)
    }
  }, [response])

  useEffect(() => {
    if (commentResult.response && commentResult.response.success) {
      const newComments = [...stateComments, commentResult.response]
      setComments(newComments)
    } // eslint-disable-next-line
  }, [commentResult.response])

  useEffect(() => {
    if (editResult.response && editResult.response.success) {
      const comment = stateComments.find(
        (c: CommentWithUserModel) => c.comment.id === editResult.request.id
      )
      if (comment) {
        comment.comment.text = editResult.request.text
        setComments(stateComments)
      }
    } // eslint-disable-next-line
  }, [editResult.response])

  useEffect(() => {
    if (likeResult.response && likeResult.response.discussion) {
      setDiscussion(likeResult.response.discussion)
    }
  }, [likeResult.response])

  const discussion: DiscussionModel =
    stateDiscussion || (response ? response.discussion : undefined)
  const comments = stateComments || (response ? response.comments : [])

  function renderHeader() {
    return (
      <Helmet>
        <title>{discussion.title} - обсудить на Holywar.club</title>
        <meta
          property="og:title"
          content={discussion.title + '- обсудить на Holywar.club'}
        />
        <meta property="og:site_name" content="holywar.club" />
        <meta property="og:image" content={discussion.imageUrl} />
        <meta property="vk:image" content={discussion.imageUrl} />
        <meta property="twitter:image" content={discussion.imageUrl} />
        <meta property="og:description" content={discussion.description} />
        <meta name="description" content={discussion.description} />
        <link rel="image_src" href={discussion.imageUrl} />
        <link rel="canonical" href={Paths.GetDiscussion(discussion.alias)} />
      </Helmet>
    )
  }

  function rednerDetails() {
    const absoluteUrl =
      env.IS_NODE || !discussion
        ? ''
        : Paths.GetShareDiscussionUrl(
            discussion.id,
            currentUser ? currentUser.id : ''
          )
    const category =
      discussion && categoriesResult.response
        ? categoriesResult.response.find(
            (c: CategoryModel) => c.id === discussion.categoryId
          )
        : undefined

    return (
      <OneColumnLayout wide>
        <DiscussionDetails
          discussion={discussion}
          user={response.user}
          categoryName={category ? category.name : undefined}
          onLikeClick={(discussionId, isLike) =>
            submitLike({ discussionId, isLike })
          }
          survey={
            <SurveyContainer
              survey={response.survey}
              surveyVotes={response.surveyVotes}
            />
          }
          editButton={
            canEdit(response, currentUser) ? (
              <Link
                style={{ fontWeight: 600, marginRight: '10px' }}
                to={Paths.GetDiscussionEdit(discussion.id)}
              >
                Изменить
              </Link>
            ) : null
          }
        />
        {currentUser ? (
          <DiscussionSharePanel>
            <Share url={absoluteUrl} />
          </DiscussionSharePanel>
        ) : null}
        {currentUser && currentUser.role === UserRole.admin ? (
          <Suspense fallback={<Spinner />}>
            <AdminDiscussionContainer discussionId={discussion.id} />
          </Suspense>
        ) : null}
      </OneColumnLayout>
    )
  }

  function renderComments() {
    return (
      <OneColumnLayout small>
        <CommentListContainer
          comments={comments}
          submittingReplyTo={
            commentResult.loading &&
            commentResult.request &&
            commentResult.request.parentCommentId
          }
          onReply={(text: string, imageUrl: string, parentCommentId: string) =>
            submitComment({
              text,
              imageUrl,
              discussionId: discussion.id,
              parentCommentId
            })
          }
          onEdit={(text: string, imageUrl: string, commentId: string) =>
            editComment({ id: commentId, text, imageUrl })
          }
        />
        {isAuthenticated ? (
          <CreateMessageForm
            error={commentResult.error}
            loading={
              commentResult.loading && !commentResult.request.parentCommentId
            }
            onSubmit={({ text, imageUrl }) =>
              submitComment({ text, imageUrl, discussionId: discussion.id })
            }
          />
        ) : (
          <p>
            Чтобы оставлять комментарии нужно{' '}
            <Link to={Paths.SignIn}>войти</Link>
          </p>
        )}
      </OneColumnLayout>
    )
  }

  function renderOtherDiscussions() {
    if (!moreResult.response) {
      return null
    }

    const moreDiscussions = moreResult.response.filter(
      (d: DiscussionModel) => d.id !== response?.discussion?.id
    )
    return (
      <>
        <H2>Другие холивары</H2>
        <DiscussionCardList
          discussions={moreDiscussions}
          component={DiscussionCardContainer}
          // adulterate={(list: any[]) => list.splice(2, 0, <AdSmall key="ad" />)}
        />
      </>
    )
  }

  return (
    <ContentPageLayout>
      {loading ? <Spinner /> : null}
      {error ? <ErrorMessage error={error} /> : null}
      {response ? (
        <>
          {renderHeader()}
          {rednerDetails()}
          {renderComments()}
        </>
      ) : null}
      {/* <AdHorisontal /> */}
      {moreResult.response ? renderOtherDiscussions() : null}
    </ContentPageLayout>
  )
})

function canEdit(
  discussionWithUser: { discussion: DiscussionModel; user: UserModel },
  currentUser: UserModel | null
) {
  if (!discussionWithUser || !currentUser) {
    return false
  }
  if (currentUser.role === UserRole.admin) {
    return true
  }
  return Boolean(
    currentUser.id === discussionWithUser.user.id &&
      !editTimeIsLeft(discussionWithUser.discussion.creationDate)
  )
}

function editTimeIsLeft(commentCreationDate: string) {
  return (
    new Date().getTime() - parseDate(commentCreationDate).getTime() >
    20 * 60 * 1000
  )
}
