import React, { useEffect, useMemo } from 'react'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'

import { useGet, usePost, useCacheItem } from 'helpers'
import { FormPageLayout } from 'layouts'
import { Spinner } from 'components/Spinner/Spinner'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { DiscussionCardForm } from 'composite'
import { Paths } from 'routing'
import { useApi } from 'helpers/api'

export const DiscussionEdit = withRouter(({ match }) => {
  const { id } = match.params
  const {
    discussionUrl,
    editDiscussionUrl,
    discussionsUrl,
    popularDiscussionsUrl,
    categoriesUrl
  } = useApi()
  const url = discussionUrl(id, true)
  const discussionResult = useGet(url)
  const categoriesResult = useGet(categoriesUrl())
  const [submit, { loading, error, response }] = usePost(editDiscussionUrl())

  const userId = discussionResult.response?.discussion?.userId
  const userDiscussionsUrls = discussionsUrl({ userId })
  const { remove: removeDiscussionFromCache } = useCacheItem(url)
  const { remove: removePopularFromCache } = useCacheItem(
    popularDiscussionsUrl()
  )
  const { remove: removeLastFromCache } = useCacheItem(discussionsUrl())
  const { remove: removeUserFromCache } = useCacheItem(userDiscussionsUrls)

  const discussion = useMemo(() => {
    if (!discussionResult.response) {
      return null
    }
    return {
      ...discussionResult.response.discussion,
      survey: discussionResult.response.survey
    }
  }, [discussionResult.response])

  useEffect(() => {
    if (response && response.success) {
      removeDiscussionFromCache()
      removePopularFromCache()
      removeLastFromCache()
      removeUserFromCache()
    }
  }, [response])

  return (
    <FormPageLayout>
      <h1>Изменить статью</h1>
      {discussionResult.loading || categoriesResult.loading ? (
        <Spinner />
      ) : null}
      {discussion && categoriesResult.response ? (
        <DiscussionCardForm
          categories={categoriesResult.response}
          initialValues={discussion}
          loading={loading}
          error={error}
          onSubmit={submit}
          submitLabel="Сохранить"
        />
      ) : null}
      {discussionResult.error ? (
        <ErrorMessage error={discussionResult.error} />
      ) : null}
      {categoriesResult.error ? (
        <ErrorMessage error={categoriesResult.error} />
      ) : null}
      {response && response.success ? <p>Изменения сохранены</p> : null}
      <p>
        <Link to={Paths.GetDiscussion(id)}>Вернуться</Link>
      </p>
    </FormPageLayout>
  )
})
