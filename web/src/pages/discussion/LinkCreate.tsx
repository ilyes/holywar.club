import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import { FormPageLayout } from 'layouts'
import { DiscussionCardForm } from 'composite'
import { usePost, mapErrorMessage, useGet } from 'helpers'
import { Paths } from 'routing'
import { ParseUrlForm, ParseResult } from 'containers'
import { useApi } from 'helpers/api'

export const LinkCreate = () => {
  const [url, setUrl] = useState('')
  const [parseResult, setParseResult] = useState<ParseResult | undefined>()
  const [discussionId, setDiscussionId] = useState('')

  const {
    createDiscussionUrl,
    categoriesUrl,
    discussionsUrl,
    popularDiscussionsUrl
  } = useApi()
  const [submit, submitResult] = usePost(createDiscussionUrl())
  const categories = useGet(categoriesUrl())
  const { clear: clearLast } = useGet(discussionsUrl())
  const { clear: clearPopular } = useGet(popularDiscussionsUrl())
  const submitError = mapErrorMessage(submitResult.error, submitResult.response)

  useEffect(() => {
    if (submitResult.response && !submitResult.error) {
      const { discussionId } = submitResult.response
      clearLast()
      clearPopular()
      setDiscussionId(discussionId)
    }
    // eslint-disable-next-line
  }, [submitResult.response, submitResult.error])

  if (discussionId) {
    return (
      <FormPageLayout>
        <div style={{ minHeight: '40vh', marginBottom: '30px' }}>
          <h1>Холивар начат</h1>
          <p>Отлично! Теперь вы можете написать первый коментарий</p>
          <p>
            <Link to={Paths.GetDiscussion(discussionId)}>
              Перейти к холивару
            </Link>
          </p>
        </div>
        {/* <AdHorisontal /> */}
      </FormPageLayout>
    )
  }

  return (
    <FormPageLayout>
      <div style={{ minHeight: '40vh', marginBottom: '30px' }}>
        <h1>Опубликовать ссылку</h1>
        <p>
          <Link to={Paths.ArticleCreate}>Написать статью</Link>
        </p>
        <ParseUrlForm
          onComplete={(result) => {
            setUrl(result.url)
            setParseResult(result)
          }}
          checkDiscussion
        />
        {parseResult && categories.response ? (
          <DiscussionCardForm
            categories={categories.response}
            initialValues={parseResult}
            loading={submitResult.loading}
            error={submitError}
            onSubmit={(discussion) => submit({ url, ...discussion })}
          />
        ) : null}
      </div>
      {/* <AdHorisontal /> */}
    </FormPageLayout>
  )
}
