import React from 'react'
import { withRouter } from 'react-router'

import { HomePageLayout } from 'layouts'
import { Paths } from 'routing'
import { H1 } from 'components'
import { Helmet } from 'react-helmet'
import { InfiniteDiscussionsListContainer } from 'containers'
import { useApi } from 'helpers/api'

export const EternalDiscussions = withRouter(({ match }) => {
  const { afterDiscussionId } = match.params
  const { eternalDiscussionsUrl } = useApi()
  return (
    <HomePageLayout title="Вечные холивары">
      <Helmet title="Вечные холивары" />
      <InfiniteDiscussionsListContainer
        fromId={afterDiscussionId}
        apiUrlCreator={(afterDiscussionId: string) =>
          eternalDiscussionsUrl({ afterDiscussionId })
        }
        pathUrlCreator={(discussionId: string) =>
          Paths.GetEternalDiscussions(discussionId)
        }
        cacheName={`ETERNAL_DISCUSSIONS`}
      />
    </HomePageLayout>
  )
})
