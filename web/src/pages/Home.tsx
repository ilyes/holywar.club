import React from 'react'

import { DiscussionCardList } from 'composite'
import { DiscussionCardBigContainer } from 'containers'
import { HomePageLayout, Placeholder } from 'layouts'
import { useGet } from 'helpers'
import { Helmet } from 'react-helmet'
import { DiscussionModel } from 'models'
import { useApi } from 'helpers/api'

export const Home = () => {
  const { popularDiscussionsUrl, featuredDiscussionsUrl } = useApi()
  const popularDiscusstionsResult = useGet(popularDiscussionsUrl())
  const featuredDiscusstionResult = useGet(featuredDiscussionsUrl())

  const discussions = (featuredDiscusstionResult.response || []).concat(
    popularDiscusstionsResult.response || []
  )
  const error =
    popularDiscusstionsResult.error || featuredDiscusstionResult.error
  const loading =
    popularDiscusstionsResult.loading || featuredDiscusstionResult.loading

  return (
    <HomePageLayout title="Популярные">
      <Helmet>
        <meta
          property="og:title"
          content="Holywar.club - ставим точки в холиварах. Обсуждение новостей. Сайт холиваров, холивар форум."
        />
        <meta property="og:site_name" content="holywar.club" />
        <meta property="og:image" content="/images/people-question.jpg" />
        <meta property="vk:image" content="/images/people-question.jpg" />
        <meta property="twitter:image" content="/images/people-question.jpg" />
        <link rel="image_src" href="/images/people-question.jpg" />
        <meta
          property="og:description"
          content="Пишите комментарии и получайте за это деньги!"
        />
        <meta
          name="description"
          content="Пишите комментарии и получайте за это деньги!"
        />
      </Helmet>
      <DiscussionCardList
        discussions={discussions}
        loading={loading}
        error={error}
        component={DiscussionCardBigContainer}
        adulterate={adulterate}
      />
    </HomePageLayout>
  )
}

function adulterate(list: any[], featured?: DiscussionModel[]) {
  // list.splice(0, 0, <EternalDiscussionsContainer key="eternal" />)
  // list.splice(4, 0, <AdSquare key="ad1" />)
  // if (list.length > 6) {
  //   list.splice(6, 0, <AdSquare key="ad2" second />)
  // }
  // if (list.length > 13) {
  //   list.splice(13, 0, <AdSquare key="ad3" third />)
  // }
  //list.length = 24
}
