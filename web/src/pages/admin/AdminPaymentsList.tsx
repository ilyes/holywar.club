import React, { useEffect, useState } from 'react'

import { PaymentList } from 'composite'
import { useGet, usePost } from 'helpers'
import { Spinner } from 'components/Spinner/Spinner'
import { OneColumnLayout, ContentPageLayout } from 'layouts'
import { useAdminApi } from 'helpers/admin/api'

export const AdminPaymentsList = () => {
  const { adminPaymentsUrl, adminSetPaymentStatusUrl } = useAdminApi()
  const { response } = useGet(adminPaymentsUrl())
  const [submit, submitResult] = usePost(adminSetPaymentStatusUrl())
  const [payments, setPayments] = useState(undefined)

  useEffect(() => {
    if (response) {
      setPayments(response)
    }
  }, [response])

  useEffect(() => {
    if (submitResult.response && submitResult.response.success) {
      const { request } = submitResult
      const list = payments as any
      const payment = list.find((p: any) => p.id === request.paymentId)
      payment.status = request.status
      setPayments([...list] as any)
    }
  }, [submitResult, payments])

  function onStatusChange(status: string, paymentId: string) {
    submit({ status, paymentId })
  }

  if (!response) return <Spinner />

  return (
    <ContentPageLayout>
      <OneColumnLayout>
        <PaymentList
          title="Заявки на выплату"
          payments={payments}
          onStatusChange={onStatusChange}
        />
      </OneColumnLayout>
    </ContentPageLayout>
  )
}
