import React from 'react'
import { useGet } from 'helpers'
import { CommentListContainer } from 'containers'
import { ContentPageLayout, OneColumnLayout } from 'layouts'
import { H1 } from 'components'
import { Spinner } from 'components/Spinner/Spinner'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { useAdminApi } from 'helpers/admin/api'

export const AdminComments = () => {
  const { commentListUrl } = useAdminApi()
  const { response, loading, error } = useGet(commentListUrl())
  return (
    <ContentPageLayout>
      <OneColumnLayout>
        <H1>Последние комменты</H1>
        {loading ? <Spinner /> : null}
        {error ? <ErrorMessage error={error} /> : null}
        {response ? (
          <CommentListContainer withLink plain comments={response} />
        ) : null}
      </OneColumnLayout>
    </ContentPageLayout>
  )
}
