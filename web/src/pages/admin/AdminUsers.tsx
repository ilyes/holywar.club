import React from 'react'

import { useGet } from 'helpers'
import { Spinner } from 'components/Spinner/Spinner'
import { ContentPageLayout } from 'layouts'
import { Paths } from 'routing'
import { Link } from 'react-router-dom'
import { useAdminApi } from 'helpers/admin/api'

export const AdminUsers = () => {
  const { lastUsersUrl } = useAdminApi()
  const { response } = useGet(lastUsersUrl())

  return (
    <ContentPageLayout>
      {!response ? (
        <Spinner />
      ) : (
        <ul>
          {response.users.map((u: any) => (
            <li>
              <Link to={Paths.GetProfile(u.id)}>
                <b style={{ fontWeight: 'normal' }}>{u.email}</b>
                <span style={{ color: 'silver' }}> [{u.creationDate}]</span>
                {u.isEmailConfirmed ? ' confirmed' : ''}
              </Link>
            </li>
          ))}
        </ul>
      )}
    </ContentPageLayout>
  )
}
