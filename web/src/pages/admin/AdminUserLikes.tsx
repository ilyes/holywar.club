import React from 'react'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'

import { useGet } from 'helpers'
import { ContentPageLayout } from 'layouts'
import { Paths } from 'routing'
import { LikeWithUserDto } from 'models'
import { Spinner } from 'components/Spinner/Spinner'
import { Avatar } from 'components/Avatar/Avatar'
import { Like } from 'components/Like/Like'
import { useAdminApi } from 'helpers/admin/api'

export const AdminUserLikes = withRouter(({ match }) => {
  const { id } = match.params
  const { userLikesUrl } = useAdminApi()
  const { response } = useGet(userLikesUrl(id))

  return (
    <ContentPageLayout>
      {!response ? (
        <Spinner />
      ) : (
        <ul>
          {response.map(({ like, user, targetUser }: LikeWithUserDto) => (
            <li style={{ listStyle: 'none', display: 'flex' }}>
              <Link to={Paths.GetProfile(user.id)}>
                <Avatar size="s" avatarUrl={user.avatarUrl} />
                <span>{user.name}</span>
              </Link>
              <Like
                likesCount={like.isLike ? 1 : 0}
                dislikesCount={like.isLike ? 0 : 1}
              />
              <Link to={Paths.GetProfile(targetUser.id)}>
                <Avatar size="s" avatarUrl={targetUser.avatarUrl} />
                <span>{targetUser.name}</span>
              </Link>
            </li>
          ))}
        </ul>
      )}
    </ContentPageLayout>
  )
})
