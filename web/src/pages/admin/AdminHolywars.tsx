import React from 'react'

import { useGet } from 'helpers'
import { H1 } from 'components'
import { ContentPageLayout, OneColumnLayout } from 'layouts'
import { DiscussionCardList } from 'composite'
import { DiscussionCardContainer } from 'containers'
import { Spinner } from 'components/Spinner/Spinner'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { useAdminApi } from 'helpers/admin/api'

export const AdminHolywars = () => {
  const { holywarCandidatesUrl } = useAdminApi()
  const { response, error } = useGet(holywarCandidatesUrl())

  return (
    <ContentPageLayout>
      <OneColumnLayout>
        <H1>Кандидаты в холивары</H1>
        {!response ? <Spinner /> : null}
        {error ? <ErrorMessage error={error} /> : null}
        {response ? (
          <DiscussionCardList
            component={DiscussionCardContainer}
            discussions={response}
          />
        ) : null}
      </OneColumnLayout>
    </ContentPageLayout>
  )
}
