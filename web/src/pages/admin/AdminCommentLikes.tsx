import React from 'react'
import { RouteComponentProps } from 'react-router'
import { Link } from 'react-router-dom'

import { useGet } from 'helpers'
import { ContentPageLayout } from 'layouts'
import { Paths } from 'routing'
import { LikeWithUserDto, IWithId } from 'models'
import { Spinner } from 'components/Spinner/Spinner'
import { Like } from 'components/Like/Like'
import { Avatar } from 'components/Avatar/Avatar'
import { useAdminApi } from 'helpers/admin/api'
import { AdminPaths } from 'routing/admin/AdminPaths'

export const AdminCommentLikes = ({ match }: RouteComponentProps<IWithId>) => {
  const { id } = match.params
  const { commentLikesUrl } = useAdminApi()
  const { response } = useGet(commentLikesUrl(id))

  return (
    <ContentPageLayout>
      {!response ? (
        <Spinner />
      ) : (
        <ul>
          {response.map(({ like, user }: LikeWithUserDto) => (
            <li>
              <Like
                likesCount={like.isLike ? 1 : 0}
                dislikesCount={like.isLike ? 0 : 1}
              />
              <Avatar size="s" avatarUrl={user.avatarUrl} />
              <Link to={Paths.GetProfile(user.id)}>Профиль</Link>
              <Link to={AdminPaths.UserLikes(user.id)}>Кого лайкал</Link>
            </li>
          ))}
        </ul>
      )}
    </ContentPageLayout>
  )
}
