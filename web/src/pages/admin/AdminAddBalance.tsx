import React, { Suspense, useState } from 'react'

import { useGet } from 'helpers'
import { ContentPageLayout, OneColumnLayout } from 'layouts'
import { H1 } from 'components'
import { Spinner } from 'components/Spinner/Spinner'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { Button } from 'components/Button/Button'
import { useAdminApi } from 'helpers/admin/api'

const AddBalanceContainer = React.lazy(
  () => import('containers/admin/AddBalanceContainer')
)

export const AdminAddBalance = () => {
  const { balanceCandidatesUrl } = useAdminApi()
  const [week, setWeek] = useState(0)
  const url = balanceCandidatesUrl({ week })
  const { loading, error, response } = useGet(url)

  function handlePrevClick() {
    const prevWeek = week ? week - 1 : response.id - 1
    setWeek(prevWeek)
  }

  return (
    <ContentPageLayout>
      <OneColumnLayout>
        <H1>Начислить деньги</H1>
        {loading ? <Spinner /> : null}
        {error ? <ErrorMessage error={error} /> : null}
        {response ? (
          <>
            <Button onClick={handlePrevClick} variant="link">
              Предыдущая неделя
            </Button>
            <Suspense fallback={<Spinner />}>
              <AddBalanceContainer
                weekUsersId={response.id}
                users={response.users}
              />
            </Suspense>
          </>
        ) : null}
      </OneColumnLayout>
    </ContentPageLayout>
  )
}
