import React, { ReactNode } from 'react'
import { UserInfo } from 'composite'
import { InfiniteDiscussionsListContainer } from 'containers'
import { ContentPageLayout, Placeholder } from 'layouts'
import { UserModel } from 'models'
import { Paths } from 'routing'
import { useApi } from 'helpers/api'

export type WithAfterDiscussionId = { afterDiscussionId: string }

type Props = {
  user: UserModel
  userLinks: ReactNode
  userAvatar?: ReactNode
  adminPanel?: ReactNode
} & WithAfterDiscussionId

export const BaseUserPage: React.FC<Props> = ({
  user,
  userLinks,
  userAvatar,
  adminPanel,
  afterDiscussionId
}) => {
  const { discussionsUrl } = useApi()
  return (
    <ContentPageLayout>
      <Placeholder />
      <UserInfo user={user} links={userLinks} avatar={userAvatar} />
      {adminPanel}
      <InfiniteDiscussionsListContainer
        fromId={afterDiscussionId}
        apiUrlCreator={(afterDiscussionId: string) =>
          discussionsUrl({ userId: user.id, afterDiscussionId })
        }
        pathUrlCreator={(discussionId: string) =>
          Paths.GetProfile(user.id, discussionId)
        }
        cacheName={Paths.GetProfile(user.id)}
      />
    </ContentPageLayout>
  )
}
