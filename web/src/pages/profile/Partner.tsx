import React from 'react'

import { ContentPageLayout } from 'layouts'
import { useGet, useConfig, useCurrentUser } from 'helpers'
import { PartnerInfo } from 'composite'
import { PlaymentsContainer } from 'containers'
import { H1 } from 'components'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { Spinner } from 'components/Spinner/Spinner'
import { useModal } from 'components/Modal'
import { useApi } from 'helpers/api'

export const Partner = () => {
  const { partnerInfoUrl } = useApi()
  const partnerResult = useGet(partnerInfoUrl())
  const config = useConfig()
  const [user] = useCurrentUser()
  const { showModal } = useModal()
  return (
    <ContentPageLayout>
      <H1>Партнерская программа</H1>
      {partnerResult.loading || !user ? <Spinner /> : null}
      {partnerResult.error ? (
        <ErrorMessage error={partnerResult.error} />
      ) : null}
      {partnerResult.response && user ? (
        <PartnerInfo
          info={partnerResult.response}
          user={user}
          weekRating={config.weekRating}
          onPaymentRequestClick={() => showModal(<PlaymentsContainer />)}
        />
      ) : null}
      {/* <AdHorisontal /> */}
    </ContentPageLayout>
  )
}
