import React, { Suspense, useCallback } from 'react'

import { ContentPageLayout, Placeholder } from 'layouts'
import { useAuth, useGet } from 'helpers'
import { UserModel, UserRole } from 'models'
import { ChangePasswordContainer, CurrentUserAvatarContainer } from 'containers'
import { Paths } from 'routing'

import { BaseUserPage } from './components/BaseUserPage'
import { withRouter } from 'react-router'
import { useModal } from 'components/Modal'
import { Spinner } from 'components/Spinner/Spinner'
import { Button } from 'components/Button/Button'
import { useApi } from 'helpers/api'

const AdminPanelContainer = React.lazy(
  () => import('containers/admin/AdminPanelContainer')
)

export const CurrentUserProfile = withRouter(({ history }) => {
  const { showModal, hideModal } = useModal()
  const { setAuthenticated } = useAuth()
  const { currentUserUrl } = useApi()
  const currentUserResult = useGet(currentUserUrl())
  const user: UserModel =
    !currentUserResult.loading &&
    !currentUserResult.error &&
    currentUserResult.response

  const handleSignOut = useCallback(() => {
    setAuthenticated('')
    history.push(Paths.Home)
  }, [])

  const handleChangePasswordClick = useCallback(() => {
    showModal(<ChangePasswordContainer onComplete={hideModal} />)
  }, [])

  if (!user) {
    return (
      <ContentPageLayout>
        <Spinner />
      </ContentPageLayout>
    )
  }

  return (
    <BaseUserPage
      user={user}
      userAvatar={<CurrentUserAvatarContainer user={user} />}
      userLinks={
        <>
          <Button variant="link" onClick={handleChangePasswordClick}>
            Изменить пароль
          </Button>
          <Button variant="link" onClick={handleSignOut}>
            Выйти
          </Button>
        </>
      }
      adminPanel={
        user?.role === UserRole.admin ? (
          <Suspense fallback={<Spinner />}>
            <AdminPanelContainer />
          </Suspense>
        ) : (
          <Placeholder />
        )
      }
      afterDiscussionId={''}
    />
  )
})
