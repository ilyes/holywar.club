import React, { Suspense } from 'react'

import { ContentPageLayout, Placeholder } from 'layouts'
import { useGet, useCurrentUser } from 'helpers'
import { SendMessageContainer } from 'containers'
import { UserRole, IWithId } from 'models'
import { BaseUserPage } from './components/BaseUserPage'
import { useCallback } from 'react'
import { RouteComponentProps } from 'react-router'
import { useModal } from 'components/Modal'
import { Spinner } from 'components/Spinner/Spinner'
import { Button } from 'components/Button/Button'
import { useApi } from 'helpers/api'

const AdminProfileContainer = React.lazy(
  () => import('containers/admin/AdminProfileContainer')
)

type Props = IWithId & { afterDiscussionId: string }

export const Profile = ({ match }: RouteComponentProps<Props>) => {
  const { id, afterDiscussionId } = match.params
  const { userUrl } = useApi()
  const profileUrl = userUrl(id)
  const { response: profile } = useGet(profileUrl)
  const { showModal, hideModal } = useModal()
  const [user] = useCurrentUser()

  const handleSendMessageClick = useCallback(() => {
    showModal(<SendMessageContainer user={profile} onComplete={hideModal} />)
  }, [showModal, hideModal, profile])

  if (!profile) {
    return (
      <ContentPageLayout>
        <Spinner />
      </ContentPageLayout>
    )
  }

  return (
    <BaseUserPage
      user={profile}
      userLinks={
        <Button variant="link" onClick={handleSendMessageClick}>
          Написать сообщение
        </Button>
      }
      adminPanel={
        user?.role === UserRole.admin && profile ? (
          <Suspense fallback={<Spinner />}>
            <AdminProfileContainer userId={profile.id} />
          </Suspense>
        ) : (
          <Placeholder />
        )
      }
      afterDiscussionId={afterDiscussionId}
    />
  )
}
