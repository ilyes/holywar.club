import React, { useEffect } from 'react'
import { withRouter } from 'react-router'
import * as H from 'history'

import { FormPageLayout } from 'layouts'
import { SignUpForm } from 'composite'
import { usePost, mapErrorMessage } from 'helpers'
import { Paths } from 'routing'
import { setAuthenticated } from 'state'
import { Helmet } from 'react-helmet'
import { useApi } from 'helpers/api'

interface SignUpProps {
  history: H.History
}

export const SignUp = withRouter(({ history }: SignUpProps) => {
  const { signUpUrl } = useApi()
  const [submit, { loading, error, response }] = usePost(signUpUrl())

  function handleSubmit(values: any) {
    if (loading) {
      return
    }
    const referralId =
      typeof localStorage !== 'undefined'
        ? localStorage.getItem('referral')
        : ''
    localStorage.removeItem('referral')
    values.referralId = referralId
    submit(values)
  }

  useEffect(() => {
    if (response && response.success === true) {
      setAuthenticated('')
      history.push(Paths.SignUpConfirm)
    }
  }, [response, history])

  const errorMessage = mapErrorMessage(error, response)
  return (
    <FormPageLayout strait>
      <Helmet title="Зарегистрироваться на Holywar.club" />
      <div>
        <h1>Регистрация</h1>
        <SignUpForm
          loading={loading}
          error={errorMessage}
          onSubmit={handleSubmit}
        />
      </div>
    </FormPageLayout>
  )
})
