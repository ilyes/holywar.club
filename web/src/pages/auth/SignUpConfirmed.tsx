import React from 'react'
import { Link } from 'react-router-dom'

import { FormPageLayout } from 'layouts'
import { Paths } from 'routing'

export const SignUpConfirmed = () => (
  <FormPageLayout>
    <div>
      <h1>Регистрация завершена</h1>
      <p>
        Поздравляем! Ваш e-mail подтвержден, теперь вы можете делиться ссылками,
        писать статьи и участвовать в холиварах!
      </p>
      <p>
        <Link className="big-text" to={Paths.SignIn}>
          Войти
        </Link>
      </p>
    </div>
  </FormPageLayout>
)
