import React from 'react'

import { FormPageLayout } from 'layouts'
import { SetPasswordForm } from 'composite'
import { mapErrorMessage, usePost } from 'helpers'
import { useApi } from 'helpers/api'

export const SetPasswordByToken = () => {
  const { changePasswordByTokenUrl } = useApi()
  const [submit, { loading, error, response }] = usePost(
    changePasswordByTokenUrl()
  )
  const token = getToken()
  const errorMessage = mapErrorMessage(error, response)
  return (
    <FormPageLayout strait>
      <h1>Создать новый пароль</h1>
      <SetPasswordForm
        loading={loading}
        error={errorMessage}
        success={response && response.success}
        onSubmit={({ password }) =>
          !loading ? submit({ password, token }) : undefined
        }
      />
    </FormPageLayout>
  )
}

function getToken(): string | null {
  if (typeof window === 'undefined') {
    return null
  }
  const urlParams = new URLSearchParams(window.location.search)
  return urlParams.get('token')
}
