import React from 'react'

import { FormPageLayout } from 'layouts'
import { RecoverPasswordForm } from 'composite'
import { mapErrorMessage, usePost } from 'helpers'
import { useApi } from 'helpers/api'

export const RecoverPassword = () => {
  const { passwordRecoverUrl } = useApi()
  const [submit, { loading, error, response }] = usePost(passwordRecoverUrl())
  const errorMessage = mapErrorMessage(error, response)
  return (
    <FormPageLayout strait>
      <h1>Восстановить пароль</h1>
      <RecoverPasswordForm
        loading={loading}
        error={errorMessage}
        success={response && response.success}
        onSubmit={(values) => (!loading ? submit(values) : undefined)}
      />
    </FormPageLayout>
  )
}
