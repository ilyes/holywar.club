import React from 'react'

import { FormPageLayout } from 'layouts'

export const SignUpConfirm = () => (
  <FormPageLayout>
    <div>
      <h1>Подтвердите e-mail</h1>
      <p>
        Чтобы завершить регистрацию нужно подтвердить e-mail. Для этого
        перейдите по ссылке отправленной на ваш электронный почтовый ящик.
      </p>
    </div>
  </FormPageLayout>
)
