import React from 'react'

import { FormPageLayout } from 'layouts'
import { SignInContainer } from 'containers'
import { Helmet } from 'react-helmet'

export const SignIn = () => (
  <FormPageLayout strait>
    <Helmet title="Войти на Holywar.club" />
    <div>
      <h1>Вход</h1>
      <SignInContainer />
    </div>
  </FormPageLayout>
)
