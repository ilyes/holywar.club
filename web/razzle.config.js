'use strict'

module.exports = {
  plugins: [
    {
      name: 'typescript',
      options: {
        useBabel: false,
        tsLoader: {
          transpileOnly: true,
          experimentalWatchApi: true
        },
        forkTsChecker: {
          tsconfig: './tsconfig.json',
          tslint: null,
          watch: './src',
          typeCheck: true
        }
      }
    },
    {
      name: 'bundle-analyzer',
      options: {
        concatenateModules: false
      }
    }
  ]
}
