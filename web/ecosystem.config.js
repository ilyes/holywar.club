module.exports = {
  apps: [
    {
      name: 'front',
      script: './server.js',
      env: {
        API_URL: 'http://api.holywar.club',
        IS_PRODUCTION: true
      }
    }
  ]
}
