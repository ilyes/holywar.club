## Конфиг nginx

```
server {
    listen        80;
    server_name   holywar.club www.holywar.club api.holywar.club www.api.holywar.club content.holywar.club www.content.holywar.club;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl; # managed by Certbot
    server_name   holywar.club www.holywar.club;

    ssl_certificate /home/ilyes/holywar.club/certs/fullchain.pem; # managed by Certbot
    ssl_certificate_key /home/ilyes/holywar.club/certs/privkey.pem; # managed by Certbot
    include /home/ilyes/holywar.club/certs/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /home/ilyes/holywar.club/certs/ssl-dhparams.pem; # managed by Certbot

    gzip on;
    gzip_disable "msie6";
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 5;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain text/css application/json application/x-javascript application/javascript text/xml application/xml application/xml+rss text/javascript;

    location /content/ {
      root /home/ilyes/holywar.club/static/content;
	    try_files $uri =404;
    }

    location /static/ {
      root /home/ilyes/holywar.club/web/public/static;
	    try_files $uri =404;
    }

    location /scripts/ {
      root /home/ilyes/holywar.club/web/public/scripts;
	    try_files $uri =404;
    }

    location = / {
	    proxy_pass http://localhost:3000;
    }

    location / {
	    root /home/ilyes/holywar.club/web/public;
      try_files $uri $uri/ @nodeproxy;
    }

    location @nodeproxy {
      proxy_pass http://localhost:3000;
    }
}

server {
    server_name   api.holywar.club www.api.holywar.club;

    location / {
        proxy_pass         http://localhost:6001;
        proxy_http_version 1.1;
        proxy_set_header   Upgrade $http_upgrade;
        proxy_set_header   Connection keep-alive;
        proxy_set_header   Host $host;
        proxy_cache_bypass $http_upgrade;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Proto $scheme;
    }

    listen 443 ssl; # managed by Certbot
    ssl_certificate /home/ilyes/holywar.club/certs/fullchain.pem; # managed by Certbot
    ssl_certificate_key /home/ilyes/holywar.club/certs/privkey.pem; # managed by Certbot
    include /home/ilyes/holywar.club/certs/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /home/ilyes/holywar.club/certs/ssl-dhparams.pem; # managed by Certbot
}

server {
	server_name content.holywar.club www.content.holywar.club;

	root /home/ilyes/holywar.club/static/content;
	index index.html;

	location / {
		try_files $uri =404;
	}

    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /home/ilyes/holywar.club/certs/fullchain.pem; # managed by Certbot
    ssl_certificate_key /home/ilyes/holywar.club/certs/privkey.pem; # managed by Certbot
    include /home/ilyes/holywar.club/certs/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /home/ilyes/holywar.club/certs/ssl-dhparams.pem; # managed by Certbot
}
```
