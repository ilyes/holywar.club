# Run

## Mongo

1. sudo mongod --dbpath ~/projects/tmp/data/mongo --replSet rs0
2. mongo
3. rs.initiate()

## Systemctl

- systemctl start holywar-api.service

- systemctl start holywar-mailer.service

- sudo mongod --fork --dbpath /home/ilyes/db/mongo --logpath /home/ilyes/db/log/mongod.log

# Deploy

dotnet publish -c Release
systemctl restart holywar-api.service
