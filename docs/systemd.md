## Файл /lib/systemd/system/holywar-api.service

```
[Unit]
Description=Holywar.club api service

[Service]
WorkingDirectory=/home/ilyes/holywar.club/api
ExecStart=/usr/bin/dotnet /home/ilyes/holywar.club/api/Holywar.Api.dll
Restart=always

# Restart service after 10 seconds if the dotnet service crashes:

RestartSec=10
KillSignal=SIGINT
SyslogIdentifier=api.holywar.club
User=ilyes
Environment=ASPNETCORE_ENVIRONMENT=Production
Environment=DOTNET_PRINT_TELEMETRY_MESSAGE=false

[Install]
WantedBy=multi-user.target
```
