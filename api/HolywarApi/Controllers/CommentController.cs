﻿using Holywar.Api.Controllers.Internal;
using Holywar.Api.Filters;
using Holywar.Services;
using Holywar.Common;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using MongoDB.Bson;
using Holywar.Api.Mapping;
using System.Collections.Generic;
using System.Linq;
using Holywar.Entities.Comments;
using Holywar.Entities.Likes;
using Holywar.Entities.Abstract;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CommentController : AppController
    {
        CommentsService commentsService;
        UsersService usersService;
        LikesService likesService;

        public CommentController(CommentsService commentsService, UsersService usersService, LikesService likesService)
        {
            this.commentsService = commentsService;
            this.usersService = usersService;
            this.likesService = likesService;
        }

        [HttpGet("of-the-week")]
        public async Task<IEnumerable<CommentWithUserModel>> CommentsOfTheWeek()
        {
            var comments = await commentsService.CommentsOfTheWeek();
            return comments.Select(c => c.ToModel());
        }

        [Auth]
        [HttpPost]
        public async Task<CreateCommentResponse> Create([FromBody] CommentRequest model)
        {
            var discussionId = model.DiscussionId.ToObjectId();
            if (discussionId == ObjectId.Empty)
            {
                return new CreateCommentResponse { Error = "IncorrentDiscussionId" };
            }
            var parentCommentId = model.ParentCommentId.ToObjectId();
            var result = await commentsService.Create(model.Text, model.ImageUrl, Auth.UserId, discussionId, parentCommentId);
            if (result.HasError)
            {
                return new CreateCommentResponse { Error = result.Error };
            }
            var user = await usersService.FindById(Auth.UserId);
            return new CreateCommentResponse
            {
                Comment = result.Comment.ToModel(),
                User = user.ToModel(true)
            };
        }

        [Auth]
        [HttpPatch]
        public async Task<ApiResponse> Update([FromBody] CommentRequest model)
        {
            var commentId = model.Id.ToObjectId();
            if (commentId == ObjectId.Empty)
            {
                return new ApiResponse { Error = "IncorrentCommentId" };
            }
            var result = await commentsService.Update(commentId, Auth.UserId, model.Text, model.ImageUrl);
            if (result.HasError)
            {
                return new CreateCommentResponse { Error = result.Error };
            }
            return new CreateCommentResponse();
        }

        [Auth]
        [HttpPost("like")]
        public async Task<LikeCommentResponse> Like([FromBody] LikeCommentRequest model)
        {
            var result = await likesService.LikeComment(model.CommentId.ToObjectId(), Auth.UserId, model.IsLike);
            if (result.HasError)
            {
                return new LikeCommentResponse { Error = result.Error };
            }
            return new LikeCommentResponse
            {
                Comment = result.Comment.ToModel()
            };
        }
    }
}
