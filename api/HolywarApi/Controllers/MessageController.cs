﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Holywar.Api.Controllers.Internal;
using Holywar.Api.Filters;
using Holywar.Api.Mapping;
using Holywar.Entities.Abstract;
using Holywar.Entities.Messages;
using Holywar.Services;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MessageController : AppController
    {
        MessagesService messagesService;

        public MessageController(MessagesService messagesService)
        {
            this.messagesService = messagesService;
        }

        [Auth]
        [HttpGet("last")]
        public async Task<IEnumerable<MessageWithUserModel>> Get()
        {
            var messages = await messagesService.Last(Auth.UserId, 5);
            return messages.ToModel();
        }

        [Auth]
        [HttpPost]
        public async Task<ApiResponse> Post([FromBody] MessageRequest message)
        {
            var result = await messagesService.CreateCommon(Auth.UserId, message.ToUserId, message.Text);
            if (result.HasError)
            {
                return new ApiResponse { Error = result.Error };
            }
            return new ApiResponse();
        }

        [Auth]
        [HttpPost("read")]
        public async Task<ApiResponse> Post()
        {
            await messagesService.SetRead(Auth.UserId);
            return new ApiResponse();
        }
    }
}
