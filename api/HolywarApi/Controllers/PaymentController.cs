﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Holywar.Common;
using Holywar.Services;
using Holywar.Api.Filters;
using Holywar.Entities.Abstract;
using Holywar.Api.Controllers.Internal;
using Holywar.Entities.Payments;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PaymentController : AppController
    {
        private PaymentsService paymentsService;

        public PaymentController(PaymentsService paymentsService)
        {
            this.paymentsService = paymentsService;
        }

        [Auth]
        [HttpGet("list")]
        public async Task<IEnumerable<PaymentDto>> Get()
        {
            return await paymentsService.Get(Auth.UserId, 5);
        }

        [Auth]
        [HttpPost]
        public async Task<ApiResponse> Create([FromBody]CreatePaymentRequest request)
        {
            var result = await paymentsService.Create(Auth.UserId, request);
            if (result.HasError)
                return new ApiResponse { Error = result.Error };

            return new ApiResponse();
        }

        [Auth]
        [HttpDelete]
        public async Task<ApiResponse> Delete([FromBody]string paymentId)
        {
            var result = await paymentsService.Delete(Auth.UserId, paymentId.ToObjectId());
            if (result.HasError)
                return new ApiResponse { Error = result.Error };

            return new ApiResponse();
        }
    }
}
