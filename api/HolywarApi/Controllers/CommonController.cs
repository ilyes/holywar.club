﻿using Microsoft.AspNetCore.Mvc;
using Holywar.Api.Controllers.Internal;
using Holywar.Common;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CommonController : AppController
    {
        [HttpGet("version")]
        public ActionResult<string> Version()
        {
            return "0.0.3";
        }

        [HttpGet("current-week")]
        public ActionResult<int> CurrentWeek()
        {
            return DateUtil.GetCurrentWeekId();
        }
    }
}
