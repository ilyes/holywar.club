﻿using Holywar.Api.Filters;
using Holywar.Entities.Abstract;
using Holywar.Entities.Users;
using Holywar.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Holywar.Api.Controllers.Internal;
using Holywar.Entities.Common;

namespace Api.Controllers.Admin
{
    [Auth(AuthRole.Admin)]
    [Route("admin/config")]
    [ApiController]
    public class AdminConfigController : AppController
    {
        private ConfigService configService;

        public AdminConfigController(ConfigService configService)
        {
            this.configService = configService;
        }

        [HttpGet]
        public async Task<Config> Get()
        {
            var config = await configService.GetConfig();
            return config;
        }

        [HttpPost]
        public async Task<ApiResponse> Post([FromBody] Config config)
        {
            await configService.SaveConfig(config);
            return new ApiResponse();
        }
    }
}
