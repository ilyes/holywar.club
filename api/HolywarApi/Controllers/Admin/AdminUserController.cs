﻿using Holywar.Api.Filters;
using Holywar.Entities.Abstract;
using Holywar.Entities.Users;
using Holywar.Services;
using Holywar.Common;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Holywar.Api.Controllers.Internal;
using System.Linq;
using System.Collections.Generic;
using Holywar.Entities.Likes;
using Holywar.Api.Mapping;

namespace Api.Controllers.Admin
{
    [Auth(AuthRole.Admin)]
    [Route("admin/user")]
    [ApiController]
    public class AdminUserController : AppController
    {
        private UsersService usersService;
        private LikesService likesService;
        
        public AdminUserController(UsersService usersService, LikesService likesService)
        {
            this.usersService = usersService;
            this.likesService = likesService;
        }

        [HttpPost("block")]
        public async Task<ApiResponse> Block([FromBody] BlockUserRequest request)
        {
            await usersService.Block(request.UserId);
            return new ApiResponse();
        }

        [HttpGet("last")]
        public async Task<LastUsersRespone> Last([FromQuery] int limit)
        {
            var users = await usersService.Last(limit);
            return new LastUsersRespone
            {
                Users = users.Select(u => new UserInfo
                {
                    Id = u.Id.ToString(),
                    CreationDate = u.CreationDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                    IsEmailConfirmed = u.IsEmailConfirmed,
                    Email = u.Email
                }).ToArray()
            };
        }

        [HttpGet("likes/{id}")]
        public async Task<IEnumerable<LikeWithUserDto>> LikesByUserId(string id)
        {
            var likes = await likesService.GetByUserId(id.ToObjectId());
            return likes.ToModel();
        }

        [HttpGet("likes-by-target-user/{id}")]
        public async Task<IEnumerable<LikeWithUserDto>> LikesByTargetUserId(string id)
        {
            var likes = await likesService.GetByUserId(id.ToObjectId());
            return likes.ToModel();
        }
    }

    public class BlockUserRequest
    {
        public string UserId { get; set; }
    }

    public class LastUsersRespone
    {
        public UserInfo[] Users { get; set; }
    }

    public class UserInfo
    {
        public string Id { get; set; }

        public string CreationDate { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public string Email { get; set; }
    }
}
