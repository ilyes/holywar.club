﻿using Holywar.Api.Filters;
using Holywar.Entities.Abstract;
using Holywar.Entities.Users;
using Holywar.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Holywar.Entities.Payments;
using Holywar.Api.Controllers.Internal;
using System.Collections.Generic;

namespace Api.Controllers.Admin
{
    [Auth(AuthRole.Admin)]
    [Route("admin/payment")]
    [ApiController]
    public class AdminPaymentsController : AppController
    {
        private PaymentsService paymentsService;

        public AdminPaymentsController(PaymentsService paymentsService)
        {
            this.paymentsService = paymentsService;
        }

        [HttpGet("list")]
        public async Task<IEnumerable<PaymentWithUserDto>> GetPendingPayments([FromQuery] GetAllRequest request)
        {
            return await paymentsService.GetAll(request);
        }

        [HttpPost("status")]
        public async Task<ApiResponse> SetStatus([FromBody] PaymentStatusRequest request)
        {
            var result = await paymentsService.SetStatus(Auth.UserId, request);
            if (result.HasError)
                return new ApiResponse { Error = result.Error };

            return new ApiResponse();
        }
    }
}
