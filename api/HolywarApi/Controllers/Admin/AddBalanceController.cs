﻿using Holywar.Api.Filters;
using Holywar.Entities.Abstract;
using Holywar.Entities.Admin.Balance;
using Holywar.Entities.Users;
using Holywar.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Linq;
using Holywar.Api.Mapping;
using Holywar.Api.Controllers.Internal;

namespace Api.Controllers.Admin
{
    [Auth(AuthRole.Admin)]
    [Route("admin/add-balance")]
    [ApiController]
    public class AddBalanceController : AppController
    {
        private UsersService usersService;

        public AddBalanceController(UsersService usersService)
        {
            this.usersService = usersService;
        }

        [HttpGet("candidates")]
        public async Task<WeekUsersResponse> GetUsersOfTheWeek([FromQuery]int? week = null)
        {
            var result = await usersService.GetWeekUsersForAddBalance(week);
            return new WeekUsersResponse
            {
                Id = result.Id,
                Users = result.Users.Select(u => u.ToModel(true)).ToArray()
            };
        }

        [HttpPost]
        public async Task<ApiResponse> SetMoney([FromBody] AddMoneyModels addMoney)
        {
            await usersService.AddMoney(Auth.UserId, addMoney.WeekUsersId, addMoney.Users);
            return new ApiResponse();
        }
    }
}
