﻿using Holywar.Api.Filters;
using Holywar.Entities.Abstract;
using Holywar.Entities.Users;
using Holywar.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Holywar.Api.Controllers.Internal;
using Holywar.Entities.Comments;
using System.Collections.Generic;
using System.Linq;
using Holywar.Api.Mapping;
using Holywar.Common;
using Holywar.Entities.Likes;

namespace Api.Controllers.Admin
{
    [Auth(AuthRole.Admin)]
    [Route("admin/comment")]
    [ApiController]
    public class AdminCommentController : AppController
    {
        private CommentsService commentsService;
        private LikesService likesService;

        public AdminCommentController(CommentsService commentsService, LikesService likesService)
        {
            this.commentsService = commentsService;
            this.likesService = likesService;
        }

        [HttpGet("list")]
        public async Task<IEnumerable<CommentWithUserModel>> GetPendingPayments()
        {
            var comments = await commentsService.Last(100);
            return comments.Select(c => c.ToModel());
        }

        [HttpPost("delete")]
        public async Task<ApiResponse> Delete([FromBody] DeleteCommentRequest request)
        {
            await commentsService.SetDeleted(request.CommentId);
            return new ApiResponse();
        }

        [HttpGet("likes/{id}")]
        public async Task<IEnumerable<LikeWithUserDto>> LikesByCommentId(string id)
        {
            var likes = await likesService.GetByCommentId(id.ToObjectId());
            return likes.ToModel();
        }
    }

    public class DeleteCommentRequest
    {
        public string CommentId { get; set; }
    }
}
