﻿using Holywar.Api.Filters;
using Holywar.Entities.Abstract;
using Holywar.Entities.Users;
using Holywar.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Holywar.Api.Controllers.Internal;
using Holywar.Entities.Categories;
using Holywar.Common;

namespace Api.Controllers.Admin
{
    [Auth(AuthRole.Admin)]
    [Route("admin/discussion")]
    [ApiController]
    public class AdminDiscussionController : AppController
    {
        private DiscussionsService discussionsService;

        public AdminDiscussionController(DiscussionsService discussionsService)
        {
            this.discussionsService = discussionsService;
        }

        [HttpPost("delete")]
        public async Task<ApiResponse> Delete([FromBody] DiscussionRequest request)
        {
            await discussionsService.SetDeleted(request.DiscussionId);
            return new ApiResponse();
        }

        [HttpPost("unwanted")]
        public async Task<ApiResponse> Unwanted([FromBody] DiscussionRequest request)
        {
            await discussionsService.SetUnwanted(request.DiscussionId);
            return new ApiResponse();
        }

        [HttpPost("ad")]
        public async Task<ApiResponse> Ad([FromBody] DiscussionRequest request)
        {
            await discussionsService.SetCategory(request.DiscussionId, CategoryAliases.Services);
            discussionsService.ClearPopularsCacheIfExists(request.DiscussionId.ToObjectId());
            return new ApiResponse();
        }

        [HttpPost("top")]
        public async Task<ApiResponse> Top([FromBody] DiscussionRequest request)
        {
            await discussionsService.SetTop(request.DiscussionId, true);
            return new ApiResponse();
        }

        [HttpPost("untop")]
        public async Task<ApiResponse> Untop([FromBody] DiscussionRequest request)
        {
            await discussionsService.SetTop(request.DiscussionId, false);
            return new ApiResponse();
        }

        [HttpPost("moderated")]
        public async Task<ApiResponse> Moderated([FromBody] DiscussionRequest request)
        {
            await discussionsService.SetModerated(request.DiscussionId, true);
            return new ApiResponse();
        }

        [HttpPost("not-moderated")]
        public async Task<ApiResponse> NotModerated([FromBody] DiscussionRequest request)
        {
            await discussionsService.SetModerated(request.DiscussionId, false);
            return new ApiResponse();
        }
    }

    public class DiscussionRequest
    {
        public string DiscussionId { get; set; }
    }
}
