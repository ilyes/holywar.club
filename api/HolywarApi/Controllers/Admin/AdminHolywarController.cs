﻿using Holywar.Api.Controllers.Internal;
using Holywar.Api.Filters;
using Holywar.Api.Mapping;
using Holywar.Services;
using Holywar.Entities.Discussions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Holywar.Entities.Auths;
using Holywar.Entities.Users;
using Holywar.Entities.Abstract;

namespace Holywar.Api.Controllers
{
    [Auth(AuthRole.Admin)]
    [Route("admin/holywar")]
    [ApiController]
    public class AdminHolywarController : AppController
    {
        DiscussionsService discussionsService;
        UsersService usersService;

        public AdminHolywarController(DiscussionsService discussionsService, UsersService usersService)
        {
            this.discussionsService = discussionsService;
            this.usersService = usersService;
        }

        [HttpGet("candidates")]
        public async Task<IEnumerable<DiscussionModel>> Candidates([FromQuery]int limit)
        {
            var candidates = await discussionsService.GetHolywarCandidates(limit);
            var users = await usersService.FindById(candidates.Select(d => d.UserId).ToArray());
            return candidates.Select(d => d.ToModel(users)).ToArray();
        }

        [HttpPost("approve")]
        public async Task<ApiResponse> Approve([FromBody] DiscussionIdModel model)
        {
            var result = await discussionsService.ApproveHolywar(Auth.UserId, model.DiscussionId);
            if (result.HasError)
            {
                return new ApiResponse { Error = result.Error };
            }

            return new ApiResponse();
        }

        [HttpPost("decline")]
        public async Task<ApiResponse> Decline([FromBody] DiscussionIdModel model)
        {
            var result = await discussionsService.DeclineHolywar(Auth.UserId, model.DiscussionId);
            if (result.HasError)
            {
                return new ApiResponse { Error = result.Error };
            }

            return new ApiResponse();
        }
    }
}
