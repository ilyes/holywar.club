﻿using Microsoft.AspNetCore.Mvc;
using Holywar.Api.Controllers.Internal;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ErrorController : AppController
    {
        public ActionResult<string> Get()
        {
            return "{\"error\":\"500\"}";
        }
    }
}
