﻿using Microsoft.AspNetCore.Mvc;
using Holywar.Api.Controllers.Internal;
using Holywar.Common;
using System.Text;
using Holywar.Api.Helpers;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LogController : AppController
    {
        [HttpPost]
        public void WriteToLog([FromBody]LogRequest request)
        {
            var message = CreateMessage(request);
            var contextInfo = LogInfoHelper.GetContextInfo(HttpContext);
            var logMessage = $"{message}\r\n{contextInfo}";

            switch (request.Level)
            {
                case "Error":
                    {
                        Log.Error(logMessage);
                        break;
                    }
                case "Warning":
                    {
                        Log.Warn(logMessage);
                        break;
                    }
                case "Info":
                    {
                        Log.Info(logMessage);
                        break;
                    }
            }
        }

        private string CreateMessage(LogRequest request)
        {
            var message = new StringBuilder();
            message.Append("Message :");
            message.AppendLine(request.Message);
            message.Append("TagName :");
            message.AppendLine(request.TagName);
            message.Append("FileName :");
            message.AppendLine(request.FileName);
            message.Append("LineNumber :");
            message.AppendLine(request.LineNumber.ToString());
            message.Append("ColumnNumber :");
            message.AppendLine(request.ColumnNumber.ToString());
            message.Append("Stack :");
            message.AppendLine(request.Stack);
            message.Append("Browser :");
            message.AppendLine(request.Browser);
            message.Append("Url :");
            message.AppendLine(request.Url);
            message.Append("RunTime :");
            message.AppendLine(request.RunTime);
            message.Append("PreviousLog :");
            foreach (var p in request.PreviousLog)
                message.AppendLine(p);
            return message.ToString();
        }
    }

    public class LogRequest
    {
        public string Level { get; set; }
        public string Message { get; set; }
        public string TagName { get; set; }
        public string FileName { get; set; }
        public int LineNumber { get; set; }
        public int ColumnNumber { get; set; }
        public string Stack { get; set; }
        public string Browser { get; set; }
        public string Url { get; set; }
        public string RunTime { get; set; }
        public string[] PreviousLog { get; set; }
    }
}
