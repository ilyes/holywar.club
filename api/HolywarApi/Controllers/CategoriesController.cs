﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Holywar.Services;
using Holywar.Entities.Categories;
using Holywar.Api.Mapping;
using Holywar.Api.Filters;
using Holywar.Entities.Abstract;
using Holywar.Api.Controllers.Internal;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoriesController : AppController
    {
        private CategoriesService categoriesService;
        private UsersService usersService;

        public CategoriesController(CategoriesService categoriesService, UsersService usersService)
        {
            this.categoriesService = categoriesService;
            this.usersService = usersService;
        }

        [HttpGet]
        public async Task<IEnumerable<CategoryModel>> Get()
        {
            var categories = await categoriesService.GetAll();
            return categories.ToModel();
        }

        [Auth]
        [HttpPost]
        public async Task<ApiResponse> Save([FromBody]SaveCategoriesRequest request)
        {
            var result = await usersService.SaveCategoryIds(Auth.UserId, request.CategoryIds);
            if (result.HasError)
                return new ApiResponse { Error = result.Error };

            return new ApiResponse();
        }
    }
}
