﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Holywar.Api.Filters;
using Holywar.Entities.Users;
using Holywar.Api.Controllers.Internal;
using Holywar.Services;
using Holywar.Entities.Abstract;

namespace Holywar.Api.Controllers.Service
{
    [Auth(AuthRole.Service)]
    [Route("service/role")]
    [ApiController]
    public class RoleController : AppController
    {
        private UsersService usersService;

        public RoleController(UsersService usersService)
        {
            this.usersService = usersService;
        }

        [HttpPost()]
        public async Task<ApiResponse> SetRole([FromBody] SetRoleRequest request)
        {
            await usersService.SetRole(request.UserId, request.Role);

            return new ApiResponse();
        }
    }

    public class SetRoleRequest
    {
        public string UserId { get; set; }
        public string Role { get; set; }
    }
}

