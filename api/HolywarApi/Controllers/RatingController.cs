﻿using Holywar.Services;
using Holywar.Api.Controllers.Internal;
using Holywar.Api.Mapping;
using Holywar.Entities.Users;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Holywar.Entities.Comments;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RatingController : AppController
    {
        private UsersService usersService;
        private CommentsService commentsService;

        public RatingController(UsersService usersService, CommentsService commentsService)
        {
            this.usersService = usersService;
            this.commentsService = commentsService;
        }

        [HttpGet()]
        public async Task<UserDto[]> Get()
        {
            var result = await usersService.GetWeekUsersByRating();
            return result.Users.Select(u => u.ToModel()).ToArray();
        }

        [HttpGet("by-likes")]
        public async Task<UserDto[]> ByLikes()
        {
            var result = await usersService.GetWeekUsersByLikes();
            return result.Users.Select(u => u.ToModel()).ToArray();
        }

        [HttpGet("by-dislikes")]
        public async Task<UserDto[]> ByDislikes()
        {
            var result = await usersService.GetWeekUsersByDislikes();
            return result.Users.Select(u => u.ToModel()).ToArray();
        }

        [HttpGet("by-balance-added")]
        public async Task<UserDto[]> ByBalanceAdded()
        {
            var result = await usersService.GetWeekUsersByBalance();
            return result.Users.Select(u => u.ToModel(withPaid: true)).ToArray();
        }

        [HttpGet("by-referals")]
        public async Task<UserDto[]> ByReferals()
        {
            var users = await usersService.GetByReferals();
            return users.Select(u => u.ToModel()).ToArray();
        }

        [HttpGet("week-comments")]
        public async Task<CommentWithUserModel[]> WeekComments([FromQuery] int limit, [FromQuery] int? weekId)
        {
            var comments = await commentsService.GetWeekComments(limit, weekId);
            return comments.Select(u => u.ToModel()).ToArray();
        }
    }
}
