﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Holywar.Common;
using Holywar.Api.Filters;
using Holywar.Services;
using Holywar.Api.Controllers.Internal;
using Holywar.Api.Mapping;
using Holywar.Entities.Auths;
using Holywar.Entities.Abstract;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthController : AppController
    {
        AuthService authService;
        UsersService userService;
        SocialNetworksService socialNetworksService;
        
        public AuthController(AuthService authService, UsersService userService, SocialNetworksService socialNetworksService)
        {
            this.authService = authService;
            this.userService = userService;
            this.socialNetworksService = socialNetworksService;
        }

        [Auth]
        [HttpGet]
        public ActionResult<AuthResponse> CheckAuth()
        {
            return Auth.ToResponse();
        }

        [HttpPost]
        public async Task<AuthResponse> SignIn([FromBody] AuthRequest model)
        {
            var result = await userService.SignInByEmailAndPassword(model.Email, model.Password);
            if (result.HasError)
                return new AuthResponse { Error = result.Error };

            var auth = await authService.Create(result.User);
            return auth.ToResponse();
        }

        [HttpPost("vk")]
        public async Task<AuthResponse> SignInVk([FromBody] AuthVkRequest model)
        {
            var result = await socialNetworksService.SignInByVkCode(model.Code, model.RedirectUri, model.ReferralId);
            if (result.HasError)
                return new AuthResponse { Error = result.Error };

            var auth = await authService.Create(result.User);
            return auth.ToResponse();
        }

        [HttpPost("facebook")]
        public async Task<AuthResponse> SignInFb([FromBody] AuthFbRequest model)
        {
            var result = await socialNetworksService.SignInByFacebookToken(model.AccessToken, model.ReferralId);
            if (result.HasError)
                return new AuthResponse { Error = result.Error };

            var auth = await authService.Create(result.User);
            return auth.ToResponse();
        }

        [Auth]
        [HttpPost("change-password")]
        public async Task<ApiResponse> ChangePassword([FromBody] ChangePasswordRequest model)
        {
            var result = await userService.ChangePassword(Auth.UserId, model.OldPassword, model.NewPassword);
            if (result.HasError)
                return new ApiResponse { Error = result.Error };

            return new ApiResponse();
        }

        [HttpPost("recover-password")]
        public async Task<ApiResponse> RecoverPassword([FromBody] RecoverPasswordRequest model)
        {
            var result = await userService.RecoverPassword(model.Email);
            if (result.HasError)
                return new ApiResponse { Error = result.Error };

            return new ApiResponse();
        }

        [HttpPost("change-password-by-token")]
        public async Task<ApiResponse> ChangePasswordByToken([FromBody] ChangePasswordByTokenRequest model)
        {
            var result = await userService.ChangePasswordByToken(model.Password, model.Token.ToObjectId());
            if (result.HasError)
                return new ApiResponse { Error = result.Error };

            return new ApiResponse();
        }

        [Auth]
        [HttpDelete("{id}")]
        public async void Delete()
        {
            await authService.Delete(Auth.Id);
        }
    }
}
