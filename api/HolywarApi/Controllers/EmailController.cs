﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Holywar.Services;
using Holywar.Entities.Abstract;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private ConfigService configService;
        private UsersService usersService;
        private EmailsService emailsService;

        public EmailController(ConfigService configService, UsersService usersService, EmailsService emailsService)
        {
            this.configService = configService;
            this.usersService = usersService;
            this.emailsService = emailsService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<string>> Get(string id)
        {
            var result = await usersService.ConfirmEmail(id);
            var config = await configService.GetConfig();
            var redirectUrl = $"{config.FrontendUrl}{string.Format(config.EmailConfirmedPathTemplate, result.Auth.Id)}";
            return new RedirectResult(redirectUrl);
        }

        [HttpGet("unsubscribe/{email}")]
        public async Task<ApiResponse> Unsubscribe(string email)
        {
            await usersService.Unsubscribe(email);
            return new ApiResponse();
        }

        //[HttpGet("test")]
        //public async Task<string> Email([FromQuery]string to = "ilyes.garifullin@gmail.com")
        //{
        //    try
        //    {
        //        await emailsService.Test(to);
        //        return "sent";
        //    }
        //    catch (Exception e)
        //    {
        //        return e.Message;
        //    }
        //}
    }
}
