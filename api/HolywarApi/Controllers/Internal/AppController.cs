﻿using Holywar.Entities.Auths;
using Microsoft.AspNetCore.Mvc;

namespace Holywar.Api.Controllers.Internal
{
    public class AppController : ControllerBase
    {
        public Auth Auth { get; set; }
    }
}
