﻿using Holywar.Api.Controllers.Internal;
using Holywar.Api.Filters;
using Holywar.Api.Mapping;
using Holywar.Services;
using Holywar.Common;
using Holywar.Entities.Discussions;
using Holywar.Entities.Likes;
using Holywar.Api.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Holywar.Entities.Auths;
using MongoDB.Bson;
using Holywar.Entities.Users;
using Holywar.Entities.Abstract;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DiscussionController : AppController
    {
        DiscussionsService discussionsService;
        LikesService likesService;
        AuthService authService;
        UsersService usersService;

        public DiscussionController(
            DiscussionsService discussionsService,
            LikesService likesService,
            AuthService authService,
            UsersService usersService)
        {
            this.discussionsService = discussionsService;
            this.likesService = likesService;
            this.authService = authService;
            this.usersService = usersService;
        }

        [HttpGet]
        public async Task<IEnumerable<DiscussionModel>> List([FromQuery] DiscussionListFilter filter)
        {
            var discussions = await discussionsService.GetList(filter);
            var users = await usersService.FindById(discussions.Select(d => d.UserId).ToArray());
            return discussions.Select(d => d.ToModel(users)).ToArray();
        }

        [HttpGet("eternal")]
        public async Task<IEnumerable<DiscussionModel>> Eternal([FromQuery] DiscussionListFilter filter)
        {
            var discussions = await discussionsService.GetEternal();
            if (!string.IsNullOrEmpty(filter.AfterDiscussionId))
            {
                var id = filter.AfterDiscussionId.ToObjectId();
                if (id != ObjectId.Empty)
                {
                    var index = discussions.FindIndex(d => d.Id == id);
                    discussions = discussions.Skip(index + 1);
                }
            }
            var limit = filter.Limit > 0 && filter.Limit < 100 ? filter.Limit : 3;
            var result = discussions.Take(limit).ToArray();
            var users = await usersService.FindById(result.Select(d => d.UserId).ToArray());
            return result.Select(d => d.ToModel(users)).ToArray();
        }

        [HttpGet("featured")]
        public async Task<ActionResult<DiscussionModel[]>> Featured()
        {
            var discussions = await discussionsService.GetFeatured();
            if (discussions == null)
                return new EmptyResult();

            var users = await usersService.FindById(discussions.Select(d => d.UserId).ToArray());
            return discussions.Select(d => d.ToModel(users)).ToArray();
        }

        [HttpGet("popular")]
        public async Task<IEnumerable<DiscussionModel>> Popular()
        {
            var discussions = await discussionsService.GetPopular();
            var users = await usersService.FindById(discussions.Select(d => d.UserId).ToArray());
            return discussions.Select(d => d.ToModel(users)).ToArray();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<DiscussionWithCommentsModel>> Get(string id)
        {
            var discussion = await discussionsService.GetByIdOrAlias(id);
            if (discussion.Discussion == null)
                return new NotFoundResult();

            if(!Request.Query.ContainsKey("notAView"))
            {
                await discussionsService.IncrementViews(discussion.Discussion);
            }

            return discussion.ToModel();
        }

        [Auth]
        [HttpPost]
        public async Task<CreateDiscussionResponse> Create([FromBody] DiscussionModel model)
        {
            var discussion = model.ToEntity();
            discussion.UserId = Auth.UserId;
            var survey = model.Survey.ToEntity();
            var result = await discussionsService.Create(discussion, survey);
            if (result.HasError)
            {
                return new CreateDiscussionResponse { Error = result.Error };
            }

            return new CreateDiscussionResponse
            {
                DiscussionId = result.Discussion.Id.ToString()
            };
        }

        [Auth]
        [HttpPost("edit")]
        public async Task<CreateDiscussionResponse> Edit([FromBody] DiscussionModel model)
        {
            var discussion = model.ToEntity();
            var survey = model.Survey.ToEntity();
            var result = await discussionsService.Edit(discussion, survey, Auth.UserId);
            if (result.HasError)
            {
                return new CreateDiscussionResponse { Error = result.Error };
            }

            return new CreateDiscussionResponse();
        }

        [Auth]
        [HttpPost("like")]
        public async Task<LikeDiscussionResponse> Like([FromBody] LikeDiscussionModel model)
        {
            var result = await likesService.LikeDiscussion(model.DiscussionId.ToObjectId(), Auth.UserId, model.IsLike);
            if (result.HasError)
            {
                return new LikeDiscussionResponse { Error = result.Error };
            }
            return new LikeDiscussionResponse
            {
                Discussion = result.Discussion.ToModel()
            };
        }

        [Auth(AuthRole.Service)]
        [HttpPost("externalView")]
        public async Task<ApiResponse> ExternalView([FromBody] ExternalViewModel model)
        {
            var result = await discussionsService.IncrementExternalViews(
                model.DiscussionId.ToObjectId(),
                model.ReferralUserId.ToObjectId());
            if (result.HasError)
            {
                return new ApiResponse { Error = result.Error };
            }
            return new ApiResponse();
        }

        private async Task<Auth> GetAuth()
        {
            var authId = AuthHelper.GetAuth(HttpContext);
            return await authService.FindById(authId);
        }
    }

    public class ExternalViewModel : DiscussionIdModel
    {
        public string ReferralUserId { get; set; }
    }
}
