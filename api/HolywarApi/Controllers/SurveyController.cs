﻿using Holywar.Api.Controllers.Internal;
using Holywar.Api.Filters;
using Holywar.Entities.Surveys;
using Holywar.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Holywar.Api.Mapping;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SurveyController : AppController
    {
        SurveysService surveysService;

        public SurveyController(SurveysService surveysService)
        {
            this.surveysService = surveysService;
        }

        [Auth]
        [HttpPost("vote")]
        public async Task<SurveyVoteResponse> Like([FromBody] SurveyVoteModel model)
        {
            var result = await surveysService.Vote(model, Auth.UserId);
            if (result.HasError)
            {
                return new SurveyVoteResponse { Error = result.Error };
            }
            return new SurveyVoteResponse
            {
                SurveyVotes = result.SurveyVotes.ToModel()
            };
        }
    }
}
