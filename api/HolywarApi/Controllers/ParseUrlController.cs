﻿using Holywar.Api.Filters;
using Holywar.Entities.Common;
using Holywar.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("parse-url")]
    [ApiController]
    public class ParseUrlController : ControllerBase
    {
        private ParserService urlParserService;

        public ParseUrlController(ParserService urlParserService)
        {
            this.urlParserService = urlParserService;
        }

        [Auth]
        [HttpPost]
        public async Task<ParseUrlResponse> Post([FromBody] ParseUrlRequest model)
        {
            var result = await urlParserService.Parse(model.Url, model.CheckDiscussion);
            if (result.HasError)
                return new ParseUrlResponse { Error = result.Error };

            return new ParseUrlResponse
            {
                Title = result.Title,
                Description = result.Description,
                ImageUrl = result.ImageUrl
            };
        }
    }
}
