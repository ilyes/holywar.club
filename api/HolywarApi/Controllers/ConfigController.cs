﻿using Microsoft.AspNetCore.Mvc;
using Holywar.Api.Controllers.Internal;
using Holywar.Services;
using System.Threading.Tasks;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ConfigController : AppController
    {
        private ConfigService configService;

        public ConfigController(ConfigService configService)
        {
            this.configService = configService;
        }

        [HttpGet]
        public async Task<ConfigRespone> Get()
        {
            var config = await configService.GetConfig();
            return new ConfigRespone
            {
                MinPaymentLimit = config.MinPaymentLimit,
                StartPaymentLimit = config.StartPaymentLimit,
                LevelLimitReduceBy = config.LevelLimitReduceBy,
                AdProvider = config.AdProvider,
                WeekRating = config.WeekRating,
                HowToDiscussionId = config.HowToDiscussionId,
                AdminUserId = config.AdminUserId,
                WeekJackpot = config.WeekJackpot,
            };
        }
    }

    public class ConfigRespone
    {
        public short MinPaymentLimit { get; set; }

        public short StartPaymentLimit { get; set; }

        public short LevelLimitReduceBy { get; set; }

        public string AdProvider { get; set; }

        public int WeekRating { get; set; }

        public string HowToDiscussionId { get; set; }

        public string AdminUserId { get; set; }

        public int WeekJackpot { get; set; }
        
    }
}
