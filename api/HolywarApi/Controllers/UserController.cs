﻿using Holywar.Services;
using Holywar.Api.Controllers.Internal;
using Holywar.Api.Filters;
using Holywar.Api.Mapping;
using Holywar.Entities.Users;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Reflection;
using System.IO;

namespace Holywar.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : AppController
    {
        private UsersService usersService;
        private IWebHostEnvironment hostingEnvironment;

        public UserController(UsersService usersService, IWebHostEnvironment hostingEnvironment)
        {
            this.usersService = usersService;
            this.hostingEnvironment = hostingEnvironment;
        }

        [Auth]
        [HttpGet]
        public async Task<UserDto> GetCurrentUser()
        {
            var user = await usersService.FindById(Auth.UserId);
            var levelProgress = await usersService.GetLevelProgress(user);
            return user.ToModel(true, levelProgress);
        }

        [HttpGet("of-the-week")]
        public async Task<IEnumerable<UserDto>> GetUsersOfTheWeek()
        {
            var result = await usersService.GetWeekUsersByLikes(5);
            return result.Users.Select(u => u.ToModel()).ToArray();
        }

        [HttpGet("{id}")]
        public async Task<UserDto> GetUser(string id)
        {
            var user = await usersService.FindById(id);
            return user.ToModel();
        }

        [Auth]
        [HttpGet("referrals")]
        public async Task<IEnumerable<UserDto>> GetReferrals()
        {
            var users = await usersService.GetReferrals(Auth.UserId);
            return users.Select(u => u.ToModel());
        }
        
        [HttpPost]
        public async Task<ActionResult<RegistrationResponse>> SignUp([FromBody] RegistrationRequest model)
        {
            var result = await usersService.Create(model.Name, model.Email, model.Password, model.ReferralId);
            if (result.HasError)
                return new RegistrationResponse { Error = result.Error };

            return new RegistrationResponse();
        }

        [Auth]
        [HttpPost("avatar")]
        public async Task<AvatarResponse> ChangeAvatar([FromForm] AvatarRequest model)
        {
            var result = await usersService.SaveAvatar(model.File, Auth.UserId);
            if (result.HasError)
                return new AvatarResponse { Error = result.Error };

            return new AvatarResponse { AvatarUrl = result.User.AvatarUrl };
        }

        [Auth]
        [HttpGet("partner-info")]
        public async Task<PartnerInfoResponse> PartnerInfo()
        {
            var weekUser = await usersService.GetWeekUser(Auth.UserId);
            if (weekUser == null)
                return null;

            return new PartnerInfoResponse
            {
                WeekUser = weekUser.ToModel(true)
            };
        }
    }

    public class AvatarRequest
    {
        public IFormFile File { get; set; }
    }
}
