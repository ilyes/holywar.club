﻿using System.Collections.Generic;
using System.Linq;
using Holywar.Entities.Likes;

namespace Holywar.Api.Mapping
{
    public static class LikeMapping
    {
        public static LikeWithUserDto ToModel(this LikeWithUser self)
        {
            return new LikeWithUserDto
            {
                Like = self.Like.ToModel(),
                User = self.User.ToModel(),
                TargetUser = self.TargetUser.ToModel()
            };
        }

        public static IEnumerable<LikeWithUserDto> ToModel(this IEnumerable<LikeWithUser> self)
        {
            return self.Select(ToModel).ToArray();
        }

        public static LikeDto ToModel(this Like self)
        {
            return new LikeDto
            {
                UserId = self.UserId.ToString(),
                TargetUserId = self.TargetUserId.ToString(),
                CommentId = self.CommentId.ToString(),
                DiscussionId = self.DiscussionId.ToString(),
                IsLike = self.IsLike
            };
        }
    }
}
