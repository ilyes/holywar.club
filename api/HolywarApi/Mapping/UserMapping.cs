﻿using Holywar.Entities.Users;
using Holywar.Services;
using MongoDB.Bson;

namespace Holywar.Api.Mapping
{
    public static class UserMapping
    {
        public static UserDto ToModel(this User self, bool fullInfo = false, LevelProgress levelProgress = null)
        {
            if (self == null)
                return null;

            return fullInfo
                ? self.ToFullModel(levelProgress)
                : self.ToShortModel();
        }

        public static UserDto ToModel(this WeekUserResult self, bool fullInfo = false, bool withPaid = false)
        {
            var result = self.User.ToModel(fullInfo);
            result.LikesCount = self.LikesCount;
            result.DislikesCount = self.DislikesCount;
            result.CommentsCount = self.CommentsCount;
            result.DiscussionsCount = self.DiscussionsCount;
            result.UniqueCommentatorsCount = self.UniqueCommentatorsCount;
            result.DiscussionViewsCount = self.DiscussionViewsCount;
            result.DiscussionExternalViewsCount = self.DiscussionExternalViewsCount;
            result.HolywarsStarted = self.HolywarsStarted;
            result.HolywarsParticipant = self.HolywarsParticipant;
            result.ReferralsCount = self.ReferralsCount;
            result.Paid = fullInfo || withPaid ? self.Paid : 0;
            result.ReferralPayments = fullInfo || withPaid ? self.ReferralPayments : 0;
            result.WeekRating = fullInfo || withPaid ? self.Rating: 0;
            return result;
        }

        private static UserDto ToShortModel(this User self)
        {
            return new UserDto
            {
                Id = self.Id.ToString(),
                Name = self.Name,
                AvatarUrl = self.AvatarUrl,
                LikesCount = self.LikesCount,
                DislikesCount = self.DislikesCount,
                HolywarsStarted = self.HolywarsStarted,
                ReferralsCount = self.ReferralsCount,
                CommentsCount = self.CommentsCount,
                DiscussionsCount = self.DiscussionsCount,
            };
        }

        private static UserDto ToFullModel(this User self, LevelProgress levelProgress = null)
        {
            var result = self.ToShortModel();
            result.Email = self.Email;
            result.ReferralId = self.ReferralId != ObjectId.Empty
                ? self.ReferralId.ToString()
                : "";
            result.Balance = self.Balance;
            result.ReferralPayments = self.ReferralPayments;
            result.TotalEarned = self.TotalEarned;
            result.Role = self.Role;
            result.CategoryIds = self.CategoryIds;
            result.PaymentLimit = self.PaymentLimit;
            result.PaymentLimitWeekReduce = self.PaymentLimitWeekReduce;
            result.Level = self.Level;
            result.LevelProgress = levelProgress;
            return result;
        }
    }
}
