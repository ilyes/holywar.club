﻿using Holywar.Entities.Common;
using Holywar.Common;

namespace Holywar.Api.Mapping
{
    public static class ConfigMapping
    {
        public static ConfigDto ToDto(this Config config)
        {
            return new ConfigDto
            {
                FrontendUrl = config.FrontendUrl,
                EmailConfirmedPathTemplate = config.EmailConfirmedPathTemplate,
                EmailConfirmPathTemplate = config.EmailConfirmPathTemplate,
                WebPath = config.WebPath,
                AvatarsFolder = config.AvatarsFolder,
                StaticUrl = config.StaticUrl,
                StaticPath = config.StaticPath,
                SetPasswordPathTemplate = config.SetPasswordPathTemplate,
                DiscussionPathTemplate = config.DiscussionPathTemplate,
                CommentReplyPathTemplate = config.CommentReplyPathTemplate,
                FeaturedDiscussionIds = config.FeaturedDiscussionIds,
                HolywarCandidatesStep = config.HolywarCandidatesStep,
            };
        }

        public static Config ToEntity(this ConfigDto config)
        {
            return new Config
            {
                FrontendUrl = config.FrontendUrl,
                EmailConfirmedPathTemplate = config.EmailConfirmedPathTemplate,
                EmailConfirmPathTemplate = config.EmailConfirmPathTemplate,
                WebPath = config.WebPath,
                AvatarsFolder = config.AvatarsFolder,
                StaticUrl = config.StaticUrl,
                StaticPath = config.StaticPath,
                SetPasswordPathTemplate = config.SetPasswordPathTemplate,
                DiscussionPathTemplate = config.DiscussionPathTemplate,
                CommentReplyPathTemplate = config.CommentReplyPathTemplate,
                FeaturedDiscussionId = config.FeaturedDiscussionId.ToObjectId(),
                HolywarCandidatesStep = config.HolywarCandidatesStep,
            };
        }
    }
}
