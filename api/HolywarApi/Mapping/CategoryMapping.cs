﻿using Holywar.Entities.Categories;
using System.Collections.Generic;
using System.Linq;

namespace Holywar.Api.Mapping
{
    public static class CategoryMapping
    {
        public static CategoryModel ToModel(this Category self)
        {
            return new CategoryModel
            {
                Id = self.Id.ToString(),
                Alias = self.Alias,
                Name = self.Name
            };
        }

        public static IEnumerable<CategoryModel> ToModel(this IEnumerable<Category> self)
        {
            return self.Select(ToModel).ToList();
        }
    }
}
