﻿
using System.Linq;
using System.Collections.Generic;

using MongoDB.Bson;

using Holywar.Services;
using Holywar.Entities.Discussions;
using Holywar.Common;
using Holywar.Entities.Users;

namespace Holywar.Api.Mapping
{
    public static class DiscussionMapping
    {
        public static DiscussionModel ToModel(this Discussion self, IEnumerable<User> users = null)
        {
            if (self == null)
                return null;

            return new DiscussionModel
            {
                Id = self.Id.ToString(),
                Alias = self.Alias,
                CreationDate = self.CreationDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                Title = self.Title,
                Description = self.Description,
                ImageUrl = self.ImageUrl,
                Url = self.Url,
                Body = self.Body != null ? self.Body.Select(ToModel).ToArray() : null,
                LikesCount = self.LikesCount,
                DislikesCount = self.DislikesCount,
                ViewsCount = self.ViewsCount,
                CommentsCount = self.CommetsCount,
                CategoryId = self.CategoryId.ToString(),
                IsHolywar = self.IsHolywar,
                Author = TryMapAuthor(self.UserId, users)
            };
        }

        private static DiscussionAuthorModel TryMapAuthor(ObjectId userId, IEnumerable<User> users)
        {
            if (users == null)
                return null;

            var user = users.FirstOrDefault(u => u.Id == userId);
            if (user == null)
                return null;

            return new DiscussionAuthorModel
            {
                UserId = user.Id.ToString(),
                Name = user.Name,
                AvatarUrl = user.AvatarUrl
            };
        }

        public static DiscussionBodyItemModel ToModel(this DiscussionBodyItem self)
        {
            return new DiscussionBodyItemModel
            {
                Type = self.Type,
                Value = self.Value
            };
        }

        public static DiscussionWithCommentsModel ToModel(this DiscussionWithComments self)
        {
            return new DiscussionWithCommentsModel
            {
                Discussion = self.Discussion.ToModel(),
                User = self.User.ToModel(),
                Comments = self.Comments.ToModel(),
                Survey = self.Survey.ToModel(),
                SurveyVotes = self.SurveyVotes.Select(s => s.ToModel()).ToArray()
            };
        }

        public static Discussion ToEntity(this DiscussionModel self)
        {
            return new Discussion
            {
                Id = self.Id.ToObjectId(),
                Url = self.Url,
                Title = self.Title,
                Description = self.Description,
                ImageUrl = self.ImageUrl,
                Body = self.Body != null 
                    ? self.Body.Select(ToEntity).ToArray()
                    : null,
                CategoryId = self.CategoryId.ToObjectId()
            };
        }

        public static DiscussionBodyItem ToEntity(this DiscussionBodyItemModel self)
        {
            return new DiscussionBodyItem
            {
                Type = self.Type,
                Value = self.Value
            };
        }
    }
}
