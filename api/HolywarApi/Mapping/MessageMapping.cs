﻿using Holywar.Entities.Messages;
using System.Collections.Generic;
using System.Linq;

namespace Holywar.Api.Mapping
{
    public static class MessageMapping
    {
        public static MessageWithUserModel ToModel(this MessageWithUser self)
        {
            return new MessageWithUserModel
            {
                Message = self.Message.ToModel(),
                FromUser = self.FromUser.ToModel()
            };
        }

        public static IEnumerable<MessageWithUserModel> ToModel(this IEnumerable<MessageWithUser> self)
        {
            return self.Select(ToModel).ToList();
        }

        public static MessageModel ToModel(this Message self)
        {
            return new MessageModel
            {
                Id = self.Id.ToString(),
                Content = self.Text,
                Type = self.Type,
                IsRead = self.IsRead,
                CreationDate = self.CreationDate.ToString("yyyy-MM-ddTHH:mm:ss")
            };
        }

        public static IEnumerable<MessageModel> ToModel(this IEnumerable<Message> self)
        {
            return self.Select(ToModel).ToList();
        }
    }
}
