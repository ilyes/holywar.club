﻿using System.Linq;
using System.Collections.Generic;
using MongoDB.Bson;
using Holywar.Repositories;
using Holywar.Entities.Comments;
using System;

namespace Holywar.Api.Mapping
{
    public static class CommentMapping
    {
        public static DateTime StartTime = new DateTime(1970, 1, 1, 0, 0, 0);

        public static CommentRequest ToModel(this Comment self)
        {
            return new CommentRequest
            {
                Id = self.Id.ToString(),
                DiscussionId = self.DiscussionId.ToString(),
                Text = self.Text,
                ImageUrl = self.ImageUrl,
                ParentCommentId = self.ParentCommentId == ObjectId.Empty ? null : self.ParentCommentId.ToString(),
                LikesCount = self.LikesCount,
                DislikesCount = self.DislikesCount,
                CreationDate = self.CreationDate.ToString("yyyy-MM-ddTHH:mm:ss")
            };
        }

        public static CommentWithUserModel ToModel(this CommentWithUser self)
        {
            return new CommentWithUserModel
            {
                Comment = self.Comment.ToModel(),
                User = self.User.ToModel(),
                DiscussionAlias = self.DiscussionAlias
            };
        }

        public static IEnumerable<CommentWithUserModel> ToModel(this IEnumerable<CommentWithUser> self)
        {
            return self.Select(ToModel).ToList();
        }
    }
}
