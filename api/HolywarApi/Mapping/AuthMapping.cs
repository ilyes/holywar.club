﻿using Holywar.Entities.Auths;

namespace Holywar.Api.Mapping
{
    public static class AuthMapping
    {
        public static AuthResponse ToResponse(this Auth self)
        {
            return new AuthResponse
            {
                Token = self.Id.ToString(),
            };
        }
    }
}
