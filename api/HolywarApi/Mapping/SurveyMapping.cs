﻿using Holywar.Common;
using Holywar.Entities.Surveys;
using System.Linq;

namespace Holywar.Api.Mapping
{
    public static class SurveyMapping
    {
        public static SurveyModel ToModel(this Survey self)
        {
            if (self == null)
                return null;

            return new SurveyModel
            {
                Id = self.Id.ToString(),
                DiscusionId = self.DiscusionId.ToString(),
                Items = self.Items.Select(i => new SurveyItemModel
                {
                    Id = i.Id.ToString(),
                    Title = i.Title
                }).ToArray()
            };
        }

        public static Survey ToEntity(this SurveyModel self)
        {
            if (self == null || self.Items.Count(i => !string.IsNullOrWhiteSpace(i.Title)) < 2)
                return null;

            return new Survey
            {
                Id = self.Id.ToObjectId(),
                DiscusionId = self.DiscusionId.ToObjectId(),
                Items = self.Items.Select(i => new SurveyItem
                {
                    Id = i.Id.ToObjectId(),
                    Title = i.Title
                }).ToArray()
            };
        }

        public static SurveyVotesModel ToModel(this SurveyVotes self)
        {
            return new SurveyVotesModel
            {
                Id = self.Id.ToString(),
                Votes = self.Votes
            };
        }

        public static SurveyVotesModel[] ToModel(this SurveyVotes[] self)
        {
            return self.Select(ToModel).ToArray();
        }
    }
}
