﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Holywar.Api.Controllers.Internal;
using Holywar.Api.Helpers;
using Holywar.Common;
using Holywar.Entities.Users;
using Holywar.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;

namespace Holywar.Api.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class AuthAttribute : Attribute, IAsyncActionFilter
    {
        private string role;

        public AuthAttribute() {}

        public AuthAttribute(string role)
        {
            this.role = role;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var authToken = AuthHelper.GetAuth(context.HttpContext);
            
            if (role == AuthRole.Service)
            {
                var config = context.HttpContext.RequestServices.GetService(typeof(IConfigurationRoot)) as IConfigurationRoot;
                var isService = await IsServiceIp(context);
                if (isService || authToken == config["ServiceAuthToken"])
                {
                    await next();
                }
                else
                {
                    context.Result = new UnauthorizedResult();
                }
                return;
            }

            var authService = context.HttpContext.RequestServices.GetService(typeof(AuthService)) as AuthService;
            var auth = await authService.FindById(authToken);
            if (auth == null)
            {
                context.Result = new UnauthorizedResult();
                return;
            }
            if (!string.IsNullOrEmpty(role))
            {
                var usersService = context.HttpContext.RequestServices.GetService(typeof(UsersService)) as UsersService;
                var user = await usersService.FindById(auth.UserId);
                if(user == null || user.Role != role)
                {
                    context.Result = new UnauthorizedResult();
                    return;
                }
            }
            var controller = context.Controller as AppController;
            if (controller != null)
            {
                controller.Auth = auth;
            }
            await next();
        }

        private async Task<bool> IsServiceIp(ActionExecutingContext context)
        {
            try
            {
                var clientIp = context.HttpContext.Connection.RemoteIpAddress;
                if (localIp.Equals(clientIp))
                {
                    return true;
                }

                var currentServerIp = GetIPAddresses();
                if (currentServerIp.Any(a => a.Equals(clientIp)))
                {
                    return true;
                }

                var configService = context.HttpContext.RequestServices.GetService(typeof(ConfigService)) as ConfigService;
                var config = await configService.GetConfig();
                if (config.AvailableServieIps == null)
                {
                    return false;
                }

                var isServiceIp = config.AvailableServieIps.Any(ip => new IPAddress(ip.Select(i => (byte)i).ToArray()).Equals(clientIp));
                return isServiceIp;
            }
            catch(Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }

        private static IPAddress localIp = new IPAddress(new byte[] { 127, 0, 0, 1 });

        private static IPAddress[] GetIPAddresses()
        {
            var ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            return ipHostInfo.AddressList;
        }
    }
}
