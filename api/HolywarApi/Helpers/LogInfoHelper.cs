﻿using Microsoft.AspNetCore.Http;
using System.Text;

namespace Holywar.Api.Helpers
{
    public class LogInfoHelper
    {
        public static string GetContextInfo(HttpContext context)
        {
            var result = new StringBuilder();

            result.Append("Handler: ");
            result.AppendLine(context.Request.Path);

            result.Append("IpAddress: ");
            result.AppendLine(context.Connection.RemoteIpAddress.ToString());

            var authId = AuthHelper.GetAuth(context);
            if (!string.IsNullOrEmpty(authId))
            {
                result.Append("AuthId: ");
                result.AppendLine(authId);
            }

            return result.ToString();
        }
    }
}
