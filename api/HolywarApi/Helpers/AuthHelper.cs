﻿using Microsoft.AspNetCore.Http;
using System.Linq;

namespace Holywar.Api.Helpers
{
    public class AuthHelper
    {
        public const string AUTH_HEADER = "Auth";

        public static string GetAuth(HttpContext context)
        {
            return context.Request.Headers[AUTH_HEADER].FirstOrDefault();
        }
    }
}
