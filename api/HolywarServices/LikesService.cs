﻿using Holywar.Common;
using Holywar.Entities.Abstract;
using Holywar.Entities.Comments;
using Holywar.Entities.Discussions;
using Holywar.Entities.Likes;
using Holywar.Entities.Users;
using Holywar.Repositories;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Holywar.Services
{
    public class LikeCommentResult : ServiceResult
    {
        public Like Like;
        public Comment Comment;
    }

    public class LikeDiscussionResult : ServiceResult
    {
        public Like Like;
        public Discussion Discussion;
    }

    public class LikesService
    {
        private LikesRepository likesRepository;
        private UsersRepository usersRepository;
        private CommentsRepository commentsRepository;
        private DiscussionsRepository discussionsRepository;
        private WeekUsersRepository weekUsersRepository;
        private WeekCommentsRepository weekCommentsRepository;

        public LikesService(
            LikesRepository likesRepository,
            UsersRepository usersRepository,
            CommentsRepository commentsRepository,
            DiscussionsRepository discussionsRepository,
            WeekUsersRepository weekUsersRepository,
            WeekCommentsRepository weekCommentsRepository)
        {
            this.likesRepository = likesRepository;
            this.usersRepository = usersRepository;
            this.commentsRepository = commentsRepository;
            this.discussionsRepository = discussionsRepository;
            this.weekUsersRepository = weekUsersRepository;
            this.weekCommentsRepository = weekCommentsRepository;
        }

        public async Task<LikeCommentResult> LikeComment(ObjectId commentId, ObjectId userId, bool isLike)
        {
            var comment = await commentsRepository.FindById(commentId);
            if (comment == null)
                return new LikeCommentResult { Error = "NoSuchComment" };
            if (comment.UserId == userId)
                return new LikeCommentResult { Error = "UnableLikeYouself" };

            var user = await usersRepository.FindById(userId);
            if (user == null)
                return new LikeCommentResult { Error = "NoSuchUser" };
            if (user.IsWriteProhibited)
                return new LikeCommentResult { Error = "Prohibited" };

            var commentUser = await usersRepository.FindById(comment.UserId);
            var like = await likesRepository.Find(l => l.CommentId == commentId && l.UserId == userId);
            if (like != null && user.Role != AuthRole.Admin)
            {
                return await Update(comment, commentUser, like, isLike); 
            }
            else
            {
                return await Create(comment, commentUser, userId, isLike);
            }
        }

        public async Task<IEnumerable<LikeWithUser>> GetByCommentId(ObjectId commentId)
        {
            var likes = await likesRepository.Where(l => l.CommentId == commentId, limit: 100);
            return await MapToLikeWithUser(likes);
        }

        public async Task<IEnumerable<LikeWithUser>> GetByTargetUserId(ObjectId userId)
        {
            var likes = await likesRepository.Where(l => l.TargetUserId == userId, limit: 100);
            return await MapToLikeWithUser(likes);
        }

        public async Task<IEnumerable<LikeWithUser>> GetByUserId(ObjectId userId)
        {
            var likes = await likesRepository.Where(l => l.UserId == userId, limit: 100);
            return await MapToLikeWithUser(likes);
        }

        private async Task<IEnumerable<LikeWithUser>> MapToLikeWithUser(IEnumerable<Like> likes)
        {
            var userIds = likes.Select(l => l.UserId).Concat(likes.Select(l => l.TargetUserId)).ToArray();
            var users = await usersRepository.FindById(userIds);
            return likes.Select(l => new LikeWithUser
            {
                Like = l,
                User = users.FirstOrDefault(u => u.Id == l.UserId),
                TargetUser = users.FirstOrDefault(u => u.Id == l.TargetUserId),
            }).ToArray();
        }


        public async Task<LikeDiscussionResult> LikeDiscussion(ObjectId discussionId, ObjectId userId, bool isLike)
        {
            var discussion = await discussionsRepository.FindById(discussionId);
            if (discussion == null)
                return new LikeDiscussionResult { Error = "NoSuchDiscussion" };
            if (discussion.UserId == userId)
                return new LikeDiscussionResult { Error = "UnableLikeYouself" };

            var user = await usersRepository.FindById(userId);
            if (user == null)
                return new LikeDiscussionResult { Error = "NoSuchUser" };
            if (user.IsWriteProhibited)
                return new LikeDiscussionResult { Error = "Prohibited" };

            var discussionUser = await usersRepository.FindById(discussion.UserId);
            var like = await likesRepository.Find(l => l.DiscussionId == discussionId && l.UserId == userId);
            if (like != null && user.Role != AuthRole.Admin)
            {
                return await Update(discussion, discussionUser, like, isLike);
            }
            else
            {
                return await Create(discussion, discussionUser, userId, isLike);
            }
        }

        private async Task<LikeDiscussionResult> Create(Discussion discussion, User discussionUser, ObjectId userId, bool isLike)
        {
            var like = new Like
            {
                CreationDate = DateTime.UtcNow,
                DiscussionId = discussion.Id,
                TargetUserId = discussion.UserId,
                UserId = userId,
                IsLike = isLike
            };

            if (isLike)
            {
                discussionUser.LikesCount++;
                discussion.LikesCount++;
            }
            else
            {
                discussionUser.DislikesCount++;
                discussion.DislikesCount++;
            }

            await likesRepository.Transaction(async () =>
            {
                await likesRepository.Add(like);
                await discussionsRepository.UpdateAsync(discussion.Id, isLike
                    ? discussionsRepository.Update.Set(d => d.LikesCount, discussion.LikesCount)
                    : discussionsRepository.Update.Set(d => d.DislikesCount, discussion.DislikesCount));
                await usersRepository.UpdateAsync(discussionUser.Id, isLike
                    ? usersRepository.Update.Set(d => d.LikesCount, discussionUser.LikesCount)
                    : usersRepository.Update.Set(d => d.DislikesCount, discussionUser.DislikesCount));
                await weekUsersRepository.ChangeLikes(discussionUser.Id, isLike ? 1 : 0, isLike ? 0 : 1);
            });

            return new LikeDiscussionResult { Like = like, Discussion = discussion };
        }

        private async Task<LikeDiscussionResult> Update(Discussion discussion, User discussionUser, Like like, bool isLike)
        {
            if (like.IsLike == isLike)
                return new LikeDiscussionResult { Like = like, Discussion = discussion };

            like.IsLike = isLike;
            if (isLike)
            {
                discussion.LikesCount++;
                discussion.DislikesCount--;
                discussionUser.LikesCount++;
                discussionUser.DislikesCount--;
            }
            else
            {
                discussion.LikesCount--;
                discussion.DislikesCount++;
                discussionUser.LikesCount--;
                discussionUser.DislikesCount++;
            }

            await likesRepository.Transaction(async () =>
            {
                await likesRepository.UpdateAsync(like.Id, l => l.IsLike, isLike);
                await discussionsRepository.UpdateAsync(discussion.Id, discussionsRepository.Update.Combine(new[] {
                    discussionsRepository.Update.Set(d => d.LikesCount, discussion.LikesCount),
                    discussionsRepository.Update.Set(d => d.DislikesCount, discussion.DislikesCount)
                }));
                await usersRepository.UpdateAsync(discussionUser.Id, usersRepository.Update.Combine(new[] {
                    usersRepository.Update.Set(u => u.LikesCount, discussionUser.LikesCount),
                    usersRepository.Update.Set(u => u.DislikesCount, discussionUser.DislikesCount)
                }));
                await weekUsersRepository.ChangeLikes(discussionUser.Id, isLike ? 1 : -1, isLike ? -1 : 1);
            });

            return new LikeDiscussionResult { Like = like, Discussion = discussion };
        }

        private async Task<LikeCommentResult> Create(Comment comment, User commentUser, ObjectId userId, bool isLike)
        {
            var like = new Like
            {
                CreationDate = DateTime.UtcNow,
                CommentId = comment.Id,
                TargetUserId = comment.UserId,
                UserId = userId,
                IsLike = isLike
            };

            if (isLike)
            {
                commentUser.LikesCount++;
                comment.LikesCount++;
            }
            else
            {
                commentUser.DislikesCount++;
                comment.DislikesCount++;
            }

            await likesRepository.Transaction(async () =>
            {
                await likesRepository.Add(like);
                await commentsRepository.UpdateAsync(comment.Id, isLike
                    ? commentsRepository.Update.Set(d => d.LikesCount, comment.LikesCount)
                    : commentsRepository.Update.Set(d => d.DislikesCount, comment.DislikesCount));
                await usersRepository.UpdateAsync(commentUser.Id, isLike
                    ? usersRepository.Update.Set(c => c.LikesCount, commentUser.LikesCount)
                    : usersRepository.Update.Set(c => c.DislikesCount, commentUser.DislikesCount));
                await weekUsersRepository.ChangeLikes(commentUser.Id, isLike ? 1 : 0, isLike ? 0 : 1);
                await weekCommentsRepository.ChangeLikes(comment.Id, isLike ? 1 : 0, isLike ? 0 : 1);
            });

            return new LikeCommentResult { Like = like, Comment = comment };
        }

        private async Task<LikeCommentResult> Update(Comment comment, User commentUser, Like like, bool isLike)
        {
            if (like.IsLike == isLike)
                return new LikeCommentResult { Like = like, Comment = comment };

            like.IsLike = isLike;
            if (isLike)
            {
                comment.LikesCount++;
                comment.DislikesCount--;
                commentUser.LikesCount++;
                commentUser.DislikesCount--;
            }
            else
            {
                comment.LikesCount--;
                comment.DislikesCount++;
                commentUser.LikesCount--;
                commentUser.DislikesCount++;
            }

            await likesRepository.Transaction(async () =>
            {
                await likesRepository.UpdateAsync(like.Id, l => l.IsLike, isLike);
                await commentsRepository.UpdateAsync(comment.Id, commentsRepository.Update.Combine(new[] {
                    commentsRepository.Update.Set(u => u.LikesCount, comment.LikesCount),
                    commentsRepository.Update.Set(u => u.DislikesCount, comment.DislikesCount)
                }));
                await usersRepository.UpdateAsync(commentUser.Id, usersRepository.Update.Combine(new[] {
                    usersRepository.Update.Set(u => u.LikesCount, commentUser.LikesCount),
                    usersRepository.Update.Set(u => u.DislikesCount, commentUser.DislikesCount)
                }));
                await weekUsersRepository.ChangeLikes(commentUser.Id, isLike ? 1 : -1, isLike ? -1 : 1);
                await weekCommentsRepository.ChangeLikes(comment.Id, isLike ? 1 : -1, isLike ? -1 : 1);
            });

            return new LikeCommentResult { Like = like, Comment = comment };
        }
    }
}
