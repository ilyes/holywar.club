﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Http;

using MongoDB.Bson;

using Holywar.Common;
using Holywar.Entities.Admin.Balance;
using Holywar.Entities.Abstract;
using Holywar.Entities.Messages;
using Holywar.Entities.Auths;
using Holywar.Entities.Users;
using Holywar.Repositories;

namespace Holywar.Services
{
    public class UsersServiceResult : ServiceResult
    {
        public User User { get; set; }
    }

    public class EmailConfirmationResult : UsersServiceResult
    {
        public Auth Auth { get; set; }
    }

    public class WeekUsersResult : ServiceResult
    {
        public int Id { get; set; }

        public WeekUserResult[] Users { get; set; }
    }

    public class WeekUserResult
    {
        public User User { get; set; }

        public int LikesCount { get; set; }

        public int DislikesCount { get; set; }
        
        public int CommentsCount { get; set; }

        public int DiscussionsCount { get; set; }

        public int Paid { get; set; }

        public int ReferralPayments { get; set; }

        public int ReferralsCount { get; set; }

        public int DiscussionViewsCount { get; set; }

        public int DiscussionExternalViewsCount { get; set; }

        public int UniqueCommentatorsCount { get; set; }
        
        public int HolywarsStarted { get; set; }

        public int HolywarsParticipant { get; set; }

        public int Rating { get; set; }
    }

    public delegate Task SaveFile(FileStream stream);

    public class UsersService
    {
        private static string SALT = "5d4ef0a9f0d26d56f8bc9752";
        private static int AVATAR_SIZE = 160;

        private UsersRepository usersRepository;
        private WeekUsersRepository weekUsersRepository;
        private TokensRepository tokensRepository;

        private AuthService authService;
        private ConfigService configService;
        private EmailsService emailsService;
        private MessagesService messagesService;

        public UsersService(
            UsersRepository usersRepository,
            EmailsService emailsService, 
            WeekUsersRepository weekUsersRepository,
            TokensRepository tokensRepository,
            AuthService authService, 
            ConfigService configService,
            MessagesService messagesService
            )
        {
            this.usersRepository = usersRepository;
            this.emailsService = emailsService;
            this.weekUsersRepository = weekUsersRepository;
            this.tokensRepository = tokensRepository;
            this.authService = authService;
            this.configService = configService;
            this.messagesService = messagesService;
        }

        public async Task<WeekUserResult> GetWeekUser(ObjectId userId)
        {
            var weekId = DateUtil.GetCurrentWeekId();
            var user = await usersRepository.FindById(userId);
            var weekUser = await weekUsersRepository.Find(w => w.UserId == userId && w.WeekId == weekId);
            return CreateWeekUserResult(user, weekUser ?? new WeekUser(), withAll: true);
        }

        public Task Delete(ObjectId userId, ObjectId objectId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<User>> GetReferrals(ObjectId userId)
        {
            return await usersRepository.Where(u => u.ReferralId == userId);
        }

        public async Task<User> FindByEmail(string email)
        {
            return await usersRepository.FindByEmail(email);
        }

        public async Task<User> FindById(string id)
        {
            return await usersRepository.FindById(id);
        }
        
        public async Task<User> FindById(ObjectId id)
        {
            return await usersRepository.FindById(id);
        }

        public async Task<UsersServiceResult> SignInByEmailAndPassword(string email, string password)
        {
            var user = await usersRepository.FindByEmail(email);
            if (user == null)
                return new UsersServiceResult { Error = "NotFound" };
            if (!user.IsEmailConfirmed || user.Hash != CreateMd5Hash(password))
                return new UsersServiceResult { Error = "NotFound" };
            return new UsersServiceResult { User = user };
        }

        public async Task<UsersServiceResult> ChangePassword(ObjectId userId, string oldPassword, string newPassword)
        {
            var user = await usersRepository.FindById(userId);
            if (user.Hash != CreateMd5Hash(oldPassword))
                return new UsersServiceResult { Error = "IncorrectPassword" };
            if(string.IsNullOrEmpty(newPassword))
                return new UsersServiceResult { Error = "PasswordToShort" };

            user.Hash = CreateMd5Hash(newPassword);
            await usersRepository.UpdateAsync(user.Id, u => u.Hash, user.Hash);
            return new UsersServiceResult { User = user };
        }

        public async Task<UsersServiceResult> SaveCategoryIds(ObjectId userId, string[] categoryIds)
        {
            var user = await usersRepository.FindById(userId);
            user.CategoryIds = categoryIds;
            await usersRepository.UpdateAsync(user.Id, u => u.CategoryIds, user.CategoryIds);
            return new UsersServiceResult { User = user };
        }

        public async Task<UsersServiceResult> RecoverPassword(string email)
        {
            var user = await usersRepository.FindByEmail(email);
            if (user == null)
                return new UsersServiceResult { Error = "NotFound" };
            var token = new Token
            {
                Type = TokenType.RecoverPassword,
                UserId = user.Id,
                ExpirationDate = DateTime.UtcNow.AddHours(1)
            };
            await tokensRepository.Add(token);
            var config = await configService.GetConfig();
            var confirmUrl = $"{config.FrontendUrl}{string.Format(config.SetPasswordPathTemplate, token.Id)}";
            await emailsService.Enqueue(
                user.Id,
                user.Email,
                "Восстановление пароля",
                "Перейдите по ссылке для того чтобы задать новый пароль",
                confirmUrl,
                "Восстановить пароль");
            return new UsersServiceResult { User = user };
        }

        public async Task<UsersServiceResult> ChangePasswordByToken(string password, ObjectId tokenId)
        {
            if (string.IsNullOrEmpty(password))
                return new UsersServiceResult { Error = "PasswordToShort" };

            var token = await tokensRepository.FindById(tokenId);
            if (token == null || token.ExpirationDate < DateTime.UtcNow)
                return new UsersServiceResult { Error = "NotFound" };

            var user = await usersRepository.FindById(token.UserId);
            if (user == null)
                return new UsersServiceResult { Error = "NotFound" };

            user.Hash = CreateMd5Hash(password);
            await usersRepository.UpdateAsync(user.Id, u => u.Hash, user.Hash);
            return new UsersServiceResult { User = user };
        }

        public async Task<UsersServiceResult> SaveAvatar(IFormFile file, ObjectId userId)
        {
            var fileExtension = Path.GetExtension(file.FileName).ToLower();
            if (fileExtension != ".jpg" && fileExtension != ".png")
                return new UsersServiceResult { Error = "IncorrectAvatarExtension" };

            var config = await configService.GetConfig();

            var fileNameToSave = $"{userId}.jpg";
            var staticFolder = StaticUtil.GetStaticPath(config.StaticPath);
            var fireDirectory = Path.Combine(staticFolder, config.AvatarsFolder);
            if (!Directory.Exists(fireDirectory))
                Directory.CreateDirectory(fireDirectory);

            var filePath = Path.Combine(fireDirectory, fileNameToSave);
            var fileUrl = $"{config.StaticUrl}{config.AvatarsFolder}{fileNameToSave}";

            FileUtil.SaveImage(file, filePath, AVATAR_SIZE);

            return await SetAvatar(fileUrl, userId);
        }

        public async Task<UsersServiceResult> SetAvatar(string fileUrl, ObjectId userId)
        {
            var user = await usersRepository.FindById(userId);
            if(user == null)
            {
                return new UsersServiceResult
                {
                    Error = "UserNotFound"
                };
            }
            user.AvatarUrl = fileUrl;
            await usersRepository.UpdateAsync(user.Id, u => u.AvatarUrl, user.AvatarUrl);
            return new UsersServiceResult { User = user };
        }

        public async Task<UsersServiceResult> Create(string name, string email, string password, string referralId)
        {
            var user = await usersRepository.FindByEmail(email);
            var refId = referralId.ToObjectId();
            var config = await configService.GetConfig();

            if (user != null)
            {
                if (user.IsEmailConfirmed)
                {
                    return new UsersServiceResult { Error = "ElreadyExists" };
                }

                user.Name = name;
                user.Email = email;
                user.Hash = CreateMd5Hash(password);
                user.ReferralId = refId;
                user.Role = AuthRole.Client;
                user.IsUnsubscribed = false;

                await usersRepository.Transaction(async () =>
                {
                    await usersRepository.UpdatesAsync(user,
                        usersRepository.Update.Set(u => u.Name, user.Name),
                        usersRepository.Update.Set(u => u.Email, user.Email),
                        usersRepository.Update.Set(u => u.Hash, user.Hash),
                        usersRepository.Update.Set(u => u.ReferralId, user.ReferralId),
                        usersRepository.Update.Set(u => u.IsUnsubscribed, user.IsUnsubscribed),
                        usersRepository.Update.Set(u => u.Role, user.Role));
                    await SendConfirmationEmail(user);
                    await TryIncrementReferralsCount(refId);
                });
            }
            else
            {
                user = new User
                {
                    Name = name,
                    Email = email,
                    Hash = CreateMd5Hash(password),
                    CreationDate = DateTime.UtcNow,
                    ReferralId = refId,
                    Role = AuthRole.Client,
                    PaymentLimit = config.StartPaymentLimit
                };

                await usersRepository.Transaction(async () =>
                {
                    await usersRepository.Add(user);
                    await SendConfirmationEmail(user);
                    await TryIncrementReferralsCount(refId);
                });
            }

            if (!string.IsNullOrEmpty(config.HowToDiscussionId))
            {
                await messagesService.Create(user.Id, MessageType.HowTo);
            }

            return new UsersServiceResult { User = user };
        }

        private async Task TryIncrementReferralsCount(ObjectId refId)
        {
            if(refId == ObjectId.Empty)
            {
                return;
            }

            var user = await FindById(refId);
            await usersRepository.UpdateAsync(user.Id, usersRepository.Update.Set(u => u.ReferralsCount, user.ReferralsCount + 1));
            await weekUsersRepository.IncrementReferralsCount(refId);
        }

        private async Task SendConfirmationEmail(User user)
        {
            var config = await configService.GetConfig();
            var confirmUrl = $"{string.Format(config.EmailConfirmPathTemplate, user.Id)}";
            await emailsService.Enqueue(
                user.Id,
                user.Email,
                "Подтверждение регистрации",
                "Для того чтобы завершить регистрацию перейдите по ссылке",
                confirmUrl,
                "Подтвердить регистрацию");
        }

        public async Task<EmailConfirmationResult> ConfirmEmail(string id)
        {
            var user = await usersRepository.FindById(id);
            if (user == null)
            {
                return null;
            }
            user.IsEmailConfirmed = true;

            Auth auth = null;
            await usersRepository.Transaction(async () =>
            {
                await usersRepository.UpdateAsync(user, u => u.IsEmailConfirmed);
                auth = await authService.Create(user);
            });

            return new EmailConfirmationResult
            {
                User = user,
                Auth = auth
            };
        }

        public async Task<ServiceResult> Unsubscribe(string email)
        {
            var user = await usersRepository.FindByEmail(email);
            if (user == null)
            {
                return new ServiceResult { Error = "NotFound" };
            }

            await usersRepository.UpdateAsync(user.Id, u => u.IsUnsubscribed, false);

            return new ServiceResult();
        }

        public async Task<WeekUsersResult> GetWeekUsersForAddBalance(int? week)
        {
            var weekUsers = !week.HasValue 
                ? await weekUsersRepository.GetLastActiveWeek()
                : await weekUsersRepository.Where(u => u.WeekId == week.Value);
            var topWeekUsers = weekUsers.OrderByDescending(u => u.Rating).ToArray();
            var weekId = topWeekUsers.FirstOrDefault()?.WeekId ?? DateUtil.GetCurrentWeekId();
            return await MapToWeekUsersResult(weekId, topWeekUsers, forAddBalance: true);
        }

        public async Task<WeekUsersResult> GetWeekUsersByRating()
        {
            var weekUsers = await weekUsersRepository.GetLastActiveWeek();
            IEnumerable<WeekUser> topWeekUsers = weekUsers.OrderByDescending(u => u.LikesCount - u.DisikesCount);
            var weekId = topWeekUsers.FirstOrDefault()?.WeekId ?? DateUtil.GetCurrentWeekId();
            return await MapToWeekUsersResult(weekId, topWeekUsers, noDislikes: true);
        }

        public async Task<WeekUsersResult> GetWeekUsersByLikes(int? limit = null)
        {
            var weekUsers = await weekUsersRepository.GetLastActiveWeek();
            IEnumerable<WeekUser> topWeekUsers = weekUsers.OrderByDescending(u => u.LikesCount);
            if (limit.HasValue)
                topWeekUsers = topWeekUsers.Take(limit.Value);
            topWeekUsers = topWeekUsers.ToArray();

            var weekId = topWeekUsers.FirstOrDefault()?.WeekId ?? DateUtil.GetCurrentWeekId();
            return await MapToWeekUsersResult(weekId, topWeekUsers, noDislikes: true);
        }

        public async Task<WeekUsersResult> GetWeekUsersByDislikes()
        {
            var weekUsers = await weekUsersRepository.GetLastActiveWeek();
            var topWeekUsers = weekUsers.OrderByDescending(u => u.DisikesCount).ToArray();
            var weekId = topWeekUsers.FirstOrDefault()?.WeekId ?? DateUtil.GetCurrentWeekId();
            return await MapToWeekUsersResult(weekId, topWeekUsers, noLikes: true);
        }

        public async Task<WeekUsersResult> GetWeekUsersByBalance()
        {
            var weekUsers = await weekUsersRepository.GetPrevActiveWeek();
            var topWeekUsers = weekUsers.OrderByDescending(u => u.Paid).ToArray();
            var weekId = topWeekUsers.FirstOrDefault()?.WeekId ?? DateUtil.GetCurrentWeekId();
            return await MapToWeekUsersResult(weekId, topWeekUsers);
        }

        public async Task<User[]> GetByReferals()
        {
            var users = (await usersRepository.Where(u => u.ReferralsCount > 0))
                .OrderByDescending(u => u.ReferralsCount)
                .ToArray();

            return users;
        }
        
        public async Task<WeekUsersResult> MapToWeekUsersResult(
            int weekId, 
            IEnumerable<WeekUser> topWeekUsers, 
            bool noLikes = false, 
            bool noDislikes = false,
            bool forAddBalance = false)
        {
            var userIds = topWeekUsers.Select(u => u.UserId);
            var users = await usersRepository.FindById(userIds);
            return new WeekUsersResult
            {
                Id = weekId,
                Users = topWeekUsers.Select(w =>
                {
                    var user = users.First(u => u.Id == w.UserId);
                    return CreateWeekUserResult(user, w, noLikes, noDislikes, forAddBalance);
                }).ToArray()
            };
        }

        private WeekUserResult CreateWeekUserResult(
            User user, 
            WeekUser weekUser,
            bool noLikes = false,
            bool noDislikes = false,
            bool withAll = false)
        {
            return new WeekUserResult
            {
                User = user,
                Paid = weekUser.Paid,
                ReferralPayments = weekUser.ReferralPayments,
                ReferralsCount = weekUser.ReferralsCount,
                LikesCount = noLikes ? 0 : weekUser.LikesCount,
                DislikesCount = noDislikes ? 0 : weekUser.DisikesCount,
                CommentsCount = weekUser.CommentsCount,
                DiscussionsCount = weekUser.DiscussionsCount,
                DiscussionViewsCount = withAll ? weekUser.DiscussionViewsCount : 0,
                DiscussionExternalViewsCount = withAll ? weekUser.DiscussionExternalViewsCount : 0,
                UniqueCommentatorsCount = withAll ? weekUser.UniqueCommentatorsCount : 0,
                HolywarsStarted = withAll ? weekUser.HolywarsStarted : 0,
                HolywarsParticipant = withAll ? weekUser.HolywarsParticipant : 0,
                Rating = withAll ? weekUser.Rating : 0
            };
        }

        public static string CreateMd5Hash(string password)
        {
            var algorithm = MD5.Create();  //or use SHA1.Create();
            var bytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(SALT + password));

            var sb = new StringBuilder();
            foreach (byte b in bytes)
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public async Task AddMoney(ObjectId adminId, int weekId, AddMoneyModel[] addMoney)
        {
            var config = await configService.GetConfig();
            var userIds = addMoney.Select(u => u.UserId.ToObjectId()).ToArray();
            var users = await usersRepository.FindById(userIds);
            var weekUsers = await weekUsersRepository.Where(u => u.WeekId == weekId);
            var isAlreadyPaid = weekUsers.Any(u => u.Paid > 0);
            foreach (var user in users)
            {
                var newAddMoney = addMoney.First(a => a.UserId == user.Id.ToString());
                var weekUser = weekUsers.First(a => a.UserId == user.Id);
                user.Balance = user.Balance - weekUser.Paid + newAddMoney.AddMoney;
                user.TotalEarned = user.TotalEarned - weekUser.Paid + newAddMoney.AddMoney;
                weekUser.Paid = newAddMoney.AddMoney;
                weekUser.PaidByAdminId = adminId;

                user.Balance = user.Balance - weekUser.ReferralPayments + newAddMoney.ReferralPayments;
                user.TotalEarned = user.TotalEarned - weekUser.ReferralPayments + newAddMoney.ReferralPayments;
                user.ReferralPayments = user.ReferralPayments - weekUser.ReferralPayments + newAddMoney.ReferralPayments;
                user.PaymentLimitWeekReduce = newAddMoney.PaymentLimitWeekReduce;
                weekUser.ReferralPayments = newAddMoney.ReferralPayments;

                user.Level = RecalculateLevel(user, config.LevelSteps);
            }

            await usersRepository.Transaction(async () =>
            {
                await Task.WhenAll(users.Select(async user => {
                    var weekUser = weekUsers.FirstOrDefault(w => w.UserId == user.Id);
                    await usersRepository.UpdatesAsync(user,
                        u => u.Balance,
                        u => u.ReferralPayments,
                        u => u.PaymentLimitWeekReduce,
                        u => u.TotalEarned,
                        u => u.Level);
                    await weekUsersRepository.UpdatesAsync(weekUser,
                        u => u.Paid,
                        u => u.ReferralPayments);
                    await weekUsersRepository.UpdateAsync(weekUser, u => u.PaidByAdminId);
                    if (!isAlreadyPaid)
                    {
                        var profileUrl = $"{config.FrontendUrl}{config.ProfileUrl}";
                        await messagesService.AddMoney(
                            user.Id,
                            weekUser.Paid,
                            weekUser.ReferralPayments);
                        await emailsService.Enqueue(
                            adminId,
                            user,
                            "Пополнение баланса",
                            $"Ваш баланс пополнен на {weekUser.Paid + weekUser.ReferralPayments} рублей и теперь составляет {user.Balance}р. "
                            + $"Текущий баланс всегда можно посомтреть в профиле.",
                            profileUrl,
                            "Перейти в профиль");
                    }
                }));
            });
        }

        public static int GetExp(User user)
        {
            return 2 * user.TotalEarned + user.DiscussionsCount + user.CommentsCount;
        }

        public static int RecalculateLevel(User user, int[] levelSteps)
        {
            var exp = GetExp(user);
            var i = 0;
            var nextLevel = 0;
            for (; i < levelSteps.Length; i++)
            {
                nextLevel += levelSteps[i + 1];
                if (nextLevel > exp)
                    return i;
            }
            return i;
        }

        public async Task Block(string userId)
        {
            await usersRepository.UpdateAsync(userId.ToObjectId(), u => u.IsWriteProhibited, true);
        }

        public async Task<IEnumerable<User>> Last(int limit)
        {
            var sort = usersRepository.Sort.Descending(u => u.CreationDate);
            return await usersRepository.Where(u => true, limit, sort);
        }

        public async Task<LevelProgress> GetLevelProgress(User user)
        {
            var result = new LevelProgress();
            var config = await configService.GetConfig();
            if (config.LevelSteps.Length <= user.Level)
                return result;
            result.Next = config.LevelSteps[user.Level + 1];
            var exp = GetExp(user);
            result.Current = exp - config.LevelSteps.Take(user.Level + 1).Sum();
            return result;
        }

        public Task<IEnumerable<User>> FindById(ObjectId[] objectIds)
        {
            return usersRepository.FindById(objectIds);
        }

        public async Task SetRole(string userId, string role)
        {
            await usersRepository.UpdateAsync(userId.ToObjectId(), u => u.Role, role);
        }
    }
}
