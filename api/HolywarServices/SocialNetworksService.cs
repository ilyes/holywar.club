﻿using Holywar.Common;
using Holywar.Entities.Abstract;
using Holywar.Entities.Auths;
using Holywar.Entities.Users;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Holywar.Services
{
    public class SocialNetworkAuthResult : ServiceResult
    {
        public User User { get; set; }
    }

    public class SocialNetworksService
    {
        private UsersService usersService;
        private EmailsService emailsService;

        public SocialNetworksService(UsersService usersService, EmailsService emailsService)
        {
            this.usersService = usersService;
            this.emailsService = emailsService;
        }

        public async Task<UsersServiceResult> SignInByVkCode(string code, string redirectUri, string referralId)
        {
            var auth = await GetVkAccessToken(code, redirectUri);
            var userInfoResponse = await GetVkUserInfo(auth.access_token, auth.user_id);
            if (userInfoResponse == null || userInfoResponse.response.Length == 0)
                return new UsersServiceResult { Error = "VkUserNotFound" };

            var email = !string.IsNullOrEmpty(auth.email) ? auth.email : emailsService.CreateFakeEmail(auth.user_id, "vk"); 
            var userInfo = userInfoResponse.response[0];
            var user = await usersService.FindByEmail(email);
            if (user != null)
                return new UsersServiceResult { User = user };

            var userResult = await usersService.Create(userInfo.first_name, email, Guid.NewGuid().ToString(), referralId);
            if (userResult.HasError)
                return userResult;

            if(string.IsNullOrEmpty(userInfo.photo_200))
                return userResult;

            return await usersService.SetAvatar(userInfo.photo_200, userResult.User.Id);
        }

        public async Task<UsersServiceResult> SignInByFacebookToken(string accessToken, string referralId)
        {
            var userInfo = await GetFacebookUserInfo(accessToken);
            var email = !string.IsNullOrEmpty(userInfo.email) ? userInfo.email : emailsService.CreateFakeEmail(userInfo.id, "fb");
            var user = await usersService.FindByEmail(email);
            if (user != null)
                return new UsersServiceResult { User = user };

            var userResult = await usersService.Create(userInfo.first_name, email, Guid.NewGuid().ToString(), referralId);
            if (userResult.HasError)
                return userResult;

            if(userInfo.picture == null || userInfo.picture.data == null || string.IsNullOrEmpty(userInfo.picture.data.url))
                return userResult;

            return await usersService.SetAvatar(userInfo.picture.data.url, userResult.User.Id);
        }

        private async Task<FacebookUserInfoResponse> GetFacebookUserInfo(string accessToken)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri($"https://graph.facebook.com/me");

            var request = new RestRequest();
            request.AddParameter("fields", "id,first_name,email,picture");
            request.AddParameter("access_token", accessToken);
            request.Method = Method.GET;
            var response = await client.ExecuteTaskAsync(request);
            var isOk = response.StatusCode == HttpStatusCode.OK;
            if (!isOk)
            {
                Log.Error($"GetVkAccessToken error. Response {response.StatusCode} {response.Content}");
                Console.WriteLine($"GetVkAccessToken error. Response {response.StatusCode} {response.Content}");
            }
            return JsonConvert.DeserializeObject<FacebookUserInfoResponse>(response.Content);
        }

        private async Task<VkAccessTokenResponse> GetVkAccessToken(string code, string redirectUri)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri("https://oauth.vk.com/access_token");

            var request = new RestRequest();
            request.AddParameter("client_id", "7160257");
            request.AddParameter("client_secret", "WET6shwWFEaw9bLEn9vw");
            request.AddParameter("redirect_uri", redirectUri);
            request.AddParameter("code", code);
            request.Method = Method.GET;
            var response = await client.ExecuteTaskAsync(request);
            var isOk = response.StatusCode == HttpStatusCode.OK;
            if (!isOk)
            {
                Log.Error($"GetVkAccessToken error. Response {response.StatusCode} {response.Content}");
                Console.WriteLine($"GetVkAccessToken error. Response {response.StatusCode} {response.Content}");
            }
            return JsonConvert.DeserializeObject<VkAccessTokenResponse>(response.Content);
        }

        private async Task<VkUserInfoResponse> GetVkUserInfo(string accessToken, string userId)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri("https://api.vk.com/method/users.get");

            var request = new RestRequest();
            request.AddParameter("user_ids", userId);
            request.AddParameter("fields", "first_name,photo_200");
            request.AddParameter("access_token", accessToken);
            request.AddParameter("v", "5.102");
            request.Method = Method.GET;
            var response = await client.ExecuteTaskAsync(request);
            var isOk = response.StatusCode == HttpStatusCode.OK;
            if (!isOk)
            {
                Log.Error($"GetVkAccessToken error. Response {response.StatusCode} {response.Content}");
                Console.WriteLine($"GetVkAccessToken error. Response {response.StatusCode} {response.Content}");
            }
            return JsonConvert.DeserializeObject<VkUserInfoResponse>(response.Content);
        }
    }
}
