﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Holywar.Common;
using Holywar.Entities.Abstract;
using Holywar.Entities.Discussions;
using Holywar.Entities.Messages;
using Holywar.Repositories;
using MongoDB.Bson;
using Newtonsoft.Json;

namespace Holywar.Services
{
    public class MessagesService 
    {
        private MessagesRepository messagesRepository;
        private UsersRepository usersRepository;
        private CommentsRepository commentsRepository;
        private ConfigService configService;

        public MessagesService(
            MessagesRepository messagesRepository,
            UsersRepository usersRepository,
            ConfigService configService,
            CommentsRepository commentsRepository) 
        {
            this.messagesRepository = messagesRepository;
            this.usersRepository = usersRepository;
            this.configService = configService;
            this.commentsRepository = commentsRepository;
        }

        public async Task<ServiceResult> CreateCommon(ObjectId fromUserId, string toUserId, string text)
        {
            var user = await usersRepository.FindById(fromUserId);
            if(user == null || user.IsWriteProhibited)
            {
                return new ServiceResult { Error = "Prohibited" };
            }

            var message = new Message
            {
                FromUserId = fromUserId,
                ToUserId = toUserId.ToObjectId(),
                Text = text,
                CreationDate = DateTime.UtcNow,
                Type = MessageType.Common
            };
            await messagesRepository.Add(message);

            return new ServiceResult();
        }

        public async Task<IEnumerable<MessageWithUser>> Last(ObjectId userId, int limit)
        {
            var sort = messagesRepository.Sort.Descending(m => m.CreationDate);
            var messages = await messagesRepository.Where(m => m.ToUserId == userId, limit, sort);
            var userIds = messages.Select(m => m.FromUserId).ToArray();
            var users = await usersRepository.FindById(userIds);
            return messages.Select(m => new MessageWithUser
            {
                Message = m,
                FromUser = users.FirstOrDefault(u => u.Id == m.FromUserId)
            });
        }

        public async Task SetRead(ObjectId userId)
        {
            var messages = await messagesRepository.Where(m => m.ToUserId == userId && !m.IsRead);
            foreach(var message in messages)
            {
                await messagesRepository.UpdateAsync(message, m => m.IsRead, true);
            }
        }

        public async Task CreateReply(ObjectId fromUserId, Discussion discussion, ObjectId parentCommentId, ObjectId commentId)
        {
            var config = await configService.GetConfig();
            var toUserId = await FindToUserId(discussion, parentCommentId);
            var commentUrlHash = string.Format(config.CommentReplyPathTemplate, commentId);

            if (fromUserId == toUserId)
                return;

            var message = new Message
            {
                FromUserId = fromUserId,
                ToUserId = toUserId,
                CreationDate = DateTime.UtcNow,
                Type = MessageType.Reply,
                Text = $"/{string.Format(config.DiscussionPathTemplate, discussion.Id)}{commentUrlHash}"
            };
            await messagesRepository.Add(message);
        }

        public async Task AddMoney(ObjectId userId, int addMoney, int referralPayments)
        {
            var message = new Message
            {
                ToUserId = userId,
                CreationDate = DateTime.UtcNow,
                Type = MessageType.AddMoney,
                Text = JsonConvert.SerializeObject(new { addMoney = addMoney, referralPayments = referralPayments })
            };
            await messagesRepository.Add(message);
        }

        public async Task Create(string toUserId, MessageType type)
        {
            await Create(toUserId.ToObjectId(), type);
        }

        public async Task Create(ObjectId toUserId, MessageType type)
        {
            var message = new Message
            {
                ToUserId = toUserId,
                CreationDate = DateTime.UtcNow,
                Type = type
            };
            await messagesRepository.Add(message);
        }

        private async Task<ObjectId> FindToUserId(Discussion discussion, ObjectId parentCommentId)
        {
            if (parentCommentId == ObjectId.Empty)
                return discussion.UserId;

            var comment = await commentsRepository.FindById(parentCommentId);
            return comment != null
                ? comment.UserId
                : discussion.UserId;
        }
    }
}
