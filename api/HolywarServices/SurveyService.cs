﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Holywar.Common;
using Holywar.Entities.Abstract;
using Holywar.Entities.Surveys;
using Holywar.Repositories;
using MongoDB.Bson;

namespace Holywar.Services
{
    public class SurveyVoteResult : ServiceResult
    {
        public SurveyVotes[] SurveyVotes { get; set; }
    }

    public class SurveysService
    {
        private SurveysRepository surveysRepository;
        private SurveyVotesRepository surveyVotesRepository;
        private SurveyUserVotesRepository surveyUserVotesRepository;

        public SurveysService(
            SurveysRepository surveysRepository,
            SurveyVotesRepository surveyVotesRepository,
            SurveyUserVotesRepository surveyUserVotesRepository)
        {
            this.surveysRepository = surveysRepository;
            this.surveyVotesRepository = surveyVotesRepository;
            this.surveyUserVotesRepository = surveyUserVotesRepository;
        }

        public async Task<SurveyVoteResult> Vote(SurveyVoteModel model, ObjectId userId)
        {
            var surveyId = model.SurveyId.ToObjectId();
            var voteForSurveyItemId = model.SurveyItemId.ToObjectId();
            
            var survey = await surveysRepository.FindById(surveyId);
            if(survey == null)
            {
                return new SurveyVoteResult { Error = "NoSurvey" };
            }
            var surveyUserVote = await surveyUserVotesRepository.Find(v => v.UserId == userId && v.SurveyId == surveyId);
            var surveyVotes = await surveyVotesRepository.Where(v => v.SurveyId == surveyId);
            if (surveyUserVote != null)
            {
                if(surveyUserVote.VoteForSurveyItemId == voteForSurveyItemId)
                {
                    return new SurveyVoteResult { Error = "AlreadyVoted" };
                }

                var oldSurvayVotes = surveyVotes.FirstOrDefault(v => v.Id == surveyUserVote.VoteForSurveyItemId);
                var newSurvayVotes = surveyVotes.FirstOrDefault(v => v.Id == voteForSurveyItemId);

                if(newSurvayVotes == null)
                {
                    newSurvayVotes = await surveyVotesRepository.Add(new SurveyVotes { Id = voteForSurveyItemId, SurveyId = surveyId });
                }

                if(oldSurvayVotes != null)
                    oldSurvayVotes.Votes -= 1;
                newSurvayVotes.Votes += 1;

                surveyUserVote.VoteForSurveyItemId = voteForSurveyItemId;
                surveyUserVote.CreationDate = DateTime.UtcNow;

                await surveysRepository.Transaction(async () =>
                {
                    if (oldSurvayVotes != null)
                        await surveyVotesRepository.UpdateAsync(oldSurvayVotes, s => s.Votes);
                    await surveyVotesRepository.UpdateAsync(newSurvayVotes, s => s.Votes);
                    await surveyUserVotesRepository.UpdateAsync(surveyUserVote, s => s.VoteForSurveyItemId);
                    await surveyUserVotesRepository.UpdateAsync(surveyUserVote, s => s.CreationDate);
                });

                return new SurveyVoteResult
                {
                    SurveyVotes = surveyVotes.ToArray()
                };
            }
            else
            {
                var surveyVote = surveyVotes.FirstOrDefault(v => v.Id == voteForSurveyItemId);
                if (surveyVote == null)
                {
                    surveyVote = await surveyVotesRepository.Add(new SurveyVotes { Id = voteForSurveyItemId, SurveyId = surveyId });
                    surveyVotes = surveyVotes.Concat(new[] { surveyVote }).ToArray();
                }

                surveyVote.Votes += 1;

                surveyUserVote = new SurveyUserVote
                {
                    SurveyId = surveyId,
                    UserId = userId,
                    VoteForSurveyItemId = voteForSurveyItemId,
                    CreationDate = DateTime.UtcNow
                };

                await surveysRepository.Transaction(async () =>
                {
                    await surveyVotesRepository.UpdateAsync(surveyVote, s => s.Votes);
                    await surveyUserVotesRepository.Add(surveyUserVote);
                });

                return new SurveyVoteResult
                {
                    SurveyVotes = surveyVotes.ToArray()
                };
            }
        }
    }
}
