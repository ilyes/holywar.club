﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Holywar.Common;
using Holywar.Entities.Abstract;
using Holywar.Entities.Messages;
using Holywar.Entities.Payments;
using Holywar.Entities.Users;
using Holywar.Repositories;
using MongoDB.Bson;

namespace Holywar.Services
{
    public class PaymentsService
    {
        private PaymentsRepository paymentsRepository;
        private UsersRepository userRepository;
        private ConfigService configService;
        private MessagesService messagesService;

        public PaymentsService(
            PaymentsRepository paymentsRepository,
            UsersRepository userRepository,
            ConfigService configService,
            MessagesService messagesService)
        {
            this.paymentsRepository = paymentsRepository;
            this.userRepository = userRepository;
            this.configService = configService;
            this.messagesService = messagesService;
        }

        public async Task<IEnumerable<PaymentDto>> Get(ObjectId userId, int limit)
        {
            var payments = await paymentsRepository.Where(p => p.UserId == userId, limit);
            return payments.Select(p => new PaymentDto
            {
                Id = p.Id.ToString(),
                Instructions = p.Instructions,
                Status = p.Status,
                Sum = p.Sum
            });
        }

        public async Task<IEnumerable<PaymentWithUserDto>> GetAll(GetAllRequest request)
        {
            var payments = await paymentsRepository.GetAll(request.AfterId.ToObjectId(), request.Limit);
            var userIds = payments.Select(p => p.UserId).Distinct();
            var users = await userRepository.FindById(userIds);
            return payments.Select(p => {
                var user = users.First(u => u.Id == p.UserId);
                return new PaymentWithUserDto
                {
                    Id = p.Id.ToString(),
                    Instructions = p.Instructions,
                    Status = p.Status,
                    Sum = p.Sum,
                    User = new UserDto
                    {
                        Id = user.Id.ToString(),
                        Name = user.Name
                    }
                };
            });
        }

        public async Task<ServiceResult> Create(ObjectId userId, CreatePaymentRequest request)
        {
            var user = await userRepository.FindById(userId);
            if (request.Sum > user.Balance)
                return new ServiceResult { Error = "BalanceToLow" };

            var config = await configService.GetConfig();
            var paymentLimitLevelReduce = user.Level * config.LevelLimitReduceBy;
            var minPayment = Math.Max(user.PaymentLimit - user.PaymentLimitWeekReduce - paymentLimitLevelReduce, config.MinPaymentLimit);
            if (request.Sum < minPayment)
                return new ServiceResult { Error = "MinPaymentExided" };

            var payment = new Payment
            {
                UserId = userId,
                CreationDate = DateTime.UtcNow,
                Instructions = request.Instructions,
                Status = PaymentStatus.Pending,
                Sum = request.Sum
            };
            user.Balance -= request.Sum;

            await paymentsRepository.Transaction(async () =>
            {
                await paymentsRepository.Add(payment);

                var u = userRepository.Update.Set(m => m.Balance, user.Balance);
                await userRepository.UpdateAsync(user.Id, u);
            });

            await messagesService.Create(config.AdminUserId, MessageType.PaymentCreated);

            return new ServiceResult();
        }

        public async Task<ServiceResult> Delete(ObjectId userId, ObjectId paymentId)
        {
            var user = await userRepository.FindById(userId);
            var payment = await paymentsRepository.FindById(paymentId);

            if (payment.UserId != userId)
                return new ServiceResult { Error = "BadRequest" };
            if (payment.Status != PaymentStatus.Pending)
                return new ServiceResult { Error = "NotPending" };

            user.Balance += payment.Sum;

            await paymentsRepository.Transaction(async () =>
            {
                await paymentsRepository.Delete(payment.Id);

                var u = userRepository.Update.Set(m => m.Balance, user.Balance);
                await userRepository.UpdateAsync(user.Id, u);
            });

            return new ServiceResult();
        }

        public async Task<ServiceResult> SetStatus(ObjectId userId, PaymentStatusRequest request)
        {
            var user = await userRepository.FindById(userId);
            if (user.Role != AuthRole.Admin)
                return new ServiceResult { Error = "NoAccess" };

            var payment = await paymentsRepository.FindById(request.PaymentId);
            if (payment.Status == PaymentStatus.Declined && request.Status != PaymentStatus.Declined && user.Balance < payment.Sum)
                return new ServiceResult { Error = "NoMoney" };

            await paymentsRepository.Transaction(async () =>
            {
                var pu = paymentsRepository.Update.Set(m => m.Status, request.Status);
                await paymentsRepository.UpdateAsync(payment.Id, pu);

                if (request.Status == PaymentStatus.Declined && payment.Status != PaymentStatus.Declined)
                {
                    user.Balance += payment.Sum;
                    var uu = userRepository.Update.Set(m => m.Balance, user.Balance);
                    await userRepository.UpdateAsync(user.Id, uu);
                }
                else if(payment.Status == PaymentStatus.Declined && request.Status != PaymentStatus.Declined)
                {
                    user.Balance -= payment.Sum;
                    var uu = userRepository.Update.Set(m => m.Balance, user.Balance);
                    await userRepository.UpdateAsync(user.Id, uu);
                }
            });

            var message = request.Status == PaymentStatus.Declined ? MessageType.PaymentDeclined : MessageType.PaymentApproved;
            await messagesService.Create(userId, message);

            return new ServiceResult();
        }
    }
}
