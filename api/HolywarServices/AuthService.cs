﻿using System;
using System.Threading.Tasks;
using Holywar.Entities.Auths;
using Holywar.Entities.Users;
using Holywar.Repositories;
using MongoDB.Bson;

namespace Holywar.Services
{
    public class AuthService 
    {
        private AuthRepository authRepository;
        private UsersRepository usersRepository;

        public AuthService(AuthRepository authRepository, UsersRepository usersRepository) 
        {
            this.authRepository = authRepository;
            this.usersRepository = usersRepository;
        }

        public async Task<Auth> Create(User user)
        {
            user.LastLogin = DateTime.UtcNow;
            await usersRepository.UpdateAsync(user, u => u.LastLogin);

            var auth = await authRepository.Find(a => a.UserId == user.Id);
            if (auth != null)
            {
                auth.ExpireAt = DateTime.UtcNow.AddMonths(1);
                await authRepository.UpdateAsync(auth, u => u.ExpireAt);
                return auth;
            }

            auth = new Auth
            {
                UserId = user.Id,
                CreationDate = DateTime.UtcNow,
                ExpireAt = DateTime.UtcNow.AddMonths(1)
            };
            await authRepository.Add(auth);
            return auth;
        }

        public async Task Delete(ObjectId id)
        {
            await authRepository.Delete(id);
        }

        public async Task<Auth> FindById(string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;
            var auth = await authRepository.FindById(id);
            if (auth == null || auth.ExpireAt < DateTime.UtcNow)
                return null;
            return auth;
        }
    }
}
