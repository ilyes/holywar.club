﻿using Holywar.Common;
using Holywar.Entities.Abstract;
using Holywar.Entities.Comments;
using Holywar.Entities.Discussions;
using Holywar.Entities.Users;
using Holywar.Repositories;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Holywar.Services
{
    public class CommentCreationResult : ServiceResult
    {
        public Comment Comment;
    }

    public class CommentsService
    {
        private CommentsRepository commentsRepository;
        private DiscussionsRepository discussionsRepository;
        private UsersRepository usersRepository;
        private WeekUsersRepository weekUsersRepository;
        private WeekCommentsRepository weekCommentsRepository;
        private MessagesService messagesService;
        private ConfigService configService;

        public CommentsService(
            CommentsRepository commentsRepository, 
            DiscussionsRepository discussionsRepository, 
            UsersRepository usersRepository,
            WeekUsersRepository weekUsersRepository,
            WeekCommentsRepository weekCommentsRepository,
            MessagesService messagesService,
            ConfigService configService) 
        {
            this.commentsRepository = commentsRepository;
            this.discussionsRepository = discussionsRepository;
            this.usersRepository = usersRepository;
            this.weekUsersRepository = weekUsersRepository;
            this.weekCommentsRepository = weekCommentsRepository;
            this.messagesService = messagesService;
            this.configService = configService;
        }

        public async Task SetDeleted(string commentId)
        {
            await commentsRepository.SetDeleted(commentId);
        }

        public async Task<CommentCreationResult> Create(string text, string imageUrl, ObjectId userId, ObjectId discussionId, ObjectId parentCommentId)
        {
            if (string.IsNullOrWhiteSpace(text) && string.IsNullOrWhiteSpace(imageUrl))
            {
                return new CommentCreationResult { Error = "ContentIsEmpty" };
            }

            var discussion = await discussionsRepository.FindById(discussionId);
            if(discussion == null)
            {
                return new CommentCreationResult { Error = "NoDiscussion" };
            }

            var user = await usersRepository.FindById(userId);
            if (user == null)
            {
                return new CommentCreationResult { Error = "NoUser" };
            }
            if (user.IsWriteProhibited)
            {
                return new CommentCreationResult { Error = "Prohibited" };
            }

            //if(parentCommentId == ObjectId.Empty && discussion.UserId == userId)
            //{
            //    return new CommentCreationResult { Error = "SelfCommentsProhibited" };
            //}

            var config = await configService.GetConfig();

            var discussionUser = discussion.UserId != user.Id
                ? await usersRepository.FindById(discussion.UserId)
                : user;
            var parentComment = parentCommentId != ObjectId.Empty ? await commentsRepository.FindById(parentCommentId) : null;

            var isUniqueCommentator = discussion.UserId != userId ? await IsUniqueCommentator(userId, discussionId) : false;
            var isUniqueReply = parentComment != null && parentComment.UserId != userId ? await IsUniqueReply(userId, parentCommentId) : false;

            var parentCommentUser = isUniqueReply ? await usersRepository.FindById(parentComment.UserId) : null;

            if (isUniqueCommentator)
            {
                discussion.UniqueCommentatorsCount++;
                discussionUser.UniqueCommentatorsCount++;
            }
            if(isUniqueReply)
            {
                parentCommentUser.UniqueRepliesCount++;
            }
            discussion.CommetsCount++;
            user.CommentsCount++;
            user.Level = UsersService.RecalculateLevel(user, config.LevelSteps);
            var comment = new Comment
            {
                Text = text,
                ImageUrl = imageUrl,
                UserId = userId,
                DiscussionId = discussionId,
                ParentCommentId = parentCommentId,
                CreationDate = DateTime.UtcNow,
                Depth = DetermineDepth(parentComment, user)
            };

            var isHolywar = comment.Depth % config.HolywarCandidatesStep == 0 && !comment.IsHolywarComment;
            var isNewHolywar = false;
            if (isHolywar)
            {
                if(discussion.HolywarState == HolywarState.Not)
                {
                    discussion.HolywarState = HolywarState.Candidate;
                    isNewHolywar = true;
                }
                else if (discussion.HolywarState == HolywarState.Approved)
                {
                    discussion.HolywarState = HolywarState.Continues;
                    isNewHolywar = true;
                }
            }

            await commentsRepository.Transaction(async () =>
            {
                await commentsRepository.Add(comment);
                if(parentComment != null)
                {
                    await commentsRepository.UpdateAsync(parentComment.Id, c => c.ReplysCount, parentComment.ReplysCount + 1);
                    await weekCommentsRepository.IncrementReplys(parentComment.Id);
                }
                await usersRepository.UpdatesAsync(user, u => u.CommentsCount, u => u.Level);
                await usersRepository.UpdateAsync(discussionUser, u => u.UniqueCommentatorsCount);
                await discussionsRepository.UpdatesAsync(discussion, d => d.CommetsCount, d => d.UniqueCommentatorsCount);
                await weekUsersRepository.IncrementComments(userId);
                
                if (isUniqueCommentator)
                {
                    await weekUsersRepository.IncrementUniqueCommentators(discussion.UserId);
                }
                if (isUniqueReply)
                {
                    await weekUsersRepository.IncrementUniqueReplies(parentCommentUser.Id);
                    await usersRepository.UpdateAsync(parentCommentUser, u => u.UniqueRepliesCount);
                }
                if (isNewHolywar)
                {
                    await discussionsRepository.UpdateAsync(discussion, d => d.HolywarState);
                }
                await messagesService.CreateReply(userId, discussion, parentCommentId, comment.Id);
            });
            
            return new CommentCreationResult { Comment = comment };
        }

        public async Task<CommentWithUser[]> GetWeekComments(int limit, int? weekId)
        {
            var weekComments = await weekCommentsRepository.FindWeekComments(weekId ?? DateUtil.GetCurrentWeekId(), limit);
            var comments = await commentsRepository.FindById(weekComments.Select(w => w.CommentId).ToArray());
            var ordered = weekComments.OrderByDescending(c => c.Rating)
                .Select(o => comments.FirstOrDefault(c => c.Id == o.CommentId))
                .ToArray();
            return await MapToUsers(ordered);
        }

        private int DetermineDepth(Comment parentComment, User user)
        {
            if (parentComment == null)
                return 0;

            if (parentComment.UserId == user.Id)
                return parentComment.Depth;

            return parentComment.Depth + 1;
        }

        public async Task<ServiceResult> Update(ObjectId commentId, ObjectId currentUserId, string text, string imageUrl)
        {
            if (string.IsNullOrWhiteSpace(text) && string.IsNullOrWhiteSpace(imageUrl))
            {
                return new ServiceResult { Error = "ContentIsEmpty" };
            }

            var comment = await commentsRepository.FindById(commentId);
            if(comment == null || comment.UserId != currentUserId)
            {
                return new ServiceResult { Error = "ProhibitedCommentId" };
            }
            if((comment.CreationDate - DateTime.UtcNow).TotalMinutes > 15)
            {
                return new ServiceResult { Error = "EditTimeLeft" };
            }

            comment.Text = text;
            comment.ImageUrl = imageUrl;

            await commentsRepository.UpdatesAsync(comment, c => c.Text, c => c.ImageUrl);

            return new ServiceResult();
        }

        private async Task<bool> IsUniqueCommentator(ObjectId userId, ObjectId discussionId)
        {
            var comment = await commentsRepository.Find(c => c.UserId == userId && c.DiscussionId == discussionId);
            return comment == null;
        }

        private async Task<bool> IsUniqueReply(ObjectId userId, ObjectId parentCommentId)
        {
            var comment = await commentsRepository.Find(c => c.UserId == userId && c.ParentCommentId == parentCommentId);
            return comment == null;
        }

        public async Task<IEnumerable<CommentWithUser>> CommentsOfTheWeek()
        {
            var date = DateTime.UtcNow.AddDays(-1);
            var sort = commentsRepository.Sort.Descending(c => c.LikesCount - c.DislikesCount);
            var comments = await commentsRepository.Where(c => c.CreationDate > date, 3, sort);
            if(comments.Count() == 0)
            {
                date = DateTime.UtcNow.AddDays(-7);
                comments = await commentsRepository.Where(c => c.CreationDate > date, 3, sort);
            }
            return await MapToUsers(comments);
        }

        public async Task<IEnumerable<CommentWithUser>> Last(int limit)
        {
            var s = commentsRepository.Sort.Descending(c => c.Id);
            var comments = await commentsRepository.Where(c => !c.IsDeleted, limit, s);
            return await MapToUsers(comments);
        }

        private async Task<CommentWithUser[]> MapToUsers(IEnumerable<Comment> comments)
        {
            var userIds = comments.Select(c => c.UserId).Distinct().ToArray();
            var users = await usersRepository.FindById(userIds);

            var discussionIds = comments.Select(c => c.DiscussionId).Distinct().ToArray();
            var discussions = await discussionsRepository.FindById(discussionIds);

            return comments.Select(c => new CommentWithUser
            {
                Comment = c,
                User = users.FirstOrDefault(u => u.Id == c.UserId),
                DiscussionAlias = discussions.FirstOrDefault(d => d.Id == c.DiscussionId)?.Alias
            }).ToArray();
        }
    }
}
