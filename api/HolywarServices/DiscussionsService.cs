﻿using Holywar.Common;
using Holywar.Repositories;
using MongoDB.Bson;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Holywar.Entities.Discussions;
using Holywar.Entities.Users;
using Holywar.Entities.Abstract;
using Holywar.Entities.Categories;
using Holywar.Entities.Comments;
using Holywar.Entities.Surveys;

namespace Holywar.Services
{
    public class DiscussionWithComments : ServiceResult
    {
        public Discussion Discussion;
        public User User;
        public IEnumerable<CommentWithUser> Comments;
        public Survey Survey;
        public IEnumerable<SurveyVotes> SurveyVotes { get; set; }
    }

    public class DiscussionCreateResult : ServiceResult
    {
        public Discussion Discussion;
    }

    public class DiscussionsService
    {
        private DiscussionsRepository discussionsRepository;
        private CommentsRepository commentsRepository;
        private UsersRepository usersRepository;
        private WeekUsersRepository weekUsersRepository;
        private CategoriesRepository categoriesRepository;
        private SurveysRepository surveysRepository;
        private SurveyVotesRepository surveyVotesRepository;
        private ConfigService configService;

        private class CachedDiscussions
        {
            public DateTime Expired;
            public IEnumerable<ObjectId> DiscussionIds;
        }

        private static CachedDiscussions cachedEternalDiscussions;
        private static CachedDiscussions cachedPopularDiscussions;

        public DiscussionsService(
            DiscussionsRepository discussionsRepository,
            CommentsRepository commentsRepository,
            UsersRepository usersRepository,
            WeekUsersRepository weekUsersRepository,
            CategoriesRepository categoriesRepository,
            SurveysRepository surveysRepository,
            SurveyVotesRepository surveyVotesRepository,
            ConfigService configService)
        {
            this.discussionsRepository = discussionsRepository;
            this.commentsRepository = commentsRepository;
            this.usersRepository = usersRepository;
            this.weekUsersRepository = weekUsersRepository;
            this.categoriesRepository = categoriesRepository;
            this.surveysRepository = surveysRepository;
            this.surveyVotesRepository = surveyVotesRepository;
            this.configService = configService;
        }

        public async Task<DiscussionCreateResult> Create(Discussion newDiscussion, Survey survey)
        {
            if (!string.IsNullOrEmpty(newDiscussion.Url))
            {
                var existingDiscussion = await discussionsRepository.Find(d => d.Url == newDiscussion.Url);
                if (existingDiscussion != null)
                {
                    return new DiscussionCreateResult { Error = "AlreadyExists" };
                }
            }

            if (survey != null)
            {
                if (survey.Items == null || survey.Items.Count(i => !string.IsNullOrWhiteSpace(i.Title)) < 2)
                    return new DiscussionCreateResult { Error = "IncorrectSurvey" };
            }

            var user = await usersRepository.FindById(newDiscussion.UserId);
            if (user == null || user.IsWriteProhibited)
            {
                return new DiscussionCreateResult { Error = "Prohibited" };
            }

            var config = await configService.GetConfig();

            user.DiscussionsCount++;
            user.Level = UsersService.RecalculateLevel(user, config.LevelSteps);

            newDiscussion.Id = ObjectId.GenerateNewId();
            newDiscussion.Alias = $"{Translit.Make(newDiscussion.Title, 150)}_{ObjectId.GenerateNewId()}";
            newDiscussion.CreationDate = DateTime.UtcNow;

            if (survey != null)
            {
                survey.DiscusionId = newDiscussion.Id;
                foreach (var surveyItem in survey.Items)
                    surveyItem.Id = ObjectId.GenerateNewId();
            }

            await discussionsRepository.Transaction(async () =>
            {
                await discussionsRepository.Add(newDiscussion);
                await usersRepository.UpdatesAsync(user, u => u.DiscussionsCount, u => u.Level);
                await weekUsersRepository.IncrementDiscussions(user.Id);
                if (survey != null)
                    await surveysRepository.Add(survey);
            });

            return new DiscussionCreateResult { Discussion = newDiscussion };
        }

        public async Task<ServiceResult> Edit(Discussion discussion, Survey survey, ObjectId userId)
        {
            var existing = await discussionsRepository.FindById(discussion.Id);
            if (existing == null)
            {
                return new ServiceResult { Error = "DiscussionNotExists" };
            }

            var user = await usersRepository.FindById(userId);
            var isAdmin = user.Role.In(AuthRole.Admin);
            if (!isAdmin)
            {
                if (existing.UserId != userId)
                {
                    return new ServiceResult { Error = "NotAnOwner" };
                }
                if ((existing.CreationDate - DateTime.UtcNow).TotalMinutes > 45)
                {
                    return new ServiceResult { Error = "EditTimeLeft" };
                }
            }

            existing.Title = discussion.Title;
            existing.Description = discussion.Description;
            existing.ImageUrl = discussion.ImageUrl;
            existing.Body = discussion.Body;
            existing.CategoryId = discussion.CategoryId;

            if (survey != null)
            {
                foreach (var surveyItem in survey.Items)
                    if (surveyItem.Id == ObjectId.Empty)
                        surveyItem.Id = ObjectId.GenerateNewId();
            }

            await discussionsRepository.Transaction(async () =>
            {
                await discussionsRepository.UpdatesAsync(existing,
                    e => e.Title,
                    e => e.Description,
                    e => e.ImageUrl);
                await discussionsRepository.UpdateAsync(existing, e => e.Body);
                await discussionsRepository.UpdateAsync(existing, e => e.CategoryId);
                if (survey == null)
                {
                    await surveysRepository.DeleteByDiscussionId(existing.Id);
                }
                else if (survey.Id == ObjectId.Empty)
                {
                    survey.DiscusionId = discussion.Id;
                    await surveysRepository.Add(survey);
                }
                else
                {
                    await surveysRepository.UpdateAsync(survey, s => s.Items);
                }
            });

            return new ServiceResult();
        }

        public async Task<ServiceResult> ApproveHolywar(ObjectId userId, string discussionId)
        {
            var id = discussionId.ToObjectId();
            if (id == ObjectId.Empty)
            {
                return new ServiceResult { Error = "IncorrectId" };
            }

            var discussion = await discussionsRepository.FindById(id);
            if (!discussion.HolywarState.In(HolywarState.Not, HolywarState.Candidate, HolywarState.Continues))
            {
                return new ServiceResult { Error = "IncorrectHolywarState" };
            }

            var holywarUsers = await FindHolywarParticipants(id);

            discussion.HolywarStateByUserId = userId;
            discussion.HolywarState = HolywarState.Approved;
            discussion.IsHolywar = true;

            var discussionUser = await usersRepository.FindById(discussion.UserId);
            if (discussion.HolywarState != HolywarState.Continues)
            {
                discussionUser.HolywarsStarted++;
            }

            await discussionsRepository.Transaction(async () =>
            {
                await discussionsRepository.UpdateAsync(discussion, d => d.HolywarStateByUserId);
                await discussionsRepository.UpdateAsync(discussion, d => d.HolywarState);
                await discussionsRepository.UpdateAsync(discussion, d => d.IsHolywar);
                if (discussion.HolywarState != HolywarState.Continues)
                {
                    await weekUsersRepository.IncrementHolywarsStarted(discussion.UserId);
                    await usersRepository.UpdateAsync(discussionUser, u => u.HolywarsStarted);
                }
                foreach (var holywarUser in holywarUsers)
                {
                    await weekUsersRepository.IncrementHolywarsParticipant(holywarUser.User.Id, holywarUser.Comments.Count);
                    holywarUser.User.HolywarsParticipates += holywarUser.Comments.Count;
                    await usersRepository.UpdateAsync(holywarUser.User, u => u.HolywarsParticipates);
                    await Task.WhenAll(holywarUser.Comments
                        .Select(c => commentsRepository.UpdateAsync(c.Id, сc => сc.IsHolywarComment, true)));
                }
            });

            await CreateEternal();

            return new ServiceResult();
        }

        public async Task<ServiceResult> DeclineHolywar(ObjectId userId, string discussionId)
        {
            var id = discussionId.ToObjectId();
            if (id == ObjectId.Empty)
            {
                return new ServiceResult { Error = "IncorrectId" };
            }

            var discussion = await discussionsRepository.FindById(id);
            var holywarUsers = await FindHolywarParticipants(id);

            discussion.HolywarStateByUserId = userId;
            discussion.HolywarState = HolywarState.Declined;
            discussion.IsHolywar = false;

            await discussionsRepository.Transaction(async () =>
            {
                await discussionsRepository.UpdateAsync(discussion, d => d.HolywarStateByUserId);
                await discussionsRepository.UpdateAsync(discussion, d => d.HolywarState);
                await discussionsRepository.UpdateAsync(discussion, d => d.IsHolywar);
            });

            await CreateEternal();

            return new ServiceResult();
        }

        /*
         * Ищет участников холивара
         * пользователи могут повторятся
         */
        private async Task<IEnumerable<HolywarParticipantUser>> FindHolywarParticipants(ObjectId discussionId)
        {
            var comments = await FindHolywarComments(discussionId);
            var result = new List<HolywarParticipantUser>();

            foreach (var comment in comments)
            {
                var participant = result.FirstOrDefault(p => p.User.Id == comment.UserId);
                if (participant != null)
                {
                    participant.Comments.Add(comment);
                }
                else
                {
                    var user = await usersRepository.FindById(comment.UserId);
                    result.Add(new HolywarParticipantUser
                    {
                        User = user,
                        Comments = new List<Comment>() { comment }
                    });
                }
            }

            return result;
        }

        class HolywarParticipantUser
        {
            public User User;
            public List<Comment> Comments;
        }

        private async Task<IEnumerable<Comment>> FindHolywarComments(ObjectId discussionId)
        {
            var config = await configService.GetConfig();
            var comments = await commentsRepository.Where(c => c.DiscussionId == discussionId && c.Depth >= config.HolywarCandidatesStep);
            var lastComments = comments.Where(c => comments.All(cc => c.Id != cc.ParentCommentId)).ToArray();
            var result = new List<Comment>();

            foreach (var comment in lastComments)
            {
                if (result.Any(c => c.Id == comment.Id))
                    continue;

                await FillParrents(comment, comments, result);
            }

            return result;
        }

        private async Task FillParrents(Comment comment, IEnumerable<Comment> source, List<Comment> destinations)
        {
            while (comment != null && comment.ParentCommentId != ObjectId.Empty && !comment.IsHolywarComment)
            {
                destinations.Add(comment);

                comment = source.FirstOrDefault(c => c.Id == comment.ParentCommentId)
                    ?? await commentsRepository.FindById(comment.ParentCommentId);
            }
        }

        public async Task<IEnumerable<Discussion>> GetList(DiscussionListFilter request)
        {
            return await discussionsRepository.GetList(request);
        }

        public async Task<IEnumerable<Discussion>> GetEternal()
        {
            if (cachedEternalDiscussions != null && cachedEternalDiscussions.Expired > DateTime.UtcNow)
            {
                var discussions = await discussionsRepository.FindById(cachedEternalDiscussions.DiscussionIds, true);
                return discussions;
            }

            var eternal = await CreateEternal();
            return eternal;
        }

        private async Task<IEnumerable<Discussion>> CreateEternal()
        {
            var top = await discussionsRepository
                .Where(c => c.IsTop);
            var discussions = await discussionsRepository
                .Where(c => c.IsHolywar, sort: discussionsRepository.Sort.Descending(d => d.Id));
            var result = top
                .Concat(
                    discussions
                    .OrderByDescending(GetDiscussionOrder)
                    .Take(100)
                    .Where(d => top.All(t => t.Id != d.Id))
                    .ToArray())
                .ToArray();

            cachedEternalDiscussions = new CachedDiscussions
            {
                Expired = DateTime.UtcNow.AddHours(1),
                DiscussionIds = result.Select(d => d.Id)
            };

            return result;
        }

        public async Task<IEnumerable<Discussion>> GetPopular()
        {
            if (cachedPopularDiscussions != null && cachedPopularDiscussions.Expired > DateTime.UtcNow)
            {
                var discussions = await discussionsRepository.FindById(cachedPopularDiscussions.DiscussionIds, true);
                return discussions;
            }

            var popular = await DeterminePopular();

            cachedPopularDiscussions = new CachedDiscussions
            {
                Expired = DateTime.UtcNow.AddHours(1),
                DiscussionIds = popular.Select(d => d.Id)
            };
            return popular;
        }

        public async Task<IEnumerable<Discussion>> DeterminePopular()
        {
            var categories = await categoriesRepository.All();
            var adCategory = categories.First(c => c.Alias == CategoryAliases.Goods);

            var featured = await GetFeatured();
            var discussions = await discussionsRepository
                .Where(c => !c.IsDeleted && !c.IsUnwanted && c.CategoryId != adCategory.Id, limit: 1000, sort: discussionsRepository.Sort.Descending(d => d.Id));
            var result = discussions
                .OrderByDescending(GetDiscussionOrder)
                .Take(100);

            if (featured != null)
            {
                result = result.Where(r => featured == null || featured.All(f => r.Id != f.Id)).ToArray();
            }

            return result;
        }

        private double GetDiscussionOrder(Discussion c)
        {
            return c.UniqueCommentatorsCount * 200
                    + c.CommetsCount * 20
                    + c.LikesCount * 10
                    + c.DislikesCount * 5
                    + c.ViewsCount
                    - Math.Sqrt((DateTime.UtcNow - c.CreationDate).TotalMinutes) * 10;
        }

        private double GetCommentOrder(CommentWithUser c)
        {
            return c.Comment.LikesCount * 10
                    + c.Comment.DislikesCount * 5
                    - Math.Sqrt((DateTime.UtcNow - c.Comment.CreationDate).TotalMinutes) * 10;
        }

        public async Task<DiscussionWithComments> GetByIdOrAlias(string id)
        {
            var discussion = await FindByIdOrAlias(id);
            if (discussion == null)
            {
                return new DiscussionWithComments
                {
                    Error = "NoDiscussion"
                };
            }
            var user = await usersRepository.FindById(discussion.UserId);
            var comments = (await commentsRepository.GetByDiscussionId(discussion.Id))
                .OrderByDescending(GetCommentOrder)
                .ToArray();
            var survey = await surveysRepository.GetByDiscussionId(discussion.Id);
            var surveyVotes = survey != null ? await surveyVotesRepository.GetBySurveyId(survey.Id) : new SurveyVotes[0];
            return new DiscussionWithComments
            {
                Discussion = discussion,
                User = user,
                Comments = comments,
                Survey = survey,
                SurveyVotes = surveyVotes
            };
        }

        public async Task<Discussion> FindByIdOrAlias(string idOrAlias)
        {
            var discussionId = idOrAlias.ToObjectId();
            if (discussionId != ObjectId.Empty)
            {
                return await discussionsRepository.FindById(discussionId);
            }
            return await discussionsRepository.Find(d => d.Alias == idOrAlias);
        }

        public async Task<IEnumerable<Discussion>> ByCategoryId(string id)
        {
            var categoryId = id.ToObjectId();
            return await discussionsRepository.Where(d => d.CategoryId == categoryId, limit: 6);
        }

        public async Task<IEnumerable<Discussion>> GetFeatured()
        {
            var config = await configService.GetConfig();
            if (config.FeatureDiscussionIds == null)
            {
                return null;
            }
            var ids = config.FeatureDiscussionIds
                .Select(id => id.ToObjectId())
                .Where(id => id != ObjectId.Empty)
                .ToArray();
            if (!ids.Any())
            {
                return null;
            }
            return await discussionsRepository.FindById(ids);
        }

        public async Task<Discussion> FindById(ObjectId id)
        {
            return await discussionsRepository.FindById(id);
        }

        public async Task IncrementViews(Discussion discussion)
        {
            discussion.ViewsCount++;

            await discussionsRepository.UpdateAsync(discussion, d => d.ViewsCount);
            await weekUsersRepository.IncrementDiscussionViews(discussion.UserId);
        }

        public async Task<ServiceResult> IncrementExternalViews(ObjectId discussionId, ObjectId referralUserId)
        {
            var discussion = await discussionsRepository.FindById(discussionId);
            if (discussion == null)
            {
                return new ServiceResult { Error = "DiscussionNotFound" };
            }

            var user = await usersRepository.FindById(discussion.UserId);

            discussion.ExternalViewsCount++;
            user.DiscussionExternalViewsCount++;

            await discussionsRepository.UpdateAsync(discussion, d => d.ExternalViewsCount);
            await usersRepository.UpdateAsync(user, u => u.DiscussionExternalViewsCount);
            await weekUsersRepository.IncrementDiscussionExternalViews(discussion.UserId);

            if(referralUserId != ObjectId.Empty)
            {
                var referralUser = await usersRepository.FindById(discussion.UserId);
                if(referralUser != null)
                {
                    await weekUsersRepository.IncrementReferralViews(referralUser.Id);
                }
            }

            return new ServiceResult();
        }

        public async Task SetDeleted(string discussionId)
        {
            await discussionsRepository.SetDeleted(discussionId);
            ClearPopularsCacheIfExists(discussionId.ToObjectId());
        }

        public async Task SetUnwanted(string discussionId)
        {
            var discussion = await FindById(discussionId.ToObjectId());
            if (discussion == null)
                return;

            await discussionsRepository.UpdateAsync(discussion.Id, d => d.IsUnwanted, true);
            ClearPopularsCacheIfExists(discussion.Id);
        }

        public async Task SetTop(string discussionId, bool value)
        {
            var discussion = await FindById(discussionId.ToObjectId());
            if (discussion == null)
                return;

            await discussionsRepository.UpdateAsync(discussion.Id, d => d.IsTop, value);
            ClearEternalCache();
        }

        public async Task SetModerated(string discussionId, bool value)
        {
            var discussion = await FindById(discussionId.ToObjectId());
            if (discussion == null)
                return;

            await discussionsRepository.UpdateAsync(discussion.Id, d => d.IsModerated, value);
            ClearPopularsCache();
        }
        
        public void ClearEternalCache()
        {
            if (cachedEternalDiscussions == null)
                return;

            cachedEternalDiscussions = null;
        }

        public void ClearPopularsCacheIfExists(ObjectId discussionId)
        {
            if (cachedPopularDiscussions == null)
                return;

            if (cachedPopularDiscussions.DiscussionIds.Any(i => i == discussionId))
                ClearPopularsCache();
        }

        public void ClearPopularsCache()
        {
            cachedPopularDiscussions = null;
        }

        public async Task SetCategory(string discussionId, string categoryAlias)
        {
            var discussion = await FindById(discussionId.ToObjectId());
            if (discussion == null)
                return;

            var category = await categoriesRepository.Find(c => c.Alias == categoryAlias);
            if (category == null)
                return;

            await discussionsRepository.UpdateAsync(discussion.Id, d => d.CategoryId, category.Id);
        }

        public async Task<IEnumerable<Discussion>> GetHolywarCandidates(int limit)
        {
            return await discussionsRepository.Where(d => d.HolywarState == HolywarState.Candidate && !d.IsUnwanted && !d.IsDeleted, limit);
        }
    }
}
