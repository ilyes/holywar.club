﻿using Holywar.Common;
using Holywar.Entities.Abstract;
using Holywar.Repositories;
using HtmlAgilityPack;
using RestSharp;
using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Holywar.Services
{
    public class ParserResult : ServiceResult
    {
        public string Title { get; set; }

        public string ImageUrl { get; set; }

        public string Description { get; set; }
    }

    public class ParserService
    {
        private DiscussionsRepository discussionsRepository;

        public ParserService(DiscussionsRepository discussionsRepository)
        {
            this.discussionsRepository = discussionsRepository;
        }

        public async Task<ParserResult> Parse(string url, bool checkDiscussion)
        {
            if (!Validation.IsUrl(url))
            {
                return new ParserResult { Error = "IncorrectUrl" };
            }

            if (checkDiscussion)
            {
                var discussion = await discussionsRepository.Find(d => d.Url == url);
                if (discussion != null)
                {
                    return new ParserResult { Error = "AlreadyExists" };
                }
            }

            var response = await GetResponse(url);
            var isOk = response.StatusCode == HttpStatusCode.OK;
            if (!isOk)
            {
                Log.Warn($"Url parse unsuccessfull response. Url {url}. Response {response.StatusCode} {response.Content}");
                return new ParserResult { Error = "UnableToLoad" };
            }

            if (response.ContentType.ToLower().Contains("image"))
            {
                return new ParserResult
                {
                    ImageUrl = url
                };
            }

            var html = GetHtml(response);

            var metas = html.DocumentNode.Descendants("meta");
            var title = metas.FirstOrDefault(m => m.GetAttributeValue("property", null) == "og:title")
                ?? html.DocumentNode.Descendants("title").FirstOrDefault();
            var description = metas.FirstOrDefault(m => m.GetAttributeValue("property", null) == "og:description")
                ?? metas.FirstOrDefault(m => m.GetAttributeValue("name", null) == "description");
            var image = metas.FirstOrDefault(m => m.GetAttributeValue("property", null) == "og:image")
                ?? metas.FirstOrDefault(m => m.GetAttributeValue("property", null) == "vk:image")
                ?? metas.FirstOrDefault(m => m.GetAttributeValue("property", null) == "twitter:image");

            var titleText = HttpUtility.HtmlDecode(title?.GetAttributeValue("content", null) ?? title.InnerText);
            var descriptionText = HttpUtility.HtmlDecode(description?.GetAttributeValue("content", "") ?? "");
            return new ParserResult
            {
                Title = titleText,
                Description = descriptionText,
                ImageUrl = image?.GetAttributeValue("content", null)
            };
        }

        public HtmlDocument GetHtml(IRestResponse response)
        {
            var encoding = DetermineEncodingByContentType(response.ContentType);
            var result = encoding.GetString(response.RawBytes);

            var html = new HtmlDocument();
            html.LoadHtml(result);
            return html;
        }

        public async Task<IRestResponse> GetResponse(string url)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(url);
            var request = new RestRequest();
            request.Method = Method.GET;
            var response = await client.ExecuteTaskAsync(request);
            return response;
        }

        private Encoding DetermineEncodingByContentType(string contentType)
        {
            // try
            // {
            //     var charset = contentType.Split(';').Select(c => c.Split('=')).FirstOrDefault(c => c[0].Trim().ToLower() == "charset");
            //     if (charset != null && charset[1].Trim().ToLower() == "cp1251")
            //     {
            //         Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            //         return Encoding.GetEncoding("windows-1251");
            //     }
            // }
            // catch (Exception e)
            // {
            //     Log.Error($"Failed to find charset from contenttype {contentType}");
            // }
            return Encoding.UTF8;
        }

    }
}
