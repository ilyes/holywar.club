﻿using Holywar.Entities.Common;
using Holywar.Repositories;
using System.Threading.Tasks;

namespace Holywar.Services
{
    public class ConfigService
    {
        private ConfigRepository configRepository;

        public ConfigService(ConfigRepository configRepository)
        {
            this.configRepository = configRepository;
        }

        public async Task<Config> GetConfig()
        {
            var config = await configRepository.GetConfig();
            if (config == null)
            {
                config = configRepository.CreateDefaultConfig();
                await configRepository.SaveConfig(config);
            }
            return config;
        }

        public Config GetConfigSync()
        {
            var config = configRepository.GetConfigSync();
            if (config == null)
            {
                config = configRepository.CreateDefaultConfig();
                configRepository.SaveConfigSync(config);
            }
            return config;
        }

        public async Task SaveConfig(Config config)
        {
            await configRepository.SaveConfig(config);
        }
    }
}
