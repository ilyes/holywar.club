﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using Holywar.Entities.Common;
using Holywar.Repositories;

using MailKit.Net.Smtp;
using MimeKit;
using MongoDB.Bson;
using System.Net;
using Holywar.Common;
using RestSharp;
using System.Text;
using Holywar.Entities.Users;

namespace Holywar.Services
{
    public class EmailsService
    {
        private const string FROM = "noreply@holywar.club";
        private const string FAKE_EMAIL_PREFIX = "!!!";
        private const string FAKE_EMAIL_PARTS_SEPARATOR = "#";

        private static string EmailTemplate;

        private EmailsRepository emailsRepository;

        public EmailsService(EmailsRepository emailsRepository)
        {
            this.emailsRepository = emailsRepository;
        }

        public async Task Enqueue(
            ObjectId userId,
            User toUser,
            string subject,
            string text,
            string buttonUrl,
            string buttonText)
        {
            if (toUser.IsUnsubscribed)
            {
                return;
            }

            await Enqueue(userId, toUser.Email, subject, text, buttonUrl, buttonText);
        }

        public async Task Enqueue(
            ObjectId userId,
            string to,
            string subject,
            string text,
            string buttonUrl,
            string buttonText)
        {
            if (IsFakeEmail(to))
                return;

            var email = new Email
            {
                CreationDate = DateTime.UtcNow,
                UserId = userId,
                To = to,
                Subject = subject,
                Html = GetEmailHtml(subject, text, buttonUrl, buttonText, to),
                Text = text
            };

            await emailsRepository.Add(email);
        }

        public async Task<IEnumerable<Email>> GetUnsent()
        {
            return await emailsRepository.Where(e => !e.IsDeleted && !e.IsSent && !e.IsSending && e.Attempts < 3, limit: 100);
        }

        private string GetEmailHtml(
            string title, 
            string text, 
            string buttonUrl, 
            string buttonText,
            string unsubscribeEmail)
        {
            if(string.IsNullOrEmpty(EmailTemplate))
            {
                EmailTemplate = ReadEmailTemplate();
            }

            return EmailTemplate
                .Replace("{{title}}", title)
                .Replace("{{text}}", text)
                .Replace("{{buttonUrl}}", buttonUrl)
                .Replace("{{buttonText}}", buttonText)
                .Replace("{{footerText}}", GetEmailFooterText())
                .Replace("{{unsubscribeEmail}}", unsubscribeEmail);
        }

        private string GetEmailFooterText()
        {
            return $"holywar.club @ {DateTime.Now.Year}";
        }

        private string ReadEmailTemplate()
        {
            if(File.Exists("./EmailTemplate.html"))
            {
                return File.ReadAllText("./EmailTemplate.html");
            }
            return File.ReadAllText("./bin/Debug/netcoreapp3.1/EmailTemplate.html");
        }

        public async Task Test(string to)
        {
            var subject = "Test text test";
            var text = "Test text test text test text test text test text test text test text test text test";
            var buttonText = "Test";
            await SendWithPostfix(new Email
            {
                To = to,
                Subject = subject,
                Html = GetEmailHtml(subject, text, "https://holywar.lcub", buttonText, "test@holywar.club"),
                Text = text
            });
        }

        public async Task Send(Email email)
        {
            if (email.IsSent)
            {
                throw new ApplicationException("Email already sent");
            }
            var updates = emailsRepository.Update.Combine(
                emailsRepository.Update.Set(e => e.IsSending, true),
                emailsRepository.Update.Set(e => e.SendingStartedAt, DateTime.UtcNow));
            await emailsRepository.UpdateAsync(email.Id, updates);

            var isOk = await SendWithSendpulse(email);
            if (isOk)
            {
                updates = emailsRepository.Update.Combine(
                    emailsRepository.Update.Set(e => e.IsSent, true),
                    emailsRepository.Update.Set(e => e.SendingCompleteAt, DateTime.UtcNow));
            }
            else
            {
                updates = emailsRepository.Update.Combine(
                    emailsRepository.Update.Set(e => e.IsSending, false),
                    emailsRepository.Update.Set(e => e.Attempts, email.Attempts + 1));
            }
            await emailsRepository.UpdateAsync(email.Id, updates);
        }

        private async Task<bool> SendWithPostfix(Email email)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(FROM, FROM));
            message.To.Add(new MailboxAddress(email.To, email.To));
            message.Subject = email.Subject;

            var builder = new BodyBuilder();
            builder.HtmlBody = email.Html;

            message.Body = builder.ToMessageBody();

            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                await client.ConnectAsync("mail.holywar.club", 25, false);
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }

            return true;
        }

        public string CreateFakeEmail(params string[] parts)
        {
            return $"{FAKE_EMAIL_PREFIX}{string.Join(FAKE_EMAIL_PARTS_SEPARATOR, parts)}";
        }

        public bool IsFakeEmail(string email)
        {
            return email.StartsWith(FAKE_EMAIL_PREFIX);
        }

        private async Task<bool> SendWithSendpulse(Email email)
        {
            var tokenResponse = await SendSendpulseRequest<SendpulseAuthResponse>("oauth/access_token", new
            {
                grant_type = "client_credentials",
                client_id = "c4573e1ecdff1e6b043140c628e2a486",
                client_secret = "4d08cd6052f1af80fdb410d4edda2c27"
            });
            if (tokenResponse.StatusCode != HttpStatusCode.OK)
            {
                Log.Error($"Email auth with Sendpulse error. Response {tokenResponse.StatusCode} {tokenResponse.Content}");
                Console.WriteLine($"Email auth with Sendpulse error. Response {tokenResponse.StatusCode} {tokenResponse.Content}");
                return false;
            }

            var response = await SendSendpulseRequest<SendpulseSendResponse>("smtp/emails", new
            {
                email = new
                {
                    text = email.Text,
                    html = Convert.ToBase64String(Encoding.UTF8.GetBytes(email.Html)),
                    subject = email.Subject,
                    from = new
                    {
                        name = "Holywar.club",
                        email = FROM
                    },
                    to = new object[]
                    {
                        new {
                            name = email.To,
                            email = email.To
                        }
                    }
                }
            }, new KeyValuePair<string, string>("Authorization", $"{ tokenResponse.Data.token_type} {tokenResponse.Data.access_token}"));
            if (response.StatusCode != HttpStatusCode.OK)
            {
                Log.Error($"Email send with Sendpulse error. Response {response.StatusCode} {response.Content}");
                Console.WriteLine($"Email send with Sendpulse error. Response {response.StatusCode} {response.Content}");
                return false;
            }
            return true;
        }

        private async Task<IRestResponse<T>> SendSendpulseRequest<T>(string uri, object json, params KeyValuePair<string, string>[] headers)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri($"https://api.sendpulse.com{"/" + uri}");
            var request = new RestRequest();
            foreach (var header in headers)
                request.AddHeader(header.Key, header.Value);
            request.AddJsonBody(json);
            request.Method = Method.POST;

            return await client.ExecuteTaskAsync<T>(request);
        }

        class SendpulseAuthResponse
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
        }

        class SendpulseSendResponse
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
        }
    }
}
