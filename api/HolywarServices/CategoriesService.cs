﻿using Holywar.Entities.Categories;
using Holywar.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Holywar.Services
{
    public class CategoriesService
    {
        private CategoriesRepository categoriesRepository;

        public CategoriesService(CategoriesRepository categoriesRepository)
        {
            this.categoriesRepository = categoriesRepository;
        }

        public async Task<IEnumerable<Category>> GetAll()
        {
            return await categoriesRepository.All();
        }

        public Task<Category> GetByAlias(string alias)
        {
            return categoriesRepository.Find(c => c.Alias == alias);
        }
    }
}
