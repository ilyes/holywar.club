﻿using MongoDB.Bson;

namespace Holywar.Common
{
    public static class ObjectIdExtensions
    {
        public static ObjectId ToObjectId(this string id)
        {
            ObjectId result;
            if (ObjectId.TryParse(id, out result))
                return result;
            return ObjectId.Empty;
        }
    }
}
