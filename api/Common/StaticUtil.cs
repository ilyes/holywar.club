﻿using System.IO;
using System.Reflection;

namespace Holywar.Common
{
    public class StaticUtil
    {
        public static string GetStaticPath(string staticFolder)
        {
            return Path.Combine(
                Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
                staticFolder);
        }
    }
}
