﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Holywar.Common
{
    public class Translit
    {
        private static Dictionary<char, string> translation = new Dictionary<char, string>()
        {
            { 'а', "a" },
            { 'б', "b" },
            { 'в', "v" },
            { 'г', "g" },
            { 'д', "d" },
            { 'е', "e" },
            { 'ё', "yo" },
            { 'ж', "zh" },
            { 'з', "z" },
            { 'и', "i" },
            { 'й', "y" },
            { 'к', "k" },
            { 'л', "l" },
            { 'м', "m" },
            { 'н', "n" },
            { 'о', "o" },
            { 'п', "p" },
            { 'р', "r" },
            { 'с', "s" },
            { 'т', "t" },
            { 'у', "u" },
            { 'ф', "f" },
            { 'х', "kh" },
            { 'ц', "ts" },
            { 'ч', "ch" },
            { 'ш', "sh" },
            { 'щ', "shch" },
            { 'ъ', "" },
            { 'ы', "y" },
            { 'ь', "" },
            { 'э', "e" },
            { 'ю', "yu" },
            { 'я', "ya" },

            { 'А', "A" },
            { 'Б', "B" },
            { 'В', "V" },
            { 'Г', "G" },
            { 'Д', "D" },
            { 'Е', "YE" },
            { 'Ё', "YO" },
            { 'Ж', "ZH" },
            { 'З', "Z" },
            { 'И', "I" },
            { 'Й', "Y" },
            { 'К', "K" },
            { 'Л', "L" },
            { 'М', "M" },
            { 'Н', "N" },
            { 'О', "O" },
            { 'П', "P" },
            { 'Р', "R" },
            { 'С', "S" },
            { 'Т', "T" },
            { 'У', "U" },
            { 'Ф', "F" },
            { 'Х', "KH" },
            { 'Ц', "TS" },
            { 'Ч', "CH" },
            { 'Ш', "SH" },
            { 'Щ', "SHCH" },
            { 'Ъ', "" },
            { 'Ы', "Y" },
            { 'Ь', "" },
            { 'Э', "E" },
            { 'Ю', "YU" },
            { 'Я', "YA" },
            { ' ', "_" }
        };

        private static Regex Eng = new Regex("[A-Za-z]");

        public static string Make(string src, int? max)
        {
            var result = string.Join("", src.Select(ToTranslit));
            if (max.HasValue && result.Length > max.Value)
                result = result.Substring(0, max.Value);
            return result;
        }

        private static string ToTranslit(char c)
        {
            string result;
            if (translation.TryGetValue(c, out result))
                return result;
            if (Eng.IsMatch(c.ToString()))
                return c.ToString();
            return "";
        }
    }
}
