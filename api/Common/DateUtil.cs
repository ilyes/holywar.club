﻿using System;

namespace Holywar.Common
{
    public static class DateUtil
    {
        public static int GetCurrentWeekId()
        {
            return GetWeekId(DateTime.UtcNow);
        }

        private static DateTime StarWeekDate = new DateTime(2019, 1, 7); // monday
        public static int GetWeekId(DateTime dateTime)
        {
            return (int)Math.Floor((dateTime - StarWeekDate).TotalDays / 7);
        }
    }
}
