﻿using System.IO;
using Microsoft.AspNetCore.Http;
using SkiaSharp;

namespace Holywar.Common
{
    public class FileUtil
    {
        public static void SaveImage(IFormFile file, string filePath, int size)
        {
            using (var fileStream = file.OpenReadStream())
            {
                using (var inputStream = new SKManagedStream(fileStream))
                {
                    using (var original = SKBitmap.Decode(inputStream))
                    {
                        int width, height;
                        if (original.Width < original.Height)
                        {
                            width = size;
                            height = original.Height * size / original.Width;
                        }
                        else
                        {
                            width = original.Width * size / original.Height;
                            height = size;
                        }

                        using (var resized = original.Resize(new SKImageInfo(width, height), SKFilterQuality.Medium))
                        {
                            if (resized == null)
                            {
                                return;
                            }

                            using (var image = SKImage.FromBitmap(resized))
                            {
                                using (var output = File.OpenWrite(filePath))
                                {
                                    image.Encode(SKEncodedImageFormat.Jpeg, 75)
                                        .SaveTo(output);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
