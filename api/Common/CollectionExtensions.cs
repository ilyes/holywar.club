﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Holywar.Common
{
    public static class CollectionExtensions
    {
        public static bool In<T>(this T self, params T[] arr)
        {
            return arr.Contains(self);
        }

        public static int FindIndex<T>(this IEnumerable<T> items, Func<T, bool> predicate)
        {
            if (items == null) throw new ArgumentNullException("items");
            if (predicate == null) throw new ArgumentNullException("predicate");

            int retVal = 0;
            foreach (var item in items)
            {
                if (predicate(item)) return retVal;
                retVal++;
            }
            return -1;
        }

        public static Queue<T> ToQueue<T>(this IEnumerable<T> self)
        {
            var result = new Queue<T>();
            foreach (var i in self)
            {
                result.Enqueue(i);
            }
            return result;
        }
    }
}
