﻿using System.Text.RegularExpressions;

namespace Holywar.Common
{
    public class Validation
    {
        private const string UrlDomainPart = @"[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?";

        public const string Email = @"^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$";
        public const string Password = @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}$";
        public const string Url = @"(http|https):\/\/" + UrlDomainPart;

        public static bool IsUrl(string str)
        {
            return new Regex(Url).IsMatch(str);
        }
    }
}
