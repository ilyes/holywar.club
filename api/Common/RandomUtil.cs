﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Holywar.Common
{
    public class RandomUtil
    {
        private static Random random = new Random(DateTime.UtcNow.Millisecond);

        /// <summary>
        /// Float from 0 to 1
        /// </summary>
        /// <returns></returns>
        public static float Float()
        {
            return (float)random.NextDouble();
        }

        public static float Float(float min, float max)
        {
            return (float)(random.NextDouble() * (max - min) + min);
        }

        public static double Double()
        {
            return random.NextDouble();
        }

        public static int Int()
        {
            return Int(0, int.MaxValue - 1);
        }

        public static int Int(int min, int max)
        {
            return random.Next(min, max + 1);
        }

        public static long Long(long min, long max)
        {
            return (long)(random.NextDouble() * (max - min) + min);
        }

        public static byte Byte(byte min, byte max)
        {
            return (byte)Int(min, max);
        }

        public static short Short(short min, short max)
        {
            return (short)Int(min, max);
        }

        public static T Param<T>(params T[] elements)
        {
            return elements[Int(0, elements.Length - 1)];
        }

        public static T Element<T>(IEnumerable<T> elements)
        {
            return elements.Skip(Int(0, elements.Count() - 1)).FirstOrDefault();
        }

        public static IEnumerable<T> Elements<T>(IEnumerable<T> elements, int count)
        {
            return elements.OrderBy(e => Double()).Take(count).ToArray();
        }

        public static IEnumerable<T> Elements<T>(IEnumerable<T> elements, int min, int max)
        {
            var count = Int(min, max);
            return Elements(elements, count);
        }

        public static bool Boolean(float trueChance = 0.5f)
        {
            return Float() < trueChance;
        }
    }

    public class RandomWeighted<T>
    {
        private RandomWeightedItem<T>[] weightedItems;
        private float total;

        public RandomWeighted(RandomWeightedItem<T>[] items)
        {
            weightedItems = items;
            total = weightedItems.Sum(i => i.Chance);
        }

        public T Random()
        {
            var r = RandomUtil.Float(0, total);
            foreach (RandomWeightedItem<T> items in weightedItems)
            {
                if (r <= items.Chance)
                    return RandomUtil.Element(items.Items);
                r -= items.Chance;
            }
            throw new Exception("RandomWeighted class bug");
        }
    }

    public class RandomWeightedItem<T>
    {
        public float Chance;
        public T[] Items;
    }
}
