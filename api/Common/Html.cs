﻿namespace Holywar.Common
{
    public class Html
    {
        public static string RemoveHtmlCodes(string input)
        {
            return System.Web.HttpUtility.HtmlDecode(input);
                //.Replace("&amp;", "&")
                //.Replace("&#8364;", "€")
                //.Replace("&#x20ac", "€")
                //.Replace("&#036;", "$")
                //.Replace("&nbsp;", " ")
                //.Replace("&quot;", "\"");
        }
    }
}
