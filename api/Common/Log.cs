﻿using NLog;
using System;

namespace Holywar.Common
{
    public class Log
    {
        public static readonly Logger logger = LogManager.GetLogger("log");

        public static void Debug(string message)
        {
            logger.Debug(message);
        }

        public static void Debug(string label, string message)
        {
            Debug($"[{label}] {message}");
        }

        public static void Info(string message)
        {
            logger.Info(message);
        }

        public static void Info(string label, string message)
        {
            Info($"[{label}] {message}");
        }

        public static void Error(Exception ex)
        {
            logger.Error(ex);
        }

        public static void Error(string message)
        {
            logger.Error(message);
        }

        public static void Warn(string message)
        {
            logger.Warn(message);
        }

        public static void Warn(string label, string message)
        {
            Warn($"[{label}] {message}");
        }

        public static void Warn(Exception ex)
        {
            logger.Warn(ex);
        }
    }
}
