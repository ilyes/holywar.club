﻿using Autofac;

namespace Holywar.Ioc
{
    public class BuilderBuilder
    {
        public static void Build(ContainerBuilder builder)
        {
            builder
                .RegisterAssemblyTypes(typeof(Services.AuthService).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsSelf()
                .InstancePerLifetimeScope();

            builder
                .RegisterType<Repositories.Internal.MongoAdapter>()
                .AsSelf();

            builder
                .RegisterType<Repositories.Internal.SeedRunner>()
                .AsSelf();

            builder
                .RegisterType<Repositories.Internal.MigrationRunner>()
                .AsSelf();
            
            builder
                .RegisterAssemblyTypes(typeof(Repositories.AuthRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsSelf()
                .InstancePerLifetimeScope();
        }
    }
}
