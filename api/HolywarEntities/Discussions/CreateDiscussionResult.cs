﻿using Holywar.Entities.Abstract;

namespace Holywar.Entities.Discussions
{
    public class CreateDiscussionResponse : ApiResponse
    {
        public string DiscussionId { get; set; }
    }
}
