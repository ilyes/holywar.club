﻿using Holywar.Entities.Comments;
using Holywar.Entities.Surveys;
using Holywar.Entities.Users;
using System.Collections.Generic;

namespace Holywar.Entities.Discussions
{
    public class DiscussionModel
    {
        public string Id { get; set; }

        public string Alias { get; set; }

        public string CreationDate { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public string Url { get; set; }

        public int LikesCount { get; set; }

        public int DislikesCount { get; set; }

        public int ViewsCount { get; set; }

        public int CommentsCount { get; set; }

        public DiscussionBodyItemModel[] Body { get; set; }

        public string CategoryId { get; set; }

        public bool IsHolywar { get; set; }

        public SurveyModel Survey { get; set; }

        public DiscussionAuthorModel Author { get; set; }
    }

    public class DiscussionAuthorModel
    {
        public string UserId { get; set; }

        public string Name { get; set; }

        public string AvatarUrl { get; set; }
    }

    public class DiscussionBodyItemModel
    {
        public DiscussionBodyItemType Type { get; set; }

        public string Value { get; set; }
    }

    public class DiscussionWithCommentsModel
    {
        public DiscussionModel Discussion { get; set; }

        public IEnumerable<CommentWithUserModel> Comments { get; set; }

        public UserDto User { get; set; }

        public SurveyModel Survey { get; set; }

        public SurveyVotesModel[] SurveyVotes { get; set; }
    }

    public class DiscussionIdModel
    {
        public string DiscussionId { get; set; }
    }
}
