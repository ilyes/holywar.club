﻿using Holywar.Entities.Abstract;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Discussions
{
    [BsonIgnoreExtraElements]
    public class Discussion : AbstractEntity
    {
        public string Alias { get; set; }

        public ObjectId UserId { get; set; }

        public ObjectId CategoryId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public string Url { get; set; }

        public DiscussionBodyItem[] Body { get; set; }

        public int CommetsCount { get; set; }

        public int UniqueCommentatorsCount { get; set; }

        public int LikesCount { get; set; }

        public int DislikesCount { get; set; }

        public int ViewsCount { get; set; }

        public int ExternalViewsCount { get; set; }

        public bool IsUnwanted { get; set; }

        public HolywarState HolywarState { get; set; }

        public ObjectId HolywarStateByUserId { get; set; }

        public bool IsHolywar { get; set; }

        public bool IsTop { get; set; }

        public bool IsModerated { get; set; }
    }

    public class DiscussionBodyItem
    {
        public DiscussionBodyItemType Type { get; set; }

        public string Value { get; set; }
    }

    public enum DiscussionBodyItemType
    {
        Text = 1,
        Image = 2,
        Url = 3,
        Video = 4,
        SubTitle = 5,
    }

    public enum HolywarState
    {
        Not = 0,
        Candidate = 1,
        Approved = 2,
        Declined = 3,
        Continues = 4
    }
}
