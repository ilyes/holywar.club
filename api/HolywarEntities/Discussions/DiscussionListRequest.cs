﻿namespace Holywar.Entities.Discussions
{
    public class DiscussionListFilter
    {
        public string UserId { get; set; }

        public string AfterDiscussionId { get; set; }

        public int Limit { get; set; }

        public string[] CategoryIds { get; set; }
    }
}
