﻿using Holywar.Entities.Users;

namespace Holywar.Entities.Messages
{
    public class MessageModel
    {
        public string Id { get; set; }

        public string Content { get; set; }

        public MessageType Type { get; set; }

        public string CreationDate { get; set; }

        public bool IsRead { get; set; }
    }

    public class MessageWithUserModel
    {
        public MessageModel Message { get; set; }

        public UserDto FromUser { get; set; }
    }
}
