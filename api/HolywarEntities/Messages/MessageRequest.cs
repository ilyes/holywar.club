﻿namespace Holywar.Entities.Messages
{
    public class MessageRequest
    {
        public string ToUserId { get; set; }
        public string Text { get; set; }
    }
}
