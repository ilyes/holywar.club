﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Holywar.Entities.Users;

namespace Holywar.Entities.Messages
{
    [BsonIgnoreExtraElements]
    public class Message : Abstract.AbstractEntity
    {
        public ObjectId FromUserId { get; set; }

        public ObjectId ToUserId { get; set; }

        public string Text { get; set; }

        public MessageType Type { get; set; }

        public bool IsRead { get; set; }
    }

    public enum MessageType
    {
        Common = 0,
        LikesCount = 1,
        Reply = 2,
        AddMoney = 3,
        PaymentCreated = 4,
        PaymentApproved = 5,
        PaymentDeclined = 6,
        HowTo = 7,
    }

    public class MessageWithUser
    {
        public Message Message { get; set; }

        public User FromUser { get; set; }
    }
}
