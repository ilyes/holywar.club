﻿namespace Holywar.Entities.Payments
{
    public class PaymentStatusRequest
    {
        public string Status { get; set; }

        public string PaymentId { get; set; }
    }
}
