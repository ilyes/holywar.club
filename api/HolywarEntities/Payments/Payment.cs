﻿using Holywar.Entities.Abstract;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Payments
{
    [BsonIgnoreExtraElements]
    public class Payment : AbstractEntity
    {
        public ObjectId UserId { get; set; }

        public string Status { get; set; }

        public int Sum { get; set; }

        public string Instructions { get; set; }
    }
}
