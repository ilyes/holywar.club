﻿using Holywar.Entities.Users;

namespace Holywar.Entities.Payments
{
    public class PaymentWithUserDto : PaymentDto
    {
        public UserDto User { get; set; }
    }
}
