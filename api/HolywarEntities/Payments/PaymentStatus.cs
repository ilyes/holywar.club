﻿namespace Holywar.Entities.Payments
{
    public static class PaymentStatus
    {
        public const string Queried = "queried";
        public const string Pending = "pending";
        public const string Paid = "paid";
        public const string Declined = "declined";
    }
}
