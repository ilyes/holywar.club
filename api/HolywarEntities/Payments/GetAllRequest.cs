﻿namespace Holywar.Entities.Payments
{
    public class GetAllRequest
    {
        public string AfterId { get; set; }

        public int Limit { get; set; }
    }
}
