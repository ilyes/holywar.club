﻿namespace Holywar.Entities.Payments
{
    public class CreatePaymentRequest
    {
        public int Sum { get; set; }

        public string Instructions { get; set; }
    }
}
