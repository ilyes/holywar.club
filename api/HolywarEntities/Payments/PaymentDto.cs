﻿namespace Holywar.Entities.Payments
{
    public class PaymentDto
    {
        public string Id { get; set; }

        public string Status { get; set; }

        public int Sum { get; set; }

        public string Instructions { get; set; }
    }
}
