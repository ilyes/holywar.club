﻿namespace Holywar.Entities.Surveys
{
    public class SurveyModel
    {
        public string Id { get; set; }

        public string DiscusionId { get; set; }

        public SurveyItemModel[] Items { get; set; }
    }

    public class SurveyItemModel
    {
        public string Id { get; set; }

        public string Title { get; set; }
    }
}
