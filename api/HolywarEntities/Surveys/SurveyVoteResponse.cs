﻿using Holywar.Entities.Abstract;

namespace Holywar.Entities.Surveys
{
    public class SurveyVoteResponse : ApiResponse
    {
        public SurveyVotesModel[] SurveyVotes { get; set; }
    }
}
