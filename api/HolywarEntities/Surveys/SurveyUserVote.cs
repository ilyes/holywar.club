﻿using Holywar.Entities.Abstract;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Surveys
{
    [BsonIgnoreExtraElements]
    public class SurveyUserVote : AbstractEntity
    {
        public ObjectId SurveyId { get; set; }

        public ObjectId UserId { get; set; }

        public ObjectId VoteForSurveyItemId { get; set; }
    }
}
