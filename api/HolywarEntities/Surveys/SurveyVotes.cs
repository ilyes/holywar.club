﻿using Holywar.Entities.Abstract;
using MongoDB.Bson;

namespace Holywar.Entities.Surveys
{
    public class SurveyVotes : AbstractEntity
    {
        public ObjectId SurveyId { get; set; }

        public int Votes { get; set; }
    }
}
