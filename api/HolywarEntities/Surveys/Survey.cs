﻿using Holywar.Entities.Abstract;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Surveys
{
    [BsonIgnoreExtraElements]
    public class Survey : AbstractEntity
    {
        public ObjectId DiscusionId { get; set; }

        public SurveyItem[] Items { get; set; }
    }

    [BsonIgnoreExtraElements]
    public class SurveyItem
    {
        public ObjectId Id { get; set; }

        public string Title { get; set; }
    }

}
