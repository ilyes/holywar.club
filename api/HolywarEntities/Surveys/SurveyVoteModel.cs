﻿namespace Holywar.Entities.Surveys
{
    public class SurveyVoteModel
    {
        public string SurveyId { get; set; }

        public string SurveyItemId { get; set; }
    }

    public class SurveyVotesModel
    {
        public string Id { get; set; }

        public int Votes { get; set; }
    }
}
