﻿namespace Holywar.Entities.Admin.Balance
{
    public class AddMoneyModels
    {
        public int WeekUsersId { get; set; }

        public AddMoneyModel[] Users { get; set; }
    }

    public class AddMoneyModel
    {
        public string UserId { get; set; }

        public int AddMoney { get; set; }

        public int ReferralPayments { get; set; }

        public int PaymentLimitWeekReduce { get; set; }
    }
}
