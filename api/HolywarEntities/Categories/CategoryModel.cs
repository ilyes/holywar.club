﻿namespace Holywar.Entities.Categories
{
    public class CategoryModel
    {
        public string Id { get; set; }
        
        public string Alias { get; set; }

        public string Name { get; set; }
    }
}
