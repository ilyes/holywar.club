﻿namespace Holywar.Entities.Categories
{
    public class SaveCategoriesRequest
    {
        public string[] CategoryIds { get; set; }
    }
}
