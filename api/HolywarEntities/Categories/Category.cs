﻿using Holywar.Entities.Abstract;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Categories
{
    [BsonIgnoreExtraElements]
    public class Category : AbstractEntity
    {
        public string Alias { get; set; }

        public string Name { get; set; }
    }
}
