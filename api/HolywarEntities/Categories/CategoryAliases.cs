﻿namespace Holywar.Entities.Categories
{
    public class CategoryAliases
    {
        public const string Politics = "Politics";
        public const string Economics = "Business";
        public const string Religion = "Religion";
        public const string Technology = "Technology";
        public const string Health = "Health";
        public const string Culture = "Culture";
        public const string Sports = "Sports";
        public const string Games = "Games";
        public const string Movies = "Movies";
        public const string Music = "Music";
        public const string Incidents = "Incidents";
        public const string Humor = "Humor";
        public const string Society = "Society";
        public const string Goods = "Goods";
        public const string Services = "Services";
    }
}
