﻿using Holywar.Entities.Abstract;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Tasks
{
    [BsonIgnoreExtraElements]
    public class TaskInfo : AbstractEntity
    {
        public string Alias { get; set; }

        public string TaskType { get; set; }

        public string Params { get; set; }

        public string Memory { get; set; }
    }
}
