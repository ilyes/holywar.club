﻿using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Common
{
    [BsonIgnoreExtraElements]
    public class Config
    {
        public string FrontendUrl { get; set; }

        public string EmailConfirmedPathTemplate { get; set; }

        public string EmailConfirmPathTemplate { get; set; }

        public string AvatarsFolder { get; set; }

        public string StaticPath { get; set; }

        public string StaticUrl { get; set; }

        public string RatingsUrl { get; set; }

        public string ProfileUrl { get; set; }

        public string SetPasswordPathTemplate { get; set; }

        public string DiscussionPathTemplate { get; set; }

        public string CommentReplyPathTemplate { get; set; }

        public string[] FeatureDiscussionIds { get; set; }

        public string HowToDiscussionId { get; set; }

        public int HolywarCandidatesStep { get; set; }

        public short MinPaymentLimit { get; set; }

        public short StartPaymentLimit { get; set; }

        public short LevelLimitReduceBy { get; set; }

        public int[] LevelSteps { get; set; }

        public string AdProvider { get; set; }

        public string AdminUserId { get; set; }

        public int[][] AvailableServieIps { get; set; }

        public int WeekRating { get; set; }

        public int WeekJackpot { get; set; }
    }

    public class AdProviders
    {
        public const string Google = "google";
        public const string Yandex = "yandex";
    }
}
