﻿using System;
using Holywar.Entities.Abstract;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Common
{
    [BsonIgnoreExtraElements]
    public class Email : AbstractEntity
    {
        public ObjectId UserId { get; set; }

        public DateTime SendingStartedAt { get; set; }

        public bool IsSending { get; set; }

        public bool IsSent { get; set; }

        public DateTime SendingCompleteAt { get; set; }

        public int Attempts { get; set; }

        public string To { get; set; }

        public string Subject { get; set; }

        public string Html { get; set; }

        public string Text { get; set; }
    }
}
