﻿namespace Holywar.Entities.Common
{
    public class ParseUrlRequest
    {
        public string Url { get; set; }

        public bool CheckDiscussion { get; set; }
    }
}
