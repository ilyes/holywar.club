﻿using Holywar.Entities.Abstract;

namespace Holywar.Entities.Common
{
    public class ParseUrlResponse : ApiResponse
    {
        public string Title { get; set; }

        public string ImageUrl { get; set; }

        public string Description { get; set; }
    }
}
