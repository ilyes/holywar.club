﻿using Holywar.Entities.Users;

namespace Holywar.Entities.Likes
{
    public class LikeWithUser
    {
        public User User { get; set; }

        public User TargetUser { get; set; }

        public Like Like { get; set; }
    }
}