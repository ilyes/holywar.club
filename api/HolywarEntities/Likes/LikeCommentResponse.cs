﻿using Holywar.Entities.Abstract;
using Holywar.Entities.Comments;

namespace Holywar.Entities.Likes
{
    public class LikeCommentResponse : ApiResponse
    {
        public CommentRequest Comment { get; set; }
    }
}
