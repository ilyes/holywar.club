﻿using Holywar.Entities.Abstract;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Likes
{
    [BsonIgnoreExtraElements]
    public class Like : AbstractEntity
    {
        public ObjectId UserId { get; set; }

        public ObjectId TargetUserId { get; set; }
        
        public ObjectId CommentId { get; set; }

        public ObjectId DiscussionId { get; set; }
        
        public bool IsLike { get; set; }
    }
}
