﻿using Holywar.Entities.Abstract;
using Holywar.Entities.Discussions;

namespace Holywar.Entities.Likes
{
    public class LikeDiscussionResponse : ApiResponse
    {
        public DiscussionModel Discussion { get; set; }
    }
}
