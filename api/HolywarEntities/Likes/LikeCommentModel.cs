﻿namespace Holywar.Entities.Likes
{
    public class LikeCommentRequest
    {
        public string CommentId { get; set; }

        public bool IsLike { get; set; }
    }
}
