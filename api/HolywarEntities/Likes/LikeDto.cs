﻿namespace Holywar.Entities.Likes
{
    public class LikeDto
    {
        public string UserId { get; set; }

        public string TargetUserId { get; set; }

        public string CommentId { get; set; }

        public string DiscussionId { get; set; }

        public bool IsLike { get; set; }
    }
}
