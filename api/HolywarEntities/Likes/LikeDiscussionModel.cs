﻿namespace Holywar.Entities.Likes
{
    public class LikeDiscussionModel
    {
        public string DiscussionId { get; set; }

        public bool IsLike { get; set; }
    }
}
