﻿using Holywar.Entities.Users;

namespace Holywar.Entities.Likes
{
    public class LikeWithUserDto
    {
        public UserDto User { get; set; }

        public UserDto TargetUser { get; set; }

        public LikeDto Like { get; set; }
    }
}