﻿using System;
using System.Collections.Generic;
using Holywar.Entities.Abstract;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Users
{
    [BsonIgnoreExtraElements]
    public class WeekUsers
    {
        public int Id { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime CreationDate { get; set; }

        public List<WeekUser> Users { get; set; }

        public bool IsPaid { get; set; }
    }

    [BsonIgnoreExtraElements]
    public class WeekUser : AbstractEntity
    {
        public int WeekId { get; set; }

        public ObjectId UserId { get; set; }

        public int LikesCount { get; set; }

        public int DisikesCount { get; set; }

        public int CommentsCount { get; set; }

        public int DiscussionsCount { get; set; }
        
        public int DiscussionViewsCount { get; set; }

        // сколько было внешних переходов на созданные пользователем статьи  
        public int DiscussionExternalViewsCount { get; set; }

        // сколько было внешних переходов по реферральной ссылке
        public int ReferralViewsCount { get; set; }
        
        public int UniqueCommentatorsCount { get; set; }

        public int UniqueRepliesCount { get; set; }

        public int HolywarsStarted { get; set; }

        public int HolywarsParticipant { get; set; }
        
        public int ReferralsCount { get; set; }

        public int Paid { get; set; }

        public int ReferralPayments { get; set; }

        public int Rating { get; set; }

        public ObjectId PaidByAdminId { get; set; }
    }
}
