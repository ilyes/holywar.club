using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Holywar.Entities.Abstract;

namespace Holywar.Entities.Users
{
    [BsonIgnoreExtraElements]
    public class User : AbstractEntity
    {
        public string Name { get; set; }

        public string Hash { get; set; }

        public string Email { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public bool IsWriteProhibited { get; set; }

        public bool IsUnsubscribed { get; set; }

        public string AvatarUrl { get; set; }

        public int Balance { get; set; }

        public int ReferralPayments { get; set; }
        
        public int TotalEarned { get; set; }
        
        public ObjectId ReferralId { get; set; }

        public string Role { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime LastLogin { get; set; }

        public string[] CategoryIds { get; set; }

        public int PaymentLimit { get; set; }

        public int PaymentLimitWeekReduce { get; set; }

        public int Level { get; set; }

        #region Statistics

        public long LikesCount { get; set; }

        public long DislikesCount { get; set; }

        public int CommentsCount { get; set; }

        public int ReferralsCount { get; set; }

        public int DiscussionsCount { get; set; }

        public int UniqueCommentatorsCount { get; set; }

        public int UniqueRepliesCount { get; set; }

        public int HolywarsStarted { get; set; }

        public int HolywarsParticipates { get; set; }

        public int DiscussionExternalViewsCount { get; set; }

        #endregion
    }
}
