﻿namespace Holywar.Entities.Users
{
    public class AuthRole
    {
        public const string Client = "client";
        public const string Admin = "admin";
        public const string Service = "service";
    }
}
