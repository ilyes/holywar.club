﻿namespace Holywar.Entities.Users
{
    public class WeekUsersResponse
    {
        public int Id { get; set; }

        public UserDto[] Users { get; set; }
    }
}
