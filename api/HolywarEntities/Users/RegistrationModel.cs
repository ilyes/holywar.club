﻿namespace Holywar.Entities.Users
{
    public class RegistrationRequest
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string ReferralId { get; set; }
    }
}
