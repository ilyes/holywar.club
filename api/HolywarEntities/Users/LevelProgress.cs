﻿namespace Holywar.Entities.Users
{
    public class LevelProgress
    {
        public int Current { get; set; }

        public int Next { get; set; }
    }
}
