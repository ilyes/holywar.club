﻿namespace Holywar.Entities.Users
{
    public class UserDto
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string AvatarUrl { get; set; }

        public long LikesCount { get; set; }

        public long DislikesCount { get; set; }

        public int CommentsCount { get; set; }

        public int DiscussionsCount { get; set; }
        
        public int DiscussionViewsCount { get; set; }

        public int DiscussionExternalViewsCount { get; set; }
        
        public int UniqueCommentatorsCount { get; set; }

        public int Balance { get; set; }

        public int Paid { get; set; }
        
        public int TotalEarned { get; set; }

        public string Role { get; set; }

        public string[] CategoryIds { get; set; }

        public int ReferralsCount { get; set; }

        public string ReferralId { get; set; }

        public int ReferralPayments { get; set; }

        public int HolywarsStarted { get; set; }

        public int HolywarsParticipant { get; set; }

        public int WeekRating { get; set; }

        public int PaymentLimit { get; set; }

        public int PaymentLimitWeekReduce { get; set; }

        public int Level { get; set; }

        public LevelProgress LevelProgress { get; set; }
    }
}
