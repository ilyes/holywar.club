﻿namespace Holywar.Entities.Users
{
    public class AvatarResponse : Abstract.ApiResponse
    {
        public string AvatarUrl { get; set; }
    }
}
