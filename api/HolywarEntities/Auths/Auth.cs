﻿using System;
using Holywar.Entities.Abstract;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Auths
{
    [BsonIgnoreExtraElements]
    public class Auth : AbstractEntity
    {
        public ObjectId UserId { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime ExpireAt { get; set; }
    }
}
