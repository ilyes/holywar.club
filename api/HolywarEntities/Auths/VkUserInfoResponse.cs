﻿namespace Holywar.Entities.Auths
{
    public class VkUserInfoResponse 
    {
        public VkUserInfox[] response { get; set; }
    }

    public class VkUserInfox
    {
        public string first_name { get; set; }
        public string photo_200 { get; set; }
    }
}
