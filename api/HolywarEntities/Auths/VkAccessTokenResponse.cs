﻿namespace Holywar.Entities.Auths
{
    public class VkAccessTokenResponse
    {
        public string user_id { get; set; }
        public string access_token { get; set; }
        public string email { get; set; }
    }
}
