﻿namespace Holywar.Entities.Auths
{
    public class RecoverPasswordRequest
    {
        public string Email { get; set; }
    }
}
