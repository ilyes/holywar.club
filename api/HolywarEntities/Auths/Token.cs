﻿using System;
using Holywar.Entities.Abstract;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Auths
{
    [BsonIgnoreExtraElements]
    public class Token : AbstractEntity
    {
        public ObjectId UserId { get; set; }

        public TokenType Type { get; set; }

        public DateTime ExpirationDate { get; set; }
    }

    public enum TokenType
    {
        RecoverPassword = 1
    }
}
