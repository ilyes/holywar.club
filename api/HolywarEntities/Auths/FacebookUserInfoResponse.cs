﻿namespace Holywar.Entities.Auths
{
    public class FacebookUserInfoResponse
    {
        public string id { get; set; }
        public string first_name { get; set; }
        public FacebookUserPicture picture { get; set; }
        public string email { get; set; }
    }

    public class FacebookUserPicture
    {
        public FacebookUserPictureData data { get; set; }
    }

    public class FacebookUserPictureData
    {
        public string url { get; set; }
    }
}
