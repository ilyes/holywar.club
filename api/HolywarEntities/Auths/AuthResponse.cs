﻿using Holywar.Entities.Abstract;

namespace Holywar.Entities.Auths
{
    public class AuthResponse : ApiResponse
    {
        public string Token { get; set; }

        public string UserId { get; set; }
    }
}
