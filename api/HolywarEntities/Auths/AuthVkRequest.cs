﻿namespace Holywar.Entities.Auths
{
    public class AuthVkRequest
    {
        public string Code { get; set; }
        public string RedirectUri { get; set; }
        public string ReferralId { get; set; }
    }
}
