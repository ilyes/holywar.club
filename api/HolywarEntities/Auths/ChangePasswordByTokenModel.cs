﻿namespace Holywar.Entities.Auths
{
    public class ChangePasswordByTokenRequest
    {
        public string Password { get; set; }

        public string Token { get; set; }
    }
}
