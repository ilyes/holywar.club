﻿namespace Holywar.Entities.Auths
{
    public class AuthFbRequest
    {
        public string AccessToken { get; set; }
        public string ReferralId { get; set; }
    }
}
