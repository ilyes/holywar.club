﻿namespace Holywar.Entities.Abstract
{
    public class ApiResponse
    {
        public bool Success { get { return string.IsNullOrEmpty(Error); } }
        public string Error { get; set; }
    }
}
