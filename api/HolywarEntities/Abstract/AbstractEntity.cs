using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Abstract
{
    public class AbstractEntity
    {
        public ObjectId Id { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime CreationDate { get; set; }

        public bool IsDeleted { get; set; }
    }
}