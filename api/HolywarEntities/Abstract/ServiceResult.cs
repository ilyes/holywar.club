﻿namespace Holywar.Entities.Abstract
{
    public class ServiceResult
    {
        public string Error { get; set; }
        public bool HasError => !string.IsNullOrEmpty(Error);
    }
}

