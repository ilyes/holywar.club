﻿namespace Holywar.Entities.Comments
{
    public class CommentRequest
    {
        public string Id { get; set; }

        public string Text { get; set; }

        public string ImageUrl { get; set; }

        public string DiscussionId { get; set; }

        public string ParentCommentId { get; set; }

        public int LikesCount { get; set; }

        public int DislikesCount { get; set; }

        public string CreationDate { get; set; }
    }
}
