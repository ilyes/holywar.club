﻿using Holywar.Entities.Users;

namespace Holywar.Entities.Comments
{
    public class CommentWithUserModel
    {
        public CommentRequest Comment { get; set; }

        public UserDto User { get; set; }

        public string DiscussionAlias { get; set; }
    }
}
