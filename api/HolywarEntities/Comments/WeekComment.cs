﻿using Holywar.Entities.Abstract;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Comments
{
    [BsonIgnoreExtraElements]
    public class WeekComment : AbstractEntity
    {
        public int WeekId { get; set; }

        public ObjectId CommentId { get; set; }

        public int LikesCount { get; set; }

        public int DisikesCount { get; set; }

        public int ReplysCount { get; set; }

        public int ViewsCount { get; set; }

        public int Rating { get; set; }
    }
}
