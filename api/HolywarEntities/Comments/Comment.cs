﻿using Holywar.Entities.Abstract;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Holywar.Entities.Comments
{
    [BsonIgnoreExtraElements]
    public class Comment : AbstractEntity
    {
        public ObjectId UserId { get; set; }

        public ObjectId DiscussionId { get; set; }

        public ObjectId ParentCommentId { get; set; }

        public string Text { get; set; }

        public string ImageUrl { get; set; }

        public int LikesCount { get; set; }

        public int DislikesCount { get; set; }

        public int ReplysCount { get; set; }

        public int Depth { get; set; }

        public bool IsHolywarComment { get; set; }
    }
}
