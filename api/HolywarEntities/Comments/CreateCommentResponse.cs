﻿using Holywar.Entities.Abstract;
using Holywar.Entities.Users;

namespace Holywar.Entities.Comments
{
    public class CreateCommentResponse : ApiResponse
    {
        public CommentRequest Comment { get; set; }

        public UserDto User { get; set; }
    }
}
