﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Holywar.Common;
using Holywar.Entities.Categories;
using Holywar.Entities.Tasks;
using Holywar.Repositories;
using Holywar.Tasker.Tasks;
using Newtonsoft.Json;

namespace Holywar.Tasker
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var container = IocConfig.Build();
            Log.Debug("Tasker stated");

            var tasksRepository = container.Resolve<TasksRepository>();
            Seed(tasksRepository);

            while (true)
            {
                //var tasksInfo = await tasksRepository.Where(t => t.Alias == "EmailSender");
                var tasksInfo = await tasksRepository.All();

                foreach (var taskInfo in tasksInfo)
                {
                    try
                    {
                        var taskType = Type.GetType(taskInfo.TaskType);
                        var task = container.Resolve(taskType) as AbstractTask;
                        var memory = await task.Run(taskInfo);
                        if (memory != null)
                        {
                            taskInfo.Memory = JsonConvert.SerializeObject(memory);
                            await tasksRepository.UpdateAsync(taskInfo, t => t.Memory);
                        }
                    }
                    catch(Exception ex)
                    {
                        Log.Error(ex);
                        Log.Debug("task info: " + taskInfo.Params);
                    }
                }
                
                Thread.Sleep(1000);
            }
        }

        private static void Seed(TasksRepository tasksRepository)
        {
            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "EmailSender",
                TaskType = "Holywar.Tasker.Tasks.EmailsTask",
                Params = "",
            });

            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "VdudTelegramParser",
                TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
                Params = GetTelegramParserParams("https://t.me/s/yurydud", CategoryAliases.Society),
            });
            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "RedakciyaTelegramParser",
                TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
                Params = GetTelegramParserParams("https://t.me/s/redakciya_channel", CategoryAliases.Society),
            });
            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "VarlamovTelegramParser",
                TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
                Params = GetTelegramParserParams("https://t.me/s/varlamov", CategoryAliases.Society),
            });
            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "PavelDurovTelegramParser",
                TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
                Params = GetTelegramParserParams("https://t.me/s/durov_russia", CategoryAliases.Technology),
            });
            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "AiNewsTelegramParser",
                TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
                Params = GetTelegramParserParams("https://t.me/s/ai_newz", CategoryAliases.Technology),
            });
            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "RussiaTodayTelegramParser",
                TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
                Params = GetTelegramParserParams("https://t.me/s/rt_russian", CategoryAliases.Politics),
            });
            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "IliaYashinTelegramParser",
                TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
                Params = GetTelegramParserParams("https://t.me/s/yashin_russia", CategoryAliases.Politics),
            });
            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "AgitblogTelegramParser",
                TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
                Params = GetTelegramParserParams("https://t.me/s/Agitblog", CategoryAliases.Politics),
            });
            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "BbcTelegramParser",
                TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
                Params = GetTelegramParserParams("https://t.me/s/bbcrussian", CategoryAliases.Politics),
            });
            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "RbcTelegramParser",
                TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
                Params = GetTelegramParserParams("https://t.me/s/rbc_news", CategoryAliases.Economics),
            });
            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "ForbesTelegramParser",
                TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
                Params = GetTelegramParserParams("https://t.me/s/forbesrussia", CategoryAliases.Economics),
            });
            tasksRepository.EnsureTask(new TaskInfo
            {
                Alias = "SportsRuTelegramParser",
                TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
                Params = GetTelegramParserParams("https://t.me/s/sportsru", CategoryAliases.Sports),
            });
            //tasksRepository.EnsureTask(new TaskInfo
            //{
            //    Alias = "Rossiya24TelegramParser",
            //    TaskType = "Holywar.Tasker.Tasks.TelegramParserTask",
            //    Params = GetTelegramParserParams("https://t.me/s/Rossiya24News"),
            //});


            //tasksRepository.EnsureTask(new TaskInfo
            //{
            //    Alias = "RbcRssParser",
            //    TaskType = "Holywar.Tasker.Tasks.RssParserTask",
            //    Params = "http://static.feed.rbc.ru/rbc/logical/footer/news.rss",
            //});
            //tasksRepository.EnsureTask(new TaskInfo
            //{
            //    Alias = "VestiRssParser",
            //    TaskType = "Holywar.Tasker.Tasks.RssParserTask",
            //    Params = "https://www.vesti.ru/vesti.rss",
            //});
        }

        private static string GetTelegramParserParams(string url, string alias = null)
        {
            return JsonConvert.SerializeObject(
                new TelegramParserTaskParams
                {
                    Url = url,
                    CategoryAlias = alias
                });
        }
    }
}
