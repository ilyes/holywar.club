﻿using Autofac;
using Holywar.Ioc;
using Holywar.Tasker.Tasks;

namespace Holywar.Tasker
{
    public class IocConfig
    {
        public static IContainer Build()
        {
            var builder = new ContainerBuilder();
            BuilderBuilder.Build(builder);

            builder
                .RegisterAssemblyTypes(typeof(EmailsTask).Assembly)
                .Where(t => t.Name.EndsWith("Task"))
                .AsSelf()
                .InstancePerLifetimeScope();

            return builder.Build();
        }
    }
}
