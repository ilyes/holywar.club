﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Holywar.Common;
using Holywar.Entities.Tasks;
using Holywar.Services;

namespace Holywar.Tasker.Tasks
{
    public class EmailsTask : AbstractTask
    {
        private EmailsService emailsService;

        public EmailsTask(EmailsService emailsService)
        {
            this.emailsService = emailsService;
        }

        public override async Task<object> Run(TaskInfo taskInfo)
        {
            var unsentEmails = await emailsService.GetUnsent();
            if (unsentEmails.Count() == 0)
            {
                return null;
            }

            Log.Debug($"{unsentEmails.Count()} unsent emails");
            foreach (var unsentEmail in unsentEmails)
            {
                try
                {
                    Log.Debug($"Sending to \"{unsentEmail.To}\"");
                    await emailsService.Send(unsentEmail);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }

            return null;
        }
    }
}
