﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Holywar.Common;
using Holywar.Entities.Categories;
using Holywar.Entities.Discussions;
using Holywar.Entities.Tasks;
using Holywar.Entities.Users;
using Holywar.Services;
using HtmlAgilityPack;
using MongoDB.Bson;
using Newtonsoft.Json;

namespace Holywar.Tasker.Tasks
{
    public class TelegramParserTaskMemory
    {
        public string LastPostId { get; set; }
    }

    public class TelegramParserTaskParams
    {
        public string Url { get; set; }

        public string CategoryAlias { get; set; }
    }

    public class TelegramParserTask : AbstractTask
    {
        private static Dictionary<string, DateTime> nextRun = new Dictionary<string, DateTime>();

        private static string[] skipParagraphs = new[] { "#ньюсдня" };

        private ParserService parserService;
        private DiscussionsService discussionsService;
        private EmailsService emailsService;
        private UsersService usersService;
        private CategoriesService categoriesService;

        public TelegramParserTask(
            ParserService parserService,
            DiscussionsService discussionsService,
            EmailsService emailsService,
            UsersService usersService,
            CategoriesService categoriesService)
        {
            this.parserService = parserService;
            this.discussionsService = discussionsService;
            this.emailsService = emailsService;
            this.usersService = usersService;
            this.categoriesService = categoriesService;
        }

        public override async Task<object> Run(TaskInfo taskInfo)
        {
            if (nextRun.ContainsKey(taskInfo.Alias))
            {
                if (nextRun[taskInfo.Alias] < DateTime.Now)
                    return null;
            }

            nextRun[taskInfo.Alias] = DateTime.Now.AddSeconds(RandomUtil.Int(60, 180));

            var taskParams = JsonConvert.DeserializeObject<TelegramParserTaskParams>(taskInfo.Params);
            var url = taskParams.Url;
            var response = await parserService.GetResponse(url);
            var isOk = response.StatusCode == HttpStatusCode.OK;
            if (!isOk)
            {
                Log.Error($"TelegramParserTask failed. Url {url}. Response {response.StatusCode} {response.Content}");
                return null;
            }

            var html = parserService.GetHtml(response);
            var user = await EnsureUser(html, url);
            if (user == null)
            {
                return null;
            }

            var messages = html.DocumentNode
                .SelectNodes("//div")
                .Elements()
                .Where(e => e.ParentNode.HasClass("tgme_widget_message_wrap"))
                .ToArray();
            var lastMessage = messages.TakeLast(2).FirstOrDefault();
            if(lastMessage == null)
            {
                Log.Error($"TelegramParserTask failed. Url {url}. No last message.");
                return null;
            }

            var postId = lastMessage.GetAttributeValue("data-post", "");
            var memory = taskInfo.Memory != null
                ? JsonConvert.DeserializeObject<TelegramParserTaskMemory>(taskInfo.Memory)
                : new TelegramParserTaskMemory();
            if (memory.LastPostId == postId)
               return null;

            var discussion = await GetDiscussion(lastMessage, postId, user, taskParams);
            if(discussion != null)
            {
                await discussionsService.Create(discussion, null);
                Log.Debug($"TelegramParserTask. Url: {url} parsed: {discussion.Url}");
            }

            return new TelegramParserTaskMemory
            {
                LastPostId = postId
            };
        }

        private async Task<Discussion> GetDiscussion(HtmlNode lastMessage, string postId, User user, TelegramParserTaskParams taskParams)
        {
            var textNode = FindChildByClass(lastMessage, "tgme_widget_message_text");
            var category = await categoriesService.GetByAlias(taskParams.CategoryAlias);

            var youtubeUrl = TryFindYoutubeUrl(textNode);
            if(youtubeUrl != null)
            {
                var result = await parserService.Parse(youtubeUrl, true);
                if (result.HasError)
                    return null;

                return new Discussion
                {
                    Title = result.Title,
                    Description = result.Description,
                    Url = youtubeUrl,
                    ImageUrl = result.ImageUrl,
                    UserId = user.Id,
                    CategoryId = category?.Id ?? ObjectId.Empty
                };
            }


            var imageUrl = FindImageUrl(lastMessage);
            var textNodeChilds = textNode.ChildNodes.ToQueue();
            var firstParagraph = GetNextParagraph(textNodeChilds);
            var seconfParagraph = GetNextParagraph(textNodeChilds);

            return new Discussion
            {
                Title = firstParagraph,
                Description = seconfParagraph,
                Body = GetBody(textNodeChilds),
                Url = $"https://t.me/s/{postId}",
                ImageUrl = imageUrl,
                UserId = user.Id,
                CategoryId = category?.Id ?? ObjectId.Empty
            };
        }

        private DiscussionBodyItem[] GetBody(Queue<HtmlNode> textNodeChilds)
        {
            var result = new List<DiscussionBodyItem>();
            var paragraph = GetNextParagraph(textNodeChilds);
            while (textNodeChilds.Count > 0)
            {
                if(!string.IsNullOrWhiteSpace(paragraph))
                    result.Add(new DiscussionBodyItem { Type = DiscussionBodyItemType.Text, Value = paragraph });
                paragraph = GetNextParagraph(textNodeChilds);
            }
            return result.ToArray();
        }

        private string GetNextParagraph(Queue<HtmlNode> textNodeChilds)
        {
            var text = GetNextParagraphText(textNodeChilds);

            while(skipParagraphs.Contains(text.Trim()))
                text = GetNextParagraphText(textNodeChilds);

            return text;
        }

        private string GetNextParagraphText(Queue<HtmlNode> textNodeChilds)
        {
            var result = "";
            while (textNodeChilds.Count > 0)
            {
                var node = textNodeChilds.Dequeue();
                if (node.Name == "br" && result.Length > 0)
                    return result;

                if (result.Length > 0)
                    result += " ";
                result += Html.RemoveHtmlCodes(node.InnerText);
            }
            return result;
        }

        private string TryFindYoutubeUrl(HtmlNode textNode)
        {
            var urlNode = FindChild(textNode, c => c.Name == "a" && c.GetAttributeValue("href", "").Contains("youtu.be"));
            return urlNode?.GetAttributeValue("href", null);
        }

        private string FindImageUrl(HtmlNode lastMessage)
        {
            var imageNode = FindChildByClass(lastMessage, "tgme_widget_message_photo_wrap");
            if (imageNode == null)
                return null;

            var style = imageNode.GetAttributeValue("style", "");
            if (string.IsNullOrEmpty(style))
                return null;

            var parts = style.Split(";");
            foreach(var part in parts)
            {
                var i = part.IndexOf(":");
                var name = part.Substring(0, i);
                var value = part.Substring(i + 1, part.Length - i - 1);
                if(name == "background-image")
                {
                    return value.Substring(5, value.Length - 7);
                }
            }

            return null;
        }

        private HtmlNode FindChildByClass(HtmlNode node, string className)
        {
            return FindChild(node, n => n.HasClass(className));
        }

        private HtmlNode FindChild(HtmlNode node, Predicate<HtmlNode> predicate)
        {
            foreach(var child in node.ChildNodes)
            {
                if (predicate(child))
                    return child;

                var subchild = FindChild(child, predicate);
                if (subchild != null)
                    return subchild;
            }

            return null;
        }

        private async Task<User> EnsureUser(HtmlDocument html, string url)
        {
            var channelInfo = html.DocumentNode
                .SelectNodes("//div")
                .Elements()
                .FirstOrDefault(e => e.HasClass("tgme_channel_info_header"));
            if (channelInfo == null)
            {
                Log.Error($"TelegramParserTask failed. Url {url}. No channel info.");
                return null;
            }

            var username = FindChildByClass(channelInfo, "tgme_channel_info_header_username").InnerText;
            var name = FindChildByClass(channelInfo, "tgme_channel_info_header_title").InnerText;
            var avatar = FindChildByClass(channelInfo, "tgme_page_photo_image").ChildNodes["img"]?.GetAttributeValue("src", "");
                
            var email = emailsService.CreateFakeEmail(username, "telegram");
            var user = await usersService.FindByEmail(email);
            if (user == null)
            {
                var userCreateResult = await usersService.Create(name, email, "", "");
                user = userCreateResult.User;
            }

            if(!string.IsNullOrEmpty(avatar) && user.AvatarUrl != avatar)
            {
                await usersService.SetAvatar(avatar, user.Id);
            }

            return user;
        }
    }
}
