﻿using System.Threading.Tasks;
using Holywar.Entities.Tasks;

namespace Holywar.Tasker.Tasks
{
    public abstract class AbstractTask
    {
        public abstract Task<object> Run(TaskInfo taskInfo);
    }
}
