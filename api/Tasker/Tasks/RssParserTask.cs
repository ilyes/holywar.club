﻿
using System;
using System.ServiceModel.Syndication;
using System.Threading.Tasks;
using System.Xml;
using Holywar.Common;
using Holywar.Entities.Tasks;

namespace Holywar.Tasker.Tasks
{
    public class RssParserTask : AbstractTask
    {
        public override Task<object> Run(TaskInfo taskInfo)
        {
            try
            {
                using (var reader = XmlReader.Create("https://visualstudiomagazine.com/rss-feeds/news.aspx"))
                {
                    var feed = SyndicationFeed.Load(reader);

                    if (feed != null)
                    {
                        foreach (var element in feed.Items)
                        {
                            Console.WriteLine($"Title: {element.Title.Text}");
                            Console.WriteLine($"Summary: {element.Summary.Text}");
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Log.Error(ex);
            }

            return null;
        }
    }
}
