﻿using Holywar.Entities.Tasks;
using Holywar.Repositories.Internal;

namespace Holywar.Repositories
{
    public class TasksRepository : AbstractRepository<TaskInfo>
    {
        public TasksRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => "tasks";

        public void EnsureTask(TaskInfo task)
        {
            var esists = FindSync(s => s.Alias == task.Alias);
            if (esists != null)
                return;

            AddSync(task);
        }
    }
}
