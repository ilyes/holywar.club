﻿using Holywar.Entities.Categories;
using Holywar.Repositories.Internal;

namespace Holywar.Repositories
{
    public class CategoriesRepository : AbstractRepository<Category>
    {
        public CategoriesRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => "categories";

        public void Seed()
        {
            EnsureCategory("Политика", CategoryAliases.Politics);
            EnsureCategory("Экономика", CategoryAliases.Economics);
            EnsureCategory("Религия", CategoryAliases.Religion);
            EnsureCategory("Спорт", CategoryAliases.Sports);
            EnsureCategory("Происшествия", CategoryAliases.Incidents);
            EnsureCategory("Общество", CategoryAliases.Society);
            EnsureCategory("Культура", CategoryAliases.Culture);
            EnsureCategory("Технологии", CategoryAliases.Technology);
            EnsureCategory("Здоровье", CategoryAliases.Health);
            EnsureCategory("Игры", CategoryAliases.Games);
            EnsureCategory("Кино", CategoryAliases.Movies);
            EnsureCategory("Музыка", CategoryAliases.Music);
            EnsureCategory("Юмор", CategoryAliases.Humor);
            EnsureCategory("Товары", CategoryAliases.Goods);
            EnsureCategory("Услуги", CategoryAliases.Services);
        }

        private void EnsureCategory(string name, string alias)
        {
            var section = FindSync(s => s.Alias == alias);
            if (section != null)
                return;

            AddSync(new Category { Name = name, Alias = alias });
        }
    }
}
