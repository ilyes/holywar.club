﻿using Holywar.Entities.Common;
using Holywar.Repositories.Internal;

namespace Holywar.Repositories
{
    public class EmailsRepository : AbstractRepository<Email>
    {
        public EmailsRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => "emails";
    }
}
