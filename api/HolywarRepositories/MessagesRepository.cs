﻿using Holywar.Entities.Messages;
using Holywar.Repositories.Internal;

namespace Holywar.Repositories
{
    public class MessagesRepository : AbstractRepository<Message>
    {
        public MessagesRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => "messages";
    }
}
