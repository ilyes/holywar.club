﻿using Holywar.Common;
using Holywar.Entities;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace Holywar.Repositories.Internal
{
    public class MongoAdapter
    {
        private MongoClient client;
        private IMongoDatabase database;

        public MongoAdapter()
        {
            client = new MongoClient();
            database = client.GetDatabase("holywar");
        }

        public IMongoCollection<TEntity> GetCollection<TEntity>(string collectionName)
        {
            return database.GetCollection<TEntity>(collectionName);
        }

        public async Task Transaction(Func<Task> func)
        {
            using (var session = await client.StartSessionAsync())
            {
                try
                {
                    session.StartTransaction();

                    await func();

                    await session.CommitTransactionAsync();
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    await session.AbortTransactionAsync();
                    throw;
                }
            }
        }
    }
}
