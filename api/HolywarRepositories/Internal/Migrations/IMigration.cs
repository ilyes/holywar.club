﻿namespace Holywar.Repositories.Internal.Migrations
{
    public interface IMigration
    {
        void Up(MongoAdapter mongo);
        void Down(MongoAdapter mongo);
    }
}
