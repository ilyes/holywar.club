﻿using Holywar.Entities.Users;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Linq;

namespace Holywar.Repositories.Internal.Migrations
{
    public class _20191206_WeekUsersRefactoring : IMigration
    {
        public void Up(MongoAdapter mongo)
        {
            var oldCollection = mongo.GetCollection<WeekUsers>(WeekUsersRepository.COLLECTION_NAME);
            var oldWeeks = oldCollection.FindSync(w => true).ToList();
            oldCollection.Database.DropCollection(WeekUsersRepository.COLLECTION_NAME);

            var newCollection = mongo.GetCollection<WeekUser>(WeekUsersRepository.COLLECTION_NAME);
            foreach (var oldWeek in oldWeeks)
            {
                if(oldWeek.Users == null)
                {
                    continue;
                }

                foreach (var weekUser in oldWeek.Users)
                {
                    weekUser.WeekId = oldWeek.Id;

                    newCollection.InsertOne(weekUser);
                }
            }
        }

        public void Down(MongoAdapter mongo)
        {
        }
    }
}
