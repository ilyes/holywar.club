﻿using Holywar.Entities.Users;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Linq;

namespace Holywar.Repositories.Internal.Migrations
{
    public class _20191109_SeedReferralPayments : IMigration
    {
        public void Up(MongoAdapter mongo)
        {
            var usersCollection = mongo.GetCollection<User>(UsersRepository.COLLECTION_NAME);
            var weekUsersCollection = mongo.GetCollection<WeekUsers>(WeekUsersRepository.COLLECTION_NAME);
            
            var users = usersCollection.Find(u => true).ToList();
            var weeks = weekUsersCollection.Find(w => true).ToList();

            var weekUsersfilter = new FilterDefinitionBuilder<WeekUsers>();
            var usersFilter = new FilterDefinitionBuilder<User>();
            var usersUpdate = new UpdateDefinitionBuilder<User>();

            foreach (var week in weeks)
            {
                foreach(var weekUser in week.Users)
                {
                    if (weekUser.Paid < 20)
                        continue;

                    var user = users.FirstOrDefault(r => r.Id == weekUser.UserId);
                    if (user == null || user.ReferralId == ObjectId.Empty)
                        continue;

                    var referralWeekUser = week.Users.FirstOrDefault(r => r.UserId == user.ReferralId);
                    if (referralWeekUser.ReferralPayments > 0)
                        continue;

                    referralWeekUser.ReferralPayments = (int)Math.Floor(weekUser.Paid / 20f);
                    var referralUser = users.FirstOrDefault(r => r.Id == referralWeekUser.UserId);
                    referralUser.Balance += referralWeekUser.ReferralPayments;
                    referralUser.ReferralPayments += referralWeekUser.ReferralPayments;
                    var f = usersFilter.Eq(u => u.Id, referralUser.Id);
                    var upRef = usersUpdate.Set(u => u.ReferralPayments, referralUser.ReferralPayments);
                    var upBal = usersUpdate.Set(u => u.Balance, referralUser.Balance);
                    usersCollection.UpdateOne(f, upRef);
                    usersCollection.UpdateOne(f, upBal);
                }

                weekUsersCollection.ReplaceOne(weekUsersfilter.Eq(w => w.Id, week.Id), week);
            }
        }

        public void Down(MongoAdapter mongo)
        {
        }
    }
}
