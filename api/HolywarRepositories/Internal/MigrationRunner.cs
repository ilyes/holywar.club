﻿using Holywar.Repositories.Internal.Migrations;
using MongoDB.Bson;
using MongoDB.Driver;
using System;

namespace Holywar.Repositories.Internal
{
    public class MigrationRunner
    {
        private MongoAdapter mongo;

        public MigrationRunner(MongoAdapter mongo)
        {
            this.mongo = mongo;
        }

        public void Run()
        {
            RunMigration<_20191109_SeedReferralPayments>(mongo);
            RunMigration<_20191206_WeekUsersRefactoring>(mongo);
        }

        private void RunMigration<T>(MongoAdapter mongo)
            where T : IMigration, new()
        {
            var collection = mongo.GetCollection<MigrationInfo>("__migrations");
            var migrationName = typeof(T).Name;
            var migrationInfo = collection.Find(m => m.Name == migrationName).FirstOrDefault();
            if (migrationInfo != null)
                return;

            var migration = new T();
            migration.Up(mongo);

            collection.InsertOne(new MigrationInfo
            {
                Name = migrationName,
                RunDate = DateTime.UtcNow
            });
        }

        class MigrationInfo
        {
            public ObjectId Id { get; set; }

            public string Name { get; set; }

            public DateTime RunDate { get; set; }
        }
    }
}
