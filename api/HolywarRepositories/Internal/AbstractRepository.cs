﻿using Holywar.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Holywar.Entities.Abstract;

namespace Holywar.Repositories.Internal
{
    public abstract class AbstractRepository<TEntity>
        where TEntity : AbstractEntity
    {
        protected IMongoCollection<TEntity> collection;
        protected FilterDefinitionBuilder<TEntity> filter;
        protected MongoAdapter mongo;

        public readonly UpdateDefinitionBuilder<TEntity> Update;
        public readonly SortDefinitionBuilder<TEntity> Sort;

        protected AbstractRepository(MongoAdapter mongo)
        {
            this.mongo = mongo;
            collection = mongo.GetCollection<TEntity>(CollectionName);
            filter = new FilterDefinitionBuilder<TEntity>();
            Update = new UpdateDefinitionBuilder<TEntity>();
            Sort = new SortDefinitionBuilder<TEntity>();
        }

        protected abstract string CollectionName { get; }

        public virtual async Task<TEntity> Add(TEntity entity)
        {
            await collection.InsertOneAsync(entity);
            return entity;
        }

        public virtual TEntity AddSync(TEntity entity)
        {
            collection.InsertOne(entity);
            return entity;
        }

        public virtual async Task<IEnumerable<TEntity>> Add(TEntity[] entities)
        {
            await collection.InsertManyAsync(entities);
            return entities;
        }

        public virtual void ReplaceSync(TEntity entity)
        {
            collection.ReplaceOne(filter.Eq(a => a.Id, entity.Id), entity);
        }

        public virtual async Task UpdateAsync(ObjectId entityId, UpdateDefinition<TEntity> upd)
        {
            await collection.UpdateOneAsync(a => a.Id == entityId, upd);
        }

        public virtual async Task UpdatesAsync(TEntity entity, params UpdateDefinition<TEntity>[] upds)
        {
            await collection.UpdateOneAsync(a => a.Id == entity.Id, Update.Combine(upds));
        }

        public virtual async Task UpdateAsync<TField>(TEntity entity, Expression<Func<TEntity, TField>> expression)
        {
            await UpdateAsync(entity, expression, expression.Compile()(entity));
        }

        public virtual async Task UpdatesAsync<TField>(TEntity entity, params Expression<Func<TEntity, TField>>[] expressions)
        {
            await collection.UpdateOneAsync(a => a.Id == entity.Id, Update.Combine(expressions.Select(e => Update.Set(e, e.Compile()(entity)))));
        }

        public virtual async Task UpdateAsync<TField>(TEntity entity, Expression<Func<TEntity, TField>> expression, TField value)
        {
            await collection.UpdateOneAsync(a => a.Id == entity.Id, Update.Set(expression, value));
        }

        public virtual async Task UpdateAsync<TField>(ObjectId entityId, Expression<Func<TEntity, TField>> expression, TField value)
        {
            await collection.UpdateOneAsync(a => a.Id == entityId, Update.Set(expression, value));
        }

        public virtual void UpdateSync(ObjectId entityId, UpdateDefinition<TEntity> upd)
        {
            collection.UpdateOne(a => a.Id == entityId, upd);
        }

        public virtual void UpdateSync<TField>(TEntity entity, params Expression<Func<TEntity, TField>>[] expressions)
        {
            collection.UpdateOne(a => a.Id == entity.Id, Update.Combine(expressions.Select(e => Update.Set(e, e.Compile()(entity)))));
        }

        public virtual void UpdateSync<TField>(ObjectId entityId, Expression<Func<TEntity, TField>> expression, TField value)
        {
            collection.UpdateOne(a => a.Id == entityId, Update.Set(expression, value));
        }

        public virtual void UpdateMany(FilterDefinition<TEntity> f, UpdateDefinition<TEntity> upd)
        {
            collection.UpdateMany(f, upd);
        }

        public virtual async Task<bool> Delete(string id)
        {
            var objectId = id.ToObjectId();
            if (objectId == ObjectId.Empty)
                return false;
            return await Delete(objectId);
        }

        public virtual async Task<bool> Delete(ObjectId id)
        {
            await collection.DeleteOneAsync(filter.Eq(a => a.Id, id));
            return true;
        }

        public virtual async Task<bool> Delete(Expression<Func<TEntity, bool>> expression)
        {
            await collection.DeleteOneAsync(expression);
            return true;
        }

        public virtual void DeleteSync(ObjectId id)
        {
            collection.DeleteOne(filter.Eq(a => a.Id, id));
        }

        public virtual async Task<bool> Delete(IEnumerable<ObjectId> ids)
        {
            await collection.DeleteManyAsync(filter.In(a => a.Id, ids));
            return true;
        }

        public async Task SetDeleted(string entityId)
        {
            var entity = await FindById(entityId.ToObjectId());
            if (entity == null)
                return;

            await UpdateAsync(entity.Id, d => d.IsDeleted, true);
        }

        public virtual async Task<TEntity> FindById(string id)
        {
            var objectId = id.ToObjectId();
            if (objectId == ObjectId.Empty)
                return null;
            return await FindById(objectId);
        }

        public virtual async Task<TEntity> FindById(ObjectId id)
        {
            return await Find(e => e.Id == id && !e.IsDeleted);
        }

        public virtual async Task<IEnumerable<TEntity>> FindById(IEnumerable<ObjectId> ids, bool keepOrder = false)
        {
            var f = filter.Where(a => ids.Contains(a.Id) && !a.IsDeleted);
            var c = await collection.FindAsync(f);
            var result = await c.ToListAsync();
            if (keepOrder)
            {
                result = ids
                    .Select(id => result.FirstOrDefault(r => r.Id == id))
                    .Where(r => r != null)
                    .ToList();
            }
            return result;
        }

        public virtual async Task<TEntity> Find(
            Expression<Func<TEntity, bool>> expression,
            Expression<Func<TEntity, object>> sort = null,
            SortDirection sortDirection = SortDirection.Ascending
        ) {
            var s = sort != null  
                ? sortDirection == SortDirection.Ascending ? Sort.Ascending(sort) : Sort.Descending(sort)
                : null;
            var o = new FindOptions<TEntity, TEntity>
            {
                Sort = s
            };
            var cursor = await collection.FindAsync(expression, o);
            return await cursor.FirstOrDefaultAsync();
        }

        public virtual TEntity FindSync(Expression<Func<TEntity, bool>> expression)
        {
            var cursor = collection.Find(expression);
            return cursor.FirstOrDefault();
        }

        public virtual async Task<IEnumerable<TEntity>> Where(Expression<Func<TEntity, bool>> expression, int limit = int.MaxValue, SortDefinition<TEntity> sort = null)
        {
            var f = filter.Where(expression);
            return await Where(f, limit, sort);
        }

        public virtual IEnumerable<TEntity> WhereSync(Expression<Func<TEntity, bool>> expression)
        {
            var f = filter.Where(expression);
            var c = collection.FindSync(f);
            return c.ToList();
        }

        public virtual async Task<IEnumerable<TEntity>> Where(FilterDefinition<TEntity> filter, int limit = int.MaxValue, SortDefinition<TEntity> sort = null)
        {
            var o = new FindOptions<TEntity, TEntity>
            {
                Limit = limit,
                Sort = sort
            };
            var c = await collection.FindAsync(filter, o);
            return await c.ToListAsync();
        }

        public virtual async Task<bool> Exists(string id)
        {
            var objectId = id.ToObjectId();
            if (objectId == ObjectId.Empty)
                return false;
            return await Exists(objectId);
        }

        public virtual async Task<bool> Exists(ObjectId id)
        {
            var f = filter.Eq(a => a.Id, id);
            return (await collection.FindAsync(f)).FirstOrDefault() != null;
        }

        public virtual async Task<long> Count()
        {
            return await collection.CountDocumentsAsync(filter.Empty);
        }

        public virtual async Task<long> Count(Expression<Func<TEntity, bool>> expression)
        {
            var f = filter.Where(expression);
            return await collection.CountDocumentsAsync(f);
        }

        public virtual long CountSync(Expression<Func<TEntity, bool>> expression)
        {
            var f = filter.Where(expression);
            return collection.CountDocuments(f);
        }

        public virtual async Task<IEnumerable<TEntity>> All()
        {
            return (await collection.FindAsync(filter.Empty)).ToList();
        }

        public virtual IEnumerable<TEntity> AllSync()
        {
            return collection.Find(filter.Empty).ToList();
        }

        public virtual TEntity First()
        {
            return collection.Find(filter.Empty).First();
        }

        public void EnsureIndex(Expression<Func<TEntity, object>> field)
        {
            var indexKeys = Builders<TEntity>.IndexKeys.Ascending(field);
            var indexModel = new CreateIndexModel<TEntity>(indexKeys, new CreateIndexOptions());
            collection.Indexes.CreateOne(indexModel);
        }

        public void EnsureIndex(Expression<Func<TEntity, object>> field1, Expression<Func<TEntity, object>> field2)
        {
            var indexKeys = Builders<TEntity>.IndexKeys
                .Ascending(field1)
                .Ascending(field2);
            var indexModel = new CreateIndexModel<TEntity>(indexKeys, new CreateIndexOptions());
            collection.Indexes.CreateOne(indexModel);
        }

        public Task Transaction(Func<Task> func) => mongo.Transaction(func);
    }
}
