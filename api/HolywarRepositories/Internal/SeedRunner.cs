﻿namespace Holywar.Repositories.Internal
{
    public class SeedRunner
    {
        ConfigRepository configRepository;
        UsersRepository usersRepository;
        AuthRepository authRepository;
        DiscussionsRepository discussionsRepository;
        LikesRepository likesRepository;
        CommentsRepository commentsRepository;
        EmailsRepository emailsRepository;
        MessagesRepository messagesRepository;
        CategoriesRepository categoriesRepository;
        WeekUsersRepository weekUsersRepository;
        SurveysRepository surveysRepository;
        SurveyVotesRepository surveyVotesRepository;
        SurveyUserVotesRepository surveyUserVotesRepository;
        TasksRepository tasksRepository;

        public SeedRunner(
            ConfigRepository configRepository, 
            UsersRepository usersRepository,
            DiscussionsRepository discussionsRepository,
            LikesRepository likesRepository,
            CommentsRepository commentsRepository,
            EmailsRepository emailsRepository,
            MessagesRepository messagesRepository,
            AuthRepository authRepository,
            CategoriesRepository categoriesRepository,
            SurveysRepository surveysRepository,
            SurveyVotesRepository surveyVotesRepository,
            SurveyUserVotesRepository surveyUserVotesRepository,
            WeekUsersRepository weekUsersRepository,
            TasksRepository tasksRepository)
        {
            this.configRepository = configRepository;
            this.usersRepository = usersRepository;
            this.authRepository = authRepository;
            this.likesRepository = likesRepository;
            this.discussionsRepository = discussionsRepository;
            this.commentsRepository = commentsRepository;
            this.emailsRepository = emailsRepository;
            this.messagesRepository = messagesRepository;
            this.categoriesRepository = categoriesRepository;
            this.surveysRepository = surveysRepository;
            this.surveyVotesRepository = surveyVotesRepository;
            this.surveyUserVotesRepository = surveyUserVotesRepository;
            this.weekUsersRepository = weekUsersRepository;
            this.tasksRepository = tasksRepository;
        }

        public void Run()
        {
            usersRepository.EnsureIndex(u => u.Email);
            usersRepository.EnsureIndex(u => u.ReferralId);
            authRepository.EnsureIndex(a => a.UserId);
            discussionsRepository.EnsureIndex(d => d.Url);
            discussionsRepository.EnsureIndex(d => d.UserId);
            discussionsRepository.EnsureIndex(d => d.CategoryId);
            discussionsRepository.EnsureIndex(d => d.Alias);
            discussionsRepository.EnsureIndex(d => d.HolywarState);
            likesRepository.EnsureIndex(l => l.CommentId);
            likesRepository.EnsureIndex(l => l.DiscussionId);
            commentsRepository.EnsureIndex(c => c.CreationDate);
            commentsRepository.EnsureIndex(c => c.DiscussionId);
            commentsRepository.EnsureIndex(c => c.UserId, c => c.DiscussionId);
            emailsRepository.EnsureIndex(c => c.IsSent);
            messagesRepository.EnsureIndex(d => d.ToUserId);
            weekUsersRepository.EnsureIndex(d => d.WeekId);
            weekUsersRepository.EnsureIndex(d => d.UserId);
            surveysRepository.EnsureIndex(d => d.DiscusionId);
            surveyVotesRepository.EnsureIndex(d => d.SurveyId);
            surveyUserVotesRepository.EnsureIndex(d => d.UserId, d => d.SurveyId);

            configRepository.Seed();
            categoriesRepository.Seed();
        }
    }
}
