﻿using Holywar.Entities.Surveys;
using Holywar.Repositories.Internal;

namespace Holywar.Repositories
{
    public class SurveyUserVotesRepository : AbstractRepository<SurveyUserVote>
    {
        public const string COLLECTION_NAME = "survey_user_votes";

        public SurveyUserVotesRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => COLLECTION_NAME;
    }
}
