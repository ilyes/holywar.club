﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Holywar.Repositories.Internal;
using MongoDB.Bson;
using MongoDB.Driver;
using Holywar.Entities.Comments;
using Holywar.Entities.Users;

namespace Holywar.Repositories
{
    public class CommentWithUser
    {
        public Comment Comment;
        public User User;
        public string DiscussionAlias;
    }

    public class CommentsRepository : AbstractRepository<Comment>
    {
        public const string COLLECION_NAME = "comments";

        private UsersRepository userRepository;

        protected override string CollectionName => COLLECION_NAME;

        public CommentsRepository(UsersRepository userRepository, MongoAdapter mongo)
            : base(mongo)
        {
            this.userRepository = userRepository;
        }

        public async Task<IEnumerable<CommentWithUser>> GetByDiscussionId(ObjectId discussionId)
        {
            var comments = await Where(c => c.DiscussionId == discussionId && !c.IsDeleted);
            var usersId = comments.Select(c => c.UserId).Distinct();
            var users = await userRepository.FindById(usersId);
            return comments.Select(c => new CommentWithUser
            {
                Comment = c,
                User = users.FirstOrDefault(u => u.Id == c.UserId)
            });
        }
    }
}