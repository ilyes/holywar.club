﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Holywar.Common;
using Holywar.Entities.Comments;
using Holywar.Repositories.Internal;

namespace Holywar.Repositories
{
    public class WeekCommentsRepository : AbstractRepository<WeekComment>
    {
        public const string COLLECTION_NAME = "week_comments";

        protected override string CollectionName => COLLECTION_NAME;

        public WeekCommentsRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        public async Task ChangeLikes(ObjectId commentId, int addLikes, int addDislikes)
        {
            var comment = await FindOrCreate(commentId);

            comment.LikesCount += addLikes;
            comment.DisikesCount += addDislikes;
            comment.Rating = CalculateRating(comment);

            await UpdatesAsync(comment,
                u => u.LikesCount,
                u => u.DisikesCount,
                u => u.Rating);
        }

        public async Task IncrementReplys(ObjectId commentId)
        {
            var comment = await FindOrCreate(commentId);

            comment.ReplysCount++;
            comment.Rating = CalculateRating(comment);

            await UpdatesAsync(comment,
                u => u.ReplysCount,
                u => u.Rating);
        }

        public async Task IncrementViews(ObjectId commentId)
        {
            var comment = await FindOrCreate(commentId);

            comment.ViewsCount++;
            comment.Rating = CalculateRating(comment);

            await UpdatesAsync(comment,
                u => u.ViewsCount,
                u => u.Rating);
        }

        public async Task<IEnumerable<WeekComment>> FindWeekComments(int weekId, int limit)
        {
            var comments = await Where(u => u.WeekId == weekId, limit: limit);
            return comments;
        }

        private int CalculateRating(WeekComment comment)
        {
            return comment.LikesCount
                + comment.DisikesCount
                + comment.ReplysCount * 10
                + comment.ViewsCount / 10;
        }

        private async Task<WeekComment> FindOrCreate(ObjectId commentId)
        {
            var weekId = DateUtil.GetCurrentWeekId();
            var comment = await Find(u => u.CommentId == commentId && u.WeekId == weekId);
            if (comment == null)
            {
                comment = new WeekComment
                {
                    CreationDate = DateTime.UtcNow,
                    CommentId = commentId,
                    WeekId = weekId
                };
                await Add(comment);
            }

            return comment;
        }
    }
}
