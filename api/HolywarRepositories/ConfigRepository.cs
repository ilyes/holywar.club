﻿using System.Threading.Tasks;
using Holywar.Entities.Common;
using Holywar.Repositories.Internal;
using MongoDB.Driver;

namespace Holywar.Repositories
{
    public class ConfigRepository
    {
        protected IMongoCollection<Config> collection;
        protected FilterDefinitionBuilder<Config> filter;

        public ConfigRepository(MongoAdapter mongo)
        {
            collection = mongo.GetCollection<Config>(CollectionName);
            filter = new FilterDefinitionBuilder<Config>();
        }

        protected string CollectionName => "config";

        public async Task<Config> GetConfig()
        {
            var cursor = await collection.FindAsync(filter.Empty);
            return await cursor.FirstOrDefaultAsync();
        }

        public Config GetConfigSync()
        {
            var cursor = collection.Find(filter.Empty);
            return cursor.FirstOrDefault();
        }

        public void Seed()
        {
            var config = GetConfigSync();
            var defaultConfig = CreateDefaultConfig();
            if (config == null)
            {
                collection.InsertOne(defaultConfig);
            }
            else
            {
                if (string.IsNullOrEmpty(config.SetPasswordPathTemplate))
                    config.SetPasswordPathTemplate = defaultConfig.SetPasswordPathTemplate;
                if (string.IsNullOrEmpty(config.DiscussionPathTemplate))
                    config.DiscussionPathTemplate = defaultConfig.DiscussionPathTemplate;
                if (string.IsNullOrEmpty(config.CommentReplyPathTemplate))
                    config.CommentReplyPathTemplate = defaultConfig.CommentReplyPathTemplate;
                if (string.IsNullOrEmpty(config.ProfileUrl))
                    config.ProfileUrl = defaultConfig.ProfileUrl;
                if (string.IsNullOrEmpty(config.RatingsUrl))
                    config.RatingsUrl = defaultConfig.RatingsUrl;
                if (config.HolywarCandidatesStep == 0)
                    config.HolywarCandidatesStep = defaultConfig.HolywarCandidatesStep;
                if (config.MinPaymentLimit == 0)
                    config.MinPaymentLimit = defaultConfig.MinPaymentLimit;
                if (config.StartPaymentLimit == 0)
                    config.StartPaymentLimit = defaultConfig.StartPaymentLimit;
                if (config.LevelLimitReduceBy == 0)
                    config.LevelLimitReduceBy = defaultConfig.LevelLimitReduceBy;
                if (config.LevelSteps == null)
                    config.LevelSteps = defaultConfig.LevelSteps;
                if (config.AdProvider == null)
                    config.AdProvider = defaultConfig.AdProvider;
                if (config.WeekRating == 0)
                    config.WeekRating = defaultConfig.WeekRating;
                if (config.WeekJackpot == 0)
                    config.WeekJackpot = defaultConfig.WeekJackpot;
                collection.ReplaceOne(e => true, config);
            }
        }

        public async Task SaveConfig(Config config)
        {
            await collection.ReplaceOneAsync(e => true, config);
        }

        public void SaveConfigSync(Config config)
        {
            collection.ReplaceOne(e => true, config);
        }

        public Config CreateDefaultConfig()
        {
            return new Config
            {
                FrontendUrl = "http://localhost:3000/",
                EmailConfirmPathTemplate = "https://localhost:6001/email/{0}",
                EmailConfirmedPathTemplate = "auth/signup/confirmed?token={0}",
                DiscussionPathTemplate = "discussion/{0}",
                ProfileUrl = "profile",
                RatingsUrl = "ratings/by-balance-added",
                CommentReplyPathTemplate = "#comment={0}",
                AvatarsFolder = @"avatars/",
                StaticUrl = "https://localhost:6001/static/",
                StaticPath = "static",
                SetPasswordPathTemplate = "auth/set-password?token={0}",
                HolywarCandidatesStep = 3,
                MinPaymentLimit = 100,
                StartPaymentLimit = 500,
                LevelLimitReduceBy = 30,
                LevelSteps = new int[] { 0, 500, 1000, 2000, 4000, 10000, 20000, 40000, 100000, 200000 },
                AdProvider = AdProviders.Google,
                WeekRating = 100,
                WeekJackpot = 1000
            };
        }
    }
}
