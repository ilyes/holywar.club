﻿using Holywar.Entities.Auths;
using Holywar.Repositories.Internal;

namespace Holywar.Repositories
{
    public class TokensRepository : AbstractRepository<Token>
    {
        public TokensRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => "tokens";
    }
}
