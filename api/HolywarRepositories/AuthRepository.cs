﻿using Holywar.Entities.Auths;
using Holywar.Repositories.Internal;

namespace Holywar.Repositories
{
    public class AuthRepository : AbstractRepository<Auth>
    {
        public AuthRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => "auth";
    }
}
