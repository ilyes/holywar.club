﻿using System;
using System.Threading.Tasks;
using Holywar.Entities.Surveys;
using Holywar.Repositories.Internal;
using MongoDB.Bson;

namespace Holywar.Repositories
{
    public class SurveysRepository : AbstractRepository<Survey>
    {
        public const string COLLECTION_NAME = "surveys";

        public SurveysRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => COLLECTION_NAME;

        public async Task<Survey> GetByDiscussionId(ObjectId discusionId)
        {
            return await Find(s => s.DiscusionId == discusionId);
        }

        public async Task DeleteByDiscussionId(ObjectId discusionId)
        {
            await Delete(s => s.DiscusionId == discusionId);
        }
    }
}
