﻿using Holywar.Repositories.Internal;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Holywar.Entities.Users;
using Holywar.Common;

namespace Holywar.Repositories
{
    public class WeekUsersRepository : AbstractRepository<WeekUser>
    {
        public const string COLLECTION_NAME = "week_users";

        protected override string CollectionName => COLLECTION_NAME;

        public WeekUsersRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        public async Task ChangeLikes(ObjectId userId, int addLikes, int addDislikes)
        {
            var user = await FindOrCreate(userId);

            user.LikesCount += addLikes;
            user.DisikesCount += addDislikes;
            user.Rating = CalculateRating(user);

            await UpdatesAsync(user,
                u => u.LikesCount,
                u => u.DisikesCount, u => u.Rating);
        }

        public async Task IncrementComments(ObjectId userId)
        {
            var user = await FindOrCreate(userId);

            user.CommentsCount++;
            user.Rating = CalculateRating(user);

            await UpdatesAsync(user, u => u.CommentsCount, u => u.Rating);
        }

        public async Task IncrementDiscussions(ObjectId userId)
        {
            var user = await FindOrCreate(userId);

            user.DiscussionsCount++;
            user.Rating = CalculateRating(user);

            await UpdatesAsync(user, u => u.DiscussionsCount, u => u.Rating);
        }

        public async Task IncrementDiscussionViews(ObjectId userId)
        {
            var user = await FindOrCreate(userId);

            user.DiscussionViewsCount++;
            user.Rating = CalculateRating(user);

            await UpdatesAsync(user, u => u.DiscussionViewsCount, u => u.Rating);
        }

        public async Task IncrementDiscussionExternalViews(ObjectId userId)
        {
            var user = await FindOrCreate(userId);

            user.DiscussionExternalViewsCount++;
            user.Rating = CalculateRating(user);

            await UpdatesAsync(user, u => u.DiscussionExternalViewsCount, u => u.Rating);
        }

        public async Task IncrementReferralViews(ObjectId userId)
        {
            var user = await FindOrCreate(userId);

            user.ReferralViewsCount++;
            user.Rating = CalculateRating(user);

            await UpdatesAsync(user, u => u.ReferralViewsCount, u => u.Rating);
        }

        public async Task IncrementUniqueCommentators(ObjectId userId)
        {
            var user = await FindOrCreate(userId);

            user.UniqueCommentatorsCount++;
            user.Rating = CalculateRating(user);

            await UpdatesAsync(user, u => u.UniqueCommentatorsCount, u => u.Rating);
        }

        public async Task IncrementUniqueReplies(ObjectId userId)
        {
            var user = await FindOrCreate(userId);

            user.UniqueRepliesCount++;
            user.Rating = CalculateRating(user);

            await UpdatesAsync(user, u => u.UniqueRepliesCount, u => u.Rating);
        }

        public async Task IncrementHolywarsStarted(ObjectId userId)
        {
            var user = await FindOrCreate(userId);

            user.HolywarsStarted++;
            user.Rating = CalculateRating(user);

            await UpdatesAsync(user, u => u.HolywarsStarted, u => u.Rating);
        }

        public async Task IncrementHolywarsParticipant(ObjectId userId, int commentsCount)
        {
            var user = await FindOrCreate(userId);

            user.HolywarsParticipant += commentsCount;
            user.Rating = CalculateRating(user);

            await UpdatesAsync(user, u => u.HolywarsParticipant, u => u.Rating);
        }

        public async Task IncrementReferralsCount(ObjectId userId)
        {
            var user = await FindOrCreate(userId);

            user.ReferralsCount++;
            user.Rating = CalculateRating(user);

            await UpdatesAsync(user, u => u.ReferralsCount, u => u.Rating);
        }

        public async Task<IEnumerable<WeekUser>> GetLastActiveWeek()
        {
            var weekId = DateUtil.GetCurrentWeekId();
            return await FindWeekUsers(weekId);
        }

        public async Task<IEnumerable<WeekUser>> GetPrevActiveWeek()
        {
            var weekId = DateUtil.GetCurrentWeekId() - 1;
            return await FindWeekUsers(weekId);
        }

        public async Task<IEnumerable<WeekUser>> FindWeekUsers(int weekId)
        {
            var user = await Find(u => u.WeekId <= weekId, sort: u => u.WeekId, sortDirection: SortDirection.Descending);
            var users = await Where(u => u.WeekId == user.WeekId);
            return users;
        }

        private int CalculateRating(WeekUser user)
        {
            return user.ReferralsCount
                + user.HolywarsStarted
                + user.HolywarsParticipant / 2
                + user.UniqueCommentatorsCount / 2
                + user.UniqueRepliesCount / 2
                + user.DiscussionExternalViewsCount / 5
                + user.ReferralViewsCount / 10
                + user.LikesCount / 10
                + user.DisikesCount / 10
                + user.DiscussionsCount / 20
                + user.DiscussionViewsCount / 50;
        }

        private async Task<WeekUser> FindOrCreate(ObjectId userId)
        {
            var weekId = DateUtil.GetCurrentWeekId();
            var user = await Find(u => u.UserId == userId && u.WeekId == weekId);
            if (user == null)
            {
                user = new WeekUser
                {
                    CreationDate = DateTime.UtcNow,
                    UserId = userId,
                    WeekId = weekId
                };
                await Add(user);
            }

            return user;
        }
    }
}
