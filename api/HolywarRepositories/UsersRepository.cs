﻿using Holywar.Entities.Users;
using Holywar.Repositories.Internal;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Holywar.Repositories
{
    public class UsersRepository : AbstractRepository<User>
    {
        public const string COLLECTION_NAME = "users";

        public UsersRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => COLLECTION_NAME;

        public async Task<User> FindByEmail(string email)
        {
            return await Find(a => a.Email == email);
        }

        internal void SeedLimits()
        {
            var users = AllSync();
            var first = users.First();
            if (first.PaymentLimit > 0)
                return;

            foreach (var user in users)
            {
                user.PaymentLimit = 500;
                UpdateSync(user, u => u.PaymentLimit);
            }
        }
    }
}
