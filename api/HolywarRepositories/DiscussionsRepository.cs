﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using Holywar.Entities.Discussions;
using Holywar.Repositories.Internal;
using Holywar.Common;
using System.Linq;
using System;

namespace Holywar.Repositories
{
    public class DiscussionsRepository : AbstractRepository<Discussion>
    {
        public DiscussionsRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => "discussions";

        public async Task<IEnumerable<Discussion>> GetList(DiscussionListFilter filter)
        {
            var s = Sort.Descending(e => e.Id);
            var limit = filter.Limit > 0 && filter.Limit <= 100 ? filter.Limit : 10;
            var f = base.filter.Empty;
            if (!string.IsNullOrEmpty(filter.AfterDiscussionId))
            {
                var afterId = filter.AfterDiscussionId.ToObjectId();
                if(afterId != ObjectId.Empty)
                    f = f & base.filter.Lt(d => d.Id, afterId);
            }
            if (!string.IsNullOrEmpty(filter.UserId))
            {
                var userId = filter.UserId.ToObjectId();
                if (userId != ObjectId.Empty)
                    f = f & base.filter.Eq(d => d.UserId, userId);
            }
            if (filter.CategoryIds != null && filter.CategoryIds.Length > 0)
            {
                var categoryIds = filter.CategoryIds.Select(i => i.ToObjectId()).ToArray();
                f = f & base.filter.Where(d => categoryIds.Contains(d.CategoryId));
            }
            f = f & base.filter.Where(d => !d.IsDeleted);
            return await Where(f, limit, s);
        }

        internal void SeedAliases()
        {
            var discussions = AllSync();
            foreach(var discussion in discussions)
            {
                discussion.Alias = $"{Translit.Make(discussion.Title, 150)}_{ObjectId.GenerateNewId()}";
                UpdateSync(discussion, d => d.Alias);
            }
        }
    }
}
