﻿using Holywar.Entities.Payments;
using Holywar.Repositories.Internal;
using MongoDB.Bson;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Holywar.Repositories
{
    public class PaymentsRepository : AbstractRepository<Payment>
    {
        public PaymentsRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => "payments";

        public async Task<IEnumerable<Payment>> GetAll(ObjectId afterId, int limit)
        {
            var s = Sort.Descending(e => e.Id);
            var f = filter.Empty;
            if (afterId != ObjectId.Empty)
            {
                f = f & filter.Lt(d => d.Id, afterId);
            }
            return await Where(f, limit, s);
        }
    }
}
