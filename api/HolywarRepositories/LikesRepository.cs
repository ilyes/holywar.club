﻿using Holywar.Entities.Likes;
using Holywar.Repositories.Internal;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Holywar.Repositories
{
    public class LikesRepository : AbstractRepository<Like>
    {
        public LikesRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => "likes";

        public async Task<IEnumerable<Like>> FindByCommentId(ObjectId commentId)
        {
            var f = filter.Eq(a => a.CommentId, commentId);
            var likes = await collection.FindAsync<Like>(f);
            return await likes.ToListAsync();
        }
    }
}
