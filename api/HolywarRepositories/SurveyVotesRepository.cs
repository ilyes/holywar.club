﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Holywar.Entities.Surveys;
using Holywar.Repositories.Internal;
using MongoDB.Bson;

namespace Holywar.Repositories
{
    public class SurveyVotesRepository : AbstractRepository<SurveyVotes>
    {
        public const string COLLECTION_NAME = "survey_votes";

        public SurveyVotesRepository(MongoAdapter mongo)
            : base(mongo)
        {
        }

        protected override string CollectionName => COLLECTION_NAME;

        public async Task<IEnumerable<SurveyVotes>> GetBySurveyId(ObjectId surveyId)
        {
            return await Where(s => s.SurveyId == surveyId);
        }
    }
}
